//
//  SharedEnums.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 29/01/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation


 //   ["shore","RIB","outrigger","longtail","speedboat","day boat","liveaboard"]

enum AccessBy : Int16 {
    case unknown = 0
    case shore = 1
    case rib = 2
    case outrigger = 4
    case speedboat = 8
    case dayboat = 16
    case liveaboard = 32
}
enum GPSFix : Int {
    case isUnknown = 0
    case isAboveSite = 1
    case isShoreEntryPoint = 2
    case isNearbyMooring = 4
    case isNowhereNear = 8
    case isPinDrop = 16
    case isList = 32
    
    func gpsFixString()-> String?{
        switch self {
        case .isShoreEntryPoint : return "at shore entry point"
        case .isAboveSite : return "above or moored to site"
        case .isNearbyMooring: return "at nearby mooring"
        default : return nil
        }
    }
}


//Value to store in Diver to allow sorting when displayed
enum BuddyStatus: Int {
    case notBuddy = 1
    case defaultBuddy = 2
    case regular = 4
    case oneOff = 8
}

enum BuddyStatusName: String {
    case notBuddy = "not a buddy"
    case defaultBuddy = "default buddy"
    case regular = " regular buddy"
    case oneOff =  "one-off / irregular buddy"
}

enum ProStatus : Int {
    case notPro = 1
    case guide = 2
    case instructor = 4
//    case assistantInstructor = 8
//    case instructorTrainer = 16
}
enum ProStatusName : String {
    case notPro = "not a pro"
    case guide = "dive guide"
    case instructor = "instructor"
//    case assistantInstructor = "assistant instructor"
//    case instructorTrainer = "instructor trainer"
}
enum ClientStatus : Int {
    case notClient = 1
    case student = 2
    case guidedDiver = 4
}
enum ClientStatusName : String {
    case notClient = "not a client"
    case student = "student"
    case guidedDiver = "guided diver"
}
// Value to decide whether or not to add a buddy to a new logEntry

enum BuddyAutofill: Int {
    case dont
    case useDefault
    case useDefaultOrLast
    case last
}

enum OtherAutofill: Int {
    case dont = 1
    case weights = 2
    case exposureSuit = 4
    case tank = 8
    case divePro = 16
    case diveOp = 32
    case trip = 64
    case boat = 128
}
//MARK:- Environment Values
enum Salinity : Int{
    case unknown = 0
    case salt = 1
    case fresh = 2
    case brackish = 4
    case layers = 8
    case highSalinity = 16
}

enum SalinityIdentitifer : String {
    case unknown
    case salt
    case fresh
    case brackish
    case layers = "salt/fresh layered"
    case highSalinity = "high salinity"
}
enum EnvironmentTypeValue : Int32 {
    case unknown = 0
    case bay = 1
    case channel = 2
    case lagoon = 4
    case cenote = 8
    case lake = 16
    case quarry = 32
    case reservoir = 64
    case river = 128
    case estuary = 256
    case mangrove = 512
    case fjord = 1024
    case seamount = 2048
    case offshoreReef = 4096
    case atoll = 8192
    case fringingReef = 16384
    case barrierReef = 32768
    case inlandTypes = 148 /* cenote, lake,quarry,reservoir and river*/
    case coastalTypes = 18183 /* fringing reef, bay,channel,lagoon,estuary,mangrove, fjord*/
    case offshoreTypes = 47104 /*  Seamount, offshore reef, barrier reef atoll */
}
enum WhereSiteLocatedType: Int16{
    case unknown = 0
    case inland = 1
    case coastal = 2
    case island = 4
    case notInlandOrOffshore = 6
    case offshore = 8
}

enum FacilityTypeValues: Int {
    case unknown=0,
    DiveResort = 1,
    DiveCenter = 2,
    Retailshop = 4,
    LiveaboardOperation = 8,
    GasFillingOperation = 16,
    PhotoVideoOperation = 32,
    Marina = 64,
    JettyOrDock = 128,
    RecompressionChamber = 256,
    CommercialDiveOperation = 512,
    DiveTravelRetailer = 1024,
    DiveClub = 2048,
    FreediveOp = 4096
}

enum FacilityOffersValues:Int64{
    case offersUnknown = 0,
    offersTrainingFrom = 1,
    offersShoreDiving = 2,
    offersBoatDiving = 4,
    offersLiveaboardTrips = 8,
    offersSnorkellingTrips = 16,
    offersBoatCharter = 32,
    offersWreckDiving = 64,
    offersIceDiving = 128,
    offersRecreationalTraining = 256,
    offersInstructorTraining = 512,
    offersTechnicalTraining = 1024,
    offersTekDivingSupport = 2048,
    offersCaveTraining = 4096,
    offersCaveDivingSupport = 8192,
    offersCommercialTraining = 16384,
    offersRebreatherTraining = 32768,
    offersRebreatherSupport = 65536,
    offersDisabledTraining = 131072,
    offersDisabledSupport = 262144,
    offersDeafTraining = 524288,
    offersDeafSupport = 1048576,
    offersFreedivingTraining = 2097152,
    offersFreedivingSupport = 4194304,
    offersNitroxFills = 8388608,
    offersTrimixFills = 16777216,
    offersEquipmentRental = 33554432,
    offersEquipmentSales = 67108864,
    offersOwnTrainingPool = 134217728,
    offersPhotoCourses = 268435456,
    offersStillsRental = 536870912,
    offersVideoCourses = 1073741824,
    offersVideoRental = 2147483648
}
