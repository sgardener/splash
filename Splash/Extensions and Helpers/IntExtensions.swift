//
//  IntExtensions.swift
//  Splash
//
//  Created by Simon Gardener on 13/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation

extension Int16 {
    
    func digitValues()-> (hundreds: Int, tens: Int, units: Int){
        let hundreds = self / 100
        let tens = (self - (hundreds * 100)) / 10
        let units = self - (hundreds * 100) - (tens * 10)
        return (Int(hundreds), Int( tens), Int(units))
    }
    
}

extension Int16 {
    //formatted duration string for surfaceInterval
    func durationString()-> String {
        guard self > 0 else {return "--"}
        return self.durationInHoursAndMinutes()
    }
    
    private func durationInHoursAndMinutes()-> String {
        let hours = self / 60
        let minutes = self - hours*60
        if self < 120 {
            return "\(self)"
        }else {
            if minutes != 0 {
                return "\(hours)\(hourText(hours)) \(minutes)"
            }else {
                return "\(hours)\(hourText(hours))"
            }
        }
    }
    
    private func hourText(_ hours:Int16)-> String{
        if hours > 1 {return "hr"} else {return "hrs"}
    }
    private func minuteText(_ minutes:Int16)-> String{
        
        if minutes == 1 { return "min"} else { return "min"}//removed a s from the multiple min
    }
}



