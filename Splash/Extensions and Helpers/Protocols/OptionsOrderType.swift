//
//  OptionsOrderType.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 08/01/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
import UIKit

protocol OptionOrder {
    associatedtype  OptionOrder: RawRepresentable
}

extension OptionOrder where /*Self: UIViewController,*/ OptionOrder.RawValue == Int {
    
    func optionOrderFor(position: Int) -> OptionOrder {
        guard let optionsOrder = OptionOrder(rawValue:position) else { fatalError("No position value") }
          return optionsOrder
    }
}

