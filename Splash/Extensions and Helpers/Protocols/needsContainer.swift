//
//  needsContainer.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 27/01/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
import CoreData
protocol NeedsDataModel {
    var container: NSPersistentContainer! {get set}
    var dataModel: DataModel! {get set}
}
