//
//  someTypeOfPerson.swift
//  Splash
//
//  Created by Simon Gardener on 04/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation

protocol SomeTypeOfPersonViewModel {
    func givenName()-> String?
    func familyName()-> String?
    func nickName()-> String?   
}
