//
//  AnotherTank.swift
//  Splash
//
//  Created by Simon Gardener on 28/12/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
protocol AnotherTank: class {
    func addTank()
}
