//
//  ExpectingDataModel.swift
//  Splash
//
//  Created by Simon Gardener on 17/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
protocol ExpectingDataModel {
    var dataModel: DataModel! {get set}
}
