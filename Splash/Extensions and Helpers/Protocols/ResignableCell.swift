//
//  ResignableCell.swift
//  Splash
//
//  Created by Simon Gardener on 15/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
import UIKit
protocol ResignableCell {
    var hiddenTextField: UITextField! {get set}
    
}
