//
//  Injectable.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 31/10/2017.
//  Copyright © 2017 Simon Gardener. All rights reserved.
//

import Foundation

protocol Injectable {
    
    associatedtype T
    
    func inject(_ :T)
    func assertDependencies()
    
}

//func inject(_: String) {
//    <#code#>
//}
