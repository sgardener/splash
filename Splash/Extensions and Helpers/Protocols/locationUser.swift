//
//  locationUser.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 04/01/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import BLTNBoard

protocol LocationUser {
    var locationService : LocationService! { get set}
    func locationDidUpdate(location: CLLocation)
    func partialFix(location:CLLocation)
    func locationWasDenied()
}

extension LocationUser {
    
    
}
