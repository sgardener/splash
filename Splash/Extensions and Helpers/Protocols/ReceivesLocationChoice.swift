//
//  ReceivesLocationChoice.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 28/01/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
protocol ReceivesLocationChoice : class {
    func setFromLocationTo(diveSite: DiveSite)
    func setFromLocationTo(facility: Facility)
    func setToLocationTo(diveSite: DiveSite)
    func setToLocationTo(facility: Facility)
    
}
