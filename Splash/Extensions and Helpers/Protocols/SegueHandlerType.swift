//
//  SegueHandlerType.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 31/10/2017.
//  Copyright © 2017 Simon Gardener. All rights reserved.
//

import Foundation
import UIKit

protocol SegueHandlerType {
    associatedtype Identifier : RawRepresentable
    
}
extension SegueHandlerType where Self: UIViewController, Identifier.RawValue == String {

    func segueIdentifierFor(segue: UIStoryboardSegue) -> Identifier {
        guard let identifier = segue.identifier, let segueIdentifier = Identifier(rawValue: identifier)  else { fatalError("Invalid Segue Identifier \(String(describing: segue.identifier)) ")}
        return segueIdentifier
    }
  
    
    func performSegueWith(identifier :Identifier, sender: Any?) {
        performSegue(withIdentifier: identifier.rawValue, sender: sender)
    }

}


