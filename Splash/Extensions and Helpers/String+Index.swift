//
//  String+Index.swift
//  Splash
//
//  Created by Simon Gardener on 23/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

extension String {
    
    var firstCharacter: String {
        let startIndex = self.startIndex
        let first =  self[...startIndex]
        return String(first)
    }

    func firstChar()-> String {
        let startIndex = self.startIndex
        let first =  self[...startIndex]
        return String(first)
    }

}
