//
//  DateExtensions.swift
//  Splash
//
//  Created by Simon Gardener on 11/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation

extension Date {
 static func formattedDate(_ date:Date, timeZone: String) -> String{
        let df = DateFormatter()
        df.dateStyle = .medium
        df.timeZone = TimeZone(identifier: timeZone)
        return df.string(from: date)
    }
  static func formattedTime(_ date:Date, timeZone: String) -> String{
        let df = DateFormatter()
    df.dateFormat = "HH:mm"
        df.timeZone = TimeZone(identifier: timeZone)
        return df.string(from: date)
    }
}

