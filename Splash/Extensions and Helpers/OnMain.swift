//
//  OnMain.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 05/01/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
func performUIUpdatesOnMain(_ updates: @escaping () -> Void) {
    DispatchQueue.main.async {
        updates()
    }
}
func performOnMain(_ task: @escaping () -> Void) {
    DispatchQueue.main.async {
        task()
    }
}

