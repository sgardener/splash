//
//  StringWranglers.swift
//  Splash
//
//  Created by Simon Gardener on 14/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class StringWranglers{
    fileprivate enum GasTypeOrder : Int{
        case air, nitrox, trimix, heliox, oxygen, helium
    }
    static let gasTypeArray = ["Air","Nitrox","Trimix","Heliox","Oxygen","Helium"]
    
    class func safetyStopDisplayStringFor(logEntry: LogEntry) -> String {
        return "\(logEntry.safetyStopTime) @ \(logEntry.safetyStopDepth.depthInUserPreference(withUnits: false))"
        
    }
    
    
    class func gasAndPercentageStringFor(_ tank:Tank) -> String {
        var gas = ""
        var percentString = ""
        let gasValue = Int(tank.gasType)
        guard let gasType = GasTypeOrder.init(rawValue: gasValue) else{ fatalError("wrong value stored for gas type - not complaint with GasTypeEnum")}
        gas = gasTypeArray[gasValue]
        switch gasType{
        case .air, .oxygen, .helium  : break
        case .nitrox:
            percentString = "\(tank.oxygenPercent.toOneDecimalPlaceString())%"
        case .trimix, .heliox:
            percentString = "\(tank.oxygenPercent.toOneDecimalPlaceString())  \(tank.heliumPercent.toOneDecimalPlaceString())"
            
        }
        return "\(gas) \(percentString)"
    }
    
    class func nameFor(_ diver :Diver) -> String{
        if let given = diver.givenName, let family = diver.familyName {
            return "\(given) \(family)"
        }else if let given = diver.givenName{
            return given
        }else if let family = diver.familyName{
            return family
        }else {
            return "missing name"
        }
    }
    class func cleanedUpText(from textField: UITextField)-> String?{
        
        if let text = textField.text{
            return text.trimmingCharacters(in: CharacterSet(charactersIn: ",? "))
        }else {
            return nil
        }
    }
    class func cleanedUpText(from textView:UITextView)-> String?{
        if let text = textView.text{
            return text.trimmingCharacters(in: CharacterSet(charactersIn: ",? "))
        }else {
            return nil
        }
    }
    class func cleanedUp(_ text:String)->String?{
        let replaced = text.replacingOccurrences(of: ";", with: ",")
        return replaced.trimmingCharacters(in: CharacterSet(charactersIn: ";,? "))
    }
    class func cleanForCSV(for item:String?)->String {
    guard var newItem =  item else { return ""}
    newItem = newItem.replacingOccurrences(of: ";", with: "🕝")
    newItem = newItem.replacingOccurrences(of: ",", with: "🕘")
    return newItem
    }
    
    
    /// - Parameter date: option Date object
    /// - Returns: a formatted human readable date string for display in the app
    /// not suitable for using wrapped up in a CSV String - see DateWrapper struct for that
    class func stringFor(date: Date?)-> String
    {
        guard date != nil else { return ""}
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MMM-dd, HH:mm"
        return dateFormatter.string(from: date!)
    }
    
    
}
