//
//  UIStoryboardSegue.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 31/10/2017.
//  Copyright © 2017 Simon Gardener. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboardSegue {
    func actualDestination () -> UIViewController {
        if self.destination.isKind(of: UINavigationController.self){
            let navController = self.destination as! UINavigationController
            return navController.topViewController!
        }else {
            return self.destination
        }
    }


}
