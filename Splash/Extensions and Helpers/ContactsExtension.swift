//
//  ContactsExtension.swift
//  Splash
//
//  Created by Simon Gardener on 22/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
import Contacts
import CoreData

extension CNContact {
    var hasPotentialCombinedName: Bool {
        let splitGivenName = self.givenName.components(separatedBy: CharacterSet.init(charactersIn: " "))
        if splitGivenName.count > 1 && self.familyName.isEmpty {
            return true
        }else { return false }
    }
    func splitGivenName()-> (first:String, middle: [String],  last :String)? {
        var names = givenName.components(separatedBy: CharacterSet(charactersIn: " ") )
        guard names.count >= 2 else { return nil }
        let first = names.removeFirst()
        let last = names.removeLast()
        let middle = names
        return (first, middle, last)
    }


    func copyDetails(to diver: Diver){
        diver.givenName = self.givenName
        diver.familyName = self.familyName
        diver.identifier = self.identifier
        diver.photo = self.thumbnailImageData
        guard let context = diver.managedObjectContext else {fatalError("diver didnt have an asscoiated context")}
        for phonelabeledValue  in self.phoneNumbers {
      
            let newPhone = PhoneNumber(context: context)
            if let label = phonelabeledValue.label {
              newPhone.label = CNLabeledValue<CNPhoneNumber>.localizedString(forLabel:label)
            }
            newPhone.value = phonelabeledValue.value.stringValue
            diver.addToPhoneNumbers(newPhone)
        }
        
        for emailLabelValue in self.emailAddresses{
            let newEmail = EmailAddress(context: context)
       //workaround as I cannot get the solution to getting the localised string
            if let label = emailLabelValue.label {
                newEmail.label = label.trimmingCharacters(in: CharacterSet(charactersIn: "_$!<>"))
            }
            newEmail.value = String(emailLabelValue.value)
            diver.addToEmailAdresses(newEmail)
        }
        
        for socialLabelValue in self.socialProfiles{
            let newSocial = SocialAccount(context:context)
            let social = socialLabelValue.value
            newSocial.label = CNSocialProfile.localizedString(forService: social.service)
            newSocial.value = CNSocialProfile.localizedString(forService: social.username)
            newSocial.url = social.urlString
            diver.addToSocialAccounts(newSocial)
            
        }
    }

}



