//
//  CoordinateRange.swift
//  Splash
//
//  Created by Simon Gardener on 16/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
import CoreLocation

struct CoordinateRange {
    var latitudePlus: Double
    var latitudeMinus: Double
    var longitudePlus: Double
    var longitudeMinus: Double
    var centerOfSearch :CLLocation
    var offset : Double
    
    init(withCenter center : CLLocation, offset: Double ) {
        centerOfSearch = center
        self.offset = offset
        latitudePlus = centerOfSearch.coordinate.latitude + offset
        latitudeMinus = centerOfSearch.coordinate.latitude - offset
        longitudePlus = centerOfSearch.coordinate.longitude + offset
        longitudeMinus = centerOfSearch.coordinate.longitude - offset
    }
    
    func searchPredicates () -> NSCompoundPredicate {
        var predicates = [NSPredicate]()
        let normalPredicate = NSPredicate.init(format: "longitude > %f and longitude < %f and latitude > %f and latitude < %f", self.longitudeMinus, self.longitudePlus, self.latitudeMinus, self.latitudePlus)
        predicates.append(normalPredicate)
        if exceedsLongitudeMinus() == true {
            predicates.append(predicateForLowLongitude())
        }
        if exceedsLongitudePlus() == true {
            predicates.append(predicateForHighLongitude())
        }
       return NSCompoundPredicate(orPredicateWithSubpredicates: predicates)
    }
    
    private func predicateForLowLongitude() -> NSPredicate {
        let underby = longitudeMinus + 180
        
        let longHigh = 180.0
        let longLow = 180.0 + underby
        
        return   NSPredicate.init(format: "longitude >%f and longitude < %f and latitude > %f and latitude < %f", longLow, longHigh, latitudeMinus, latitudePlus)
    }
    
    private func predicateForHighLongitude() -> NSPredicate {
        let overby = longitudePlus - 180
        let longLow = -180.0
        let longHigh = longLow + overby
        return   NSPredicate.init(format: "longitude >%f and longitude < %f and latitude > %f and latitude < %f", longLow, longHigh, latitudeMinus, latitudePlus)
    }
    
    private func exceedsLongitudeMinus()-> Bool{
        if longitudeMinus < -180 {
            return true
        }
        return false
    }
    private func exceedsLongitudePlus()-> Bool {
        if longitudePlus > 180 {
            return true
        }
        return false
    }
}


