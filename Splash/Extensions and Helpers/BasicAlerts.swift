//
//  BasicAlerts.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 21/11/2017.
//  Copyright © 2017 Simon Gardener. All rights reserved.
//

import Foundation
import UIKit

//typealias CompletionHandler = () -> Void

extension UIViewController {

    /// simple alert - for information purpose only , user must click ok to  proceed
    ///No action taken
    /// - Parameters:
    ///   - title: alert title
    ///   - message: information
    func showAlert(withTitle title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    /// alert with a completion handler
    ///
    /// - Parameters:
    ///   - title: alert title
    ///   - message: message info
    ///   - completion: a block
    func showAlert(withTitle title: String?, message: String?, completion:((_ action:UIAlertAction) -> Void)? = nil ){
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: completion)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    alert.addAction(action)
        alert.addAction(cancelAction)
    present(alert, animated: true, completion: nil)
    }
}

