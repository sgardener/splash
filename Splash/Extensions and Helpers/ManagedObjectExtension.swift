//  ManagedObjectExtension.swift
//  Splash
//
//  Created by Simon Gardener on 20/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.

import Foundation
import CoreData

extension NSManagedObject{
    /// revert returns a NSManagedObject to its previous committed state
    /// by iterating over its keys
    
    func revert(){
        //        print("** ** ** \(self.changedValues())")
        //        print("*********************************")
        //        print("changed value .keys \(self.changedValues().keys)")
        //        print("*********************************")
        //        print("Array of changedValue().keys \(Array(self.changedValues().keys))")
        //        print("There are \(Array(self.changedValues().keys).count) changedValue keys")
        //        print("*********************************")
        let changedKeys = Array(self.changedValues().keys)
        if changedKeys.count > 0 {
            let cv =  self.committedValues(forKeys: changedKeys)
            //            print("Commited value :\(cv)")
            //            print("*********************************")
            for (key,value) in cv{
                
                //                print("committed = \(self.committedValues(forKeys: [key])) changed = \(String(describing: self.changedValues()[key] )))")
                //                if value is NSNull {
                //                    print ("key - \(key) has value Null - doing nothing")
                //                }else {
                //                    print("SETTING KEY\(key) with VALUE :\(value)")
                self.setValue(value, forKey: key)
                //                }
            }
        }
    }
}
