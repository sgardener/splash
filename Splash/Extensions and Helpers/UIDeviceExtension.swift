//
//  UIDeviceExtension.swift
//  Splash
//
//  Created by Simon Gardener on 29/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

extension UIDevice{
    
    static func ownersName() -> String?{
        let deviceName = current.name
        // in english language the original format for device name is first last's iPhone
        // if we strip away the 's... we get a name string to look up in contacts
  
       if let range = deviceName.range(of: "’s") {
            print("owner name \(String(deviceName[..<range.lowerBound]))")
            return String(deviceName[..<range.lowerBound])
        }
        else {
            return nil
        }
    }
}













