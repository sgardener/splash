//
//  Double+Conversion.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 21/01/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Swift
import UIKit

extension Double {
    
    func formattedDistanceString() -> String {
        
        let unitsString: String = UserDefaults.distanceUnits() == .metric ? "KM": "NM"
        let distanceInKM = self / 1000
        let correctDistance = UserDefaults.distanceUnits() == .metric ? distanceInKM : distanceInKM * 0.5399568
        
        return String(format: "%.2f %@", correctDistance, unitsString)
        
    }
    func toOneDecimalPlaceString() -> String {
        return String(format: "%.1f",self )
    }
    func toZeroDecimalPlaceString() -> String {
        return String(format: "%.f",self )
    }
  
    func toFourDecimalPlaces()-> String {
         return String(format: "%.4f",self )
    }
    func asOneDecimalPlaceWithPercentString() -> String{
        return String(format: "%.1f %%",self)
    }
    func digitValues()-> (hundreds: Int, tens: Int, singles: Int, tenths: Int){
        let hundreds = Int(self / 100)
        let tens = Int((self - Double(hundreds * 100)) / 10)
        let singles = Int(self - Double (hundreds * 100) - Double(tens * 10))
        let tenths = Int((self - Double (hundreds * 100) - Double(tens * 10) - Double(singles)) * 10)
        return (hundreds, tens, singles, tenths)
    }
    
    //MARK:- weightConversion
    // return a weight string in imperial or metric depending on the user preference
    // IMPORTANT - expects a metric weight - as thats how weight is stored in coredata
    func weightInUserPreference()-> String {
        
        if UserDefaults.weightUnits() == .imperial {
            let weightString = self.convertKilosToPounds().toOneDecimalPlaceString()
            return "\(weightString)lbs"
        }else {
            let weightString = self.toOneDecimalPlaceString()
            return "\(weightString) kg"
        }
    }
    func convertKilosToPounds() -> Double{
        return self * 2.2
    }
    func convertPoundsToKilos()-> Double {
        return self / 2.2
    }
    //MARK:- Depth Conversion
    
    func convertMetersToFeet()-> Double {
        return self*3.3
    }
    func convertFeetToMeters()-> Double {
        return self/3.3
    }
    // return a depth string in imperial or metric depending on the user preference
    // IMPORTANT - expects a metric depth - as thats how depth is stored in coredata
    func depthInUserPreference(withUnits units: Bool = true)-> String {
        if UserDefaults.depthUnits() == .imperial {
            let depthString = self.convertMetersToFeet().toZeroDecimalPlaceString()  
            return  units == true ? "\(depthString) ft":depthString
        }else{
            let depthString = self.toOneDecimalPlaceString()
            return  units == true ? "\(depthString)m" : depthString
        }
    }
    
    
    //MARK:- Temperature Conversion
    
    //returns a temperature string depending on the user's preference
    //IMPORTANT - Expecting a metric vale fromcoredata/logEntry
    
    func temperatureInUserPreference(withUnits units: Bool = true)-> String{
        if UserDefaults.temperatureUnits() == .imperial {
            let temperatureString = self.convertCelciusToFahrenheit().toOneDecimalPlaceString()
            return units ? "\(temperatureString)°F": temperatureString
        }else {
            return units ? "\(self.toOneDecimalPlaceString())°C" : self.toOneDecimalPlaceString()
        }
    }
    
    func temperatureAsIntInUserPreference(withUnits units: Bool = true)-> String{
        if UserDefaults.temperatureUnits() == .imperial {
            let temperatureString = Int(self.convertCelciusToFahrenheit())
            return units ? "\(temperatureString)°F": "\(temperatureString)"
        }else {
            return units ? "\(Int(self))°C" : "\(Int(self))"
        }
    }
    func convertCelciusToFahrenheit()-> Double{
        return (9.0/5.0 * self ) + 32
    }
    func convertFahrenheitToCelcius()-> Double {
        return 5.0/9.0 * (self - 32.0)
    }

    /// takes a double and extracts the values at the tens,units and tenths position
    ///
    /// - Returns: a tuple of 3 Int
    func digitsAtPositions()->(tens:Int,singles:Int,tenths:Int){
        var number = Int(self * 10.0)
        let tens = number / 100
        number = number - tens * 100
        let singles = number / 10
        let tenths = number - singles * 10
        return (tens,singles,tenths)
    }

}

