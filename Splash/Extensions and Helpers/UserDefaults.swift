//
//  UserDefaults.swift
//  Splash(Swift)Tests
//
//  Created by Simon Gardener on 31/10/2017.
//  Copyright © 2017 Simon Gardener. All rights reserved.
//

import Foundation

enum AllUnits : Int {
    case metric
    case imperial
    case mixed
}
enum UnitsNotation: Int {
    case metric
    case imperial
}
struct UserDefaultsKeys {
    static let haveBeenSet = "haveBeenSet"
    static let depthUnits = "depthUnits"
    static let pressureUnits = "pressureUnits"
    static let temperatureUnits = "temperatureUnits"
    static let weightUnits = "weightUnits"
    static let distanceUnits = "distanceUnits"
    
    static let isDepthImperial = "isDepthImperial"
    static let isPressureImperial = "isPressureImperial"
    static let isTemperatureImperial = "isTemperatureImperial"
    static let isWeightImperial = "isWeightImperial"
    static let isDistanceImperial = "isDistanceImperial"
    
    static let regardLastDiveAsMostRecentlyAdded = "regardLastDiveAsMostRecentlyAdded"
    
    static let usesTables = "usesTables"
    static let isLoggingMultiLevel = "isLoggingMultiLevel"
    

    static let buddyAutofill = "buddyAutofill"
    static let otherAutofillItems = "otherAutofillItems"
    
    static let locations = "locations"
    static let timeNotation = "timeNotation"
    static let unitsNotation = "unitsNotation"
    static let temperatureNotation = "temperatureNotation"
    
    static let displayNameIsNickName = "displayName"
    
    static let guiding = "guiding"
    static let instructing = "instructing"
    static let assisting = "assisting"
    static let clientIsStudent = "clientStudent"
    static let clientIsGuidedDiver = "clientGuidedDiver"
    static let clientIsBothStudentAndGuided = "clientBoth"
    
    static let seedVersion = "seedVersion"
    
    static let triggerDive = "triggerDive"
    
    static let assName = "associatedName"
    static let assWeb = "associatedWeb"
    static let tabBarOrder = "tabBarOrder"
    
    static let onboarded = "onboarded"
}



extension UserDefaults {
    //MARK: - ProModes
    
    static func hasOnboarded()-> Bool {
        return UserDefaults.standard.bool(forKey: UserDefaultsKeys.onboarded)
    }
    static func setOnboarded(to bool: Bool){
        UserDefaults.standard.set(bool, forKey: UserDefaultsKeys.onboarded)
    }
    static func haveBeenSet()-> Bool{
        return UserDefaults.standard.bool(forKey: UserDefaultsKeys.haveBeenSet)
    }
    static func setSeedVersion(_ versionNumber: Int){
        UserDefaults.standard.set(versionNumber, forKey: UserDefaultsKeys.seedVersion)
    }
    static func seedVersion ()-> Int {
        return UserDefaults.standard.integer(forKey: UserDefaultsKeys.seedVersion)
    }
    static func setHaveBeenSet(to bool:Bool){
        UserDefaults.standard.set(bool, forKey: UserDefaultsKeys.haveBeenSet)
    }
    
    static func newLogEntryUsesGuideMode()-> Bool{
        return UserDefaults.standard.bool(forKey: UserDefaultsKeys.guiding)
    }
    static func setGuideMode(to bool:Bool){
    UserDefaults.standard.set(bool, forKey: UserDefaultsKeys.guiding)
    }
    
    static func newLogEntryUsesAssistingMode()-> Bool{
        return UserDefaults.standard.bool(forKey: UserDefaultsKeys.assisting)
    }
    static func setAssistingMode(to bool:Bool){
        UserDefaults.standard.set(bool, forKey: UserDefaultsKeys.assisting)
    }
    
    static func newLogEntryUsesInstructingMode()-> Bool{
        return UserDefaults.standard.bool(forKey: UserDefaultsKeys.instructing)
    }
    static func setInstuctingMode(to bool:Bool){
        UserDefaults.standard.set(bool, forKey: UserDefaultsKeys.instructing)
    }
    static func newClientDiversIsStudent()-> Bool {
        return UserDefaults.standard.bool(forKey: UserDefaultsKeys.clientIsStudent)
    }
    static func setClientDiverIsStudent(to bool: Bool) {
        UserDefaults.standard.set(bool, forKey: UserDefaultsKeys.clientIsStudent)
    }
    static func newClientDiverIsGuidedDiver()->Bool{
        return UserDefaults.standard.bool(forKey: UserDefaultsKeys.clientIsGuidedDiver)
    }
    static func setClientDiverIsGuidedDiver(to bool: Bool){
        UserDefaults.standard.set(bool, forKey: UserDefaultsKeys.clientIsGuidedDiver)
    }
    static func newClientDiverIsBoth()->Bool{
        return (UserDefaults.standard.bool(forKey: UserDefaultsKeys.clientIsStudent) && UserDefaults.standard.bool(forKey: UserDefaultsKeys.clientIsGuidedDiver))
    }
    
    //MARK: - DsplayName is Nickname
    
    static func useNickNameAsDisplayName()-> Bool{
        return UserDefaults.standard.bool(forKey: UserDefaultsKeys.displayNameIsNickName)
    }
    static func setNickNameIsDisplayName(to bool: Bool) {
        UserDefaults.standard.set(bool, forKey: UserDefaultsKeys.displayNameIsNickName)
    }
    //MARK: -  Last Dive is regarded as
    
    static func regardLastDiveAsMostRecentlyAdded() -> Bool{
        return UserDefaults.standard.bool(forKey: UserDefaultsKeys.regardLastDiveAsMostRecentlyAdded)
    }
    static func setRegardLastDiveAsMostRecentlyAdded(to bool: Bool) {
        UserDefaults.standard.set(bool, forKey: UserDefaultsKeys.regardLastDiveAsMostRecentlyAdded)
    }

    
    //MARK: - New Log Entry
    
    static func newLogUsesTables()-> Bool {
        return UserDefaults.standard.bool(forKey: UserDefaultsKeys.usesTables)
    }
    static func setNewLogUsesTables(bool: Bool) {
        UserDefaults.standard.set(bool, forKey: UserDefaultsKeys.usesTables)
    }
    
    static func newLogIsMultiLevel()-> Bool {
        return UserDefaults.standard.bool(forKey: UserDefaultsKeys.isLoggingMultiLevel)
    }
    static func setNewLogIsMultiLevel(bool: Bool){
        UserDefaults.standard.set(bool, forKey: UserDefaultsKeys.isLoggingMultiLevel)
    }
   
  
    
     //MARK: - Buddy Autofill
    static func setBuddyAutofill(value:Int) {
       UserDefaults.standard.set(value, forKey: UserDefaultsKeys.buddyAutofill )
    }
    static func buddyAutofillValue()-> Int{
        return UserDefaults.standard.integer(forKey: UserDefaultsKeys.buddyAutofill)
    }
       //MARK: - other autofill items
    
    static func setOtherAutofillItems(value: Int){
        UserDefaults.standard.set(value, forKey: UserDefaultsKeys.otherAutofillItems)
    }
    static func otherAutofillItemsValue() -> Int{
        return UserDefaults.standard.integer(forKey: UserDefaultsKeys.otherAutofillItems )
    }
    //MARK: - Measurements
    static func postMeasurementChangeNotification(){
        NotificationCenter.default.post(name: Notification.Name.init("depthUnitsChanged") , object: nil)
    }
    static func allMeasures()-> AllUnits {
        if allMeasurementsAre(.metric) {
            return .metric
        }else if allMeasurementsAre(.imperial){
            return .imperial
        }else{
            return .mixed
        }
    }
    static func allMeasurementsAre(_ units: UnitsNotation) -> Bool{
        if (UserDefaults.depthUnits() == units
            && UserDefaults.pressureUnits() == units
            && UserDefaults.temperatureUnits() == units
            && UserDefaults.weightUnits() == units
            && UserDefaults.distanceUnits() == units){
            return true
        }
        return false
    }
    

    static func setAllMeasurement(units: UnitsNotation) {
        UserDefaults.setDepth(units: units)
        UserDefaults.setPressure(units: units)
        UserDefaults.setTemperature(units: units)
        UserDefaults.setDistance(units: units)
        UserDefaults.setWeight(units: units)
    }
    
    static func setDepth(units: UnitsNotation){
        UserDefaults.standard.set(units.rawValue, forKey: UserDefaultsKeys.depthUnits)
        postMeasurementChangeNotification()
    }
    static func depthUnits() ->  UnitsNotation{
        let storedValue = UserDefaults.standard.integer(forKey: UserDefaultsKeys.depthUnits)
        return UnitsNotation(rawValue: storedValue) ?? UnitsNotation.metric
    }
    static func setPressure(units: UnitsNotation) {
        UserDefaults.standard.set(units.rawValue, forKey: UserDefaultsKeys.pressureUnits)
    }
    static func pressureUnits() -> UnitsNotation{
        let storedValue = UserDefaults.standard.integer(forKey: UserDefaultsKeys.pressureUnits)
        return UnitsNotation(rawValue: storedValue) ?? UnitsNotation.metric
    }
    
    static func setTemperature(units: UnitsNotation){
        UserDefaults.standard.set(units.rawValue, forKey: UserDefaultsKeys.temperatureUnits)
    }
    static func temperatureUnits() -> UnitsNotation {
        let storedValue = UserDefaults.standard.integer(forKey: UserDefaultsKeys.temperatureUnits)
        return UnitsNotation(rawValue: storedValue) ?? UnitsNotation.metric
    }
    static func setWeight(units: UnitsNotation){
        UserDefaults.standard.set(units.rawValue , forKey: UserDefaultsKeys.weightUnits)
    }
    static func weightUnits() -> UnitsNotation {
        let storedValue = UserDefaults.standard.integer(forKey: UserDefaultsKeys.weightUnits)
        return UnitsNotation(rawValue: storedValue) ?? UnitsNotation.metric
    }
    static func setDistance(units: UnitsNotation){
        UserDefaults.standard.set(units.rawValue, forKey: UserDefaultsKeys.distanceUnits)
    }
    
    static func distanceUnits()-> UnitsNotation{
        let storedValue = UserDefaults.standard.integer(forKey: UserDefaultsKeys.distanceUnits )
        return UnitsNotation(rawValue: storedValue) ?? UnitsNotation.metric
    }
    
    //MARK:- trigger dive
    
    static func shouldTriggerDive()-> Bool{
        return  UserDefaults.standard.bool(forKey: UserDefaultsKeys.triggerDive)
        
    }
    static func setTriggerDive(bool: Bool){
        UserDefaults.standard.set(bool, forKey: UserDefaultsKeys.triggerDive)
        
    }
    
    
    //MARK:- Associated Name and Web
    static func associatedName()-> String?{
        return UserDefaults.standard.string(forKey: UserDefaultsKeys.assName)
    
    }
    static func associateWeb()-> String?{
        return UserDefaults.standard.string(forKey: UserDefaultsKeys.assWeb)
    }
    static func setAssociatedName(name: String?){
        UserDefaults.standard.set(name, forKey: UserDefaultsKeys.assName)
    }
    static func setAssociatedWeb(web:String?){
        UserDefaults.standard.set(web, forKey: UserDefaultsKeys.assWeb)
    }
    
    //MARK:- TabBarOrder
    static func setTabBarOrderArray(_ tabBarOrder:[String]){
    UserDefaults.standard.set(tabBarOrder, forKey: UserDefaultsKeys.tabBarOrder)
    }
    static func tabBarOrderArray()-> [String]?{
        return UserDefaults.standard.array(forKey: UserDefaultsKeys.tabBarOrder) as? [String]
        
    }
}
