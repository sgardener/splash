//
//  CoreDataExtensions.swift
//  Splash
//
//  Created by Simon Gardener on 17/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
import CoreData


extension NSManagedObject {
    func cancelChanges() {
        managedObjectContext?.refresh(self, mergeChanges: false)
    }
}

