//
//  GeocoderService.swift
//  Splash
//
//  Created by Simon Gardener on 18/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
import CoreLocation

struct GeocoderService {
    //MARK:- FACILITY
    static func insertAll(placemarks:[CLPlacemark]?,into facility:Facility){
        guard let placemark = placemarks?.first else { return }
        if let country = placemark.country{
            facility.country = country
        }
        if let state = placemark.administrativeArea {
            facility.stateProvince = state
        }
        if let county = placemark.subAdministrativeArea {
            facility.county = county
        }
        if let street = placemark.thoroughfare {
            facility.streetName = street
        }
        if let town = placemark.locality{
            facility.cityTown = town
        }
        if let local = placemark.subLocality{
            facility.localAreaName  = local
        }
        if let postcode = placemark.postalCode{
            facility.postcode = postcode
        }
        
    }
    static func insertNew(placemarks:[CLPlacemark]?,into facility:Facility){
        guard let placemark = placemarks?.first else { return }
        if let country = placemark.country, facility.country == nil {
            facility.country = country
        }
        if let state = placemark.administrativeArea, facility.stateProvince == nil {
            facility.stateProvince = state
        }
        if let county = placemark.subAdministrativeArea, facility.county == nil {
            facility.county = county
        }
        if let street = placemark.thoroughfare, facility.streetName == nil {
            facility.streetName = street
        }
        if let town = placemark.locality, facility .cityTown == nil {
            facility.cityTown = town
        }
        if let local = placemark.subLocality, facility.localAreaName == nil {
            facility.localAreaName  = local
        }
        if let postcode = placemark.postalCode, facility.postcode == nil {
            facility.postcode = postcode
        }
        
    }
    static func keysForStoredValues(in facility: Facility)->[String]?{
        var alreadyStoredKeys = [String]()
        let keysToCheck = ["Country","stateProvince","county","streetName","localAreaName","cityTown",]
        for key in keysToCheck{
            if facility.value(forKey: key) != nil {
                alreadyStoredKeys.append(key)
            }
        }
        if alreadyStoredKeys.count > 0 {
            return alreadyStoredKeys
        }else{
            return nil
        }
    }
    //MARK:- SItes
    
    static  func insertAll(placemarks:[CLPlacemark]?, into site:DiveSite){
        guard let placemark = placemarks?.first else { return }
        if let country = placemark.country{
            site.country = country
        }
        if let state = placemark.administrativeArea{
            site.stateProvince = state
        }
        if let county = placemark.subAdministrativeArea{
            site.county = county
        }
        if let town = placemark.locality{
            site.cityTown = town
        }
        if let local = placemark.subLocality{
            site.localAreaName = local
        }
        if let ocean = placemark.ocean{
            site.bodyOfWater = ocean
        } else if let water = placemark.inlandWater {
            site.bodyOfWater = water
        }
    }
    
    static func insertNew(placemarks:[CLPlacemark]?, into site:DiveSite){
        guard let placemark = placemarks?.first else { return }
        
        if let country = placemark.country, site.country == nil {
            site.country = country
        }
        if let state = placemark.administrativeArea, site.stateProvince == nil {
            site.stateProvince = state
        }
        if let county = placemark.subAdministrativeArea, site.county == nil {
            site.county = county
        }
        if let town = placemark.locality, site.cityTown == nil {
            site.cityTown = town
        }
        if let local = placemark.subLocality, site.localAreaName == nil{
            site.localAreaName = local
        }
        if let ocean = placemark.ocean, site.bodyOfWater == nil {
            site.bodyOfWater = ocean
        } else if let water = placemark.inlandWater, site.bodyOfWater == nil {
            site.bodyOfWater = water
        }
    }
    
    static func lookUp(location: CLLocation, with handler:@escaping ([CLPlacemark]?, Error?)->Void ){
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location, completionHandler: handler)
    }
    
    static func keysForStoredValues(in site: DiveSite)->[String]?{
        var alreadyStoredKeys = [String]()
        let keysToCheck = ["Country","stateProvince","county","localAreaName","cityTown","bodyOfWater"]
        for key in keysToCheck{
            if site.value(forKey: key) != nil {
                alreadyStoredKeys.append(key)
            }
        }
        if alreadyStoredKeys.count > 0 {
            return alreadyStoredKeys
        }else{
            return nil
        }
    }
}


