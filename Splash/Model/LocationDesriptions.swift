//
//  LocationDesriptions.swift
//  Splash
//
//  Created by Simon Gardener on 11/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
struct LocationDescriptions{
    
    static func locationStringFor(_ logEntry: LogEntry) -> String {
        var locationString = "location: --"
        
        if let island = logEntry.diveSite?.islandAtoll , !island.isEmpty , let country = logEntry.diveSiteCountry, !country.isEmpty {
            locationString = island + ", " + country
        }else if let country = logEntry.diveSiteCountry, !country.isEmpty {
            locationString = country
        }
        return locationString
    }
}
