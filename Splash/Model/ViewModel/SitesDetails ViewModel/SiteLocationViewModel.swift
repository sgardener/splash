//
//  SiteLocationViewModel.swift
//  Splash
//
//  Created by Simon Gardener on 11/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreLocation

class SiteLocationViewModel {
    var dataModel: DataModel!
    var site:DiveSite!
    init(with dm:DataModel){
        dataModel = dm
        assert(dm.site != nil , "site is nil")
        site = dataModel.site
    }
    enum locationOrder:Int{ case name, alternativeName, bodyOfWater, islandAtoll, localAreaName, cityTown, county, stateProvince, country, tapToUseLast}
    let locationAtribute = [ "name", "alternativeName", "bodyOfWater", "islandAtoll", "localAreaName", "cityTown", "county", "stateProvince", "country", "tapToUseLast"]
    var sectionHeader = ["name and location","Coordinates - tap to update","this GPS fix is...","Dive site location can be public?","access to site possible by"]
    let salinityTypes = [0:"unknown", 1:"salt",2:"fresh",4:"brackish",8:"mixed layers",16:"high salinity"];
    
    //  _locationFieldNamesAndKeys = [["Dive site name - required","name"],["Dive site alt. name","alternativeName"],["Ocean/sea or inland water name","bodyOfWater"],["Island/atoll","islandAtoll"],["Local area name","localAreaName"],["City/town","cityTown"],["County","county"],["State/province","stateProvince"],["Country - required","country"],["Tap to use last location details","YOU DONT USE THIS ONE !!!"]];
    
    var locationPlaceholders = ["Dive site name - required","Dive site alt. name","Ocean/sea or inland water name","Island/atoll","Local area name","City/town","County","State/province","Country - required","Tap for last location details"];
    
   // var accessByFieldName = ["shore","RIB","outrigger","longtail","speed boat","day boat","liveaboard"];
    
    var gpsLocationIsOptions = ["the dive site","the shore entry point","nearby / a near mooring","nowhere near - i'll update later"]
    
    //MARK:-
    
    func numberOfSections()-> Int{
        return sectionHeader.count
    }
    
    func numberOfRowsIn(_ section: Int)-> Int{
        let sectionName = optionOrderFor(position: section)
        switch sectionName {
        case .nameLocation :
            return locationPlaceholders.count - 1
        case .coordTap, .canBePublic: return 1
            
        case .thisGPSIs:
            return gpsLocationIsOptions.count
        case .accessToSite:
            return AccessibleBy.accessByFieldName.count
        }
    }
    
    
    func headerStringFor(_ section:Int)->String{
        return sectionHeader[section]
    }
    //MARK:-Location
    func save(location loc:CLLocation){
        dataModel.site?.longitude = loc.coordinate.longitude as NSNumber
        dataModel.site?.latitude = loc.coordinate.latitude as NSNumber
    }
    
    func longitude()-> String?{
        guard site.keepLocationSecret == 0 else { return "as submitter requested."}
        if let longitude = site.longitude as? Double{
            return longitude.toFourDecimalPlaces()
        }else {return "is unknown."}
    }
    func latitude()-> String{
        guard site.keepLocationSecret == 0 else {return "exact location hidden"}
        if let latitude = site.latitude as? Double {
            return latitude.toFourDecimalPlaces()
        }else {return "Exact site location"}
    }
    //MARK:- Secrecy
    func secrecyString ()-> String {
        if dataModel.site?.keepLocationSecret == 0 {
            return "Yes. Site location is well known"
        }else {
            return "No. Protect exact location"}
    }
    //MARK:- GPS IS
    
    func GPSIsOption(for row : Int) -> String {
        return gpsLocationIsOptions[row]
    }
    func gpsIsAccessoryForRow(at indexPath: IndexPath) -> UITableViewCell.AccessoryType {
        
        let row = indexPath.row
        let existingValue = Int(dataModel.site!.gpsFix)
        if existingValue == GPSFix.isUnknown.rawValue
        {
            return .none
        }else if existingValue & Int(pow(2, Double(row))) == Int(pow(2,Double(row)) ){
            return .checkmark
        }
        return .none
    }
    func gpsIsTextForRow(at indexPath: IndexPath) -> String{
        return gpsLocationIsOptions[indexPath.row]
    }
    
    func gpsIsValue ()-> Int{
        return Int(dataModel.site!.gpsFix)
    }
    func setGpsIs(to value: Int){
        dataModel.site?.gpsFix = Int16(value)
    }
    func flipGpsOption(at row: Int){
        let value = Int(pow(2, Double(row)))
        if value & gpsIsValue() == value {
            setGpsIs(to: 0)
        }else {
            setGpsIs(to: value)
        }
    }
    //MARK:- Access options
    func accessby()-> Int{
        return Int((dataModel.site?.accessBy)!)
    }
    func setAccessBy(value:Int){
        dataModel.site?.accessBy = Int16(value)
    }
    func accessOption(for row :Int) -> String {
        return AccessibleBy.accessByFieldName[row]
    }
    func accessOptionAccessory(for row:Int) ->UITableViewCell.AccessoryType{
        let rowValue = Int(pow(2, Double(row)))
        
        if rowValue & accessby() == rowValue {
            return .checkmark
        } else {
            return .none
        }
    }
    func flipAccessOrderOption(at indexPath: IndexPath){
        let rowValue = Int(pow(2, Double(indexPath.row)))
        setAccessBy(value: rowValue^accessby())
    }
    //MARK:- Name Location
    
    func placeholder(for indexPath:IndexPath)-> String{
        if indexPath.section == 0 {
            return locationPlaceholders[indexPath.row]
        }else {
            return "?"
        }
    }
    func value(for indexPath:IndexPath) -> String?{
        if indexPath.row == locationOrder.tapToUseLast.rawValue {
            return locationPlaceholders[indexPath.row]
        }else {
            return dataModel.site?.value(forKey: locationAtribute[indexPath.row]) as? String
        }
    }
    
    func process(_ textField: UITextField){
        let text = StringWranglers.cleanedUpText(from: textField)
        if text?.isEmpty == false {
        dataModel.site?.setValue(text, forKey: locationAtribute[textField.tag])
        }else {
           dataModel.site?.setValue(nil, forKey: locationAtribute[textField.tag])
        }
    }
    
}
extension SiteLocationViewModel: SegueHandlerType , OptionOrder{
    enum Identifier: String {
        case nameLocation, coordTap, thisGPSIs, canBePublic, accessToSite
    }
    enum OptionOrder: Int {
        case nameLocation, coordTap, thisGPSIs, canBePublic, accessToSite
    }
}

struct AccessibleBy {
    static var accessByFieldName = ["shore","RIB","outrigger","longtail","speed boat","day boat","liveaboard"];

}
