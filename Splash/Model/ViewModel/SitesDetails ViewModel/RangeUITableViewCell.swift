//
//  RangeUITableViewCell.swift
//  Splash
//
//  Created by Simon Gardener on 24/03/2019.
//  Copyright © 2019 Simon Gardener. All rights reserved.
//

import UIKit
import RangeUISlider

class RangeUITableViewCell: UITableViewCell {

    @IBOutlet weak var rangeUISlider: RangeUISlider!
    @IBOutlet weak var upper: UILabel!
    @IBOutlet weak var lower: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
