//
//  SiteEnvironmentViewModel.swift
//  Splash
//
//  Created by Simon Gardener on 11/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
class  SiteEnvironmentViewModel {
    var dataModel: DataModel!
    let sectionHeader = ["Site environment is ...", "Site type is a ...", "Site features are ...", "Bottom is ...", "Site suitable for ...", "Water is ..."]
//    static let suitableForType = ["beginners","recreational diving","technical diving","night diving","snorkelling","freediving"]
//    static let whereType = ["inland", "coastal", "around island","offshore"]
    enum WhereSite :Int{
        case inland,coastal,aroundIsland,offShore
    }
//    static let salinityType = ["salt","fresh","brackish","mixed layers","high salinity"]
//    static let bottomType = ["sand","rock","mud","coral","sea grass","there is no bottom !"]
    enum Bottom :Int16 { case sand, rock, mud, coral, seagrass, none}
  
//    static let featureTypes = ["altitude","boulders","coral garden","cave","cavern","drift","drop-off","ice","kelp","muck","pinnacle(s)","plateau","slope","swim-thrus","wall","wreck","archeological interest","evil genius undersea base"]
//
//   static let environmentIsTypes = ["bay","channel","lagoon","cenote","lake","quarry","reservoir","river","estuary","mangrove","fjord","seamount","offshore reef","fringing reef","barrier reef","atoll"]
    enum environment:Int {
        case bay = 1, channel,lagoon,cenote,lake,quarry,reservoir,river,estuary,mangrove, fjord,seamount,offshoreReef,fringingReef,barrierReef,atoll
    }
    let keys = ["environment", "whereSiteLocated", "features", "bottomComposition", "suitableFor", "salinity"]
    let  whereNotInlandOrOffshore = 6
    var allTypes: [[String]]!
    
    enum EnvironmentSections:Int {
        case environmentIs, whereSiteIs, featuresAre, bottomIs,suitablefor, waterIs
    }
    
    
    init(with dm:DataModel){
        dataModel = dm
        allTypes = [Environmentals.environmentIsTypes, Environmentals.whereType, Environmentals.featureTypes, Environmentals.bottomType, Environmentals.suitableForType, Environmentals.salinityType]
    }
    func headerStringFor(_ section:Int)->String{
        return sectionHeader[section]
    }
    func numberOfSections()-> Int{
        return sectionHeader.count
    }
    func rows(in section: Int)-> Int {
        return allTypes[section].count
    }
    func label(at indexPath: IndexPath)-> String{
        return allTypes[indexPath.section][indexPath.row]
    }
    func checkMark(at indexPath:IndexPath)-> Bool{
        let attributeKey = keys[indexPath.section]
        let value = Int32(pow(2,Double(indexPath.row)))
        let attributeValue = dataModel.site?.value(forKey: attributeKey) as! Int32
        if attributeValue & value == value {return true} else { return false }
    }
    fileprivate func handleEnvironmentIsFlip(with indexPath :IndexPath){
        let rowValue =  Int32(pow(2, Double(indexPath.row)))
        let storedValue = (dataModel.site?.environment)!
        if storedValue & rowValue == rowValue {
            dataModel.site?.environment = 0
        }else {
            dataModel.site?.environment = rowValue
        }
        switch indexPath.row {
        case environment.bay.rawValue, environment.fringingReef.rawValue, environment.lagoon.rawValue :
            setToSalt()
            setToNotInlandOrOffShore()
        case environment.lake.rawValue,environment.reservoir.rawValue,environment.river.rawValue,environment.quarry.rawValue:
            setToFresh()
            setToInland()
        case environment.mangrove.rawValue,environment.estuary.rawValue:
            setToBrackish()
            setToCoastal()
            
        case environment.seamount.rawValue,environment.offshoreReef.rawValue,environment.barrierReef.rawValue,environment.atoll.rawValue:
            setToOffshore()
            setToSalt()
            
        case environment.cenote.rawValue:
            setToInland()
            setToBrackish()
            
        case environment.channel.rawValue:
            setToSalt()
            
        case environment.fjord.rawValue:
            setToSalt()
            setToCoastal()
        default: break
        }
        
      
    }
    fileprivate func handleWhereTypeIsFlip(with indexPath: IndexPath){
        let rowValue =  Int32(pow(2, Double(indexPath.row)))
        if (dataModel.site?.whereSiteLocated)! & rowValue == rowValue{
            dataModel.site?.whereSiteLocated = 0
        }else {
            dataModel.site?.whereSiteLocated = rowValue
        }
        
        switch indexPath.row {
        case WhereSite.inland.rawValue :
            setToFresh()
            unsetNotInlandTypes()
        case WhereSite.coastal.rawValue, WhereSite.aroundIsland.rawValue:
            setToSalt()
            unsetNotCoastalTypes()
        case WhereSite.offShore.rawValue:
            setToSalt()
            unsetNotOffShoreTypes()
        default:break
        }
        
    }
    func handleFeaturesAreFlip(with indexPath: IndexPath){
        //xor these values as ,ultiple possible
        let rowValue =  Int32(pow(2, Double(indexPath.row)))
        if indexPath.row != 17 {// evilgenius
            dataModel.site?.features = (dataModel.site?.features)! ^ rowValue
        }
    }
    
    
    /// changes bottom value in response to user tapping cell
    /// convoluted logic - as no bottom need to clear the other value - other values need to flip their bit and if turning on and no bottom is on, need to flip nobottom bit so it goes off
   
    func handleBottomFlip(with indexPath: IndexPath){
        let noBottomRowValue = Int16(pow(2, Double(Bottom.none.rawValue)))
        let rowValue =  Int16(pow(2, Double(indexPath.row)))
        if indexPath.row == Bottom.none.rawValue {
            if (dataModel.site?.bottomComposition) == rowValue {
                dataModel.site?.bottomComposition = 0
            }else {
                dataModel.site?.bottomComposition = rowValue
            }
        } else {
            var noBottomNeedsOffing = false
            if ( ((dataModel.site?.bottomComposition)! & rowValue != rowValue) &&
                ((dataModel.site?.bottomComposition)! & noBottomRowValue == noBottomRowValue )
                ){
                noBottomNeedsOffing = true
            }
            dataModel.site?.bottomComposition = (dataModel.site?.bottomComposition)! ^ rowValue
            if noBottomNeedsOffing == true {
              dataModel.site?.bottomComposition = (dataModel.site?.bottomComposition)! ^ noBottomRowValue
            }
        }

    }
    func handleSuitableForFlip(at indexPath:IndexPath){
        let rowValue =  Int16(pow(2, Double(indexPath.row)))
        dataModel.site?.suitableFor = (dataModel.site?.suitableFor)! ^ rowValue
    }
    func handleSalinityFlip( at indexPath: IndexPath){
    let rowValue =  Int16(pow(2, Double(indexPath.row)))
        if (dataModel.site?.salinity )! & rowValue == rowValue {
            dataModel.site?.salinity = 0
        }else {
            dataModel.site?.salinity = rowValue
        }
    }
    func flipSelection(at indexPath:IndexPath) {
        switch indexPath.section{
        case EnvironmentSections.environmentIs.rawValue:
            handleEnvironmentIsFlip(with: indexPath)
        case EnvironmentSections.whereSiteIs.rawValue:
            handleWhereTypeIsFlip(with: indexPath)
        case EnvironmentSections.featuresAre.rawValue:
            handleFeaturesAreFlip(with: indexPath)
        case EnvironmentSections.bottomIs.rawValue:
            handleBottomFlip(with: indexPath)
        case EnvironmentSections.suitablefor.rawValue:
            handleSuitableForFlip(at: indexPath)
        case EnvironmentSections.waterIs.rawValue:
            handleSalinityFlip(at: indexPath)
        default: break
        }
        
    }
    
}

// MARK: - handlers for sideeffects of setting values
//MARK: - Environment
extension SiteEnvironmentViewModel {
    func setToFresh(){
        dataModel.site?.salinity = Int16(Salinity.fresh.rawValue)
    }
    func setToSalt() {
        dataModel.site?.salinity = Int16(Salinity.salt.rawValue)
    }
    func setToBrackish() {
        dataModel.site?.salinity = Int16(Salinity.brackish.rawValue)
    }
    func unsetNotCoastalTypes(){
        dataModel.site?.environment = Int32((dataModel.site?.environment)!) & EnvironmentTypeValue.coastalTypes.rawValue
    }
    func unsetNotInlandTypes(){
        dataModel.site?.environment = Int32((dataModel.site?.environment)!) & EnvironmentTypeValue.inlandTypes.rawValue
    }
    func unsetNotOffShoreTypes (){
        dataModel.site?.environment = Int32((dataModel.site?.environment)!) & EnvironmentTypeValue.offshoreTypes.rawValue
    }
    
    //MARK: where
    func setToNotInlandOrOffShore(){
        dataModel.site?.whereSiteLocated = ((dataModel.site?.whereSiteLocated)!) & Int32(WhereSiteLocatedType.notInlandOrOffshore.rawValue)
    }
    func setToInland(){
        dataModel.site?.whereSiteLocated = Int32(WhereSiteLocatedType.inland.rawValue)
    }
    func setToCoastal(){
        dataModel.site?.whereSiteLocated = Int32(WhereSiteLocatedType.coastal.rawValue)
    }
    func setToOffshore(){
        dataModel.site?.whereSiteLocated = Int32(WhereSiteLocatedType.offshore.rawValue)
    }
    
    
    //MARK:- TODO
    //review various checkmarks
    
    //MARK:- CHecks
//    func checkIfCOastalIsland()-> Bool{
//        let whereValue = (dataModel.site?.whereSiteLocated)!
//        if ((whereValue & WhereSiteLocatedType.inland.rawValue) == whereValue || (whereValue & WhereSiteLocatedType.offshore.rawValue) == whereValue) {
//
//
//        }
//    }
    
}

struct Environmentals {
    static let sectionHeader = ["Site environment is ...", "Site type is a ...", "Site features are ...", "Bottom is ...", "Site suitable for ...", "Water is ..."]
    static let suitableForType = ["beginners","recreational diving","technical diving","night diving","snorkelling","freediving"]
    static let whereType = ["inland", "coastal", "around island","offshore"]
    static let salinityType = ["salt","fresh","brackish","mixed layers","high salinity"]
    static let bottomType = ["sand","rock","mud","coral","sea grass","there is no bottom !"]
    static let featureTypes = ["altitude","boulders","coral garden","cave","cavern","drift","drop-off","ice","kelp","muck","pinnacle(s)","plateau","slope","swim-thrus","wall","wreck","archeological interest","evil genius undersea base"]
    
    static let environmentIsTypes = ["bay","channel","lagoon","cenote","lake","quarry","reservoir","river","estuary","mangrove","fjord","seamount","offshore reef","fringing reef","barrier reef","atoll"]
}

//-(BOOL) checkIfCoastalIsland{
//    if (([self.site.whereSiteLocated intValue] & WhereInland) ||  ([self.site.whereSiteLocated intValue] & WhereOffshore)) {
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Mangroves ? " message:@"Mangrove should be 'coastal' or 'around island', not 'inland', on an 'atoll' or 'offshore'. Please alter 'Site location is' section before trying to set." preferredStyle:UIAlertControllerStyleAlert ];
//        UIAlertAction * action = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:nil];
//        [alert addAction:action];
//        [self.parentView presentViewController:alert animated:YES completion:nil];
//        return  NO;
//    }
//    return YES;
//}
//
//-(BOOL) checkMangroveRange{
//    float latitude = [self.site.latitude floatValue];
//    if (latitude < -29.00 || latitude >29.00) {
//
//        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Are You Sure ?" message:@"Mangrove are typically found between -25 and +25 degree. you are some distance outside this range. If this is really a mangrove environment please write that in 'Notes' section" preferredStyle:UIAlertControllerStyleAlert ];
//        UIAlertAction * action = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:nil];
//        [alert addAction:action];
//        [self.parentView presentViewController:alert animated:YES completion:nil];
//        return NO;
//    }
//    return YES;
//}

