//
//  CurrentCell.swift
//  Splash
//
//  Created by Simon Gardener on 13/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class CurrentCell: UITableViewCell {

    @IBOutlet weak var currentSlider: UISlider!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
