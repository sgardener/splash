//
//  SiteDataViewModel.swift
//  Splash
//
//  Created by Simon Gardener on 11/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation

class SiteDataViewModel{
    var dataModel: DataModel!
    var sectionHeader = ["Site depth range.","Typical dive depth range.","High season visibility range.","Low season visibility range.","Water temperature range.","Current"]
    var lowerAttributes = ["minimumDepth", "avDepthRangeShallow", "highSeasonVisibilityRangeLow", "lowSeasonVisibilityRangeLow", "temperatureRangeLow"]
    
    let upperAttributes = [ "maximumDepth", "avDepthRangeDeeper", "highSeasonVisibilityRangeHigh", "lowSeasonVisibilityRangeHigh", "temperatureRangeHigh"]
    enum CurrentFactors:Int { case occurs, strength}
    let currentAttributes = ["currentFrequency","currentStrength"]
    let currentLabel = ["Occurs:","Strength:"]
    let currentStrength = ["N/A","Mild","Mild to Steady","Mild to Strong","Mild to Insane","Steady","Steady to Strong", "Steady to insane","Strong","Strong to insane","Hell's Teeth!"]
    let currentFrequency = ["Dont know", "Never","Rarely","Sometimes","no value 4","50/50","novalue 6","Usually","no value 8","Almost Always","Always","error OVERSHOT VALUE"]
    let minimumValues = [ -1.0, -1.0, -1.0, -1.0, -3.0, -1.0, -1.0, -1.0, -1.0, 28]
    let maximumValues = [ 121.0, 121.0, 51.0, 51.0, 41.0, 121.0, 121.0, 257.0, 257.0, 105.0]
    init(with dm:DataModel){
        dataModel = dm
        
    }
    func numberOfSections()-> Int{
        return sectionHeader.count
    }
    func numberOfRow(in section: Int)-> Int{
        switch section{
        case OptionOrder.current.rawValue: return 2
        default: return 1
        }
        
    }
    func headerStringFor(_ section:Int)->String{
        
        //FIXME: - this always gets the metric header
        
        return sectionHeader[section]
    }
    func upperValue(at indexPath:IndexPath)-> Double {
       
        print( dataModel.site?.value(forKey: upperAttributes[indexPath.section]) as Any)
        return dataModel.site?.value(forKey: upperAttributes[indexPath.section]) as! Double
    }
    func lowerValue(at indexPath:IndexPath)->Double{
        return dataModel.site?.value(forKey: lowerAttributes[indexPath.section]) as! Double
    }
    func setUpperValue(at section: Int, with value: Double){
        dataModel.site?.setValue(value, forKey: upperAttributes[section])
    }
    func setLowerValue(at section:Int, with value: Double ){
        var theValue = value
        if theValue < 0 { theValue = -1 }
        dataModel.site?.setValue(value, forKey: lowerAttributes[section])
    }
   
    func minimumValue(at indexPath:IndexPath)-> Double{
        return minimumValues[indexPath.section]
    }
    func maximumValue(at indexPath:IndexPath)-> Double{
        return maximumValues[indexPath.section]
    }
    
    //MARK:- label text for range sliders
   
    func  labelTextForValue(at index:Int,lower lowerAttribute: Bool)-> String {
        var value : Double
        if lowerAttribute == true {
            value = dataModel.site?.value(forKey: lowerAttributes[index]) as! Double
        } else{
            value = dataModel.site?.value(forKey: upperAttributes[index]) as! Double
        }
        let rangeType = optionOrderFor(position: index)
        switch rangeType{
        case .depthRange, .typicalDepthRange:
            return depthStringFor(value)
        case .highSeasonViz, .lowSeasonViz:
           return vizStringFor(value)
        case .waterTemp:
            return tempStringFor(value)
        case .current:
           return ""
        }
    }
    
    fileprivate func vizStringFor(_ value: Double)-> String{
        let isMetric = (UserDefaults.distanceUnits()  == .metric)
        switch value {
        case -1 ... -0.000001 : return "Unknown"
        case 51: return "Unknown"
        default:
        if isMetric == true {
            return "\(value.toZeroDecimalPlaceString()) m"
        }else {
            return "\(value.convertMetersToFeet().toZeroDecimalPlaceString()) ft"
            }
        }
    }
    fileprivate func tempStringFor(_ value: Double)->String {
         let isMetric = (UserDefaults.temperatureUnits() == .metric)
        switch value {
        case -3 ... -2.000001 : return "Unknown"
        case 41: return "Unknown"
        default:
            if isMetric == true {
                return "\(value.toZeroDecimalPlaceString())°C"
            }else {
                return"\(value.convertCelciusToFahrenheit().toZeroDecimalPlaceString())°F"
            }
       
        }
    }
    fileprivate func depthStringFor(_ value:Double)->String {
        let isMetric = (UserDefaults.distanceUnits()  == .metric)
        
        switch value {
        case -1 ... -0.0000001 : return "Unknown"
        case  121: return "Unknown"
        case 0...100.9999:
            if isMetric == true {
                let number = value.toZeroDecimalPlaceString()
                return "\(number) m"
            }else {
                let number = value.convertMetersToFeet().toZeroDecimalPlaceString()
               return "\(number) ft"
            }
        case 101...104.999999 :
            return isMetric == true ? "> 50 m":"> 165 ft"
        case 105...109.999999:
            return isMetric == true ? "> 100 m":"> 330 ft"
        case 110...114.999999:
            return isMetric == true ? "> 150 m":"> 500 ft"
        case 115...120.999999:
            return isMetric == true ? "> 200 m":"> 660 ft"
        default: return "Unknown"
        }
    }


    //MARK:- current Slider
    func currentSliderValue(for indexPath: IndexPath)-> Float{
        switch indexPath.row{
        case CurrentFactors.occurs.rawValue:
            return  Float((dataModel.site?.currentFrequency)!)
        case CurrentFactors.strength.rawValue:
            return Float((dataModel.site?.currentStrength)!)
        default : return 1000.0
        }
    }
    func setCurrentValue(for index:Int, with value: Float){
        var newValue: Double
        switch index{
        case CurrentFactors.occurs.rawValue:
            newValue = bandedValue(for: value)
        case CurrentFactors.strength.rawValue:
            newValue = (Double(Int(value * 10.0)))/10
        default: fatalError()
        }
        print("SETTING VALUE  -  \(value), \(newValue) forKey \(currentAttributes[index]) ")
        dataModel.site?.setValue(newValue, forKey: currentAttributes[index])
       
    }
    func bandedValue(for frequency: Float)-> Double{
        switch frequency{
        case 0.0 : return 0.0
        case 0.0001...0.124999999 : return 0.1
        case 0.125...0.2499999 : return 0.2
        case 0.25...0.3749999 : return 0.3
        case 0.375...0.59999: return 0.5
        case 0.6...0.799999: return 0.7
        case 0.8...0.949999: return 0.9
        case 0.95...1.0: return 1.0
        default: return 1.1//never
        }
  
    }
    func currentNameLabel(for indexPath:IndexPath)->String{
        return currentLabel[indexPath.row]
    }
    func currentValue(for indexPath:IndexPath)-> String{
        switch indexPath.row {
        case CurrentFactors.occurs.rawValue : return currentOccursString()
        case CurrentFactors.strength.rawValue:
            return currentStrengthString()
        default:
            return "BANANA"
        }
    }
    fileprivate func currentOccursString()-> String{
        let occurs = (dataModel.site?.currentFrequency)! * 10
        print("freq + Int \(occurs) , \(Int(occurs))")
        return currentFrequency[Int(occurs)]
        
    }
    fileprivate func currentStrengthString()-> String{
        
        let strength = (dataModel.site?.currentStrength)! * 10
        print("freq + Int \(strength) , \(Int(strength))")
        return currentStrength[Int(strength)]
        
    }
}
extension SiteDataViewModel :SegueHandlerType, OptionOrder{
    enum Identifier: String {
        case depthRange,typicalDepthRange,highSeasonViz, lowSeasonViz, waterTemp, current
        
    }
    enum OptionOrder : Int {
        case depthRange,typicalDepthRange,highSeasonViz, lowSeasonViz, waterTemp, current
    }
}
