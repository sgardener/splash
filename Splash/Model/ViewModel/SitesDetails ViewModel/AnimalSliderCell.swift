//
//  AnimalSliderCell.swift
//  Splash
//
//  Created by Simon Gardener on 13/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class AnimalSliderCell: UITableViewCell {

    @IBOutlet weak var animalLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
