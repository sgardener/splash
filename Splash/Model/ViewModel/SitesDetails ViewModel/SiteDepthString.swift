//
//  SiteDepthString.swift
//  Splash
//
//  Created by Simon Gardener on 06/05/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation

struct SiteDepth{
    
    /// provides a correcly formatted  string for a depth (or distance in case of visibility value).
    ///
    /// - Parameter value: a Double
    /// - Returns: a String in the users prefered units m/ft.  Values over 100 return  ">x"  value rather the number
    static func stringFor(_ value:Double)->String {
        let isMetric = (UserDefaults.distanceUnits()  == .metric)
        switch value {
        case -1,121: return "Unknown"
        case 0...100.9999:
            if isMetric == true {
                let number = value.toZeroDecimalPlaceString()
                return "\(number)m"
            }else {
                let number = value.convertMetersToFeet().toZeroDecimalPlaceString()
                return "\(number)ft"
            }
        case 101...104.9999 :
            return isMetric == true ? ">50m":">165ft"
        case 105...109.9999:
            return isMetric == true ? ">100m":">330ft"
        case 110...114.9999:
            return isMetric == true ? ">150m":">500ft"
        case 115...120.9999:
            return isMetric == true ? ">200m":">660ft"
        default: return ""
        
        }
    }
}
