//
//  SiteNoteViewModel.swift
//  Splash
//
//  Created by Simon Gardener on 11/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class SiteNoteViewModel{
    var dataModel: DataModel!
    var sectionHeader = ["Dive site notes","tag note author as ","On this site.."]
    var beasties: [(name:String, attribute:String)] = [("sharks","sharkPossibility"),("mantas","mantaPossibility"),("turtles","turtlePossibility")]
    init(with dm:DataModel){
        dataModel = dm
    }
    func numberOfSections()-> Int{
        
        return sectionHeader.count
    }
    func headerStringFor(_ section:Int)->String{
        return sectionHeader[section]
    }
    func numberOfRows(in section: Int)-> Int{
        let sectionName = optionOrderFor(position: section)
        switch sectionName{
        case .note, .credit:
            return 1
            
        case .beasties:
            return beasties.count
        }
    }
    func personToCredit()-> String {
        let person = UserDefaults.associatedName()
        let web =  UserDefaults.associateWeb()
        return person ?? web ?? "Anonymous"
    }
    func webToCredit()-> String {
        let person = UserDefaults.associatedName()
        let web =  UserDefaults.associateWeb()
        if person != nil{
            return web ?? "tap to enter credit details"
        }else{
            return "tap to enter credit details"
        }
        
    }
    //MARK:- Animal Stuff
    
    func animalString(at indexPath:IndexPath)->String{
        return "\(beasties[indexPath.row].name) \(AnimalPossibility.possibilityString(for:  dataModel.site?.value(forKey: beasties[indexPath.row].attribute) as! Float ))"
     
        
    }
    func possibility(at indexPath:IndexPath)-> Float{
        return dataModel.site?.value(forKey: beasties[indexPath.row].attribute) as! Float
    }
    func setPossibility(value:Float, tag:Int){
        dataModel.site?.setValue(value, forKey: beasties[tag].attribute)
    }
    //MARK:- Note
    func note()-> String?{
        return  dataModel.site?.note
    }
    func setNote(with text:String?){
        
        if let note = text {
            dataModel.site?.note = StringWranglers.cleanedUp(note)
        }
    }
}

extension SiteNoteViewModel:  SegueHandlerType, OptionOrder {
    enum Identifier: String {
        case note,credit,beasties
    }
    enum OptionOrder: Int {
        case note,credit,beasties
    }
}
