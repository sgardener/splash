//
//  SitePreparer.swift
//  Splash
//
//  Created by Simon Gardener on 17/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
class SitePreparer{
    
    class func prepare(site:DiveSite){
        site.maximumDepth = 121.0
        site.avDepthRangeDeeper = 121.0
        site.highSeasonVisibilityRangeHigh = 51.0
        site.lowSeasonVisibilityRangeHigh = 51.0
        site.temperatureRangeHigh = 41.0
        
        site.minimumDepth = -1.0
        site.avDepthRangeShallow = -1.0
        site.highSeasonVisibilityRangeLow = -1.0
        site.lowSeasonVisibilityRangeLow = -1.0
        site.temperatureRangeLow = -3.0
        
        site.addedByUser = true
    }
}
