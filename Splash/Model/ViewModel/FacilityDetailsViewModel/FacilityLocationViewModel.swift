//
//  FacilityLocationViewModel.swift
//  Splash
//
//  Created by Simon Gardener on 15/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreLocation

class  FacilityLocationViewModel {
    var dataModel: DataModel!
    var facility: Facility!
    init(with dm : DataModel){
        dataModel = dm
        assert (dm.facility != nil, "facility is nil")  
        facility = dm.facility
        //
        //        var myTextField = UITextField()
        //        myTextField.keyboardType = .url
    }
    
    let sectionHeader = ["name, contacts & address","coordinates - tap to startUpdate"]
    let locationFieldNamesAndKeys = [["Facility name - required","name"],
                                     ["Web site URL","website"],
                                     ["Email address","email"],
                                     ["Phone number","phoneNumber" ],
                                     ["Fax number","faxNumber" ],
                                     ["Building number/name","buildingNameNumber" ],
                                     ["Street name","streetName" ],
                                     ["Local area name","localAreaName" ],
                                     ["City/Town","cityTown" ],
                                     ["Island/Atoll","islandAtoll" ],
                                     ["County","county" ],
                                     ["State/Province","stateProvince"],
                                     ["Country - required","country" ],
                                     ["Post code","postcode" ],
                                     ["Tap for last location details","Tap has no key value"]]
    let locationFieldKeyboardType : [UIKeyboardType] =
        [.default, .URL, .emailAddress, .numbersAndPunctuation, .numbersAndPunctuation, .numbersAndPunctuation, .default, .default, .default, .default, .default, .default, .default, .default, .default]
    let capitalisation :[UITextAutocapitalizationType] = [
        .words , .none, .none, .none, .none, .words,.words, .words, .words, .words,.words,.words,.words, .allCharacters ,.none ]
    func numberOfSections()->Int {
        return 2
    }
    func numberOfRowsIn(_ section:Int)->Int{
        switch section{
        case FacilityLocationSections.location.rawValue  :
            return locationFieldNamesAndKeys.count - 1
        case FacilityLocationSections.gps.rawValue:
            return 1
            
        default: return 0
        }
    }
    func headerStringFor(_ section:Int)->String{
        return sectionHeader[section]
    }
    func placeholder(at indexPath:IndexPath)->String{
        return locationFieldNamesAndKeys[indexPath.row][0]
    }
    func value(at indexPath:IndexPath)-> String?{
        if indexPath.row == OptionOrder.tapToBringForward.rawValue{
            return locationFieldNamesAndKeys[indexPath.row][0]
        }
        else {
            let key = locationFieldNamesAndKeys[indexPath.row][1]
            
            if let keyValue = dataModel.facility?.value(forKey: key  ) as? String{
                return keyValue
            }else {
                return nil
            }
        }
    }
    func capitalization(at indexPath:IndexPath)-> UITextAutocapitalizationType {
        return capitalisation[indexPath.row]
    }
    func keyboardType(at indexPath: IndexPath) -> UIKeyboardType {
        return locationFieldKeyboardType[indexPath.row]
    }
    func process(_ textField: UITextField){
        let text = StringWranglers.cleanedUpText(from: textField)
        if text?.isEmpty == false {
            dataModel.facility?.setValue(text, forKey: locationFieldNamesAndKeys[textField.tag][1])
        }else {
            dataModel.facility?.setValue(nil, forKey: locationFieldNamesAndKeys[textField.tag][1])
        }
    }
    func save(location loc:CLLocation){
        dataModel.facility?.longitude = loc.coordinate.longitude as NSNumber
        dataModel.facility?.latitude = loc.coordinate.latitude as NSNumber
    }
    func longitude()-> String?{
        if let longitude = facility.longitude as? Double{
            return longitude.toFourDecimalPlaces()
        }else {return nil}
    }
    func latitude()-> String?{
        if let latitude = facility.latitude as? Double {
            return latitude.toFourDecimalPlaces()
        }else {return nil}
    }
}

extension FacilityLocationViewModel {
    enum FacilityLocationSections:Int {
        case location, gps
    }
    
    enum OptionOrder:Int {
        case  facilityName,
        webSiteUrl,
        emailAddress,
        phoneNumber,
        faxNumber,
        buildingN,
        streetName,
        localAreaName,
        cityTown,
        island,
        county,
        stateProvince,
        country,
        postCode,
        tapToBringForward
    }
}
