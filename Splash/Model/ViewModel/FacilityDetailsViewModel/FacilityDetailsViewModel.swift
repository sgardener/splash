//
//  FacilityDetailsViewModel.swift
//  Splash
//
//  Created by Simon Gardener on 06/03/2019.
//  Copyright © 2019 Simon Gardener. All rights reserved.
//

import Foundation
import CoreData

class FacilityDetailsViewModel {
    var facility: Facility!
    
    init(with fac: Facility){
        facility = fac
    }

    func addressAsString()-> String {
        var addressElements = [String?]()
        addressElements.append(facility.buildingNameNumber)
        addressElements.append(facility.streetName)
        addressElements.append(facility.localAreaName)
        addressElements.append(facility.cityTown)
        addressElements.append(facility.islandAtoll)
        addressElements.append(facility.county)
        addressElements.append(facility.postcode)
    
        return addressElements.compactMap{$0}.joined(separator: ", ")
    }
    func isAffiliated()-> Bool{
        let count1 = facility.resortType?.allObjects.count ?? 0
        let count2 = facility.centerType?.allObjects.count ?? 0
        if count1 + count2 > 0 {
            return true
        }else {return false}
    }
    func hasType()-> Bool{
        return facility.type > 0 ? true:false
    }
    func typeOfFacility()-> String {
        let variousTypes = FacilityTypeViewModel.facilityTypes
        var arrayOfTypes = [String]()
        let typeValue = facility.type
        for (index, value) in variousTypes.enumerated(){
            if typeValue & Int32(pow(2,(Double(index)))) == typeValue {
                arrayOfTypes.append(value)
            }
        }
        return arrayOfTypes.joined(separator: ", ")
    }
    
    func numberOfRowForTrainingFrom()->Int {
        guard let trainingAgencies = facility.trainingAgency, trainingAgencies.count > 0 else { return 0}
        return 1
    }
    func trainingBy()-> String {
        let trainingAgencies = facility.trainingAgency?.allObjects as! [TrainingAgency]
        return trainingAgencies.map{$0.name!}.joined(separator: ", ")
    }
    
    func doesOfferService()-> Bool {
        return facility.offers > 0 ? true:false
    }
    
    func servicesOffered()-> String {
        let variousServices = FacilityOffersViewModel.facilityOffers
        let servicesValue = Int(facility.offers)
        return someThings(matching: servicesValue, in: variousServices)
    }
//    func servicesOffered()-> String {
//        let variousServices = FacilityOffersViewModel.facilityOffers
//        var arrayOfServices = [String]()
//        let servicesValue = facility.offers
//        for (index,value) in variousServices.enumerated() {
//            let aServiceValue = Int64(pow(2, (Double(index))))
//            if servicesValue & aServiceValue == aServiceValue {
//                arrayOfServices.append(value)
//            }
//        }
//        return arrayOfServices.joined(separator: ",  ")
//    }
    
    func someThings(matching positionValue: Int, in things:[String])-> String{
        var arrayOfStuff = [String]()
        for (index, value) in things.enumerated(){
            let aPosition = Int(pow(2, Double(index)))
            if aPosition & positionValue == aPosition {
                arrayOfStuff.append(value)
            }
        }
        return arrayOfStuff.joined(separator: ", ")
    }
    
    func affiliation()-> String{
        let resortTypes = (facility.resortType?.allObjects as! [ResortType]).map{$0.type!}.joined(separator: ", ")
        let centerTypes = (facility.centerType?.allObjects as! [CenterType]).map{$0.type!}.joined(separator: ", ")
        var bothTypes = [String]()
        if !resortTypes.isEmpty { bothTypes.append(resortTypes)}
        if !centerTypes.isEmpty { bothTypes.append(centerTypes)}
        return bothTypes.joined(separator: ", ")
    }
}
