//
//  FacilityOffersViewModel.swift
//  Splash
//
//  Created by Simon Gardener on 15/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
class FacilityOffersViewModel {
    // made static so we can access from Facility Wrapper and not duplicate definition - useful if i later make changes
    static let facilityOffers = ["training from ...","shore diving","boat diving","liveaboard trips","snorkelling trips","boat charter","wreck diving","ice diving","recreational training","instructor training","technical training","tek diving support","cave training","cave diving support","commercial training","rebreather training","rebreather support","training for disabled people","support for disabled divers","training for deaf people","support for deaf divers","freediving training","freediving support","nitrox fills","trimix fills","equipment rental","equipment sales","own training pool","photography courses","stills camera rental","video courses","video camera rental"]

    
    var dataModel: DataModel!
    
    init(with dm : DataModel){
        dataModel = dm
    }

    func numberOfRows()-> Int {
        return FacilityOffersViewModel.facilityOffers.count
    }
    let sectionHeader = "this facility offers..."
    
    func header()-> String{
        return  sectionHeader
    }
    func textString(at index:Int)-> String {
        return FacilityOffersViewModel.facilityOffers[index]
    }
    
    func isCheckmark(at index:Int)-> Bool {
        if index == 0 {
            return false
        }else {
            let comparisonValue = Int64(pow(2,(Double(index))))
            if (dataModel.facility?.offers)! & comparisonValue == comparisonValue{
               return true
            }else{ return false}
        }
    }
    
    func flipValue(at index:Int){
        let rowName = optionOrderFor(position: index)
        //print("row : \(index), name \(rowName.name()) value: \(rowName.value())")
       // let comparisonValue = Int32(pow(2,(Double(index))))
        dataModel.facility?.offers = (dataModel.facility?.offers)! ^ Int64(rowName.value())
    }
}


extension FacilityOffersViewModel: OptionOrder{
    enum OptionOrder: Int{
        case TrainingFrom,
        ShoreDiving ,
        BoatDiving,
        LiveaboardTrips,
        SnorkellingTrips,
        BoatCharter,
        WreckDiving,
        IceDiving,
        RecreationalTraining,
        InstructorTraining,
        TechnicalTraiining,
        TekDivingSupport,
        CaveTraining,
        CaveDivingSupport,
        CommercialTraining,
        RebreatherTraining,
        RebreatherSupport,
        TrainingForDisabledPeople,
        SupportForDisabledDivers,
        TrainingForDeafPeople,
        SupportForDeafDivers,
        TrainingForFreediving,
        SupportForFreediving,
        NitroxFills,
        TrimixFills,
        EquipmentRental,
        EquipmentSales,
        OwnTrainingPool,
        PhotoCourse,
        StillsRental,
        VideoCourse,
        VideoRental
        
        func value()-> Int64{
            return Int64(pow(2, Double(self.rawValue)))
        }
        
        func name()-> String{
            return String(describing: self)
        }
    }
}
