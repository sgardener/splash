//
//  FacilityTypeViewModel.swift
//  Splash
//
//  Created by Simon Gardener on 15/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation

class FacilityTypeViewModel{
    
    //static so can access from SiteWrapper Struct without duplicating 
    static let facilityTypes = ["dive resort","dive center","retail shop","liveaboard operation","gas filling station","photo-video operation","marina","jetty or dock","recompression chamber","commercial dive operation","dive travel retailer","dive club - non commercial","freediving club or business"]
    
    var dataModel: DataModel!
    
    init(with dm : DataModel){
        dataModel = dm
    }

    let sectionHeader = "this facility is a..."
    func header()-> String{
        return  sectionHeader
    }
    
    func numberOfRows()-> Int {
        return FacilityTypeViewModel.facilityTypes.count
    }
    
    func textString(at index:Int)-> String {
        return FacilityTypeViewModel.facilityTypes[index]
    }
    
    func isCheckmark(at index:Int)-> Bool {
        if index == 0 {
            return false
        }else {
            let comparisonValue = Int32(pow(2,(Double(index))))
            if (dataModel.facility?.type)! & comparisonValue == comparisonValue{
                return true
            }else {
                return false
            }
        }
    }
    func flipValue(at indexPath: IndexPath){
        let comparisonValue = Int32(pow(2,Double(indexPath.row)))
        dataModel.facility?.type = dataModel.facility!.type ^ comparisonValue
    }
}
extension FacilityTypeViewModel : OptionOrder{
    enum OptionOrder: Int {
        case diveResort, DiveCenter, retailshop ,liveaboardOperation ,
        gasFillingOperation , photoVideoOperation,  marina, jettyOrDock , recompressionChamber, commercialDiveOperation, diveTravelRetailer, dveClub, freediveOp
    }
}

