//
//  SiteBasicsViewModel.swift
//  Splash
//
//  Created by Simon Gardener on 09/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreLocation

class SiteBasicsViewModel{
    
    var dataModel: DataModel!
    var mode:Mode!
    let siteBasicsLocationPlaceHolders = ["Dive site name- required", "Island/atoll - optional", "Country - required"]
    let gpsLocationIsOptions = ["the dive site","the shore entry point","nearby","nowhere near - I'll update later"]
    let sitesBasicsHeaders = ["Name and location","co-ordinates - tap to update","This GPS location is...","Water Salinity","tell us more ?"]
    let salinityTypes = [0:"unknown", 1:"salt",2:"fresh",4:"brackish",8:"mixed layers",16:"high salinity"];

    init(dataModel dm:DataModel){
        self.dataModel = dm
    }
    
    func site()->DiveSite{
        guard let diveSite = dataModel.site else { fatalError("requesting site before creation or insertion")}
        return diveSite
    }
    func save(location loc:CLLocation){
        dataModel.site?.longitude = loc.coordinate.longitude as NSNumber
        dataModel.site?.latitude = loc.coordinate.latitude as NSNumber
    }
    func siteNeedsSubmitting(){
        dataModel.site?.needsSubmitting = true
    }
    func longitude()-> String?{
        if let longitude = site().longitude as? Double{
            return String(longitude)
        }else {return nil}
    }
    func latitude()-> String?{
        if let latitude = site().latitude as? Double {
            return String(latitude)
        }else {return nil}
    }
    func newSite()-> DiveSite{
        let site = dataModel.newDiveSite()
        return site
    }
    func deleteNewSite(){
        let theSite = site()
        let context = dataModel.container.viewContext
        
        context.delete(theSite)
        dataModel.saveContext()
        
    }
    func cancelChanges(){
        dataModel.site?.cancelChanges()
    }
    
    func canSave()-> Bool{
        if  dataModel.site?.name == nil {
            return false
        }
        if dataModel.site?.country  == nil { return false

        }
        return true
    }

    func numberOfRowsIn(section:Int)-> Int {
        switch section {
        case 0 : return siteBasicsLocationPlaceHolders.count
        case 2 : return gpsLocationIsOptions.count // gpslocation is
        default: return 1
        }
    }
    func headerString(for section: Int)-> String{
        return sitesBasicsHeaders[section]
    }
    
    func siteName()->String?{
        return dataModel.site?.name
    }
   
    func setSiteName(_ name :String?) {
     dataModel.site?.name = name
    }
    func handleSiteBasics(_ textField: UITextField){
        var text:String? = StringWranglers.cleanedUpText(from: textField)
        if text?.isEmpty == true {
            text = nil
        }
        if textField.tag == SiteBasicsEditor.SiteBasicsLocation.name.rawValue {
            setSiteName(text)
        }else {
            setIslandAtoll(text)
        }
    }
    func islandAtoll()-> String?{
        return dataModel.site?.islandAtoll
    }
    func setIslandAtoll(_ islandAtoll: String?){
        dataModel.site?.islandAtoll = islandAtoll
    }
    
    func locationPlaceholder(at row: Int)-> String?{
        return siteBasicsLocationPlaceHolders[row]
    }
    func country()-> String?{
        return dataModel.site?.country
    }
    func setCountry(_ country: String){
        dataModel.site?.country = country
    }
    func gpsIsTextForRow(at indexPath: IndexPath) -> String{
        return gpsLocationIsOptions[indexPath.row]
    }
    func gpsIsAccessoryForRow(at indexPath: IndexPath) -> UITableViewCell.AccessoryType {
       
        let row = indexPath.row
        let existingValue = Int(dataModel.site!.gpsFix)
        if existingValue == GPSFix.isUnknown.rawValue
        {
            return .none
        }else if existingValue & Int(pow(2, Double(row))) == Int(pow(2,Double(row)) ){
            return .checkmark
        }
       return .none
    }
    func gpsIsValue ()-> Int{
        
        return Int(dataModel.site!.gpsFix)
    }
    func setGpsIs(to value: Int){
        dataModel.site?.gpsFix = Int16(value)
    }
    func flipGpsOption(at row: Int){
         let value = Int(pow(2, Double(row)))
        if value & gpsIsValue() == value {
            setGpsIs(to: 0)
        }else {
            setGpsIs(to: value)
        }
        
    }
    func salinity()-> String{
        let salinityValue = Int(dataModel.site!.salinity)
        if let text = salinityTypes[salinityValue]{
            return text
        }else {
            return "bad value for salinity"
        }
    }
    func setSalinity(value:Int16){
        dataModel.site?.salinity = value
    }
}


