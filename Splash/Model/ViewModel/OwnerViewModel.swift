//  OwnerViewModel.swift
//  Splash
//
//  Created by Simon Gardener on 29/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.

import UIKit
import CoreData
class OwnerViewModel: DiverViewModel {
    
    var owner: LBOwner?
    var currentPolicy: InsurancePolicy!
    
    var insuranceAttributes = ["name", "companyName", "number", "policyType", "emergencyPhoneNumber","N/A for polciy datesDate", "depthLimits", "emergenceEmailAddress", "website", "remindWhenExpiring", "note"]
    var insuranceDetailLabels = ["policy name","company name", "policy number", "policy type", "emergency phone number", "N/Aexpires and starts on", "maximum depth covered","email contact", "website", "remindWhenExpiring", "note"]
    var insurancePlaceholders = ["enter policy name","enter insurer name","enter policy number","select policy type","enter emergency phone number","N/A policy dates","enter depth limit","enter email contact","enter website"," expiring cell","note"]


    var insuranceKeyboard : [UIKeyboardType] = [ .default, .default, .numbersAndPunctuation, .default, .numbersAndPunctuation, .default, .numbersAndPunctuation, .emailAddress, .URL, .default, .default]
    var sectionHeaders = ["Diver Identity","Experience",nil,"Emergency Stuff",nil,nil,"Phone","Email","Social"]

    override init(with dm: DataModel){
    super.init(with: dm)
        owner = dm.logBookOwner()
    }
    func ownerDetailsSectionHeaders(for section: Int)-> String?{
        return sectionHeaders[section]
    }
    
    func set(_ policy: InsurancePolicy){
        currentPolicy = policy
    }
    func policy()-> InsurancePolicy{
        return currentPolicy
        
    }
    func frcForInsurancePolicies()-> NSFetchedResultsController<InsurancePolicy> {
       return  dataModel.frcForInsurancePolicies()
    }
    func hasPolicies() -> Bool {
        if dataModel.numberOfInsurancePolicies() > 0 { return true } else { return false }
    }
    func cancelNewPolicy(){
        dataModel.container.viewContext.delete(currentPolicy)
    }
    func newInsurancePolicy(){
        currentPolicy = dataModel.newInsurancePolicy()
    }
    func insurancePlaceholder(at indexPath: IndexPath)-> String{
        return insurancePlaceholders[indexPath.section]
    }
    func textForPolicyAttibute(at indexPath: IndexPath)->String?{
        return currentPolicy.value(forKey: insuranceAttributes[indexPath.section]) as? String
    }
    func labelData(at indexPath:IndexPath)-> String{
        return insuranceDetailLabels[indexPath.section]
    }
    func store(data: String?, at section:Int){
        currentPolicy.setValue(data, forKey: insuranceAttributes[section])
    }
    func textForInsuranceNote()-> String?{
        return currentPolicy.note
    }
    func storeNote(_ noteText:String?){
        currentPolicy.note = noteText
    }
    func keyboardType(at indexPath:IndexPath)-> UIKeyboardType {
        return insuranceKeyboard[indexPath.section]
    }
    
    //MARK:- Policy Type
    func frcForPolicyType()-> NSFetchedResultsController<InsurancePolicyType>
    {
        return dataModel.frcForInsurancePolicyTypes()
    }
    func accessoryTypeFor(_ policyType: InsurancePolicyType)-> UITableViewCell.AccessoryType{
        return  currentPolicy.type == policyType ? .checkmark: .none
    }
    func userChangedStatus(of policyType: InsurancePolicyType){
        if currentPolicy.type  == policyType {
            currentPolicy.type = nil
        }else {
            currentPolicy.type = policyType
        }
    }
    //MARK:- NAme Stuff
    
    override func familyName()-> String? {
        return dataModel.owner?.familyName
    }

    override func setFamilyName(_ theName:String?){
        if let name = theName, !name.isEmpty {
            dataModel.owner?.familyName = name
        } else {
            dataModel.owner?.familyName = nil
        }
    }
    override func givenName()-> String? {
        return dataModel.owner?.givenName
    }
    override  func setGivenName(_ theName: String?){
        if let name = theName, !name.isEmpty {
            dataModel.owner?.givenName = name
        } else {
            dataModel.owner?.givenName = nil
        }
    }
    
    override func nickName()-> String? {
        return dataModel.owner?.nickName
    }
    override func setNickName(_ theName: String?){
        if let name = theName, !name.isEmpty {
            dataModel.owner?.nickName = name
        } else {
            dataModel.owner?.nickName = nil
        }
    }
   override func displayName()-> String? {
        if let nickName = dataModel.owner?.nickName, nickName.isEmpty != true, UserDefaults.useNickNameAsDisplayName() == true{
            return nickName
        }else {
            return (StringWranglers.nameFor(dataModel.diver!))
        }
    }
    //MARK:- Certifications

   override  func certificationsString()->String? {
        if let certifications = dataModel.owner?.holdsCertification {
            let certArray = certifications.sortedArray(using: certSortDescriptor) as! [Certification]
            var certString = certArray.reduce(""){$0 + ($1.type?.name!)!+"\n"}
            certString = certString.trimmingCharacters(in: .newlines)
            return certString
        }
        return nil
    }
    
    //MARK:- logged Dives
    
    func loggedDives()-> String? {
        if let  numberOfLoggedDives = dataModel.owner?.loggedDives, !numberOfLoggedDives.isEmpty {
            return numberOfLoggedDives
        }else{
            return nil
        }
    }
    func setLoggedDives(text: String?){
        if let loggedNumber = text, !loggedNumber.isEmpty  {
            dataModel.owner?.loggedDives = loggedNumber
        }else{
            dataModel.owner?.loggedDives = nil
        }
    }
    
   // MARK:- dates for policy

    
   // MARK:-
//   override func save(){
//        dataModel.saveContext()
//    }
    //func delete( _ object:NSManagedObject){
   //     dataModel.container.viewContext.delete(object)
  // }
    override func diverImage()-> UIImage? {
        guard let photoData = dataModel.owner?.photo else { return nil }
        return UIImage(data: photoData)
    }
    override func set(diverImage: UIImage?){
        guard let image = diverImage, let data = image.pngData() else {
            return}
        dataModel.owner?.photo = data
    }
}
