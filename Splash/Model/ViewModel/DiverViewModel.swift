//
//  DiverViewModel.swift
//  Splash
//
//  Created by Simon Gardener on 20/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.

import UIKit
import Contacts
import CoreData

// NOTE  - functionality used only by divers - ie buddies,pro and clinets can use dataModel.diver
//NOTE - functionality used by divers AND Diver subclasses - owner and emergency contacts - MUST use dataModel.theDiver



class DiverViewModel {
    
    init(with dm: DataModel){
        dataModel = dm
    }
    var dataModel: DataModel
    
    let studentValue :Int16 = 2
    let guidedValue :Int16 = 4
    let dmValue :Int16 = 2
    var mode: Mode!
    let notOfThisType = 1
    let clientStatusName = ["not client","Student","Guided Diver"]
    let proStatusName = ["not dive pro","guide","instructor","assistant instructor","instructor trainer"]
    let buddyStatusName = ["not buddy","Default Buddy","Regular Buddy","Irregular/One Off Buddy"]
    let diverStatusHeader = ["Buddy Status","Pro Status","Client Status"]
    let placeholder = ["blank","given name","family name","nick-name"]
    let buddiesSectionHeader = [2:"Default",4:"Regular",8:"Irregular"]
    var placeholderPESLabels = ["enter label","enter label","enter social service"]
    var placeholderPESValues = ["enter phone number","enter email address","enter account name/id"]
    
    let certSortDescriptor = [NSSortDescriptor(key: #keyPath(Certification.type.type ), ascending: false), NSSortDescriptor(key: #keyPath(Certification.type.displayOrder), ascending: false)]
    
    
    //MARK:- Social/Phone/Email values for divers
    var emails:[EmailAddress]!
    var socials:[SocialAccount]!
    var phones:[PhoneNumber]!
    
    func updatePhonesEmailsSocials(){
        updateEmails()
        updatePhones()
        updateSocial()
    }
    func updateEmails(){
        emails = getEmails()
    }
    func updateSocial(){
        socials = getSocial()
    }
    func updatePhones(){
        phones = getPhoneNums()
        
    }
    func getEmails()->[EmailAddress]{
        if let setOfEmails = dataModel.theDiver?.emailAdresses as? Set<EmailAddress>{
            print ("*******************here are the emails ... \(setOfEmails)")
            return Array(setOfEmails)
        }else        {
            return [EmailAddress]()
        }
    }
    func getPhoneNums()-> [PhoneNumber]{
        if let setOfPhoneNums = dataModel.theDiver?.phoneNumbers as? Set<PhoneNumber>{
            
            return Array(setOfPhoneNums)
        }else {
            return [PhoneNumber]()
        }
    }
    
    func getSocial()-> [SocialAccount]{
        if let setOfSocial = dataModel.theDiver?.socialAccounts  as? Set<SocialAccount>{
            return Array(setOfSocial)
        }else {
            return [SocialAccount]()
        }
    }
    
    func phoneCount()-> Int{
        return phones.count
    }
    func emailCount()-> Int{
        return emails.count
    }
    func socialCount()->Int{
        return socials.count
    }
    func diveProStatusRowCount()-> Int{
        print("diver pro status =\(String(describing: diver()?.proStatus))")
        if diver()?.proStatus == 1 {
            return 1
        }else{
            return 2
        }
    }
    
    func emailLabel(at row: Int) -> String?{
        return  emails[row].label
    }
    func emailData(at row:Int) -> String? {
        return emails[row].value
    }
    
    func socialLabel(at row:Int) -> String? {
        return socials[row].label
    }
    func socialData(at row:Int)-> String? {
        return socials[row].value
    }
    
    func phoneLabel(at row: Int)-> String? {
        return phones[row].label
    }
    func phoneData(at row: Int) -> String? {
        return phones[row].value
    }
    
    func removeContactDataObject(at indexPath:IndexPath, offsetBy: Int){
        let objectType = indexPath.section - offsetBy
        switch objectType {
        case 0 :
            let  object = phones.remove(at: indexPath.row)
            delete(object)
        case 1 :
            let  object = emails.remove(at: indexPath.row)
            delete(object)
        case 2 :
            let  object = socials.remove(at: indexPath.row)
            delete(object)
        default: break
        }
    }
    func placeholderForPhoneEmailSocialLabel(at index: Int, offsetBy: Int)-> String{
        let actualIndex = index - offsetBy
        guard 0...2 ~= actualIndex  else { fatalError("problem with index value - check you havent added a section and need to change the value in the line above")}
        return placeholderPESLabels[actualIndex]
        
    }
    func placeholderforPhoneEmailSocialValue(at index: Int, offsetBy: Int)->String{
        let actualIndex = index - offsetBy
        guard 0...2 ~= actualIndex  else { fatalError("problem with index value - check you havent added a section and need to change the value in the line above")}
        return placeholderPESValues[actualIndex]
    }
    
    func addNewPhoneNumber(){
        dataModel.theDiver?.addToPhoneNumbers(dataModel.newPhoneNumber())
        updatePhones()
    }
    func addNewEmail(){
        dataModel.theDiver?.addToEmailAdresses(dataModel.newEmail())
        updateEmails()
    }
    
    func addNewSocial(){
        dataModel.theDiver?.addToSocialAccounts(dataModel.newSocial())
        updateSocial()
    }
    
    //MARK: - DiverCrud
    
    func diver()-> Diver?{
        return dataModel.diver
    }
    func set(diver: Diver){
        dataModel.diver = diver
    }
    func deleteNewDiver(){
        dataModel.container.viewContext.delete(dataModel.diver!)
    }
    func delete( _ object:NSManagedObject){
        dataModel.container.viewContext.delete(object)
    }
    func addNewDiver(){
        let _ = dataModel.newDiver()
    }
    
    func canDelete(_ diver:Diver)-> Bool{
        return dataModel.canDelete(diver)
    }
    // MARK:- Diver Object In Use ?
    
    func logEntryBuddiesContain(_ diver: Diver)-> Bool {
        guard let buddies = dataModel.logEntry?.buddiedBy else { return false }
        return buddies.contains(diver)
    }
    func logEntryProsContain(_ diver: Diver)-> Bool {
        guard let pros = dataModel.logEntry?.ledBy else { return false }
        return pros.contains(diver)
    }
    func certificationContains(_ diver:Diver)->Bool {
        return dataModel.cert == diver
    }
    
    func logEntryStudentsContains(_ diver: Diver)-> Bool{
        guard let students = dataModel.logEntry?.instructed else { return false}
        return students.contains(diver)
    }
    func logEntryGuidedDiversContains(_ diver: Diver)-> Bool {
        guard let guided = dataModel.logEntry?.supervised else { return false}
        return guided.contains(diver)
    }
    
    
    
    //MARK: -  Add/Remove buddies, pros, students,supervised
    func addBuddy(_ diver:Diver){
        guard let logEntry = dataModel.logEntry else { fatalError("no log entry") }
        logEntry.addToBuddiedBy(diver)
    }
    func removeBuddy(_ diver:Diver){
        guard let logEntry = dataModel.logEntry else { fatalError("no log entry") }
        logEntry.removeFromBuddiedBy(diver)
    }
    func addPro(_ diver:Diver){
        guard let logEntry = dataModel.logEntry else { fatalError("no log entry")}
        logEntry.addToLedBy(diver)
    }
    func removePro(_ diver:Diver){
        guard let logEntry = dataModel.logEntry else { fatalError("no log entry") }
        logEntry.removeFromLedBy(diver)
    }
    func addStudent(_ diver: Diver){
        guard let logEntry = dataModel.logEntry else { fatalError("no log Entry") }
        logEntry.addToInstructed(diver)
    }
    func removeStudent(_ diver: Diver){
        guard let logEntry = dataModel.logEntry else { fatalError("no log Entry") }
        logEntry.removeFromInstructed(diver)
    }
    func addSupervised(_ diver: Diver){
        guard let logEntry = dataModel.logEntry else { fatalError("no log Entry") }
        logEntry.addToSupervised(diver)
    }
    func removeSupervised(_ diver: Diver){
        guard let logEntry = dataModel.logEntry else { fatalError("no log Entry") }
        logEntry.removeFromSupervised(diver)
    }
    func makeCertifyingInstructor(_ diver:Diver){
        dataModel.cert?.certifiedBy = diver
    }
    func removeCertifyingInstructor(_ diver:Diver){
        dataModel.cert?.certifiedBy = nil
    }
    //MARK:- Certifications
    
    func certificationsString()->String?{
        if let certifications = dataModel.diver?.holdsCertification {
            let certArray = certifications.sortedArray(using: certSortDescriptor) as! [Certification]
            var certString = certArray.reduce(""){$0 + ($1.type?.name!)!+"\n"}
            certString = certString.trimmingCharacters(in: .newlines)
            return certString
        }
        return ""
    }
    
    //MARK: - Random
    
    
    fileprivate func logEntriesUse(_ diver: Diver)-> Bool {
        if  diver.ledDives?.count == 0 && diver.buddiedOn?.count == 0 && diver.wasSupervisedOn!.count == 0  && diver.instructedOn?.count == 0 {
            return false
        } else {
            return true
        }
    }
    
    
    func messageForNameConflictIssue(contact: CNContact) -> String {
        return "Splash found a contact with no familiy name and the following 'first name' \(contact.givenName)\n\n. What would you like to do?"
    }
    
    func deleteMessageFor(_ diver :Diver)-> String{
        if logEntriesUse(diver) == false {
            return "Delete \(displayName() ?? "this diver" )?"
        }else{
            return complexMessageFor(diver)
        }
    }
    
    /// constructs an informative message for 'deleteing a diver' alertController
    ///
    /// - Parameter diver: takes a diver NSManagedObject sublass
    /// - Returns: the message string
    
    func complexMessageFor(_ diver: Diver)-> String {
        var message = " \(displayName() ?? "WARNING!\nThis diver") is in use by:\n"
        if let count = diver.ledDives?.count {
            message.append("\(count) log enties as a guide or instructor\n")
        }
        if let count = diver.buddiedOn?.count {
            message.append("\(count) log entries as a buddy\n")
        }
        if let count = diver.wasSupervisedOn?.count {
            message.append("\(count) log entries as a guided diver\n")
        }
        if let count = diver.instructedOn?.count {
            message.append("\(count) log entries as a student\n")
        }
        message.append("Deleting this diver will remove the diver from those log entries\n\n. You probably shouldn't rewrite history .")
        return message
    }
    func alreadyExistsDiverWith(givenName: String?, familyName:String?)-> Bool {
        return dataModel.alreadyExistsDiverWith(givenName, familyName)
    }
    func diverImage()-> UIImage? {
        guard let photoData = dataModel.theDiver?.photo else { return nil }
        return UIImage(data: photoData)
    }
    func set(diverImage: UIImage?){
        guard let image = diverImage, let data = image.pngData() else {
            return}
        dataModel.theDiver?.photo = data
    }
    
    func buddyHeader(for section : Int)->String {
        guard let header = buddiesSectionHeader[section] else {fatalError("buddyHeader no valid dictionary index")}
        return header
    }
    func setBuddyStatus(to value :Int){
        dataModel.diver?.buddyStatus = Int16(value)
    }
    func setProStatus(){
        dataModel.diver?.proStatus = dmValue
    }
    func setClientStatus(){
        if UserDefaults.newClientDiverIsBoth(){
            dataModel.diver?.clientStatus = 6
        }else if UserDefaults.newClientDiverIsGuidedDiver(){
            dataModel.diver?.clientStatus = guidedValue
        }else {
            dataModel.diver?.clientStatus = studentValue
        }
    }
    func setGuidedStatus(){
        dataModel.diver?.clientStatus = guidedValue
    }
    func setStudentStatus(){
        dataModel.diver?.clientStatus = studentValue
    }
    
    func addDetails(from contact:CNContact, with nameElements:(first:String, middle :[String], last:String)? ){
        
        contact.copyDetails(to: dataModel.diver!)
        
        if let names = nameElements {
            // this is for combined names issues
            dataModel.diver?.givenName = names.first
            dataModel.diver?.familyName = names.last
        }
    }
    func save(){
        dataModel.saveContext()
    }
    func placeholderTextFor(_ row: Int)-> String {
        return placeholder[row]
    }
    
    func proNumber()->String?{
        return diver()?.proNumber
    }
    //MARK:- FRC stuff
    
    func frcForBuddies ()-> NSFetchedResultsController<Diver> {
        return dataModel.fetchedResultsControllerForBuddies()
    }
    func frcForPros ()-> NSFetchedResultsController<Diver> {
        return dataModel.fetchedResultsControllerForDivePros()
    }
    func frcForInstructors ()-> NSFetchedResultsController<Diver> {
        return dataModel.fetchedResultsControllerForInstructors()
    }
    func frcForClients ()-> NSFetchedResultsController<Diver> {
        return dataModel.fetchedResultsControllerForClientDivers()
    }
    func frcForAllDivers ()-> NSFetchedResultsController<Diver> {
        return dataModel.fetchedResultsControllerForAllDivers()
    }
    func frcForStudents ()-> NSFetchedResultsController<Diver>{
        return dataModel.fetchedResultsControllerForStudents()
    }
    func frcForGuidedDivers()-> NSFetchedResultsController<Diver>{
        return dataModel.fetchedResultsControllerForGuidedDivers()
    }
    
    //MARK:- NAme Stuff
    
    func familyName()-> String? {
        return dataModel.theDiver?.familyName
    }
    func setFamilyName(_ theName:String?){
        if let name = theName, !name.isEmpty {
            dataModel.theDiver?.familyName = name
        } else {
            dataModel.theDiver?.familyName = nil
        }
    }
    
    func givenName()-> String? {
        return dataModel.theDiver?.givenName
    }
    func setGivenName(_ theName: String?){
        if let name = theName, !name.isEmpty {
            dataModel.theDiver?.givenName = name
        } else {
            dataModel.theDiver?.givenName = nil
        }
    }
    
    func nickName()-> String? {
        return dataModel.theDiver?.nickName
    }
    func setNickName(_ theName: String?){
        if let name = theName, !name.isEmpty {
            dataModel.theDiver?.nickName = name
        } else {
            dataModel.theDiver?.nickName = nil
        }
    }
    func displayName()-> String? {
        if let nickName = dataModel.theDiver?.nickName, nickName.isEmpty != true, UserDefaults.useNickNameAsDisplayName() == true{
            return nickName
        }else {
            return (StringWranglers.nameFor(dataModel.theDiver!))
        }
    }
    
    //MARK:- Color Labels for Divers
    func buddyLabelColour ()-> UIColor {
        let status = Int((dataModel.theDiver?.buddyStatus)!)
        return status > 1 ? .black: .lightGray
    }
    func instructorLabelColour ()-> UIColor {
        guard let proType = dataModel.theDiver?.proStatus, (4...16).contains(proType) else { return .lightGray}
        return .black
    }
    func guideLabelColour ()-> UIColor {
        guard let proType = dataModel.theDiver?.proStatus, Int(proType) & ProStatus.guide.rawValue == ProStatus.guide.rawValue  else { return .lightGray}
        return .black
    }
    func studentLabelColour()-> UIColor {
        let status = Int((dataModel.theDiver?.clientStatus)!)
        return status & ClientStatus.student.rawValue == ClientStatus.student.rawValue ? .black : .lightGray
    }
    func guidedDiverLabelColour()-> UIColor {
        let status = Int((dataModel.theDiver?.clientStatus)!)
        return status & ClientStatus.guidedDiver.rawValue == ClientStatus.guidedDiver.rawValue ? .black : .lightGray
    }
    //MARK:- Instructoror and Buddy Stuff
    func typeOfBuddy()-> String {
        let status = Int((dataModel.diver?.buddyStatus)!)
        switch (status) {
        case BuddyStatus.notBuddy.rawValue:
            return "Buddy"
        case BuddyStatus.defaultBuddy.rawValue:
            return buddyStatusName[1]
        case BuddyStatus.regular.rawValue:
            return buddyStatusName[2]
        case BuddyStatus.oneOff.rawValue:
            return buddyStatusName[3]
        default :
            return ""
        }
    }
    func buddyStatusType(row:Int)-> String {
        return buddyStatusName[row]
    }
    func buddyStatusAccessoryTypeFor(row:Int)-> UITableViewCell.AccessoryType{
        let statusValue = (dataModel.diver?.buddyStatus)!
        return compareRow(row, withValue: statusValue)
    }
    func compareRow(_ row:Int, withValue value: Int16) -> UITableViewCell.AccessoryType{
        let compareValue = Int16(pow(2, Double(row)))
        return  (value & compareValue == compareValue)  ?  .checkmark : .none
    }
    func proStatusType(row:Int)-> String{
        return proStatusName[row]
    }
    func proStatusAccessoryTypeFor(row:Int)-> UITableViewCell.AccessoryType{
        let statusValue = (dataModel.diver?.proStatus)!
        return compareRow(row, withValue: statusValue)
    }
    
    func setBuddyStatusWith(_ row: Int){
        if row == 1 && dataModel.diver?.buddyStatus != 2/* tapped default and not the default */ {
            dataModel.removeExistingDefaultBuddy()
        }
        dataModel.diver?.buddyStatus = Int16(pow(2, Double(row)))
    }
    func setProStatusWith(_ row: Int){
        if row == 0  {
            dataModel.diver?.proStatus = 1 // not a pro
        } else {
            let compareValue = Int(pow(2, Double(row)))
            var status = Int((dataModel.diver?.proStatus)!)
            newValueFor(&status, compareValue)
            dataModel.diver?.proStatus = Int16(status)
        }
        
    }
    
    fileprivate func newValueFor(_ status: inout Int, _ compareValue: Int) {
        if status & compareValue == 0 {
            status = status | compareValue
            if status & notOfThisType == notOfThisType {
                status = status ^ notOfThisType
            }
        } else {
            status = status ^ compareValue // turn it off
            if status == 0 { status = notOfThisType }
        }
    }
    
    func setClientStatusWith(_ row: Int){
        if row == 0  {
            dataModel.diver?.clientStatus = 1 // not a pro
        } else {
            let compareValue = Int(pow(2, Double(row)))
            var status = Int((dataModel.diver?.clientStatus)!)
            newValueFor(&status, compareValue)
            dataModel.diver?.clientStatus = Int16(status)
        }
        
    }
    
    func clientStatusType(row:Int)-> String{
        return clientStatusName[row]
    }
    
    func clientStatusAccessoryTypeFor(row:Int)-> UITableViewCell.AccessoryType{
        let statusValue = (dataModel.diver?.clientStatus)!
        return compareRow(row, withValue: statusValue)}
    
    func diverStatusHeaderStringFor(_ section: Int)-> String {
        return diverStatusHeader[section]
    }
    
    var numberOfBuddyOptions: Int  {
        return buddyStatusName.count
    }
    var numberOfProOptions: Int  {
        return proStatusName.count
    }
    var numberOfClientOptions: Int {
        return clientStatusName.count
    }
}
