//
//  ConditionData.swift
//  Splash
//
//  Created by Simon Gardener on 13/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation


class ConditionData {
    static let conditionLabels = ["Water temp","Air temp","Season","Weather","Visibility","Current","Salinity","Surface"]
    static let weather = ["--","Sunny","Hazy","Cloudy-partially","Cloudy","Cloudy-dark","Foggy","Rain-light","Rain-heavy","Rain-tropical storm","Thunderstorm","Hail","Snow"]
    static let current = ["--", "None","Very mild","Mild","Moderate","Strong","Hell's teeth!"]
    static let salinity = ["--","Salt","Fresh","Brackish","Mixed Layers"]
    static let surface = ["--","Like glass","Calm","Small swell","Medium swell","Large swell","Choppy","Breaking waves"]
    static let season = ["--","Summer","Spring","Autumn","Winter","Dry season","Rainy Season"]
    static let visibility = ["--","Terrible","Poor","Okay","Good","Excellent"]
}

