//
//  ProfileViewModel.swift
//  Splash
//
//  Created by Simon Gardener on 11/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation

class ProfileViewModel {
    
    init(with dm: DataModel){
        dataModel = dm
    }
    var dataModel: DataModel
    
    var attributeStrings = ["Time in:","Dive time:","Max depth:","Bottom time:","Safety stop:","Surface Interval:","Used tables:","Multi level profile:","Deco dive:"]
    
    var usesTables :Bool {
        get {if dataModel.logEntry?.logWithTables == true { return true } else { return false }}
        set {dataModel.logEntry?.logWithTables = newValue}
    }
    var isMultiLevel :Bool {
        get{if dataModel.logEntry?.logAsMultilevel == true {
            return true } else { return false}}
        set{
            dataModel.logEntry?.logAsMultilevel = newValue}
    }
    var isDecoDive : Bool {
        get{if dataModel.logEntry?.logAsDecoDive == true { return true } else { return false }}
        set{ dataModel.logEntry?.logAsDecoDive = newValue}
    }
    /// returns a dive duration as a TimeInterval (seconds)
    private var diveLength : TimeInterval {
        guard let diveTime = dataModel.logEntry?.duration else { return 0 }
        return  TimeInterval( diveTime * 60 )
    }
    
    func surfaceInterval ()-> String{
        guard let si = dataModel.logEntry?.surfaceInterval, si != 0 else {return "--"}
        return "\(si.durationString())"
    }
    func timeIn ()-> String {
        guard let time =  dataModel.logEntry?.timeIn else { return "--:--"}
        return "\(Date.formattedTime(time, timeZone: (dataModel.logEntry?.timeZone)!))"
    }
    func timeOut ()-> String {
        guard let time = dataModel.logEntry?.timeOut else { return "--:--" }
        return "\(Date.formattedTime(time, timeZone: (dataModel.logEntry?.timeZone)!))"
    }
    func maxDepth(withUnits: Bool = true)-> String {
        guard let maxDepth = dataModel.logEntry?.depthMax, maxDepth != 0 else { return "---" }
        
        return maxDepth.depthInUserPreference(withUnits: withUnits)
    }
    func safetyStop()-> String {
        var ssTime = "--"
        var ssDepth = "--"
        
        if let sst = dataModel.logEntry?.safetyStopTime, sst != 0{
            ssTime = String(sst)
        }
        if let ssd = dataModel.logEntry?.safetyStopDepth, ssd != 0{
            ssDepth = ssd.depthInUserPreference()
        }
        return "\(ssTime)@\(ssDepth)"
        
    }
    func totalDiveTime()-> String {
        guard let duration = dataModel.logEntry?.duration, duration != 0 else { return "-:--"}
        return "\(duration.durationString())"
    }
    func bottomTime()-> String {
        guard let bt = dataModel.logEntry?.bottomTime , bt != 0 else { return  "-:--"}
        return bt.durationString()
    }
    func bottomTimeShort()-> String{
        guard let bt = dataModel.logEntry?.bottomTime , bt != 0 else { return  "--"}
        return "\(bt)"
    }
    func startPressure()-> String? {
        guard dataModel.logEntry?.logWithTables == true else {return nil}
        //  let pg = startPG()
        return startPG()
        //       return "spg:\(pg)"
    }
    func startPressureLabel()-> String?{
        guard dataModel.logEntry?.logWithTables == true else { return nil}
        return "SPG"
    }
    func endPressure()-> String? {
        guard dataModel.logEntry?.logWithTables == true else {return nil}
        return endPG()
    }
    func endPressureLabel ()-> String? {
        guard dataModel.logEntry?.logWithTables == true else { return nil }
        return "EPG"
    }
    func endPGHeaderLabel()-> String? {
        guard dataModel.logEntry?.logWithTables == true else { return nil }
        return "EPG"
    }
    func firstIntermediatePressure()-> String? {
        guard dataModel.logEntry?.logWithTables == true else {return nil}
        guard let pg = dataModel.logEntry?.pressureGroupFirstLevel else { return "-" }
        return pg
    }
    func secondIntermediatePressure()-> String? {
        guard dataModel.logEntry?.logWithTables == true else {return nil}
        guard let pg = dataModel.logEntry?.pressureGroupSecondLevel else { return "-" }
        return pg
    }
    
    func secondTime()-> String {
        guard let time = dataModel.logEntry?.bottomTimeSecondLevel, time != 0 else { return "-:--"}
        return time.durationString()
    }
    func secondTimeShort()-> String {
        guard let time = dataModel.logEntry?.bottomTimeSecondLevel, time != 0 else { return "--"}
        return "\(time)"
    }
    func firstDepth(withUnits units: Bool = true)-> String {
        guard let depth = dataModel.logEntry?.depthMax, depth != 0 else  {
            return "---"
        }
        return depth.depthInUserPreference(withUnits: units)
    }
    func secondDepth(withUnits units: Bool = true)-> String {
        guard let depth = dataModel.logEntry?.depthSecondLevel, depth != 0 else { return "---" }
       
        return  depth.depthInUserPreference(withUnits: units)
    }
    func thirdDepth(withUnits units: Bool = true)-> String {
        guard let depth = dataModel.logEntry?.depthThirdLevel, depth != 0 else { return "---" }
       
    return  depth.depthInUserPreference(withUnits: units)
    }
    
    func thirdTime()-> String { 
        guard let time = dataModel.logEntry?.bottomTimeThirdLevel, time != 0 else { return "-:--"}
        return time.durationString()
    }
    func thirdTimeShort()-> String {
        guard let time = dataModel.logEntry?.bottomTimeThirdLevel, time != 0 else { return "-:--"}
        return "\(time)"
    }

    
    //MARK: - ProfileData Methods
    
    func numberOfSections()-> Int{
        return OptionOrder.decoStops.rawValue + 1
    }
    func numberOfRowsIn(_ section:Int) -> Int{
        let sectionName = optionOrderFor(position: section)
        switch  sectionName {
        case .timeIn, .diveTime, .maxDepth, .bottomTime, .safetyStop, .surfaceInterval, .decoStops:
            return 1
            
        case .usesDiveTables :
            if (dataModel.logEntry?.logWithTables)! == true{
                return 2
            } else {
                return 1
            }
            
        case .multiLevelDive:
            if dataModel.logEntry?.logAsMultilevel    == true {return 4} else {return 1}
        }
    }
    
    func attributeLabelForSection(section:Int) -> String{
        return attributeStrings[section]
    }
    func startPG()-> String {
        return dataModel.logEntry?.pressureGroupIn ?? "-"
    }
    func endPG()-> String {
        return dataModel.logEntry?.pressureGroupOut ?? "-"
    }
    func firstIntermediatePG()-> String {
        return dataModel.logEntry?.pressureGroupFirstLevel ?? "-"
    }
    func secondIntermediatePG ()-> String{
        return dataModel.logEntry?.pressureGroupSecondLevel ?? "-"
    }
    //
    //    func depth2nd()-> String {
    //        guard let d2 = Int(dataModel.logEntry?.depthSecondLevel) else { return}
    //        return dataModel.logEntry?.depthSecondLevel ?? "--" }
    //    func depth3rd()-> String {
    //        return dataModel.logEntry?.depthSecondLevel ?? "--"
    //    }
    
    //MARK:- Quick Access
    func timeZone()-> TimeZone{
        let tzName = (dataModel.logEntry?.timeZone)!
        return TimeZone(identifier: tzName)!
    }
    func timeIn()-> Date {
        return (dataModel.logEntry?.timeIn!)!
    }
    
    //MARK:- Updaters
    func userUpdated(date: Date) {
        dataModel.logEntry?.timeIn = date
    }
    func updateTimeOut() {
        
        let duration = diveLength
        if duration > 0 {
            dataModel.logEntry?.timeOut = dataModel.logEntry?.timeIn?.addingTimeInterval(duration)
        }
    }
}

extension ProfileViewModel  : OptionOrder{
    
    enum OptionOrder : Int {
        case timeIn
        // case timeOut
        case diveTime
        case maxDepth
        case bottomTime
        case safetyStop
        case surfaceInterval
        case usesDiveTables
        case multiLevelDive
        case decoStops
    }
}
