//
//  EmergencyContactViewModel.swift
//  Splash
//
//  Created by Simon Gardener on 03/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.

import UIKit
import CoreData
import Contacts

class EmergencyContactViewModel :DiverViewModel {
    
    var owner: LBOwner?
    var currentContact: EmergencyContact!
    
    var contactOrderStrings = [ "Primary Contact", "Secondary Contact","Contact of last resort","contact order undeclared"];
    
    init(with dm: DataModel, lbo:LBOwner, mode: Mode){
        super.init(with: dm)
        self.mode = mode
        owner = lbo
        dataModel.owner = owner
        
    }
    
    func frcForEmergencyContacts()-> NSFetchedResultsController<EmergencyContact>{
        return dataModel.frcForEmergencyContact()
    }
    
    func hasEmergencyContacts()-> Bool{
        if dataModel.numberOfEmergencyContacts() > 0 {
            return true
        }else{
            return false
        }
    }
    
    func newEmergencyContact(){
        currentContact = dataModel.newEmergencyContact()
   
       //
        dataModel.emergencyContact = currentContact
        
    }
    func addSelected(_ contact: CNContact){
        newEmergencyContact()
        contact.copyDetails(to: currentContact)
    }
    func displayName(for emergencyContact: EmergencyContact)-> String {
        return StringWranglers.nameFor(emergencyContact)
    }
    func relationship(for emergencyContact: EmergencyContact ) -> String {
        return emergencyContact.relationshipTo!
    }
    func relationshipString()-> String{
        return currentContact.relationshipTo!
    }
    func orderString()-> String{
        return contactOrderStrings[Int(currentContact.contactOrder)]
    }
    func order(for emergencyContact: EmergencyContact)-> String {
        return contactOrderStrings[Int(emergencyContact.contactOrder)]
    }
    func contactAndRelationship(for emergencyContact: EmergencyContact)-> String {
        return "\(order(for: emergencyContact)), \(relationship(for:emergencyContact))"
    }
    func set(emergencyContact: EmergencyContact){
        currentContact = emergencyContact
        dataModel.emergencyContact = emergencyContact
    }
    func emergencyContactNote()-> String? {
        return currentContact.note
    }
    func storeNote(_ textView: UITextView){
        currentContact.note = textView.text
    }
    func emergencyContactRelationship()-> String?{
        return currentContact.relationshipTo
    }
    func setEmergencyContact(relationship: String){
        currentContact.relationshipTo = relationship
    }
    func setEmergencyContact(contactOrder: Int){
        currentContact.contactOrder = Int16(contactOrder)
    }
    func cancelNewContact(){
        dataModel.container.viewContext.delete(currentContact)
    }
    //MARK:- Name Stuff
    
//   override func familyName()-> String? {
//        return dataModel.emergencyContact?.familyName
//    }
//    override func setFamilyName(_ name:String?){
//        dataModel.emergencyContact?.familyName = name
//    }
//
//    override func givenName()-> String? {
//        return dataModel.emergencyContact?.givenName
//    }
//    override func setGivenName(_ name: String?){
//        dataModel.emergencyContact?.givenName = name
//    }
//
//    override func nickName()-> String? {
//        return dataModel.emergencyContact?.nickName
//    }
//    override func setNickName(_ name: String?){
//        dataModel.emergencyContact?.nickName = name
//    }
//    override func displayName()-> String? {
//        if let nickName = dataModel.emergencyContact?.nickName, nickName.isEmpty != true, UserDefaults.useNickNameAsDisplayName() == true{
//            return nickName
//        }else {
//            return (StringWranglers.nameFor(dataModel.diver!))
//        }
//    }
//

  override func diverImage()-> UIImage? {
        guard let photoData = dataModel.emergencyContact?.photo else { return nil }
        return UIImage(data: photoData)
    }
 override func set(diverImage: UIImage?){
        guard let image = diverImage, let data = image.pngData() else {
            return}
        dataModel.emergencyContact?.photo = data
    }
}

