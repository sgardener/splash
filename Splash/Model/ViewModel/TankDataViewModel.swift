//
//  TankDataViewModel.swift
//  Splash
//
//  Created by Simon Gardener on 07/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation


class TankDataViewModel {
    var tank: Tank
    //var attribute = ["Start pressure:","End pressure:"," Tank size:", "Material:", "Supply", "GasType", "0₂", "He"]
    var gasType = ["Air","Nitrox", "Trimix", "Heliox", "0₂", "He"]
    var materials = [["Aluminium","Steel","Carbon"],["Al","Stl","C"]]
    var numOfTanks = ["Single", "Manifolded-Twins","Semi-closed", "Closed-Circuit"]
    var gasSupply = ["Open circuit","Closed circuit", "Semi-closed"]
    
    init (tank:Tank){
        self.tank = tank
    }
    func pressureIn (_ withUnits:Bool = true) -> String {
        
        let pressureUnit = tank.pressureIsInBar ? "bar" : "psi"
        if withUnits {
            return "\(tank.startPressure) \(pressureUnit)"
        }else {
            return "\(tank.startPressure)"
        }
    }
    
    func pressureOut (_ withUnits:Bool = true) -> String {
        
        let pressureUnit = tank.pressureIsInBar ? "bar" : "psi"
        if withUnits {
            return "\(tank.endPressure) \(pressureUnit)"
        }else {
            return "\(tank.endPressure)"
        }
    }
    
    func sizeMaterial(_ fullText: Bool = true)->String{
        
        var abreviationIndex = 0
        if !fullText { abreviationIndex = 1}
        
        
        let volumeUnit = tank.volumeIsinLiters == true ? "ltr" : "ft³"
        var config = ""
        if tank.manifoldedtwins == true {
            config = "Twins, "
        }else if Int(tank.supplyType) == GasSupplyType.closedCircuit.rawValue {
            config = "CCR, "
        }else if Int(tank.supplyType) == GasSupplyType.semiClosed.rawValue{
            config = "SCR, "
        }
        let material  = materials[abreviationIndex][Int(tank.tankMaterial)]
        let volume = String(format:"%.1f",tank.volume)
        return "\(config)\(material) \(volume) \(volumeUnit) "
    }
    
    func gasPercentage()->String{
        let gas = gasType[Int(tank.gasType)]
        var percentString = ""
        switch Int(tank.gasType){
        case GasType.nitrox.rawValue :
            percentString = ", O₂: \(tank.oxygenPercent)%"
        case GasType.trimix.rawValue, GasType.heliox.rawValue:
            percentString = ", O₂: \(tank.oxygenPercent)%, He: \(tank.heliumPercent)%"
        default: break
        }
        return "\(gas)\(percentString)"
        
    }
 
//    func attributeForSection(_ section:Int) -> String{
//        return attribute[section]
//        
//    }
    func data()-> String{
        return "HELP"
    }
}


enum GasSupplyType: Int {
    case openCircuit
    case closedCircuit
    case semiClosed
}
enum GasType: Int {
    case air, nitrox, trimix, heliox, O2, He
}
enum TankMaterials: Int {
    case aluminium, steel, carbon
}
enum SingleOrTwin: Int {
    case single, manifolded
}
