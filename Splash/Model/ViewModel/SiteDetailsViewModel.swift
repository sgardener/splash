//
//  SiteDetailsViewModel.swift
//  Splash
//
//  Created by Simon Gardener on 06/03/2019.
//  Copyright © 2019 Simon Gardener. All rights reserved.
//

import Foundation
import CoreData

class SiteDetailsViewModel {
    
    let maximumDepthString = "maximumDepth"
    let minimumDepthString = "minimumDepth"
    let typicalMaximumDepthString = "avDepthRangeDeeper"
    let typicalMinimumDepthString = "avDepthRangeShallow"
    var site: DiveSite!
    
    let currentStrength = ["N/A","Mild","Mild to Steady","Mild to Strong","Mild to Insane","Steady","Steady to Strong", "Steady to insane","Strong","Strong to insane","Hell's Teeth!"]
    let currentFrequency = ["Dont know", "Never","Rarely","Sometimes","no value 4","50/50","novalue 6","Usually","no value 8","Almost Always","Always","error OVERSHOT VALUE"]
    init(with diveSite: DiveSite){
        site = diveSite
    }
    
    func numberOfRowsForAccessible() -> Int {
        guard site.accessBy != 0 else {return 0}
        return 1
    }
    func accessibleBy()-> String{
        let indexes = arrayOfIndexes(from: Int(site.accessBy))
        return listOfThings(in: AccessibleBy.accessByFieldName, at: indexes)
    }
    func numberOfRowsForAlternativeName()-> Int {
        guard site.alternativeName != nil else { return 0}
        return 1
    }
    func alternativeNameString()-> String{
        return site.alternativeName!
    }
    func locationAsString()-> String {
        var locationElements = [String?]()
        locationElements.append(site.islandAtoll)
        locationElements.append(site.bodyOfWater)
        locationElements.append(site.localAreaName)
        locationElements.append(site.cityTown)
        locationElements.append(site.stateProvince)
        
        let location = locationElements.compactMap{$0}.joined(separator: ", ")
        return location.isEmpty ? "no location description":location
    }
    
    func hasDepthRange()->Bool{
        guard  site.maximumDepth == 121 else { return true
        }
        return false
    }
    func hasTypicalDepthRange()-> Bool{
        guard site.avDepthRangeDeeper == 121 else { return true }
        return false
    }
    fileprivate func depthRangeString(forMaximumKey maxKey : String, andMinimumKey minKey : String)-> String{
        let maxDepth = site.value(forKey: maxKey) as! Double
        let toString = maxDepth > 100.0 ? "to" : "-"
        let max = SiteDepth.stringFor(site.value(forKey: maxKey) as! Double)
        if site.value(forKey: maxKey) as! Double != -1 {
            let min = SiteDepth.stringFor(site.value(forKey: minKey) as! Double)
            return "\(min) \(toString) \(max)"
        }else{
            return "Maximum : \(max)"
        }
    }
    func depthRangeString()-> String {
        return depthRangeString(forMaximumKey: maximumDepthString, andMinimumKey: minimumDepthString)
    }
    func typicalDepthRangeString()-> String {
        return depthRangeString(forMaximumKey: typicalMaximumDepthString, andMinimumKey: typicalMinimumDepthString)
    }
    func numberofRowsForBottomComposition()-> Int {
        if site.bottomComposition == 0 { return 0 }
        return 1
    }

    func bottomComposition()->String {
        let value = Int(site.bottomComposition)
        return "\(listOfThings(in: Environmentals.bottomType, at: arrayOfIndexes(from: value)))"
    }
    func numberOfRowsForSuitable()-> Int {
        if site.suitableFor == 0 { return 0 }
        return 1
    }
    func suitableFor()-> String {
        let value = Int(site.suitableFor)
        return "\(listOfThings(in: Environmentals.suitableForType, at: arrayOfIndexes(from: value)))"
    }
    func numberIfRowsForSalinity()-> Int{
        if site.salinity == 0 {return 0}
        return 1
    }
    func salinity()->String {
        let index = Int(log2(Double(site.salinity)))
        return "\(Environmentals.salinityType[index])"
    }
    func numberOfRowsForSiteEnvironment()-> Int{
        if site.environment > 0 { return 1 }
        return 0
    }
    func siteEnvironmentString()->String {
        let index = Int(log2(Double(site.environment)))
        return "\(Environmentals.environmentIsTypes[index])"
    }
    func numberofRowsForSiteLocated()-> Int {
        if site.whereSiteLocated > 0 { return 1 }
        return 0
    }
    func siteLocatedString()->String{
        let index = Int(log2(Double(site.whereSiteLocated)))
        return "\(Environmentals.whereType[index])"
    }
    func numberOfRowsForSiteFeatures()-> Int{
        if site.features > 0 {  return 1 }
        return 0
    }
    func siteFeaturesString()->String{
        let featureValue = Int(site.features)
        return "\(listOfThings(in: Environmentals.featureTypes, at: arrayOfIndexes(from: featureValue)))"
    }
   
    func numberOfRowsForTemperature()-> Int{
        if (site.temperatureRangeLow != -3) || (site.temperatureRangeHigh != 41) { return 1 }
        return 0
    }
    func temperatureString()-> String{
        if site.temperatureRangeLow == -3 {
            return "up to \(site.temperatureRangeHigh.temperatureAsIntInUserPreference())"
        } else if site.temperatureRangeHigh == 41 {
            return "as low as \(site.temperatureRangeLow.temperatureAsIntInUserPreference())"
        }else {
            return "\(site.temperatureRangeLow.temperatureAsIntInUserPreference()) - \(site.temperatureRangeHigh.temperatureAsIntInUserPreference())"
        }
    }
    func numberOfRowsForHighSeasonVisibility()-> Int {
        guard site.highSeasonVisibilityRangeLow != -1, site.highSeasonVisibilityRangeHigh < 51 else { return 0 }
        return 1
    }
    func highSeasonVisibilityString()-> String {
        if site.highSeasonVisibilityRangeLow == -1 {
            return "up to \(SiteDepth.stringFor(site.highSeasonVisibilityRangeHigh))"
        }else if site.highSeasonVisibilityRangeHigh == 51 {
            return "as low as \(SiteDepth.stringFor(site.highSeasonVisibilityRangeLow))"
        }else {
            return "\(SiteDepth.stringFor(site.highSeasonVisibilityRangeLow)) - \(SiteDepth.stringFor(site.highSeasonVisibilityRangeHigh)) "
        }
    }
    func numberOfRowsForLowSeasonVisability()-> Int {
        guard site.lowSeasonVisibilityRangeLow != -1, site.lowSeasonVisibilityRangeHigh < 51 else { return 0 }
        return 1
    }
    func lowSeasonVisibility()-> String{
        
        if site.lowSeasonVisibilityRangeLow == -1 {
            return "up to \(SiteDepth.stringFor(site.lowSeasonVisibilityRangeLow))"
        }else if site.lowSeasonVisibilityRangeHigh == 51 {
            return "as low as \(SiteDepth.stringFor(site.lowSeasonVisibilityRangeLow))"
        }else {
            return "\(SiteDepth.stringFor(site.lowSeasonVisibilityRangeLow)) - \(SiteDepth.stringFor(site.lowSeasonVisibilityRangeHigh))"
        }
    }
    
    func numberOfRowsForCurrent()-> Int {
        guard site.currentFrequency != 0 || site.currentStrength != 0 else { return 0}
        return 1
    }
    func currentString()->String{
        if site.currentStrength == 0 || site.currentFrequency == 0.1 {
            return "occurs: \(currentFrequency[Int(site.currentFrequency * 10)])"
            
        }else if site.currentFrequency == 0 {
            return "strength: \(currentStrength[Int(site.currentStrength * 10)])"
        }else {
            return "occurs: \(currentFrequency[Int(site.currentFrequency * 10)])\nstrength: \(currentStrength[Int(site.currentStrength * 10)])"
        }
    }
    
    //MARK:- tools
    /// Generates a list of items from an array of items and an array of indexes
    ///
    /// - Parameters:
    ///   - array: an array of StringValues
    ///   - Indexes: an array of Indexes
    /// - Returns: an String list of the objects in the array at the indexes
    func listOfThings(in array :[String], at Indexes:[Int])-> String{
        var thingsArray = [String]()
        for index in Indexes {
            thingsArray.append(array[index])
        }
        return thingsArray.joined(separator: ", ")
    }
    
    /// Calculates and returns index values from a given number's bit values
    ///
    /// - Parameter number: an Int - option
    /// - Returns: an array of Integers to be used as indexes
    func arrayOfIndexes(from number: Int)->[Int]{
        var arrayOfIndex = [Int]()
        var value = 1
        var position = 0
        while value <= number{
            let comparisonValue = Int(pow(2, Double(position)))
            if comparisonValue & number == comparisonValue {
                arrayOfIndex.append(position)
            }
            position += 1
            value *= 2
        }
        return arrayOfIndex
    }
}
