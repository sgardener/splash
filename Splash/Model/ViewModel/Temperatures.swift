//
//  Temperatures.swift
//  Splash
//
//  Created by Simon Gardener on 13/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation

class Temperature {
    static func stringFor(temperature : NSNumber?)-> String {
        guard let temperature = temperature as? Int16 else {
            return "--"
        }
        if UserDefaults.temperatureUnits() == .metric {
            return metricTemperatureFor(temperatureInCentigrade: temperature)
        }
        else {
            return imperialTemperatureFor(temperatureInCentigrade:temperature)
        }
    }
    
    static private func metricTemperatureFor(temperatureInCentigrade :Int16) -> String {
        if temperatureInCentigrade == 200 {
            return "--°C"
        } else {
            return "\(temperatureInCentigrade)°C"
        }
    }
    static private func imperialTemperatureFor(temperatureInCentigrade : Int16) -> String {
        if temperatureInCentigrade == 200 {
            return "--°F"
        } else {
            let inFarenheit = Int(9.0 / 5.0 * Double(temperatureInCentigrade) + 32.0)
            return "\(inFarenheit)°F"
        }
    }
}
