//
//  DiveTimeClassifier.swift
//  Splash
//
//  Created by Simon Gardener on 05/06/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
import Solar
import CoreLocation
enum DiveTimeClassification {
    case unknown, day, night, sunset, sunrise, earlyMorning , lateAfternoon
}
struct  DiveTimeClassifier {

    func classify(_ logEntry:LogEntry)-> DiveTimeClassification{
        
        guard let timeIn = logEntry.timeIn, let timeOut = logEntry.timeOut else {
            //print("Didnt get both time in and time out")
            return .unknown }
        
        guard let coordinate = getLocation(for: logEntry) else { return .unknown }
        
        let startSolar = Solar.init(for: timeIn, coordinate: coordinate)
        let endSolar = Solar.init(for: timeOut, coordinate: coordinate)
        
        if startSolar?.isNighttime == true && endSolar?.isNighttime == true {
            return .night
        }
        if startSolar?.isDaytime == true && endSolar?.isNighttime == true {
            return .sunset
        }
        if startSolar?.isNighttime == true && endSolar?.isDaytime == true {
            return .sunrise
        }
        if startSolar?.isDaytime == true && endSolar?.isDaytime == true {
            return .day
        }
        return .unknown
    }
    
    

    fileprivate func getLocation(for logEntry:LogEntry)-> CLLocationCoordinate2D? {
        if let location = logEntry.diveSite?.location?.coordinate {
            return location
        }
        if let location = logEntry.diveSite?.generalArea?.coordinate {
            return location
        }
        return nil
    }
    
  
}
