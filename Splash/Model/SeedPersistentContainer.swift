//
//  SeedPersistentContainer.swift
//  Splash
//
//  Created by Simon Gardener on 23/08/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
import CoreData
class SeedPersistentContainer : NSPersistentContainer {
    override class func defaultDirectoryURL() -> URL {
        return FileManager.default.temporaryDirectory
    }
}
