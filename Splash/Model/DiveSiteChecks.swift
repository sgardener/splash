//
//  DiveSiteChecks.swift
//  Splash
//
//  Created by Simon Gardener on 20/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation

extension DiveSite{
    
     func isNamed()->Bool{
        guard let theName = name, theName.isEmpty == false else {return false}
        return true
    }
    
     func hasCountry()->Bool {
        guard let theCountry = country, theCountry.isEmpty == false else {return false}
        return true
    }
    func hasLocation()->Bool {
        guard let _ = longitude else  {return false}
        return true
    }
    func hasGPSIs()->Bool{
        if gpsFix == 0 {return false}
        return true
    }
}
