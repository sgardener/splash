//
//  SeedModel.swift
//  Splash
//
//  Created by Simon Gardener on 10/07/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
import CoreData

class SeedModel{
    private var isReadOnly = false
    private var shouldAddStoreAsynchronously = true
    private var shouldMigrateStoreAutomatically = true
    private var shouldInferMappingModelAutomatically = true
    private let newObjectPredicate = NSPredicate(format: "seedAdded > %d", UserDefaults.seedVersion())
    private let updatedObjectPredicate = NSPredicate(format: "seedUpdated > %d AND seedUpdated > seedAdded   ", UserDefaults.seedVersion())
    
    private let SQLFileName = "/SplashDiveLog.sqlite"
    private let SHMFileName = "/SplashDiveLog.sqlite-shm"
    private let WALFileName = "/SplashDiveLog.sqlite-wal"
    private let DBName = "SplashDiveLog"
    private let sqlType = "sqlite"
    private let shmType = "sqlite-shm"
    private let walType = "sqlite-wal"
    
    let defaultDestination = SeedPersistentContainer.defaultDirectoryURL().relativePath
    
    //MARK:-
    lazy var container: SeedPersistentContainer = {
        let persistentContainer = SeedPersistentContainer(name: "SplashDiveLog")
        let fm = FileManager.default
        
        let sqlitePath = Bundle.main.path(forResource: "SplashDiveLog", ofType: "sqlite")
        
        guard FileManager.default.fileExists(atPath: sqlitePath!) else {
            fatalError("There Should always be a bundled DB, if there isn't , I shouldnt ship it :)")
        }
        guard let storeUrl = URL.init(string: sqlitePath!) else {
            fatalError("should always be able to get a URL from string")
        }
        
        moveDataBaseForUpdate()
        let storeDesc = storeDescription(with: storeUrl)
        persistentContainer.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error loading persistent store \(error), \(error.userInfo)")
            }
        })
        return persistentContainer
    }()
    
    private func storeDescription(with url: URL) -> NSPersistentStoreDescription {
        let description = NSPersistentStoreDescription(url: url)
        description.shouldMigrateStoreAutomatically = shouldMigrateStoreAutomatically
        description.shouldInferMappingModelAutomatically = shouldInferMappingModelAutomatically
        description.shouldAddStoreAsynchronously = shouldAddStoreAsynchronously
        description.isReadOnly = isReadOnly
        return description
    }
    
    func moveDataBaseForUpdate() {
        
        guard FileManager.default.fileExists(atPath: fileSourcePath(forType: sqlType)), FileManager.default.fileExists(atPath: fileSourcePath(forType: shmType)), FileManager.default.fileExists(atPath: fileSourcePath(forType: walType)) else {
            fatalError("There Should always be a bundled DB, if there isn't , i shouldnt ship it :)")
        }
        //let fileDestination = fileDestinationPath(forType: sqlType)
        
        do {
            try FileManager.default.copyItem(at: sourceURL(forType: sqlType) , to: destinationURL(for: SQLFileName))
            try FileManager.default.copyItem(at: sourceURL(forType: shmType), to: destinationURL(for: SHMFileName))
            try FileManager.default.copyItem(at: sourceURL(forType: walType), to: destinationURL(for: WALFileName))
            
        } catch {
            print("=========\n UPDATE sqllite,shm and wal files COPY DIDNT WORK.CLean out TMP before running again")
            print(error.localizedDescription)
            fatalError()
        }
    }
    
    
    func removeTemporaryFiles(){
        for store in container.viewContext.persistentStoreCoordinator!.persistentStores {
            try! container.viewContext.persistentStoreCoordinator!.remove(store)
        }
        do {
            try FileManager.default.removeItem(atPath: fileDestinationPath(forType: sqlType))
            try FileManager.default.removeItem(atPath: fileDestinationPath(forType: walType))
            try FileManager.default.removeItem(atPath: fileDestinationPath(forType: shmType))
        }catch{
            print(error.localizedDescription)
        }
    }
    
    private func destinationURL(for file:String) -> URL{
        return URL(fileURLWithPath: defaultDestination + file)
    }
    
    private func fileDestinationPath(forType type: String)-> String {
        let path = defaultDestination + "/\(DBName).\(type)"
        return path
    }
    private func fileSourcePath(forType type : String)-> String {
        guard let path = Bundle.main.path(forResource: DBName, ofType: type) else {
            fatalError ("Should have been a resource for database file  of type \(type)")
        }
        return path
    }
    private func sourceURL(forType type: String)-> URL {
        return URL(fileURLWithPath: fileSourcePath(forType: type))
    }
    
    
    //MARK:- Fetches
    func dataVersion()-> Int16 {
        let versionNumber = fetchDataVersion().number
        return versionNumber
    }
    func fetchDataVersion()-> DataVersion {
        let fetchRequest : NSFetchRequest<DataVersion> = DataVersion.fetchRequest()
        do{
            let version = try container.viewContext.fetch(fetchRequest)
            guard version.count == 1, let dataVersion = version.first else { fatalError("expectingn one version number object , got \(version.count)")}
            return (dataVersion)
            
        }catch {
            let nserror = error as NSError
            fatalError("error fetching the dataVersion\(nserror)")
        }
    }
    // MARK: Generic Counts
    fileprivate func printEntityTotals(_ allRecords: Int, _ updatedRecords: Int, _ newRecords: Int) {
        print ("+++++++++++++++++")
        print ("total records= \(allRecords)")
        print ("total updates= \(updatedRecords)")
        print ("total new = \(newRecords)")
        print ("+++++++++++++++++")
    }
    
    /**
     Uses generics to display a count for all, new and updated entities in the seed file.
     Will be adapted used for an updater display
     - Returns: a tuple of newRecordsCount and updatedRecordsCount both Int
     
     */
    func countOfAllNewAndUpdatedEntities()->(newRecordsCount: Int, updatedRecordsCount: Int ){
        
        let managedOArray = [BoatType.self, CertificationType.self, DiveTag.self, CenterType.self, ResortType.self,TrainingAgency.self, GeneralArea.self, DiveSite.self, Facility.self]
        var allRecords = 0
        var updatedRecords = 0
        var newRecords = 0
        
        for etype in  managedOArray {
         //   print("\(etype) all: \(countAll(entity: etype)) new: \(countNew(entity: etype)), updated \(countUpdated(entity: etype))")
            allRecords += countAll(entity: etype)
            newRecords += countNew(entity: etype)
            updatedRecords += countUpdated(entity: etype)
        }
     //   printEntityTotals(allRecords, updatedRecords, newRecords)
        return (newRecords, updatedRecords)
    }
    // MARK: Generic fetch
    
    func countAll<T: NSManagedObject>(entity: T.Type) -> Int {
        return count(entity: entity, predicate: nil)
    }
    
    func countNew<T: NSManagedObject>(entity:T.Type)-> Int {
        return count(entity: entity, predicate: newObjectPredicate)
        
    }
    func countUpdated<T: NSManagedObject>(entity:T.Type)-> Int {
        return count(entity: entity, predicate: updatedObjectPredicate)
    }
    
    /**
     Uses generics to returns a count for an entity type
     - Parameters:
     - entity: a NSManagedObject subclass - not an instance
     */
    fileprivate func count<T:NSManagedObject>(entity: T.Type,predicate: NSPredicate? ) ->Int {
        let entityName = String(describing: entity)
        let request = NSFetchRequest<T>(entityName: entityName)
        request.predicate = predicate
        do {
            return try container.viewContext.count(for: request)
        } catch {
            print(error.localizedDescription)
            return 0
        }
    }
    
    func fetchAll<T:NSManagedObject>(_ entity: T.Type) -> [T]{
        return  fetchEntity(entity, predicate: nil)
    }
    
    func fetchUpdated<T:NSManagedObject>(_ entity: T.Type) ->[T]{
        return fetchEntity(entity, predicate: updatedObjectPredicate)
    }
    
    func fetchNew<T:NSManagedObject>(_ entity: T.Type)->[T]{
        return fetchEntity(entity, predicate: newObjectPredicate)
    }
    
    func fetchThistlegorm()->DiveSite{
        let predicate = NSPredicate(format: "name = %@", "Thistlegorm")
        let x = fetchEntity(DiveSite.self, predicate: predicate)
        let this = x[0]
        this.seedAdded = 5
        return x[0]
    }
    func upodateNutmeg(){
        let predicate = NSPredicate(format: "name contains %@ ", "Nutmeg")
        let c = fetchEntity(Facility.self, predicate: predicate)[0]
        c.seedAdded = 5
    }
    func updateBlueMarlin(){
        let predicate = NSPredicate(format: "name contains %@ ", "Blue Marlin")
        let c = fetchEntity(Facility.self, predicate: predicate)[0]
        c.seedAdded = 5
    }
    fileprivate func fetchEntity<T:NSManagedObject>(_ entity: T.Type, predicate :NSPredicate?) -> [T] {
        let entityName = String(describing: entity)
        let fetchRequest = NSFetchRequest<T>(entityName: entityName)
        fetchRequest.predicate = predicate
        do {
            return try container.viewContext.fetch(fetchRequest)
        } catch{
            print(error.localizedDescription)
            return []
        }
    }
}
