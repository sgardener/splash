//
//  ModelUpdater.swift
//  Splash
//
//  Created by Simon Gardener on 10/07/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
import CoreData

/// copies updated and new database items from the seed to the model
class ModelUpdater{
    var dataModel: DataModel
    var seedModel = SeedModel()
    
    let simpleManagedObjectTypes = [BoatType.self, CertificationType.self, DiveTag.self, CenterType.self, ResortType.self,TrainingAgency.self, GeneralArea.self ]
    let complexManagedObjectTypes = [DiveSite.self, Facility.self]
    init(_ dataModel: DataModel) {
        self.dataModel = dataModel
    }
    
    class func updateIsNeeded()-> Bool {
        if seedVersion > UserDefaults.seedVersion() {
            return true
        } else {
            return false
        }
    }
    
    func update(){
      //  dataModel.numberOfSites()
        logDataVersion()
       // print("this many updates to perform \( seedModel.countOfAllNewAndUpdatedEntities())")
        updateAndAddSimplerObjectTypes()
        updateAndAddComplexObjectTypes()
        dataModel.setDataVersionTo(Int16(seedVersion))
        logDataVersion()
        UserDefaults.setSeedVersion(seedVersion)
        seedModel.removeTemporaryFiles()
        dataModel.saveContext()
    }
    
    fileprivate func changeThistlegormTo5(){
        
    }
    fileprivate func updateAndAddComplexObjectTypes() {
        if objectTypesToAdd([DiveSite.self]).count == 1 {
            addDiveSites()
        }
        if objectTypesToUpdate([DiveSite.self]).count == 1 {
            updateDiveSites()
        }
        if objectTypesToAdd([Facility.self]).count == 1 {
            addFacilities()
        }
        if objectTypesToUpdate([Facility.self]).count == 1 {
            updateFacilities()
        }
        
    }
    fileprivate func addDiveSites(){
        let objects = seedModel.fetchNew(DiveSite.self)
        for object in objects {
            let newObject = newInstanceOf(object) as! DiveSite
            cloneAttritubtes(from: object, to: newObject)
            //now sort out relationsship to general area
            cloneGeneralAreaRelationship(from: object, to: newObject)
        }
    }
    
    
    fileprivate func updateDiveSites(){
        let objects = seedModel.fetchUpdated(DiveSite.self)
       // print("\(objects.count) sites to update")
        for object in objects {
            let objectID = object.value(forKey: "id") as! String
            let predicate = NSPredicate(format: "id == %@", objectID)
            let matchingObjects = dataModel.fetchEntity(DiveSite.self, predicate: predicate)
            guard matchingObjects.count == 1,  let matchingObject = matchingObjects.first else {
                fatalError("problem with matching objects count \(matchingObjects.count) != 1 and matchingObjecst = \(matchingObjects) ")
            }
            //before cloning grab the attributes that the user may have set
            let regular = matchingObject.regularSite
            let favourite = matchingObject.userFavourite
            let confirmedLocation = matchingObject.userAlreadyConfirmedLocation
            let confirmedAccess = matchingObject.userAlreadyConfirmedAccessBy
            let userSuppliedName = matchingObject.userSuppliedName
            let preferName = matchingObject.preferUserSuppliedName
            cloneAttritubtes(from: object, to: matchingObject)
            //replace the users values
            matchingObject.userFavourite = favourite
            matchingObject.regularSite = regular
            matchingObject.userAlreadyConfirmedLocation = confirmedLocation
            matchingObject.userAlreadyConfirmedAccessBy = confirmedAccess
            matchingObject.userSuppliedName = userSuppliedName
            matchingObject.preferUserSuppliedName = preferName
            
            cloneGeneralAreaRelationship(from: object, to: matchingObject)
        }
    }
    fileprivate func cloneGeneralAreaRelationship(from source: DiveSite, to target: DiveSite ){
        // get the seed general area's id
        guard let generalAreaId = source.generalArea?.id else {
            return
        }
        //get the general area from the users database
        let predicate = NSPredicate(format: "id == %d", generalAreaId)
        let generalAreas = dataModel.fetchEntity(GeneralArea.self, predicate: predicate)
        guard generalAreas.count < 2  else { fatalError("expected less general areas. got more than 1)")}
        guard let generalArea = generalAreas.first else {print("No general area for this one in database - if this happens then i have an error in my database I'm supplying - or i didnt add the new general area so i have a logic error in that "); return }
        
        target.generalArea = generalArea
        
    }

    
    fileprivate func cloneFacilityRelationships(from source: Facility, to target: Facility){
        //first which relationships -  trainingAgency, resortType, centerType
        
        let agencyIds = (source.trainingAgency?.allObjects as! [TrainingAgency]).map{$0.id}
        let resortIds = (source.resortType?.allObjects as! [ResortType]).map{$0.id}
        let centerIds = (source.centerType?.allObjects as! [CenterType]).map{$0.id}
        
        for id in agencyIds {
            let predicate = NSPredicate(format:"id = %d",id)
            if let agency = dataModel.fetchEntity(TrainingAgency.self, predicate: predicate).first {
                target.trainingAgency?.adding(agency)
            }
        }
        
        for id in resortIds {
            let predicate = NSPredicate(format:"id = %d",id)
            if let resort = dataModel.fetchEntity(ResortType.self, predicate: predicate).first{
                target.resortType?.adding(resort)
            }
        }
        
        for id in centerIds {
            let predicate = NSPredicate(format:"id = %d",id)
            if let center = dataModel.fetchEntity(CenterType.self, predicate: predicate).first{
                target.centerType?.adding(center)
            }
        }
    }
    fileprivate func addFacilities(){
        let objects = seedModel.fetchNew(Facility.self)
        for object in objects {
            let newObject = newInstanceOf(object) as! Facility
            cloneAttritubtes(from: object, to: newObject)

            cloneFacilityRelationships(from: object, to: newObject)
    
        }
    }
    fileprivate func updateFacilities(){
        let objects = seedModel.fetchUpdated(Facility.self)
        //print("\(objects.count) facilities to update")
        for object in objects {
            let objectID = object.value(forKey: "id") as! String
            let predicate = NSPredicate(format: "id == %@", objectID)
            let matchingObjects = dataModel.fetchEntity(Facility.self, predicate: predicate)
            guard matchingObjects.count == 1,  let matchingObject = matchingObjects.first else {
                fatalError("problem with matching objects count \(matchingObjects.count) != 1 and matchingObjecst = \(matchingObjects) ")
            }
            let regular = matchingObject.isRegularFacility
            cloneAttritubtes(from: object, to: matchingObject)
            matchingObject.isRegularFacility = regular
            cloneFacilityRelationships(from: object, to: matchingObject)
        }
    }
    
    fileprivate func updateAndAddSimplerObjectTypes() {
        //next bit handles updating for simple types
        let objectTypesNeedingUpdate = objectTypesToUpdate(simpleManagedObjectTypes)
        updateObjectTypes(objectTypesNeedingUpdate)
        let objectTypesNeedingAdding = objectTypesToAdd(simpleManagedObjectTypes)
        addObjectTypes(objectTypesNeedingAdding)
    }
    
    
    /// copies objects from seed database to the users Database
    ///
    /// - Parameter types: an Array of NSManagedObject subclass Types that have objects that need adding
    fileprivate func addObjectTypes(_ types: [NSManagedObject.Type]) {
        for type in types {
            let objects = seedModel.fetchNew(type)
            var count = 0
            for object in objects {
                let newObject = newInstanceOf(object)
                cloneAttritubtes(from: object, to: newObject)
                count += 1
            }
            
        }
    }
    
    /// generates and returns a new NSManagedObject of same type as the object it is passed
    ///
    /// - Parameter object: A NSManagedObject subclass instance
    /// - Returns: A NSManagedObject subclass instance
    fileprivate func newInstanceOf(_ object: NSManagedObject) ->  NSManagedObject{
        let entityName = object.entity.name
        return NSEntityDescription.insertNewObject(forEntityName: entityName!, into: dataModel.container.viewContext)
    }
    
    /// updates objects in users Database
    ///
    /// - Parameter types: an Array of NSManagedObject subclass Types that have objects that need updating
    fileprivate func updateObjectTypes(_ types: [NSManagedObject.Type]) {
        for type in types {
            
            let objects = seedModel.fetchUpdated(type)
            
            for object in objects {
                //fetch existing object
                let objectID = object.value(forKey: "id") as! Int32
                let predicate = NSPredicate(format: "id == %d",objectID)
                let matchingObjects = dataModel.fetchEntity(type, predicate: predicate)
                guard matchingObjects.count == 1,  let matchingObject = matchingObjects.first else {
                    fatalError("problem with metching objects count \(matchingObjects.count) != 1 and matchingObjecst = \(matchingObjects) ")
                }
                cloneAttritubtes(from: object, to: matchingObject)
            }
        }
    }
    
    
    /// copies all attributes, not relationships from one entity to another entity
    ///
    /// - Parameters:
    ///   - sourceObject: An NSManagedObject subclass
    ///   - targetObject: AN NSManagedObject , of same subclass  as the sourceObject
    fileprivate func cloneAttritubtes(from sourceObject: NSManagedObject, to targetObject:NSManagedObject) {
        let attributes = NSEntityDescription.entity(forEntityName: sourceObject.entity.name!, in: dataModel.container.viewContext)?.attributesByName
        for (key, _) in attributes!{
            
            targetObject.setValue(sourceObject.value(forKey: key), forKey: key)
        }
    }
    
    fileprivate func logDataVersion(){
        let versionNumber = dataModel.dataVersion()
        let seedVersion = seedModel.dataVersion()
        print("-->> WORKING DATA STORE version \(versionNumber)")
        print("-->> SEED DATA STORE version\(seedVersion)")
    }
    
    
    /// determines which NSManagedObjectTypes in the new seed Database have  new objects to be added and returns them as an array
    ///
    /// - Returns: an array of NSManagedObject.Type
    fileprivate func objectTypesToAdd(_ typesToCheck: [NSManagedObject.Type]) -> [NSManagedObject.Type] {
        var objectTypeToAdd : [NSManagedObject.Type] = []
        for moType in typesToCheck {
            if seedModel.countNew(entity: moType) > 0 {
                objectTypeToAdd.append(moType)
            }
        }
        return objectTypeToAdd
    }
    
    /// determines which NSManagedObjectTypes in the new seed Database have updated objects to be added and returns them as an array
    ///
    /// - Returns: an array of NSManagedObject.Type
    fileprivate func objectTypesToUpdate(_ typesToCheck:[NSManagedObject.Type]) -> [NSManagedObject.Type] {
        var objectTypeToUpdate : [NSManagedObject.Type] = []
        for moType in typesToCheck {
            if seedModel.countUpdated(entity: moType) > 0 {
                objectTypeToUpdate.append(moType)
            }
        }
        return objectTypeToUpdate
        
    }
  
    func addTag(){
        
    }
 
}
