//
//  TemporaryFileRemover.swift
//  Splash
//
//  Created by Simon Gardener on 18/11/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation

struct TemporaryFileRemover {
    
    static func removeSqlFiles() {
        let sqlType = "sqlite"
        let shmType = "sqlite-shm"
        let walType = "sqlite-wal"
        do {
            try FileManager.default.removeItem(atPath: fileDestinationPath(forType: sqlType))
            try FileManager.default.removeItem(atPath: fileDestinationPath(forType: walType))
            try FileManager.default.removeItem(atPath: fileDestinationPath(forType: shmType))
        }catch{
            print(error.localizedDescription)
        }
    }
    static func fileDestinationPath(forType type: String)-> String {
        let DBName = "SplashDiveLog"
        let defaultDestination = SeedPersistentContainer.defaultDirectoryURL().relativePath
        let path = defaultDestination + "/\(DBName).\(type)"
        print(path)
        return path
    }
    
}
