//
//  LogEntryPreparer.swift
//  Splash
//
//  Created by Simon Gardener on 19/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.

import Foundation
class LogEntryPreparer {
    
    weak var dataModel: DataModel?
    enum TagKey : String {
        case Guiding, Instructing, Assisting
    }
    init(dateModel:DataModel) {
        dataModel = dateModel
    }
    func prepare(logEntry: LogEntry){
        logEntry.id = UUID().uuidString
        setInitialTimeInFor(logEntry)
        setDiveNumberFor(logEntry)
        logEntry.logAsMultilevel =  UserDefaults.newLogIsMultiLevel()
        logEntry.logWithTables = UserDefaults.newLogUsesTables()
        setBuddyFor(logEntry)
        setOtherCarryForwardValuesFor(logEntry)
        setProModeforLogEntry(logEntry)
    }
    
    private func setProModeforLogEntry(_ logEntry:LogEntry){
        if UserDefaults.newLogEntryUsesGuideMode() == true {
          logEntry.isGuiding = true
            logEntry.addToTaggedWith((dataModel?.tagFor(name: TagKey.Guiding.rawValue))!)
        }
        if UserDefaults.newLogEntryUsesInstructingMode() == true {
            logEntry.isInstructing = true
           // dataModel?.printAllTags()
            logEntry.addToTaggedWith((dataModel?.tagFor(name:TagKey.Instructing.rawValue))!)
        }
        if UserDefaults.newLogEntryUsesAssistingMode() == true {
            logEntry.isAssisting = true
            logEntry.addToTaggedWith((dataModel?.tagFor(name: TagKey.Assisting.rawValue))!)
        }
    }
    
    private func setDiveNumberFor(_ logEntry: LogEntry){
        guard let previousLogEntry = dataModel?.previousLogEntry() else {
            logEntry.diveNumber = 1
            return
        }
        logEntry.diveNumber = previousLogEntry.diveNumber + 1
    }
    private func setInitialTimeInFor(_ logEntry: LogEntry){
        logEntry.timeIn = Date()// dateWithoutSeconds()
        logEntry.timeZone = TimeZone.current.identifier
        logEntry.createdModifiedDate = Date()
        
    }
    private func dateWithoutSeconds()-> Date {
        // date without seconds is better for some date calculations later on
        //done this ways as  getting all the component and setting the seconds to zero caused the minutes to round up!
        var date = Date()
        let calendar = Calendar.current
        let unitFlags: Set<Calendar.Component> = [.minute,.hour,.day,.month,.year,.timeZone,.era,.calendar]
        let dateComponents = calendar.dateComponents(unitFlags, from: date)
        
        date  = dateComponents.date!
        print (date, date.timeIntervalSinceReferenceDate)
        
        return date
    }
    private func setBuddyFor(_ logEntry: LogEntry) {
        let buddyOption = UserDefaults.buddyAutofillValue()
        switch buddyOption {
        case BuddyAutofill.useDefault.rawValue:
            setUsualBuddyFor(logEntry: logEntry)
        case BuddyAutofill.useDefaultOrLast.rawValue:
            setUsualOrLastBuddyFor(logEntry: logEntry)
        case BuddyAutofill.last.rawValue :
            setLastBuddyFor(logEntry)
        default : break
        }
    }
    private func setUsualBuddyFor(logEntry: LogEntry){
        guard let buddy = dataModel?.usualBuddy() else {return}
        logEntry.addToBuddiedBy(buddy)
    }
    
    private func setUsualOrLastBuddyFor(logEntry: LogEntry){
        guard let buddy = dataModel?.usualBuddy() else {
            setLastBuddyFor(logEntry)
            return}
        
        logEntry.addToBuddiedBy(buddy)
    }
    private func setLastBuddyFor(_ logEntry: LogEntry){
        guard let previousLogEntry = dataModel?.previousLogEntry() else {return}
        guard let previousBuddies = previousLogEntry.buddiedBy else {return}
        logEntry.addToBuddiedBy(previousBuddies)
    }
    
    private func setOtherCarryForwardValuesFor(_ logEntry: LogEntry){
        let autoFillValue = UserDefaults.otherAutofillItemsValue()
        guard autoFillValue != 1 else {
            addDefaultTankConfigurationTo(logEntry)
            return }
        guard let previousLogEntry = dataModel?.previousLogEntry() else {
            addDefaultTankConfigurationTo(logEntry)
            return }
        
        if autoFillValue & OtherAutofill.weights.rawValue == OtherAutofill.weights.rawValue {
            logEntry.weight = previousLogEntry.weight
        }
        if autoFillValue & OtherAutofill.exposureSuit.rawValue == OtherAutofill.exposureSuit.rawValue {
            logEntry.exposureSuit = previousLogEntry.exposureSuit
        }
        if autoFillValue & OtherAutofill.tank.rawValue == OtherAutofill.tank.rawValue {
            addPreviousDiveTanksTo(logEntry)
        }
        if autoFillValue & OtherAutofill.divePro.rawValue == OtherAutofill.divePro.rawValue {
            logEntry.ledBy = previousLogEntry.ledBy
        }
        if autoFillValue & OtherAutofill.diveOp.rawValue == OtherAutofill.diveOp.rawValue {
            logEntry.facility = previousLogEntry.facility
            logEntry.facilityName = previousLogEntry.facilityName
        }
        if autoFillValue & OtherAutofill.trip.rawValue == OtherAutofill.trip.rawValue {
            logEntry.trip = previousLogEntry.trip
        }
        if autoFillValue & OtherAutofill.boat.rawValue == OtherAutofill.boat.rawValue{
            logEntry.divedFromBoat = previousLogEntry.divedFromBoat
        }
       
    }

    private func addPreviousDiveTanksTo(_ logEntry: LogEntry){
        guard let previousLogEntry = dataModel?.previousLogEntry() else {
            addDefaultTankConfigurationTo(logEntry)
            return }
        
        guard let tanks  = previousLogEntry.gasSupply, tanks.count != 0  else { addDefaultTankConfigurationTo(logEntry)
            return }
        
        for tank in tanks {
            if let previousTank = tank as? Tank {
                if let newTank = dataModel?.newTank() {
                    TankHelper.copyTankValues(to: newTank, from: previousTank)
                    logEntry.addToGasSupply(newTank)
                }
            }
        }
    }
    private func addDefaultTankConfigurationTo(_ logEntry: LogEntry){
        guard let configuration = dataModel?.defaultTankConfiguration() else { return }
        guard let tanks = configuration.has else {return}
        for tank in tanks {
            if let previousTank = tank as? Tank {
                if let newTank = dataModel?.newTank(){
                    TankHelper.copyTankValues(to: newTank, from: previousTank)
                    newTank.pressureIsInBar = UserDefaults.pressureUnits() == .metric ? true: false
                    logEntry.addToGasSupply(newTank)
                }
            }
        }
    }
}
