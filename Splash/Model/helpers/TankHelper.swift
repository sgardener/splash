//
//  TankHelper.swift
//  Splash
//
//  Created by Simon Gardener on 10/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation

struct TankHelper {
    
    static func copyTankValues(to newTank: Tank, from previousTank: Tank) {
        newTank.displayOrder = previousTank.displayOrder
        newTank.gasType = previousTank.gasType
        newTank.supplyType = previousTank.supplyType
        newTank.tankMaterial = previousTank.tankMaterial
        newTank.workingPressure = previousTank.workingPressure
        newTank.heliumPercent = previousTank.heliumPercent
        newTank.oxygenPercent = previousTank.oxygenPercent
        newTank.volume = previousTank.volume
        newTank.volumeIsinLiters = previousTank.volumeIsinLiters
        newTank.name = previousTank.name
        newTank.manifoldedtwins = previousTank.manifoldedtwins
        newTank.pressureIsInBar = previousTank.pressureIsInBar
    }
    
    static func prepNew(_ tank: Tank, with dataModel: DataModel){
        if let config = dataModel.defaultTankConfiguration(), config.has?.count == 1 {
            let previousTank = config.has?.anyObject() as! Tank
            TankHelper.copyTankValues(to: tank, from: previousTank)
        } else {
            if UserDefaults.pressureUnits() == .imperial  {
                tank.pressureIsInBar = false
                tank.volumeIsinLiters = false
                tank.volume = 80
                tank.workingPressure = 3000
            }  
        }
        if let lastPosition = dataModel.logEntry?.gasSupply?.count {
            tank.displayOrder =  Int16(lastPosition + 1)
        }else {
           tank.displayOrder =  1
        }
    }
}
