//
//  NoteEditor.swift
//  Splash
//
//  Created by Simon Gardener on 10/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class NoteEditor: UIViewController, UITextViewDelegate {

    @IBOutlet weak var noteTextView: UITextView!
    var dataModel: DataModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        setUpNavBar()
        noteTextView.text = dataModel.logEntry?.notes
        noteTextView.becomeFirstResponder()
    }
    func setUpNavBar(){
        navigationController?.navigationBar.prefersLargeTitles = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        noteTextView.endEditing(true)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == ";" {
            return false
        }
    return true
    }
  
    func textViewDidEndEditing(_ textView: UITextView) {
       dataModel.logEntry?.notes = noteTextView.text
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return true
    }
}
extension NoteEditor : Injectable{
    func inject(_ dm: DataModel) {
        dataModel = dm
        
    }
    
    func assertDependencies() {
        assert(dataModel != nil, "Expected a datamodel -didnt get one")
    }
    
    typealias T = DataModel
    


}

