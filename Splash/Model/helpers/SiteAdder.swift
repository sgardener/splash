//
//  SiteAdder.swift
//  Splash
//
//  Created by Simon Gardener on 22/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation

class SiteAdder {

    class func add(_ site: DiveSite, to logEntry: LogEntry){
        logEntry.diveSiteID = site.id
        logEntry.diveSiteName = site.name
        logEntry.diveSiteCountry = site.country
        logEntry.diveSite = site
        logEntry.salinity = site.salinity
    }
}
