//
//  ChangeChecker.swift
//  Splash
//
//  Created by Simon Gardener on 03/05/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
class ChangeChecker{
    

    class func noteChangesTo(_ site: DiveSite) {
        
        let changes = site.changedValues()
        let changeCount = changes.count
        guard changeCount > 0 else {return}
        site.updatedByUser = true
        var changedSet = Set<String>()
        if let previousChangedAttributes = site.changedAttributes {
            changedSet = Set((previousChangedAttributes.components(separatedBy: "-").map{$0}))
        }
        for (key, _) in changes {
            changedSet.insert(key)
        }
        site.changedAttributes = changedSet.joined(separator: "-")
        site.dateUpdatedByUser = Date.init()
        site.needsSubmitting = true
        
      
    }
    class func noteChangesTo(_ facility: Facility) {
        
        let changes = facility.changedValues()
        let changeCount = changes.count
        guard changeCount > 0 else {return}
        facility.updatedByUser = true
        var changedSet = Set<String>()
        if let previousChangedAttributes = facility.changedAttributes {
            changedSet = Set((previousChangedAttributes.components(separatedBy: "-").map{$0}))
        }
        for (key, _) in changes {
            changedSet.insert(key)
        }
        facility.changedAttributes = changedSet.joined(separator: "-")
        facility.dateUpdatedByUser = Date.init()
        facility.needsSubmitting = true
        
    }
}

