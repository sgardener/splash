//
//  AnimalPossibility.swift
//  Splash
//
//  Created by Simon Gardener on 13/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

struct AnimalPossibility {
    
    static func possibilityString(for value: Float)-> String {
        switch value {
        case 0: return "... I don't know"
        case 0.00001...0.09999 : return "are never here"
        case 0.1...0.19999 : return "are rare (->5%)"
        case 0.2...0.29999 : return "are uncommon (5-15%)"
        case 0.3...0.39999 : return "visit occasionally (15-30%)"
        case 0.4...0.49999 : return "are common (30-60%)"
        case 0.5...0.59999 : return "are common in season"
        case 0.6...0.69999 : return "are likely (60-80%)"
        case 0.7...0.79999 : return "are likely in season"
        case 0.8...0.89999 : return "are highly likely (80-95%)"
        case 0.9...0.9888 : return "are highly likely in season"
        case 0.9889...1 : return"are almost certain"
        default: return" "
        }
    }
}
