//
//  GeneralArea+CoreDataClass.swift
//  Splash
//
//  Created by Simon Gardener on 26/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//
//

import Foundation
import CoreData
import CoreLocation

@objc(GeneralArea)
public class GeneralArea: NSManagedObject {

    // this assumes that latitude and longitude exist - should never be called upon if they don't. predicates should have this dealt with
    
    var location: CLLocation {
        return CLLocation(latitude: Double(truncating: latitude!), longitude: Double(truncating: longitude!))
    }
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2DMake(Double(truncating: latitude!),Double(truncating: longitude!))
    }
}
