//
//  Diver+CoreDataClass.swift
//  Splash
//
//  Created by Simon Gardener on 16/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Diver)
public class Diver: NSManagedObject {
    func firstAndLastName() -> String {
        let firstAndLastNames = [givenName, familyName].compactMap{$0}
        let name = firstAndLastNames.joined(separator: " ")
        if name.isEmpty == true{ return "unknown"
        }else {
            return "\(name)"
        }
    }
    
    var displayName : String {
        guard let nName = nickName , nName.isEmpty == false else {return firstAndLastName()}
        return nName
    }
}
