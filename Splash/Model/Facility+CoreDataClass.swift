//
//  Facility+CoreDataClass.swift
//  Splash
//
//  Created by Simon Gardener on 04/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//
//

import Foundation
import CoreData
import CoreLocation

@objc(Facility)
public class Facility: NSManagedObject {
    @objc  func firstLetterForIndex()-> String {
        let startIndex = name!.startIndex
        let first = name![...startIndex]
        return String(first)
    }
    var coordinate: CLLocationCoordinate2D? {
        guard latitude != nil , longitude != nil else {return nil}
        return CLLocationCoordinate2DMake(Double(truncating: latitude!),Double(truncating: longitude!))
    }
    
    //for making map points
    
    var title: String {
        return name!
    }
    var location: CLLocation? {
        guard latitude != nil , longitude != nil else {return nil}
        
        return CLLocation(latitude: Double(truncating: latitude!), longitude: Double(truncating: longitude!))
    }
}


