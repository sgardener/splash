//
//  DataModel.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 16/11/2017.
//  Copyright © 2017 Simon Gardener. All rights reserved.
//

import Foundation
import UIKit
import CoreData


class DataModel{
    
    private let SQLFileName = "/SplashDiveLog.sqlite"
    private let SHMFileName = "/SplashDiveLog.sqlite-shm"
    private let WALFileName = "/SplashDiveLog.sqlite-wal"
    private let DBName = "SplashDiveLog"
    private let sqlType = "sqlite"
    private let shmType = "sqlite-shm"
    private let walType = "sqlite-wal"
    
    private let newObjectPredicate = NSPredicate(format: "seedAdded > %d", UserDefaults.seedVersion())
    private let updatedObjectPredicate = NSPredicate(format: "seedUpdated > %d AND seedUpdated < seedAdded   ", seedVersion)
    
    //MARK: - Core Data Stack
    lazy var container: NSPersistentContainer = {
        let persistentContainer = NSPersistentContainer(name: "SplashDiveLog")
        let sqlitePath = Bundle.main.path(forResource: "SplashDiveLog", ofType: "sqlite")
        moveDataBaseIfFirstTime()
        persistentContainer.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error loading persistent store \(error), \(error.userInfo)")
            }
        })
        return persistentContainer
    }()
    
    func moveDataBaseIfFirstTime() {
        
        let sqlitePath = Bundle.main.path(forResource: DBName , ofType: sqlType)
        
        let shmPath = Bundle.main.path(forResource: DBName, ofType: shmType)
        let walPath = Bundle.main.path(forResource: DBName, ofType: walType)
        
        guard FileManager.default.fileExists(atPath: sqlitePath!), FileManager.default.fileExists(atPath: shmPath!), FileManager.default.fileExists(atPath: walPath!) else {
            fatalError("There Should always be a bundled DB, if there isn't , i shouldnt ship it :)")
        }
        
        let destinationPathSQL = NSPersistentContainer.defaultDirectoryURL().relativePath + SQLFileName
        
        guard  !FileManager.default.fileExists(atPath: destinationPathSQL) else {
            // could put the check oif needs updating code in here - currently in app delegate
            return }
        
        let originalSQLURL = URL(fileURLWithPath: sqlitePath!)
        let originalShmURL = URL(fileURLWithPath: shmPath!)
        let originalWalURL = URL(fileURLWithPath: walPath!)
        let destinationPathSHM = NSPersistentContainer.defaultDirectoryURL().relativePath + SHMFileName
        let destinationPathWAL = NSPersistentContainer.defaultDirectoryURL().relativePath + WALFileName
        let destinationSQLURL = URL(fileURLWithPath: destinationPathSQL)
        let destinationSHMURL = URL(fileURLWithPath: destinationPathSHM)
        let destinationWALURL = URL(fileURLWithPath: destinationPathWAL)
        
        do {
            try FileManager.default.copyItem(at: originalSQLURL, to: destinationSQLURL)
            try FileManager.default.copyItem(at: originalShmURL, to: destinationSHMURL)
            try FileManager.default.copyItem(at: originalWalURL, to: destinationWALURL)
            
            UserDefaults.setSeedVersion(seedVersion)
        } catch {
            print(error.localizedDescription)
        }
        
    }
    
    func dataVersion()-> Int16 {
        let versionNumber = fetchDataVersion().number
        return versionNumber
        
    }
    func setDataVersionTo(_ versionNumber: Int16) {
        let version = fetchDataVersion()
        version.number = versionNumber
    }
    
    func fetchDataVersion()-> DataVersion {
        let fetchRequest : NSFetchRequest<DataVersion> = DataVersion.fetchRequest()
        do{
            let version = try container.viewContext.fetch(fetchRequest)
            guard version.count == 1, let dataVersion = version.first else { fatalError("more than one version number object \(version)")}
            return (dataVersion)
            
        }catch {
            let nserror = error as NSError
            fatalError("error fetching the dataVersion\(nserror)")
        }
    }
    
    func checkIfNeedsUpdate(){
        if UserDefaults.seedVersion() < seedVersion {
            print("FILE EXISTS = - update is needed  \n")
        }
    }
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = container.viewContext
        
        if context.hasChanges {
            do {
                try context.save()
                
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error while saving context\(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    // MARK:- Generic fetch
    func fetchEntity<T:NSManagedObject>(_ entity: T.Type, predicate :NSPredicate?) -> [T] {
        let entityName = String(describing: entity)
        let fetchRequest = NSFetchRequest<T>(entityName: entityName)
        fetchRequest.predicate = predicate
        do {
            return try container.viewContext.fetch(fetchRequest)
        } catch{
            print(error.localizedDescription)
            return []
        }
    }
    //MARK:- Generic count
    func countEntity<T:NSManagedObject>(_ entity:T.Type)-> Int{
        let entityName = String(describing: entity)
        let fetchRequest = NSFetchRequest<T>(entityName: entityName)
        do {
            let number = try container.viewContext.count(for: fetchRequest)
            return number
        }
        catch {
            print(error.localizedDescription)
            return 0
        }
    }
    // MARK:- Properties
    
//    func numberOfSites(){
//        let fr : NSFetchRequest<DiveSite> = DiveSite.fetchRequest()
//        let count = try? container.viewContext.count(for: fr)
//    }
    
    var logEntry : LogEntry?
    
    var diver: Diver? {
        didSet{
            theDiver = diver
        }
    }
    var owner: LBOwner? {
        didSet{
            theDiver = owner
        }
    }
    var emergencyContact: EmergencyContact?{
        didSet{
            theDiver = emergencyContact
        }
    }
    
    var theDiver: Diver? // used as common for diver and owner and emergencycontact - because can't all use diver as a logowner has emergencyContact aand divers and would  add to self if just used diver as place for all 3 to be stored!
    
    var site: DiveSite?
    var facility: Facility?
    
    var cert: Certification?
    
    let proLevel = 2
    let instructorLevel = 4
    
    //MARK: - LogEntries
    func numberOfLogEntries ()-> Int {
        let fetchRequest : NSFetchRequest<LogEntry> = LogEntry.fetchRequest()
        let count = try? container.viewContext.count(for: fetchRequest)
        return count != nil ? count! : 0
    }
    
    
    func highestNumberedLogEntry ()-> Int {
        let fetchRequest : NSFetchRequest<LogEntry> = LogEntry.fetchRequest()
        let highestLogNumberSort = NSSortDescriptor(key: "diveNumber", ascending: false)
        fetchRequest.sortDescriptors = [highestLogNumberSort]
        fetchRequest.fetchLimit = 1
        do {
            let logEntries = try container.viewContext.fetch(fetchRequest)
            if let number = (logEntries.first)?.diveNumber {
                return Int(number)
            } else {
                return 0
            }
        }catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.localizedDescription)")
            return 0
        }
    }
    
    func  mostRecentDive ()-> LogEntry?{
        
        let fetchRequest : NSFetchRequest<LogEntry> = LogEntry.fetchRequest()
        let highestLogNumberSort = NSSortDescriptor(key: "timeIn", ascending: false)
        fetchRequest.sortDescriptors = [highestLogNumberSort]
        fetchRequest.fetchLimit = 1
        do {
            let logEntries = try container.viewContext.fetch(fetchRequest)
            return logEntries.first
        }catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.localizedDescription)")
            return nil
        }
    }
    
    //MARK: Number of
    
    
    func numberOfSitesMarkedAsRegular()-> Int {
        let fetchRequest = fetchRequestForRegularSites()
        let count =  try? container.viewContext.count(for: fetchRequest)
        return count != nil  ? count! : 0
    }
    func numberOfSitesMarkedAsFavourite()-> Int {
        let fetchRequest = fetchRequestForFavouriteSites()
        let count = try? container.viewContext.count(for: fetchRequest)
        return count != nil ? count! : 0
    }
    //MARK:- Fetches
    
    func previousLogEntry()-> LogEntry?{
        let sortDescriptor = sortDescriptorForPreviousLogEntry()
        let fetchRequest : NSFetchRequest<LogEntry> = LogEntry.fetchRequest()
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.fetchLimit = 2
        do {
            let previousDives =  try container.viewContext.fetch(fetchRequest)
            // newly created logentry will be first item
            
            if previousDives.count < 2{
                return nil
            } else{
                return previousDives[1]
            }
        } catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.localizedDescription)")
            return nil
        }
    }
    
    private func sortDescriptorForPreviousLogEntry ()-> NSSortDescriptor {
        var sortDescriptor : NSSortDescriptor
        if UserDefaults.regardLastDiveAsMostRecentlyAdded() == true {
            sortDescriptor = NSSortDescriptor(key: #keyPath(LogEntry.createdModifiedDate), ascending:false)
        }else{
            sortDescriptor = NSSortDescriptor(key: #keyPath(LogEntry.timeIn), ascending: false)
        }
        
        return sortDescriptor
    }
    
    
    func usualBuddy()-> Diver?{
        let fetchRequest :NSFetchRequest<Diver> = Diver.fetchRequest()
        let predicate = NSPredicate(format: "buddyStatus = %d", BuddyStatus.defaultBuddy.rawValue)
        fetchRequest.predicate = predicate
        
        do {
            let usualBuddy = try container.viewContext.fetch(fetchRequest)
            return usualBuddy.first
        } catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.localizedDescription)")
            return nil
        }
    }
    //MARK:- AGencies
    func allAgenciesForCenterType()-> [String]{
        let fetchRequest:NSFetchRequest<CenterType> = CenterType.fetchRequest()
        fetchRequest.propertiesToFetch = ["agency"]
        let sortByAgency = NSSortDescriptor(key: #keyPath(CenterType.agency), ascending: true)
        fetchRequest.sortDescriptors = [sortByAgency]
        do{
            let result =  try container.viewContext.fetch(fetchRequest)
            var agencyArray = [String]()
            
            for agency in result {
                if !agencyArray.contains(agency.agency!){
                    agencyArray.append(agency.agency!)
                }
            }
            return agencyArray
        }catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.localizedDescription)")
            return [String]()
        }
    }
    func allAgenciesForResortType()-> [String]{
        let fetchRequest:NSFetchRequest<ResortType> = ResortType.fetchRequest()
        fetchRequest.propertiesToFetch = ["agency"]
        
        let sortByAgency = NSSortDescriptor(key: #keyPath(ResortType.agency), ascending: true)
        fetchRequest.sortDescriptors = [sortByAgency]
        do{
            let result =  try container.viewContext.fetch(fetchRequest)
            var agencyArray = [String]()
            
            for agency in result {
                if !agencyArray.contains(agency.agency!){
                    agencyArray.append(agency.agency!)
                }
            }
            return agencyArray
        }catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.localizedDescription)")
            return [String]()
        }
    }
    //MARK:- ResortTypes
    func frcForResortTypes(for agency: String)-> NSFetchedResultsController<ResortType> {
        let fetchRequest = fetchRequestForResortType()
        let predicate = NSPredicate(format: "agency == %@",agency)
        fetchRequest.predicate = predicate
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: container.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        return frc
    }
    
    func fetchRequestForResortType()-> NSFetchRequest<ResortType>{
        let fetchRequest: NSFetchRequest<ResortType> = ResortType.fetchRequest()
        let sortByAgency = NSSortDescriptor(key: #keyPath(ResortType.type), ascending: true)
        fetchRequest.sortDescriptors = [sortByAgency]
        return fetchRequest
    }
    //MARK:- CenterTypes
    func frcForCenterTypes(for agency: String)-> NSFetchedResultsController<CenterType> {
        let fetchRequest = fetchRequestForCenterType()
        let predicate = NSPredicate(format: "agency == %@",agency)
        fetchRequest.predicate = predicate
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: container.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        return frc
    }
    func fetchRequestForCenterType()-> NSFetchRequest<CenterType>{
        let fetchRequest: NSFetchRequest<CenterType> = CenterType.fetchRequest()
        let sortByAgency = NSSortDescriptor(key: #keyPath(CenterType.type), ascending: true)
        fetchRequest.sortDescriptors = [sortByAgency]
        return fetchRequest
    }
    //MARK:- Facility
    func newFacility()->Facility {
        facility = Facility(context:container.viewContext)
        facility?.addedByUser = true
        facility?.needsSubmitting = true
        facility?.dateLogged = Date.init()
        facility?.id = UUID().uuidString
        facility?.whoSubbed =  UIDevice.current.identifierForVendor!.uuidString
        return facility!
    }
    func addFacilityToLogEntry() {
        guard facility != nil else {fatalError("facility is nil")}
        guard logEntry != nil else {fatalError("logentrty is nil")}
        logEntry?.facility = facility
        logEntry?.facilityName = facility?.name
    }
    func numberOfItemsNeedingSubmitting ()->Int{
        return numberOfFaciltiesThatNeedSubmitting() + numberOfSitesThatNeedSubmitting() + countEntity(ConfirmValues.self)
    }
    func numberOfFaciltiesThatNeedSubmitting()->Int{
        let fetchRequest = container.managedObjectModel.fetchRequestTemplate(forName: "UnsubmittedFacilities")
        let count = try? container.viewContext.count(for: fetchRequest!)
        if let thisMany = count {
            return thisMany
        }else { return 0 }
    }
    func numberOfSitesThatNeedSubmitting()->Int{
        let fetchRequest = container.managedObjectModel.fetchRequestTemplate(forName: "allUnsubSitesXNowhereNear")
        let count = try? container.viewContext.count(for: fetchRequest!)
        if let thisMany = count {
            return thisMany
        }else { return 0}
    }
    //MARK:- Fetches Facilities
    func facilitiesThatNeedSubmitting()-> [Facility]{
        let fetchRequest = container.managedObjectModel.fetchRequestTemplate(forName: "UnsubmittedFacilities")
        do {
            return try container.viewContext.fetch(fetchRequest!) as! [Facility]
        }
        catch {
            fatalError("fetching unsubmitted facilities failed")
        }
    }
    func allFacilitiesIn(range: NSPredicate) -> [Facility]? {
        let fetchRequest: NSFetchRequest<Facility>  = Facility.fetchRequest()
        fetchRequest.predicate = range
        do {
            return try container.viewContext.fetch(fetchRequest)
        }catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.localizedDescription)")
            return nil
        }
    }
    func numberOfFacilitiesMarkedAsRegular()-> Int {
        let fetchRequest = fetchRequestForRegularFacilities()
        let count = try? container.viewContext.count(for: fetchRequest)
        return count != nil ? count! : 0
    }
    
    func fetchRequestForRegularFacilities()-> NSFetchRequest<Facility> {
        let fetchRequest: NSFetchRequest<Facility>  = Facility.fetchRequest()
        let predicate = NSPredicate(format:"isRegularFacility == true")
        fetchRequest.predicate = predicate
        let countrySD = NSSortDescriptor(key: #keyPath(Facility.country), ascending: true)
        let nameSD = NSSortDescriptor(key: #keyPath(Facility.name), ascending:true)
        fetchRequest.sortDescriptors = [countrySD,nameSD]
        return fetchRequest
    }
    func regularFacilites()-> [Facility] {
        let fetchRequest = fetchRequestForRegularFacilities()
        do{
            return try container.viewContext.fetch(fetchRequest)
            
        }catch{
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.localizedDescription)")
            return [Facility]()
        }
    }
    
    func resetFacilitiesAfterSending(){
        let facilities = facilitiesThatNeedSubmitting()
        for facility in facilities {
            facility.needsSubmitting = false
            facility.dateUpdatedByUser = nil
            facility.changedAttributes = nil
        }
    }
    //MARK:- ConfirmValues
    func confirmValue(matching siteID: String)-> ConfirmValues {
        let predicate = NSPredicate(format: "siteID == %@", siteID)
        var cv = fetchEntity(ConfirmValues.self, predicate:predicate)
        if cv == [] {
            cv = [newConfirmValues(forSiteID: siteID)]
        }
        return cv[0]
    }
    fileprivate func newConfirmValues(forSiteID siteID: String) -> ConfirmValues{
        let confirmValue = ConfirmValues(context: container.viewContext)
        confirmValue.siteID = siteID
        return confirmValue
    }
    func removeAllConfirmedValueObjects(){
        let confirms = fetchEntity(ConfirmValues.self, predicate: nil)
        for confirm in confirms {
            container.viewContext.delete(confirm)
        }
        
    }
    
    //MARK:- Dive Sites
    func newDiveSite()->DiveSite{
        site = DiveSite(context: container.viewContext)
        site?.addedByUser = true
        site?.needsSubmitting = true
        site?.dateLogged = Date.init()
        site?.id = UUID().uuidString
        site?.whoSubbed = UIDevice.current.identifierForVendor!.uuidString
        site?.noteAssocName = UserDefaults.associatedName()
        site?.noteAssocWeb = UserDefaults.associateWeb()
        //  SitePreparer.prepare(site: site!) - now all defaults on new site creation in model
        return site!
    }
    func addDiveSiteToLogEntry(){
        guard site != nil else { fatalError("No site!")}
        guard logEntry != nil else { fatalError("No LogEntry")}
        logEntry?.diveSite = site
        logEntry?.diveSiteName = site?.name
        logEntry?.diveSiteCountry = site?.country
        logEntry?.diveSiteID = (site?.id)!
    }
    
    //MARK:- Fetches Dive Sites
    func sitesThatNeedSubmitting()-> [DiveSite]{
        let fetchRequest = container.managedObjectModel.fetchRequestTemplate(forName: "allUnsubSitesXNowhereNear")
        do {
            return try container.viewContext.fetch(fetchRequest!) as! [DiveSite]
        }
        catch {
            fatalError("fetching unsubmitted sites failed")
            
        }
    }
    
    func allDiveSitesIn(range: NSPredicate) -> [DiveSite]? {
        
        let fetchRequest : NSFetchRequest<DiveSite> = DiveSite.fetchRequest()
        fetchRequest.predicate = range
        do {
            return try container.viewContext.fetch(fetchRequest)
        }
        catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        return nil
    }
    
    func allGeneralAreasIn(range:NSPredicate) -> [GeneralArea]? {
        let fetchRequest: NSFetchRequest<GeneralArea> = GeneralArea.fetchRequest()
        fetchRequest.predicate = range
        do {
            return try container.viewContext.fetch(fetchRequest)
        } catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        return nil
    }
    
    func regularDiveSites()-> [DiveSite] {
        let fetchRequest = fetchRequestForRegularSites()
        do{
            return try container.viewContext.fetch(fetchRequest)
            
        }catch{
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.localizedDescription)")
            return [DiveSite]()
        }
    }
    func favouriteDiveSites()-> [DiveSite] {
        let fetchRequest = fetchRequestForFavouriteSites()
        do{
            return try container.viewContext.fetch(fetchRequest)
            
        }catch{
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.localizedDescription)")
            return [DiveSite]()
        }
    }
    fileprivate func fetchDiveSites(_ fetchRequest: NSFetchRequest<DiveSite>)->[DiveSite]? {
        do{
            let diveSites = try container.viewContext.fetch(fetchRequest)
            return diveSites
        }catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.localizedDescription)")
            return nil
        }
    }
    
    
    func fetchRequestForRegularSites()-> NSFetchRequest<DiveSite> {
        let predicate = NSPredicate(format:"regularSite == true")
        return fetchRequestForDiveSitesWith(predicate: predicate)
    }
    func fetchRequestForFavouriteSites()-> NSFetchRequest<DiveSite> {
        let predicate = NSPredicate(format:"userFavourite == true")
        return fetchRequestForDiveSitesWith(predicate: predicate)
    }
    
    func fetchRequestForDiveSitesWith(predicate: NSPredicate)-> NSFetchRequest<DiveSite> {
        let fetchRequest : NSFetchRequest<DiveSite>  = DiveSite.fetchRequest()
        fetchRequest.predicate = predicate
        let sortDescriptor = NSSortDescriptor(key: #keyPath(DiveSite.name), ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        return fetchRequest
    }
    func resetSitesAfterSending(){
        let sites = sitesThatNeedSubmitting()
        for site in sites{
            site.needsSubmitting = false
            site.dateUpdatedByUser = nil
            site.changedAttributes = nil
        }
    }
    //MARK:- Fetches General Areas
    func allDiveSitesIn(generalArea :GeneralArea) -> [DiveSite]?{
        let fetchRequest : NSFetchRequest<DiveSite> = DiveSite.fetchRequest()
        let predicate = NSPredicate(format:"generalArea = %@",generalArea)
        fetchRequest.predicate = predicate
        let nameSort = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [nameSort]
        return fetchDiveSites(fetchRequest)
    }
    
    func unknownLocationDiveSitesIn(generalArea :GeneralArea) -> [DiveSite]?{
        let diveSites = allDiveSitesIn(generalArea: generalArea)
        return diveSites?.filter{$0.latitude == nil}
    }
    
    func allGeneralAreas() -> [GeneralArea] {
        let fetchRequest :NSFetchRequest<GeneralArea>  = GeneralArea.fetchRequest()
        do{
            let allAreas = try container.viewContext.fetch(fetchRequest)
            return allAreas
        }catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.localizedDescription)")
            fatalError(" if there are no General Areas its because there are no GA in the database, - which is a show stopper error")
        }
    }
    //currently usused as i went with the generic fetchEntity
    func generalArea(withID generalAreaId: Int32) -> GeneralArea? {
        
        let fetchRequest : NSFetchRequest<GeneralArea> = GeneralArea.fetchRequest()
        let predicate = NSPredicate(format: "id = %@",generalAreaId)
        fetchRequest.predicate = predicate
        do {
            let fetchResult =  try container.viewContext.fetch(fetchRequest)
            guard fetchResult.count < 2 else {
                fatalError ("matches = \(fetchResult.count) database corrupted - there should never be two or more matching general areas")
            }
            if fetchResult.count == 0 {return nil}
            return fetchResult.first
        }catch {
            let fetchError = error as NSError
            print("\(fetchError),\(fetchError.localizedDescription) \n when expecting a single generalArea")
        }
        
        return nil
    }
    //MARK:- TrainingAgencies
    func allTrainingAgencies()-> [TrainingAgency]{
        let fetchRequest: NSFetchRequest<TrainingAgency> = TrainingAgency.fetchRequest()
        let sortBy = NSSortDescriptor(key: #keyPath(TrainingAgency.name), ascending: true)
        fetchRequest.sortDescriptors = [sortBy]
        do{
            return try container.viewContext.fetch(fetchRequest)
        }catch{
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.localizedDescription)")
            return [TrainingAgency]()
        }
    }
    func frcForTrainingAgencies()-> NSFetchedResultsController<TrainingAgency>{
        let fetchRequest: NSFetchRequest<TrainingAgency> = TrainingAgency.fetchRequest()
        let sortBy = NSSortDescriptor(key: #keyPath(TrainingAgency.name), ascending: true)
        fetchRequest.sortDescriptors = [sortBy]
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: container.viewContext, sectionNameKeyPath: "name", cacheName: nil)
        return frc
    }
    //MARK:- TANK
    
    func newTank() -> Tank {
        return Tank(context:container.viewContext)
    }
    
    func defaultTankConfiguration()-> Configuration?{
        let fetchRequest : NSFetchRequest<Configuration> = Configuration.fetchRequest()
        let predicate = NSPredicate(format:"defaultConfiguration == true")
        fetchRequest.predicate = predicate
        do{
            let defaultConfiguration = try container.viewContext.fetch(fetchRequest)
            guard defaultConfiguration.count > 0 else {return nil}
            return defaultConfiguration.first
        }catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.localizedDescription)")
            return nil
        }
    }
    
    //MARK: - Fetched Result Controllers
    //MARK: Divers
    
    func alreadyExistsDiverWith(_ givenName: String?, _ familyName:String?)-> Bool {
        var predicate: NSPredicate!
        let fetchRequest = fetchRequestForDiver()
        if let gName = givenName, let fName = familyName{
            predicate = NSPredicate(format:"familyName = %@ && givenName = %@ && listAsDiver = true",fName,gName)
        }else if let gName = givenName {
            predicate = NSPredicate(format:"givenName = %@ && familyName = nil && listAsDiver = true",gName)
        }else if let fName = familyName{
            predicate = NSPredicate(format:"givenName = nil && familyName = %@ && listAsDiver = true",fName)
        }
        fetchRequest.predicate = predicate
        
        if let count = try? container.viewContext.count(for: fetchRequest) {
            if count == 0 {
                return false
            } else {
                return true
            }
        }else {
            fatalError("Couldnt get count for existing diver")
        }
    }
    
    func ensureADefaultDiver(){
        guard numberOfAllDivers() == 0 else {return}
        let diver = newDiver()
        diver.givenName = "Lisa"
        diver.familyName = "Gardener"
        diver.nickName = "Lisa The Diver"
        diver.buddyStatus = 1
        diver.proStatus = 4
        diver.clientStatus = 1
    }
    fileprivate func sortedAllDiversFetchedRequest()->NSFetchRequest<Diver>{
        let fetchRequest = fetchRequestForDiver()
        let sortByFamilyName = NSSortDescriptor(key: #keyPath(Diver.familyName), ascending: true)
        let sortByFirstName = NSSortDescriptor(key: #keyPath(Diver.givenName), ascending: true)
        fetchRequest.sortDescriptors = [sortByFamilyName, sortByFirstName]
        return fetchRequest
    }
    
    func sortedBuddiesFetchRequest()-> NSFetchRequest<Diver>{
        let fetchRequest = fetchRequestForDiver()
        let predicate = NSPredicate(format:"buddyStatus > 1")
        let statusSortDescriptor = NSSortDescriptor(key: #keyPath(Diver.buddyStatus), ascending: true)
        let sortByFamilyName = NSSortDescriptor(key: #keyPath(Diver.familyName), ascending: true)
        let sortByFirstName = NSSortDescriptor(key: #keyPath(Diver.givenName), ascending: true)
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = [statusSortDescriptor,sortByFamilyName,sortByFirstName]
        return fetchRequest
    }
    
    func sortedProFetchRequest(minimumLevel proStatus: Int)-> NSFetchRequest<Diver>{
        let fetchRequest = fetchRequestForDiver()
        let predicate = NSPredicate(format:"proStatus >= %d",proStatus)
        let recentSortDescriptor = NSSortDescriptor(key: #keyPath(Diver.lastDiveWithWas), ascending: false)
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = [recentSortDescriptor]
        return fetchRequest
    }
    func sortedStudentFetchRequest()-> NSFetchRequest<Diver>{
        let fetchRequest = fetchRequestForDiver()
        let predicate = NSPredicate(format:"clientStatus = 2 or clientStatus = 6")
        let recentSortDescriptor = NSSortDescriptor(key: #keyPath(Diver.lastDiveWithWas), ascending: false)
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = [recentSortDescriptor]
        return fetchRequest
    }
    
    func sortedGuidedDiverFetchRequest()-> NSFetchRequest<Diver>{
        let fetchRequest = fetchRequestForDiver()
        let predicate = NSPredicate(format:"clientStatus = 4 or clientStatus = 6")
        let recentSortDescriptor = NSSortDescriptor(key: #keyPath(Diver.lastDiveWithWas), ascending: false)
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = [recentSortDescriptor]
        return fetchRequest
    }
    
    func sortedClientFetchRequest()-> NSFetchRequest<Diver>{
        let fetchRequest = fetchRequestForDiver()
        let predicate = NSPredicate(format:"clientStatus > 1")
        let statusSortDescriptor = NSSortDescriptor(key: #keyPath(Diver.clientStatus), ascending: false)
        let recentSortDescriptor = NSSortDescriptor(key: #keyPath(Diver.lastDiveWithWas), ascending: false)
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = [statusSortDescriptor,recentSortDescriptor]
        return fetchRequest
    }
    
    func fetchedResultsControllerForAllDivers()-> NSFetchedResultsController<Diver>{
        
        return NSFetchedResultsController(fetchRequest: sortedAllDiversFetchedRequest(), managedObjectContext: container.viewContext, sectionNameKeyPath: #keyPath(Diver.familyName), cacheName: nil)
        
    }
    
    func fetchedResultsControllerForClientDivers()-> NSFetchedResultsController<Diver>{
        return NSFetchedResultsController(fetchRequest: sortedClientFetchRequest(), managedObjectContext: container.viewContext, sectionNameKeyPath: #keyPath(Diver.clientStatus), cacheName: nil)
    }
    func fetchedResultsControllerForStudents()->NSFetchedResultsController<Diver>{
        return NSFetchedResultsController(fetchRequest: sortedStudentFetchRequest(), managedObjectContext: container.viewContext, sectionNameKeyPath: nil, cacheName: nil)
    }
    func fetchedResultsControllerForGuidedDivers()->NSFetchedResultsController<Diver>{
        return NSFetchedResultsController(fetchRequest: sortedGuidedDiverFetchRequest(), managedObjectContext: container.viewContext, sectionNameKeyPath: nil, cacheName: nil)
    }
    func fetchedResultsControllerForBuddies()-> NSFetchedResultsController<Diver>{
        return NSFetchedResultsController(fetchRequest: sortedBuddiesFetchRequest(), managedObjectContext: container.viewContext, sectionNameKeyPath: #keyPath(Diver.buddyStatus), cacheName: nil)
    }
    func fetchedResultsControllerForDivePros()->NSFetchedResultsController<Diver>{
        return NSFetchedResultsController(fetchRequest: sortedProFetchRequest(minimumLevel: proLevel), managedObjectContext: container.viewContext, sectionNameKeyPath: nil, cacheName: nil)
    }
    func fetchedResultsControllerForInstructors()-> NSFetchedResultsController<Diver> {
        return NSFetchedResultsController(fetchRequest: sortedProFetchRequest(minimumLevel: instructorLevel), managedObjectContext: container.viewContext, sectionNameKeyPath: nil, cacheName: nil)
    }
    
    func fetchRequestForDiver()-> NSFetchRequest<Diver>{
        let fr :NSFetchRequest<Diver> = Diver.fetchRequest()
        let predicate = NSPredicate(format:"listAsDiver = true")
        fr.predicate = predicate
        return fr
    }
    
    //MARK number of divers
    
    func numberOfAllDivers()-> Int {
        let fetchRequest = fetchRequestForDiver()
        let count = try? container.viewContext.count(for: fetchRequest)
        return count != nil ? count! : 0
    }
    
    func numberOfBuddies()->Int {
        let fetchRequest = sortedBuddiesFetchRequest()
        let count = try? container.viewContext.count(for: fetchRequest)
        return count != nil ? count! : 0
    }
    func numberOfClients()-> Int {
        let fetchRequest = sortedClientFetchRequest()
        let count = try? container.viewContext.count(for: fetchRequest)
        return count != nil ? count! : 0
    }
    func numberOfPros()-> Int {
        let fetchRequest = sortedProFetchRequest(minimumLevel: 2)
        let count = try? container.viewContext.count(for: fetchRequest)
        return count != nil ? count! : 0
    }
    //MARK: Tags
    
    func fetchedResultsControllerForDiveTags()-> NSFetchedResultsController<DiveTag>{
        let fetchRequest: NSFetchRequest<DiveTag> = DiveTag.fetchRequest()
//print("main store number of divetags = \(String(describing: try? container.viewContext.count(for: fetchRequest)))")
        let sortByCatOrder = NSSortDescriptor(key: #keyPath(DiveTag.tagCategory), ascending: true)
        let sortByOrder = NSSortDescriptor(key: #keyPath(DiveTag.tagOrder), ascending: true)
        fetchRequest.sortDescriptors = [sortByCatOrder,sortByOrder]
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: container.viewContext, sectionNameKeyPath: #keyPath(DiveTag.tagCategory), cacheName: nil)
        return frc
    }
    func printAllTags() {
        let fetchRequest: NSFetchRequest<DiveTag> = DiveTag.fetchRequest()
        do{
            let x = try container.viewContext.fetch(fetchRequest)
            for tag in x {
                print(tag.name!)
                
            }        }catch{
                let fetchError = error as NSError
                print("\(fetchError), \(fetchError.localizedDescription)")
        }
    }
    func tagFor(name:String) -> DiveTag? {
        let fetchRequest: NSFetchRequest<DiveTag> = DiveTag.fetchRequest()
        let predicate = NSPredicate(format: "name == %@",name)
        fetchRequest.predicate = predicate
        do{
            let x = try container.viewContext.fetch(fetchRequest)
            return x.first
        }catch{
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.localizedDescription)")
            return nil
        }
    }
    
    
    func setTimeTagsfor(classification: DiveTimeClassification){
        
        let predicate = NSPredicate(format: "tagCategory = %@","time")
        let timeTags = getDiveTags(for: predicate)
        var timePredicate : NSPredicate
        
        switch classification {
        case .unknown: return
        case .day:
            timePredicate = NSPredicate(format:"name = %@", "Day")
        case .night:
            timePredicate = NSPredicate(format:"name = %@", "Night")
        case .sunset:
            timePredicate = NSPredicate(format:"name = %@", "Sunset")
        case .sunrise:
            timePredicate = NSPredicate(format:"name = %@", "Sunrise")
        case .earlyMorning :
            timePredicate = NSPredicate(format:"name = %@", "Early morning")
        case .lateAfternoon:
            timePredicate = NSPredicate(format:"name = %@", "Late afternoon")
        }
        
        print("removing time tags from \(logEntry?.diveSiteName ?? "no name")")
        print(logEntry?.taggedWith?.count as Any)
        for tag in timeTags {
            logEntry?.removeFromTaggedWith(tag)
        }
        if let addTag = getDiveTags(for: timePredicate).first{
            logEntry?.addToTaggedWith(addTag)
        }
    }
    
    
    func recentlyAddedDiveTags() -> [DiveTag] {
        return getDiveTags(for:newObjectPredicate)
    }
    func recentlyUpdatedDiveTags() -> [DiveTag] {
        return getDiveTags(for: updatedObjectPredicate)
    }
    
    fileprivate func getDiveTags(for predicate: NSPredicate)->[DiveTag]{
        let fetchRequest : NSFetchRequest<DiveTag> = DiveTag.fetchRequest()
        fetchRequest.predicate = predicate
        do {
            let tags = try container.viewContext.fetch(fetchRequest)
            
            return tags
        }catch{
            print("tag fetching error \(error)")
            return []
        }
        
    }
    
    func fetchedResultsControllerForDiveSitesWithSectionNameKeyPathCountry()-> NSFetchedResultsController<DiveSite> {
        let fetchRequest: NSFetchRequest<DiveSite> = DiveSite.fetchRequest()
        let countrySortDescriptor = NSSortDescriptor(key: #keyPath(DiveSite.country), ascending: true)
        let nameSortDescriptor = NSSortDescriptor(key: #keyPath(DiveSite.name), ascending: true)
        fetchRequest.sortDescriptors = [countrySortDescriptor,nameSortDescriptor]
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: container.viewContext,
                                                                  sectionNameKeyPath: "country",
                                                                  cacheName: nil)
        
        return fetchedResultsController
    }
    //MARK: Facilities
    func fetchedResultsControllerForFacilityWithSectionNameKeyPathCountry()-> NSFetchedResultsController<Facility> {
        let fetchRequest: NSFetchRequest<Facility> = Facility.fetchRequest()
        let countrySortDescriptor = NSSortDescriptor(key: #keyPath(Facility.country), ascending: true)
        let nameSortDescriptor = NSSortDescriptor(key: #keyPath(Facility.name), ascending: true)
        fetchRequest.sortDescriptors = [countrySortDescriptor,nameSortDescriptor]
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: container.viewContext,
                                                                  sectionNameKeyPath: "country",
                                                                  cacheName: nil)
        
        return fetchedResultsController
    }
    //MARK:- InsurancePolicy
    func newInsurancePolicy()-> InsurancePolicy{
        let policy = InsurancePolicy(context: container.viewContext)
        return policy
    }
    func frcForInsurancePolicies()-> NSFetchedResultsController<InsurancePolicy>{
        let fetchRequest : NSFetchRequest<InsurancePolicy> = InsurancePolicy.fetchRequest()
        let expiryDate = NSSortDescriptor(key: #keyPath(InsurancePolicy.endDate), ascending: false)
        fetchRequest.sortDescriptors = [expiryDate]
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: container.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        return frc
    }
    func numberOfInsurancePolicies()-> Int{
        let fetchRequest : NSFetchRequest<InsurancePolicy> = InsurancePolicy.fetchRequest()
        let count = try? container.viewContext.count(for: fetchRequest)
        return count != nil ? count! : 0
    }
    //MARK:- InsurancePolicyTypes
    func frcForInsurancePolicyTypes()-> NSFetchedResultsController<InsurancePolicyType> {
        let fetchRequest : NSFetchRequest<InsurancePolicyType> = InsurancePolicyType.fetchRequest()
        let sortByOrder = NSSortDescriptor(key: #keyPath(InsurancePolicyType.displayOrder), ascending: true)
        fetchRequest.sortDescriptors=[sortByOrder]
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: container.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        return frc
    }
    //MARK: - ESP
    func newPhoneNumber()-> PhoneNumber{
        return PhoneNumber(context: container.viewContext)
    }
    func newEmail()-> EmailAddress{
        return EmailAddress(context: container.viewContext)
    }
    
    func newSocial()-> SocialAccount{
        return SocialAccount(context: container.viewContext )
    }
    //MARK: - EmergencyContacts
    func frcForEmergencyContact()-> NSFetchedResultsController<EmergencyContact>{
        let fetchRequest :NSFetchRequest<EmergencyContact> = EmergencyContact.fetchRequest()
        let sortByOrder = NSSortDescriptor(key: #keyPath(EmergencyContact.contactOrder), ascending: true)
        fetchRequest.sortDescriptors = [sortByOrder]
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: container.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        return frc
    }
    func numberOfEmergencyContacts()-> Int{
        let fetchRequest :NSFetchRequest<EmergencyContact> = EmergencyContact.fetchRequest()
        let count = try? container.viewContext.count(for: fetchRequest)
        return count != nil ? count! : 0
    }
    
    func newEmergencyContact()-> EmergencyContact{
        let ec = EmergencyContact(context: container.viewContext)
        ec.listAsDiver = false
        owner?.addToEmergencyContact(ec)
        return ec
    }
    //MARK:- LOGBOOKOWNER
    func logBookOwnerFetchRequest()-> NSFetchRequest<LBOwner>{
        return LBOwner.fetchRequest()
    }
    func numberOfLogBookOwners()-> Int{
        let count = try? container.viewContext.count(for: logBookOwnerFetchRequest())
        return  count ?? 0
    }
    func hasLogBookOwner()-> Bool{
        if numberOfLogBookOwners() == 1 { return true} else {return false}
    }
    func logBookOwner()-> LBOwner?{
        let fetchRequest = logBookOwnerFetchRequest()
        do {
            let owners = try container.viewContext.fetch(fetchRequest)
            if owners.count == 1 {
                owner = owners.first
                return owners.first}
        } catch {
            let fetchError = error as NSError
            print("Unable to Execute Fetch Request for owner")
            print("\(fetchError), \(fetchError.localizedDescription)")
            return nil
        }
        return nil
    }
    
    func newLBOwner()-> LBOwner {
        let owner = LBOwner(context:container.viewContext)
        owner.listAsDiver = false
        return owner
    }
    //MARK: - CERTIFICATIONS
    
    func fetchedResultsControllerForDiversCerts ()-> NSFetchedResultsController<Certification>{
        let fetchRequest : NSFetchRequest<Certification> = Certification.fetchRequest()
        let predicate = NSPredicate(format:"heldBy == %@", theDiver!)
        fetchRequest.predicate = predicate
        let primarySortOrder = NSSortDescriptor(key: #keyPath(Certification.type.type ), ascending: true)
        let secondarySortOrder = NSSortDescriptor(key: #keyPath(Certification.type.displayOrder), ascending: true)
        fetchRequest.sortDescriptors = [primarySortOrder,secondarySortOrder]
        return NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: container.viewContext, sectionNameKeyPath: nil, cacheName: nil)
    }
    
    
    func newCertification()-> Certification{
        return Certification(context: container.viewContext)
    }
    
    //MARK: - Certification Type
    func fetchedResultsControllerForCertsTypeFor(_ agency: String)-> NSFetchedResultsController<CertificationType> {
        let fetchRequest : NSFetchRequest<CertificationType> = CertificationType.fetchRequest()
        let predicate = NSPredicate(format:"agency == %@",agency)
        fetchRequest.predicate = predicate
        let primarySortOrder = NSSortDescriptor(key: #keyPath(CertificationType.type ), ascending: true)
        let secondarySortOrder = NSSortDescriptor(key: #keyPath(CertificationType.displayOrder), ascending: true)
        fetchRequest.sortDescriptors = [primarySortOrder,secondarySortOrder]
        return NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: container.viewContext, sectionNameKeyPath: #keyPath(CertificationType.type), cacheName: nil)
    }
    
    func fetchedResultsControllerForCertificationTypes()-> NSFetchedResultsController<CertificationType>{
        let fetchRequest : NSFetchRequest<CertificationType> = CertificationType.fetchRequest()
        let sortOrder = NSSortDescriptor(key: #keyPath(CertificationType.agency), ascending : true)
        fetchRequest.sortDescriptors = [sortOrder]
        return NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: container.viewContext, sectionNameKeyPath: #keyPath(CertificationType.agency), cacheName: nil)
    }
    
    func newLogEntry()-> LogEntry {
        let newLogEntry = LogEntry(context: container.viewContext)
        let entryPreparer = LogEntryPreparer(dateModel: self)
        entryPreparer.prepare(logEntry: newLogEntry)
        logEntry = newLogEntry
        return newLogEntry
    }
    
    func newSignature()-> Signature {
        return Signature(context: container.viewContext)
    }
    func newDiveBoat()-> DiveBoat {
        return DiveBoat(context:container.viewContext)
    }
    func newTrip()-> Trip{
        return Trip(context:container.viewContext)
    }
    
    func newDiver()-> Diver {
        diver = Diver(context:container.viewContext)
        theDiver = diver
        return diver!
    }
    //MARK: - remove
    func removeExistingDefaultBuddy(){
        let fetchRequest = fetchRequestForDiver()
        let predicate = NSPredicate(format:"buddyStatus == 2")
        fetchRequest.predicate = predicate
        do{
            let defaultBuddy = try container.viewContext.fetch(fetchRequest)
            if let onlyDefaultBuddy = defaultBuddy.first{
                onlyDefaultBuddy.buddyStatus = 4
            }
        }catch {
            let fetchError = error as NSError
            print("Unable to Execute Fetch Request for default buddy")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
    }
    //MARK:- Interrogation
    func canDelete(_ diver:Diver)-> Bool{
        let links = logEntriesLinkedTo(diver)
        let total = links.buddy + links.guided + links.led + links.student
        if total == 0 { return true } else { return false }
        
        
    }
    func logEntriesLinkedTo(_ diver:Diver)-> (buddy:Int, led:Int, student: Int, guided:Int ){
        let buddyCount = diver.buddiedOn?.count ?? 0
        let ledCount = diver.ledDives?.count ?? 0
        let studentCount = diver.instructedOn?.count ?? 0
        let guidedCount = diver.wasSupervisedOn?.count ?? 0
        
        return (buddyCount,ledCount,studentCount,guidedCount)
    }
}
