//
//  BulletineDataSource.swift
//  Splash
//
//  Created by Simon Gardener on 17/10/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
import BLTNBoard
import CoreLocation

struct BulletinDataSource {
  
    //MARK:Welcome Screens
    
    static func welcomePage()-> BLTNPageItem {
        let page = BLTNPageItem(title: "Welcome to Splash")
        page.descriptionText = "Splash is the location aware Dive Log that helps you log dives quickly and easily."
        
        page.image = UIImage(named: "SplashForIntro")
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            page.next = BulletinDataSource.welcomeLocationPermissionPage()
        } else {
            page.next = BulletinDataSource.spreadTheWordPage()
        }
        
        page.actionButtonTitle = "Next"
        page.isDismissable = false
        
        page.actionHandler = { item in
            item.manager?.displayNextItem()
        }
        return page
    }
    
    static func welcomeLocationPermissionPage()-> BLTNPageItem {
        let page = BLTNPageItem(title: "Location Permission")
        page.descriptionText = "Splash uses your location to automatically fill in the nearest known dive site and to capture and refine site and facility locations.\n\n We are not tracking you."
        page.image = UIImage(named: "Location")
        page.actionButtonTitle = "Understood - Give Permission"
        page.next = BulletinDataSource.checkOutSettings()
        page.image = UIImage(named: "LocationPrompt")
        
        page.isDismissable = false
        page.actionHandler = { item in
            PermissionsManager.shared.requestWhenInUseLocation()
            item.manager?.displayNextItem()
        }
        return page
    }
    
    static func  checkOutSettings ()-> BLTNPageItem {
        let page = BLTNPageItem(title: "Lazy Settings.")
        page.descriptionText = "Save time by setting options to carry forward details such as guide, buddy, weights and tank from one dive to the next."
        page.next = BulletinDataSource.spreadTheWordPage()
        page.actionButtonTitle = "Next"
        page.requiresCloseButton = false
        
        page.actionHandler = { item in
            item.manager?.displayNextItem()
        }
        return page
        
    }
    static func spreadTheWordPage()-> BLTNPageItem {
        let page = BLTNPageItem(title: "Spread the Word")
        page.image = UIImage(named: "ShareShape")
        page.descriptionText = "As you log new sites, that data improves the app for all divers.\n\n More users means a better app, sooner, for you."
        page.actionButtonTitle = "Next"
        page.next = BulletinDataSource.rememberPage()
        page.actionHandler = { item in
            item.manager?.displayNextItem()
        }
        page.isDismissable = false
        
        return page
    }


    static func rememberPage()-> BLTNPageItem {
        let page = BLTNPageItem(title:"Satellites")
        page.image = UIImage(named: "Satellite")
        page.descriptionText = "GPS will often work better and faster on deck than inside a boat - particularly a metal one."
        page.actionButtonTitle = "Get Started"
        page.requiresCloseButton = false
        page.actionHandler = { item in
            item.manager?.dismissBulletin(animated: true)
        }
        return page
    }

    static func locationWasDeniedPage()-> BLTNPageItem{
        let page = BLTNPageItem(title: "Location Permission Needed")
        page.descriptionText = "To work fully, Splash needs access to GPS.\n\nIn Settings select 'Privacy' -> 'Location Services' -> 'Splash' -> 'While Using the App'"
        page.actionButtonTitle = "I'll Go To Settings"
        page.image = UIImage(named: "LocationPrompt")
        page.actionHandler = {item in
            item.manager?.dismissBulletin(animated: true)
        }
        page.isDismissable = false
        return page
    }
//    static func locationWasDeniedPage()-> BLTNPageItem{
//        let page = BLTNPageItem(title: "Location Permission Needed")
//        page.descriptionText = "To work fully, Splash needs access to GPS.\n\nIn Settings select 'Privacy' -> 'Location Services' -> 'Splash' -> 'While Using the App'"
//        page.actionButtonTitle = "Go To Settings"
//        page.alternativeButtonTitle = "I'll do it later."
//        page.image = UIImage(named: "LocationPrompt")
//        page.actionHandler = {item in
//            UIApplication.shared.open(URL(string:"App-Prefs:root=Privacy&path=LOCATION")!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
//            item.manager?.dismissBulletin(animated: true)
//        }
//        page.alternativeHandler = {item in
//            item.manager?.dismissBulletin(animated: true)
//        }
//
//        page.isDismissable = false
//        return page
//    }
//    To work fully, Splash needs access to GPS.\n\nIn Settings select 'Privacy' -> 'Location Services' -> 'Splash' -> 'While Using the App'
}
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}


