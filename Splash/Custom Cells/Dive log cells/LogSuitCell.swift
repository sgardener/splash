//
//  LogSuitCell.swift
//  Splash
//
//  Created by Simon Gardener on 11/06/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class LogSuitCell: UITableViewCell {

    @IBOutlet weak var suitTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configure(with logEntry: LogEntry){
        if let suitDescription = logEntry.exposureSuit, suitDescription.isEmpty == false {
            suitTextField.text = suitDescription
        }else {
            suitTextField.text = nil
        }
        
    }
}
