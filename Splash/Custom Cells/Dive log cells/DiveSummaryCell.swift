//
//  DiveSummaryCell.swift
//  Splash
//
//  Created by Simon Gardener on 11/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class DiveSummaryCell: UITableViewCell {
    
    @IBOutlet weak var siteNameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var maxDepthLabel: UILabel!
    @IBOutlet weak var timeInLabel: UILabel!
    @IBOutlet weak var diveNumberLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    fileprivate let nightTag = "Night"
    fileprivate let sunriseTag = "Sunrise"
    fileprivate let sunsetTag = "Sunset"
    
    fileprivate let nightCellColour = #colorLiteral(red: 0.05189453125, green: 0.05189453125, blue: 0.05189453125, alpha: 1)
    fileprivate let nightTextColor = UIColor.white
    fileprivate let nightLighterText = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 0.9)
    fileprivate let dayLighterText = #colorLiteral(red: 0.5, green: 0.5, blue: 0.5, alpha: 1)
    fileprivate let stradlesBackgroundColor = #colorLiteral(red: 0.28712893, green: 0.28712893, blue: 0.28712893, alpha: 1)
    fileprivate let crepuscularBackgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)// #colorLiteral(red: 1, green: 0.2981745419, blue: 0.05502568437, alpha: 1)  // #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1)  #colorLiteral(red: 0.7350217986, green: 0.7350217986, blue: 0.7350217986, alpha: 1)
   
    fileprivate let crepuscularTextColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    fileprivate let crepuscularLighterText = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 0.9)
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func configureWith(_ logEntry: LogEntry){
        
        setColorScheme(for: logEntry)
        siteNameLabel.text = logEntry.diveSiteName ?? "Nowhere"
        maxDepthLabel.text = maxDepthString(for: logEntry)
        diveNumberLabel.text = "#" + String(describing: logEntry.diveNumber )
        locationLabel.text = locationStringFor(logEntry)
        dateLabel.text = formattedDate(timeIn: logEntry.timeIn!, timeZone: logEntry.timeZone!)
        timeInLabel.text = formattedTime(timeIn: logEntry.timeIn!, timeZone: logEntry.timeZone!)
    }
    func maxDepthString(for logEntry: LogEntry)-> String {
        guard logEntry.depthMax != 0 else { return "---" }
        
        return logEntry.depthMax.depthInUserPreference()
    }
    
    private func locationStringFor(_ logEntry: LogEntry) -> String {
        var locationString = "location: --"
        var locationArray = [String]()
       
        if let country = logEntry.diveSiteCountry {
            locationArray.append(country)
        }
        if let island = logEntry.diveSite?.islandAtoll , !island.isEmpty {
            if !locationArray.contains(island){
                locationArray.append(island)
            }
        }else {
            if let localArea = logEntry.diveSite?.localAreaName, !localArea.isEmpty {
                if !locationArray.contains(localArea) {
                    locationArray.append(localArea)
                }
            }
        }
        locationString =  locationArray.joined(separator: ", ")
        return locationString
        
        
//
//
//        if let island = logEntry.diveSite?.islandAtoll , !island.isEmpty , let country = logEntry.diveSiteCountry, !country.isEmpty {
//            locationString = island + ", " + country
//        }else if let country = logEntry.diveSiteCountry, !country.isEmpty {
//            locationString = country
//        }
//        return locationString
    }
    
    
    private func setColorScheme(for logEntry: LogEntry){
        let tags  = logEntry.taggedWith as! Set<DiveTag>
        let nightTag = tags.filter{$0.name == self.nightTag }
        if nightTag.count == 1 {
            setNightColourScheme()
            return
        }
        let crepusularTags = tags.filter{$0.name == sunsetTag || $0.name == sunriseTag }
        if crepusularTags.count > 0{
            setCrepuscularColorScheme()
            return
        }
        
        setDayColorScheme()
    }
    
    private func setDayColorScheme(){
        backgroundColor =  nil
        siteNameLabel.textColor = nil
        
        locationLabel.textColor = dayLighterText
        timeInLabel.textColor = dayLighterText
        maxDepthLabel.textColor  = dayLighterText
        diveNumberLabel.textColor = dayLighterText
        dateLabel.textColor = dayLighterText
        
    }
    private func setNightColourScheme(){
        backgroundColor = nightCellColour
        siteNameLabel.textColor = nightTextColor
        locationLabel.textColor = nightLighterText
        timeInLabel.textColor = nightLighterText
        maxDepthLabel.textColor  = nightLighterText
        diveNumberLabel.textColor = nightLighterText
        dateLabel.textColor = nightLighterText
        
    }
    private func setStradlesDayNightScheme(){
        backgroundColor = stradlesBackgroundColor
        siteNameLabel.textColor = crepuscularTextColor
        locationLabel.textColor = crepuscularLighterText
        timeInLabel.textColor = crepuscularLighterText
        maxDepthLabel.textColor  = crepuscularLighterText
        diveNumberLabel.textColor = crepuscularLighterText
        dateLabel.textColor = crepuscularLighterText
        
    }
    private func setCrepuscularColorScheme(){
        
        backgroundColor = crepuscularBackgroundColor
        siteNameLabel.textColor = crepuscularTextColor
        
        locationLabel.textColor = crepuscularLighterText
        timeInLabel.textColor = crepuscularLighterText
        maxDepthLabel.textColor  = crepuscularLighterText
        diveNumberLabel.textColor = crepuscularLighterText
        dateLabel.textColor = crepuscularLighterText
    }
    private func formattedDate(timeIn:Date, timeZone: String) -> String{
        let df = DateFormatter()
        // df.dateStyle = .full
        if traitCollection.preferredContentSizeCategory <= .accessibilityMedium {
            df.dateFormat = "EEE, MMM dd, YYYY"
        } else{
        df.dateStyle = .short
        }
        df.timeZone = TimeZone(identifier: timeZone)
        return df.string(from: timeIn)
    }
    private func formattedTime(timeIn:Date, timeZone: String) -> String{
        let df = DateFormatter()
        df.timeStyle = .short
        df.timeZone = TimeZone(identifier: timeZone)
        return df.string(from: timeIn)
    }
}

