//
//  EquipmentCell.swift
//  Splash
//
//  Created by Simon Gardener on 10/06/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class EquipmentCell: UITableViewCell {
    
    @IBOutlet weak var exposureLabel: UILabel!
    @IBOutlet weak var exposureDetailsNameLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var weightNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    func configure(with logEntry:LogEntry){
        let exposureisHidden : Bool
        let weightisHidden : Bool
        if let suit = logEntry.exposureSuit, suit.isEmpty == false {
            exposureLabel.text = suit
            exposureisHidden = false
        }
        else{ exposureisHidden = true }
        setVisibility(to: exposureisHidden, forNameLabel: exposureDetailsNameLabel, dataLabel: exposureLabel)
        
        if let weight = logEntry.weight as? Double {
            weightLabel.text = weight.weightInUserPreference()
            weightisHidden = false
        } else { weightisHidden = true}
        setVisibility(to: weightisHidden, forNameLabel: weightNameLabel, dataLabel: weightLabel)
    }
   
    fileprivate func setVisibility(to visibility : Bool, forNameLabel nameLabel : UILabel, dataLabel : UILabel){
        nameLabel.isHidden = visibility
        dataLabel.isHidden = visibility
        
        
    }
}
