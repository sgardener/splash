//
//  DiveTagCell.swift
//  Splash
//
//  Created by Simon Gardener on 16/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class DiveTagCell: UITableViewCell {

    @IBOutlet weak var tagLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configure(with logEntry: LogEntry){
        if let tags = logEntry.taggedWith{
            let categorySort = NSSortDescriptor(key: "categoryOrder", ascending: true)
            let tagSort = NSSortDescriptor(key: "tagOrder", ascending: true)
            let tagArray  = tags.sortedArray(using: [categorySort,tagSort]) as! [DiveTag]
            // let toDisplay = tagArray.reduce(""){$0 + $1.name! + ", "} // problem here as this would pruce string with , on the end
            let toDisplay = (tagArray.compactMap{$0.name}).joined(separator: ", ")
            tagLabel.text = toDisplay
        }else {
            tagLabel.text = ""
        }
    }
}
