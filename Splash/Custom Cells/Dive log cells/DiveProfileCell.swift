//
//  DiveProfileMultiLevelCell.swift
//  Splash
//
//  Created by Simon Gardener on 11/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import DeviceKit

class DiveProfileCell: UITableViewCell {
//regular profile property labels
    @IBOutlet weak var surfaceIntervalLabel: UILabel!
    @IBOutlet weak var timeInLabel: UILabel!
    @IBOutlet weak var timeOutLabel: UILabel!
    @IBOutlet weak var safetyStopLabel: UILabel!
    @IBOutlet weak var totalDiveTimeLabel: UILabel!
    @IBOutlet weak var startPressureLabel: UILabel!
    @IBOutlet weak var endPressureLabel: UILabel!
    @IBOutlet weak var maxDepthLabel: UILabel!
    @IBOutlet weak var bottomTimeLabel: UILabel!
    //stackview
    
    @IBOutlet weak var PGStack: UIStackView!
    @IBOutlet weak var PGLabelsStackView: UIStackView!
    @IBOutlet weak var PGLabelIn: UILabel!
    @IBOutlet weak var PGLabelOut: UILabel!
    
    //property labels for multilevel logging
    @IBOutlet weak var firstIntermediatePressureLabel: UILabel!
    @IBOutlet weak var secondIntermediatePressureLabel: UILabel!
    @IBOutlet weak var thirdAndFinalPressureGroupLabel: UILabel!
    @IBOutlet weak var secondTimeLabel: UILabel!
    @IBOutlet weak var secondDepthLabel: UILabel!
    @IBOutlet weak var thirdTimeLabel: UILabel!
    @IBOutlet weak var thirdDepthLabel: UILabel!
    
    @IBOutlet weak var depthLinSV: UILabel!
    @IBOutlet weak var levelLSV: UIStackView!
    @IBOutlet weak var endPGHeaderLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    fileprivate func setMultiLevelValues(_ viewModel: ProfileViewModel, _ showUnits: Bool) {
        setUpMultiCLevelEpgIfRequired(by: viewModel)
        
        secondTimeLabel.text = viewModel.secondTime()
        thirdTimeLabel.text = viewModel.thirdTime()
        
        maxDepthLabel.text = viewModel.firstDepth(withUnits: showUnits)
        secondDepthLabel.text = viewModel.secondDepth(withUnits: showUnits)
        thirdDepthLabel.text = viewModel.thirdDepth(withUnits: showUnits)
    }
    
    fileprivate func setBaseLabels(_ viewModel: ProfileViewModel) {
        timeInLabel.text = viewModel.timeIn()
        timeOutLabel.text = viewModel.timeOut()
        surfaceIntervalLabel.text = viewModel.surfaceInterval()
        safetyStopLabel.text = viewModel.safetyStop()
        totalDiveTimeLabel.text = viewModel.totalDiveTime()
        
        startPressureLabel.text = viewModel.startPressure()
        endPressureLabel.text = viewModel.endPressure()
        if let pgLabelIn = PGLabelIn {
            pgLabelIn.text = viewModel.startPressureLabel()
        }
        if let pgLabelOut = PGLabelOut {
            pgLabelOut.text = viewModel.endPressureLabel()
        }
        maxDepthLabel.text = viewModel.maxDepth()
        bottomTimeLabel.text = viewModel.bottomTime()
    }
    
    func populateWith(_ viewModel:ProfileViewModel){
        let device = Device.current
        var showUnits = true
        if (device == .iPhoneSE || device == .simulator(.iPhoneSE)) && traitCollection.preferredContentSizeCategory >= .accessibilityLarge && viewModel.usesTables == true {
            showUnits = false
        }
        
        setBaseLabels(viewModel)
        
       PGStack.isHidden = !viewModel.usesTables
        
        if viewModel.isMultiLevel {
            setMultiLevelValues(viewModel, showUnits)
        }
    }
    
    fileprivate func setUpMultiCLevelEpgIfRequired(by viewModel: ProfileViewModel){
       
        let usesTables = viewModel.usesTables
        setMulitPressureGroupVisibility(usesTables)
        if usesTables {
            endPGHeaderLabel.text = viewModel.endPGHeaderLabel()
            firstIntermediatePressureLabel.text = viewModel.firstIntermediatePressure()
            secondIntermediatePressureLabel.text = viewModel.secondIntermediatePressure()
            thirdAndFinalPressureGroupLabel.text = viewModel.endPressure()
        }
    }

    fileprivate func setMulitPressureGroupVisibility(_ usesTables: Bool){
        
        let hidden = !usesTables
        endPGHeaderLabel.isHidden = hidden
        firstIntermediatePressureLabel.isHidden = hidden
        secondIntermediatePressureLabel.isHidden = hidden
        thirdAndFinalPressureGroupLabel.isHidden = hidden
    }
}
