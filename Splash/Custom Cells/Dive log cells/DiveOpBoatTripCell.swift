//
//  DiveOpBoatTripCell.swift
//  Splash
//
//  Created by Simon Gardener on 11/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class DiveOpBoatTripCell: UITableViewCell {

    @IBOutlet weak var opBoatTripLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(with logEntry:LogEntry) {
        
        var  info:[String] = []
        if let name = logEntry.trip?.name {
            info.append(name)
        }
        if let name = logEntry.divedFromBoat?.name{
            info.append(name)
        }
        if let name = logEntry.facility?.name{
            info.append(name)
        }

        let allText = info.joined(separator: ",\n")
        
        opBoatTripLabel.text = allText
    }
}
