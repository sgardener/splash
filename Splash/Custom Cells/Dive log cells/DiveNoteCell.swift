//
//  DiveNoteCell.swift
//  Splash
//
//  Created by Simon Gardener on 11/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class DiveNoteCell: UITableViewCell {

    @IBOutlet weak var noteLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func populateWithData(_ logEntry:LogEntry){
        noteLabel.text = logEntry.notes
    }
    
}
