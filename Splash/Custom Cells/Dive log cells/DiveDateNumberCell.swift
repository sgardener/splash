//
//  DiveDateNumberCell.swift
//  Splash
//
//  Created by Simon Gardener on 11/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class DiveDateNumberCell: UITableViewCell {

    @IBOutlet weak var diveDateLabel: UILabel!
    @IBOutlet weak var diveNumberLabel: UILabel!
    @IBOutlet weak var timeInLabel: UILabel!
    
    @IBOutlet weak var stackView: UIStackView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        if traitCollection.preferredContentSizeCategory >= .accessibilityMedium {
           stackView.axis = .vertical
            diveDateLabel.textAlignment = .natural
        } else {
            stackView.axis = .horizontal
            diveDateLabel.textAlignment = .right
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureWith(_ logEntry: LogEntry){
        
        diveDateLabel.text =  formattedDate(timeIn: logEntry.timeIn!, timeZone: logEntry.timeZone!)
        diveNumberLabel.text = "No." + String(describing: logEntry.diveNumber)
      //  timeInLabel.text = "" //formattedTime(timeIn: logEntry.timeIn!, timeZone: logEntry.timeZone!)
    }
    private func formattedDate(timeIn:Date, timeZone: String) -> String{
        let df = DateFormatter()
        df.dateStyle = .medium
        df.timeZone = TimeZone(identifier: timeZone)
        return df.string(from: timeIn)
    }
    private func formattedTime(timeIn:Date, timeZone: String) -> String{
        let df = DateFormatter()
        df.timeStyle = .short
        df.timeZone = TimeZone(identifier: timeZone)
        return df.string(from: timeIn)
    }
}
