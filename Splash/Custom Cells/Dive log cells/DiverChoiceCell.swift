//
//  DiverChoiceCell.swift
//  Splash
//
//  Created by Simon Gardener on 25/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class DiverChoiceCell: DiverSummaryCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configure(with diver: Diver, isLogged :Bool ) {
        super.configure(with: diver)
        
        if isLogged == true {
            accessoryType = .checkmark
        } else {
            accessoryType = .none
    }
    }
}
