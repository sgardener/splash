//
//  DiveProfileSingleLevelCell.swift
//  Splash
//
//  Created by Simon Gardener on 11/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class DiveProfileSingleLevelCell: UITableViewCell {

    @IBOutlet weak var surfaceIntervalLabel: UILabel!
    @IBOutlet weak var startPressureLabel: UILabel!
    @IBOutlet weak var endPressureLabel: UILabel!
    @IBOutlet weak var timeInLabel: UILabel!
    @IBOutlet weak var bottomTimeLabel: UILabel!
    @IBOutlet weak var maxDepthLabel: UILabel!
    @IBOutlet weak var safetyStopLabel: UILabel!
    @IBOutlet weak var totalDiveTime: UILabel!
    @IBOutlet weak var timeOutLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
