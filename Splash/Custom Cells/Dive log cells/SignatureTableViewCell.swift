//
//  SingatureTableViewCell.swift
//  Splash
//
//  Created by Simon Gardener on 08/02/2019.
//  Copyright © 2019 Simon Gardener. All rights reserved.
//

import UIKit

class SignatureTableViewCell: UITableViewCell {

    @IBOutlet weak var signatureImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configure(with logEntry: LogEntry ){
        if let signature = logEntry.signature?.image {
            let sigImage = UIImage.init(data: signature)
            signatureImageView.image = sigImage
        }
    }
}
