//
//  ExposureSuitCell.swift
//  Splash
//
//  Created by Simon Gardener on 10/06/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class ExposureSuitCell: UITableViewCell {

    @IBOutlet weak var exposureLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(with logEntry:LogEntry){
        if let suit = logEntry.exposureSuit, suit.isEmpty == false {
            exposureLabel.text = suit
        }
        else{ exposureLabel.text = nil }
        
    }
}
