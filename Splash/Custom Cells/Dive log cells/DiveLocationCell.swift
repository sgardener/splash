//
//  DiveLocationCell.swift
//  Splash
//
//  Created by Simon Gardener on 11/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class DiveLocationCell: UITableViewCell {
    
    @IBOutlet weak var siteNameLabel: UILabel!
    @IBOutlet weak var siteLocationLabel: UILabel!
    
    @IBOutlet weak var compassImage: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configure(withLogEntry logEntry: LogEntry, shouldSpin: Bool, addedAutomatically added : Bool) {
        
        siteNameLabel.text = logEntry.diveSiteName ?? "needs dive site name"
        colorLocationLabel(forLogEntry: logEntry, addedAutomatically: added)
        spinnerShouldSpin(shouldSpin)
        siteLocationLabel.text = locationStringFor(logEntry)
    }
    
    private func spinnerShouldSpin(_ shouldSpin: Bool) {
        if shouldSpin == true  {
            spinner.startAnimating()
        }
        else {
            spinner.stopAnimating()
        }
    }
    
    private func colorLocationLabel(forLogEntry logeEntry: LogEntry, addedAutomatically added: Bool) {
        if added {
            siteNameLabel.textColor = .blue
            siteLocationLabel.textColor = .blue
        } else {
            siteNameLabel.textColor = .black
            siteLocationLabel.textColor = .black
        }
    }
    
    private func locationStringFor(_ logEntry: LogEntry) -> String {
        var locationString = "location: --"
        var locationArray = [String]()
        
        if let country = logEntry.diveSiteCountry {
            locationArray.append(country)
        }
        if let island = logEntry.diveSite?.islandAtoll , !island.isEmpty {
            if !locationArray.contains(island){
                locationArray.append(island)
            }
        }else {
            if let localArea = logEntry.diveSite?.localAreaName, !localArea.isEmpty {
                if !locationArray.contains(localArea) {
                    locationArray.append(localArea)
                }
            }
        }
        locationString =  locationArray.joined(separator: ", ")
        return locationString
    }
    
}
