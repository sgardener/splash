//
//  DiveTankCell.swift
//  Splash
//
//  Created by Simon Gardener on 11/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class DiveTankCell: UITableViewCell {

    @IBOutlet weak var tankSizeMaterialLabel: UILabel!
    @IBOutlet weak var tankGasPercentageLabel: UILabel!
    @IBOutlet weak var tankInPressureLabel: UILabel!
    @IBOutlet weak var tankOutPressureLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func populateTankCellWith(tvm: TankDataViewModel){
        var larger = true
        if traitCollection.preferredContentSizeCategory >= .extraExtraExtraLarge {
            larger = false
        }
        tankSizeMaterialLabel.text = tvm.sizeMaterial(larger)
        tankGasPercentageLabel.text = tvm.gasPercentage()
        tankInPressureLabel.text = tvm.pressureIn(larger)
        tankOutPressureLabel.text = tvm.pressureOut(larger)
        
        
    }
}
