//
//  MultiLevelCell.swift
//  Splash
//
//  Created by Simon Gardener on 12/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class MultiLevelCell: UITableViewCell , ResignableCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var hiddenTextField: UITextField!
    @IBOutlet weak var depthLabel: UILabel!
    
    @IBOutlet weak var depthNumber: UILabel!
    @IBOutlet weak var endPGLabel: UILabel?
    
    @IBOutlet weak var timeNumber: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
