//
//  SwitchCell.swift
//  Splash
//
//  Created by Simon Gardener on 12/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class SwitchCell: UITableViewCell {

    
    @IBOutlet weak var attributeLabel: UILabel!
    @IBOutlet weak var theSwitch: UISwitch!
    @IBOutlet weak var hiddenTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
