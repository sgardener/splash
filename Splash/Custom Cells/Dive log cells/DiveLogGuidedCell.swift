//
//  DiveLogGuidedCell.swift
//  Splash
//
//  Created by Simon Gardener on 27/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class DiveLogGuidedCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configure(with logEntry:LogEntry){
        guard let divers = logEntry.supervised, divers.count > 0 else  { label.text = nil
            return }
        guard divers.count > 1 else { label.text = (divers.anyObject() as! Diver).displayName
            return }
        let nameSort = NSSortDescriptor(key: "familyName", ascending: true)
        let sortedDivers = divers.sortedArray(using: [nameSort]) as! [Diver]
        let displayDivers =  sortedDivers.reduce(""){$0 + $1.displayName + "   "}
        label.text = displayDivers
    }

}
