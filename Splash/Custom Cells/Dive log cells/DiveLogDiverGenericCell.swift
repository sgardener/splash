//
//  DiveLogDiverGenericCell.swift
//  Splash
//
//  Created by Simon Gardener on 27/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class DiveLogDiverGenericCell: UITableViewCell {

  
    @IBOutlet weak var namesLabel: UILabel!
    @IBOutlet weak var buddyLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configure(with logEntry:LogEntry, attribute: String){
        guard let divers:NSSet = logEntry.value(forKey: attribute) as? NSSet ,divers.count > 0 else  { namesLabel.text = nil
            return }
        guard divers.count > 1 else { namesLabel.text = (divers.anyObject() as! Diver).displayName
            return }
        let nameSort = NSSortDescriptor(key: "familyName", ascending: true)
        let sortedDivers = divers.sortedArray(using: [nameSort]) as! [Diver]
        let nameArray = sortedDivers.map{$0.displayName}
        let nameString = nameArray.joined(separator: "\n")
        namesLabel.text = nameString
        
    }
}
