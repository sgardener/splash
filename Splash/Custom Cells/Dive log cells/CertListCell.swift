//
//  CertListCell.swift
//  Splash
//
//  Created by Simon Gardener on 31/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class CertListCell: UITableViewCell {

    @IBOutlet weak var certificationList: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureWith(viewModel:DiverViewModel){
        certificationList.text = viewModel.certificationsString()
    }
}
