//
//  DiveLogBuddyCell.swift
//  Splash
//
//  Created by Simon Gardener on 16/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class DiveLogBuddyCell: UITableViewCell {
    @IBOutlet weak var buddyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configure(with logEntry:LogEntry){
        guard let buddies = logEntry.buddiedBy, buddies.count > 0 else  { buddyLabel.text = nil
            return }
        guard buddies.count > 1 else { buddyLabel.text = (buddies.anyObject() as! Diver).displayName
            return }
        let nameSort = NSSortDescriptor(key: "familyName", ascending: true)
        let sortedBuddies = buddies.sortedArray(using: [nameSort]) as! [Diver]
        let displayBuddies =  sortedBuddies.reduce(""){$0 + $1.displayName + "   "}
        buddyLabel.text = displayBuddies    
    }

}
