//
//  LogWeightCell.swift
//  Splash
//
//  Created by Simon Gardener on 11/06/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class LogWeightCell: UITableViewCell {


    @IBOutlet weak var triggeringTextField: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(with logEntry: LogEntry, toolbar: UIToolbar, picker: WeightPicker){
        if let weight = logEntry.weight as? Double {
        textLabel?.text = "\(weight.weightInUserPreference() )"
        }else { textLabel?.text = "--" }

       triggeringTextField.inputAccessoryView = toolbar
        triggeringTextField.inputView = picker
    
    }
}
