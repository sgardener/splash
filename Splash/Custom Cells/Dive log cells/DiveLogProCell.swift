//
//  DiveLogProCell.swift
//  Splash
//
//  Created by Simon Gardener on 16/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class DiveLogProCell: UITableViewCell {

    @IBOutlet weak var prosLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(with logEntry:LogEntry){
        guard let pros = logEntry.ledBy, pros.count > 0 else {
            prosLabel.text = nil
            return}
        guard pros.count > 1 else {
            prosLabel.text = (pros.anyObject() as! Diver).displayName
            return}
        let nameSort = NSSortDescriptor(key: "displayName",ascending: true)
           let sortedPros = pros.sortedArray(using: [nameSort]) as! [Diver]
        let displayPros = sortedPros.reduce(""){$0 + $1.displayName + "   "}
        print("displayPros \(displayPros)")
        prosLabel.text = displayPros
    }
    
}

