//
//  DiveConditionsCell.swift
//  Splash
//
//  Created by Simon Gardener on 11/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class DiveConditionsCell: UITableViewCell {

    @IBOutlet weak var seasonLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var visabilityLabel: UILabel!
    @IBOutlet weak var currentLabel: UILabel!
    @IBOutlet weak var salinityLabel: UILabel!
    @IBOutlet weak var surfaceLabel: UILabel!
    @IBOutlet weak var waterTempLabel: UILabel!
    @IBOutlet weak var airTemperatureLabel: UILabel!
    
    @IBOutlet weak var waterNameLabel: UILabel!
    @IBOutlet weak var airNameLabel: UILabel!
    @IBOutlet weak var currentNameLabel: UILabel!
    @IBOutlet weak var visibilityNameLabel: UILabel!
    @IBOutlet weak var weatherNameLabel: UILabel!
    @IBOutlet weak var seasonNameLabel: UILabel!
    @IBOutlet weak var surfaceNameLabel: UILabel!
    @IBOutlet weak var salinityNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configure(with logEntry: LogEntry){
        seasonLabel.text = ConditionData.season[Int(logEntry.season)]
        weatherLabel.text = ConditionData.weather[Int(logEntry.weatherConditions)]
        visabilityLabel.text = ConditionData.visibility[Int(logEntry.visibilityRelative)]
        currentLabel.text = ConditionData.current[Int(logEntry.currentStrength)]
        salinityLabel.text = ConditionData.salinity[Int(logEntry.salinity)]
        surfaceLabel.text = ConditionData.surface[Int(logEntry.surfaceConditions)]
        waterTempLabel.text = Temperature.stringFor(temperature: logEntry.waterTemperature)
        airTemperatureLabel.text = Temperature.stringFor(temperature: logEntry.airTemperature)

        setVisibility()
        
    }
    fileprivate func setVisibility(){
        setVisibility(forNameLabel: waterNameLabel, dataLabel: waterTempLabel)
        setVisibility(forNameLabel: airNameLabel, dataLabel: airTemperatureLabel)
        setVisibility(forNameLabel: seasonNameLabel, dataLabel: seasonLabel)
        setVisibility(forNameLabel: weatherNameLabel, dataLabel: weatherLabel)
        setVisibility(forNameLabel: visibilityNameLabel, dataLabel: visabilityLabel)
        setVisibility(forNameLabel: currentNameLabel, dataLabel: currentLabel)
        setVisibility(forNameLabel: salinityNameLabel, dataLabel: salinityLabel)
        setVisibility(forNameLabel: surfaceNameLabel, dataLabel: surfaceLabel)
    }

    fileprivate func  setVisibility(forNameLabel nameLabel : UILabel, dataLabel : UILabel ){
        
        let visBool = dataLabel.text == "--"
        nameLabel.isHidden = visBool
        dataLabel.isHidden = visBool
    }
}
