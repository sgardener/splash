//
//  PolicyLabelDataCell.swift
//  Splash
//
//  Created by Simon Gardener on 02/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class PolicyLabelDataCell: UITableViewCell {


    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configure(with viewModel: OwnerViewModel, at indexPath:IndexPath){
        textField.placeholder = viewModel.insurancePlaceholder(at: indexPath)
        textField.keyboardType = viewModel.keyboardType(at: indexPath)
        textField.tag = indexPath.section
        textField.text = viewModel.textForPolicyAttibute(at: indexPath)
        label.text = viewModel.labelData(at: indexPath)
    }
}
