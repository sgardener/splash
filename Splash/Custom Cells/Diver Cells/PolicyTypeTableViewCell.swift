//
//  PolicyTypeTableViewCell.swift
//  Splash
//
//  Created by Simon Gardener on 05/02/2019.
//  Copyright © 2019 Simon Gardener. All rights reserved.
//

import UIKit

class PolicyTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var policyTypeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(with ip: InsurancePolicy){
        if let type = ip.type?.type {
            policyTypeLabel.text = type
            policyTypeLabel.textColor = .black
        }else {
            policyTypeLabel.text = "add policy type"
            policyTypeLabel.textColor = .lightGray

        }
       
    }
}
