//
//  LabelDataTextPairCell.swift
//  Splash
//
//  Created by Simon Gardener on 01/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class LabelDataTextPairCell: UITableViewCell {

    @IBOutlet weak var dataTextField: UITextField!
    @IBOutlet weak var labelTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
