//
//  PolicyDateTableViewCell.swift
//  Splash
//
//  Created by Simon Gardener on 04/02/2019.
//  Copyright © 2019 Simon Gardener. All rights reserved.
//

import UIKit

class PolicyDateTableViewCell: UITableViewCell {

    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configure(with policy: InsurancePolicy){
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        
        if let start = policy.startDate{
            startDateLabel.textColor = .black
        startDateLabel.text = dateFormatter.string(from: start)
        }else {
            startDateLabel.textColor = .lightGray
            startDateLabel.text = "--/--/--"

        }
        if let end = policy.endDate {
            endDateLabel.textColor = textColorFor(expiryDate: end)
            endDateLabel.text = dateFormatter.string(from: end)
        }else{
            endDateLabel.textColor = .lightGray
            endDateLabel.text =  "--/--/--"
            
        }
    }
    
    
    /// works out textColor needed for expiry date
    /// expired = red , within 15 days = orange > 15 Black
    /// - Parameter expiryDate: policy end date
    /// - Returns: UIColor, either red orange or black
    
    fileprivate func textColorFor(expiryDate: Date)-> UIColor {
        if expiryDate.timeIntervalSinceNow < 0 {
            return .red
        }
        let fifteenDays : TimeInterval = 1296000
        if expiryDate.timeIntervalSinceNow > 0.0 && expiryDate.timeIntervalSinceNow <= fifteenDays {
            return .orange
        }
        return .black
    }
}

