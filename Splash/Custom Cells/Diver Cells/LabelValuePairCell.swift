//
//  LabelValuePairCell.swift
//  Splash
//
//  Created by Simon Gardener on 04/01/2019.
//  Copyright © 2019 Simon Gardener. All rights reserved.
//

import UIKit

class LabelValuePairCell: UITableViewCell {
    let lightBlueLabelColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var labelLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configure(withLabel label: String?, data: String?, placeHolderLabel: String, placeHolderData : String){
        if let label = label {
            labelLabel.text = label
            labelLabel.textColor = lightBlueLabelColor
        } else {
            labelLabel.text = placeHolderLabel
            labelLabel.textColor = .gray
        }
        if let data = data {
            valueLabel.text  = data
            valueLabel.textColor = .darkText
        }else {
            valueLabel.text  = placeHolderData
            valueLabel.textColor = .gray
        }
        
    }
}
