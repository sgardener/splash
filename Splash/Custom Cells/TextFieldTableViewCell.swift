//
//  TextFieldTableViewCell.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 06/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var textField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
