//
//  BasicWithHeaderTableViewCell.swift
//  Splash
//
//  Created by Simon Gardener on 08/03/2019.
//  Copyright © 2019 Simon Gardener. All rights reserved.
//

import UIKit

class BasicWithHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureWith(header : String, info : String){
        self.header.text = header
        label.text = info
    }
}
