//
//  SwitcherTableViewCell.swift
//  Splash
//
//  Created by Simon Gardener on 05/03/2019.
//  Copyright © 2019 Simon Gardener. All rights reserved.
//

import UIKit

class SwitcherTableViewCell: UITableViewCell {

   
    @IBOutlet weak var markSwitch: UISwitch!
    @IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    func configureWith(_ thing: String, bool: Bool) {
        markSwitch.isOn = bool
        label.text = "is one of my \(thing)"
    }
}
