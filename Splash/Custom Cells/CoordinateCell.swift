//
//  CoordinateCell.swift
//  Splash
//
//  Created by Simon Gardener on 10/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class CoordinateCell: UITableViewCell {

    @IBOutlet weak var latitude: UILabel!
    @IBOutlet weak var longitude: UILabel!
    @IBOutlet weak var accuracy: UILabel!
    @IBOutlet weak var accuracyLabel: UILabel!
    @IBOutlet weak var dataRecentLabel: UILabel!
    @IBOutlet weak var recent: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var accuracyStack: UIStackView!
    @IBOutlet weak var timelyStack: UIStackView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  //  configure(with viewModel)

}
