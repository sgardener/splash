//
//  PickerCell.swift
//  Splash
//
//  Created by Simon Gardener on 04/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class PickerCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var triggeringTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
