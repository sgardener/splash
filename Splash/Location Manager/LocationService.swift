//  LocationService.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 21/11/2017.
//  Copyright © 2017 Simon Gardener. All rights reserved.


import Foundation
import CoreLocation
import UIKit

typealias Seconds = Double

class LocationService: NSObject, CLLocationManagerDelegate {
    
    let manager: CLLocationManager
    weak var owner : (UIViewController & LocationUser)?
    let within : Seconds
    var requestCount = 0
    
    init(owner: UIViewController & LocationUser, desiredAccuracy: CLLocationAccuracy, within: Seconds ){
    
        manager = CLLocationManager()
        self.owner = owner
        self.within = within
        super.init()
        manager.delegate = self
        manager.desiredAccuracy = desiredAccuracy
        requestCount = 0
    }
    
    fileprivate func checkLocationServicesEnabled() {
        if !CLLocationManager.locationServicesEnabled(){
            pushUserToTurnLocationServicesOn(message: "Please enable Location Services, Its currently off.\nMany features of this app require your location.\n\nGo to Settings -> Privacy -> Location Services")
        }else {
            checkUserHadntPreviouslyDeniedLocationUse()
        }
        
    }
    
    fileprivate func checkUserHadntPreviouslyDeniedLocationUse() {
        if CLLocationManager.authorizationStatus() == .denied {
            owner?.locationWasDenied()
  //          pushUserToTurnLocationServicesOn(message: " You previous refused access to location. To grant permission you will need to go to Settings -> Privacy-> Location Services, scroll down and select Splash. Chose the 'While Using the App' option.")
        }else {
            checkIfUserHasBeenAskedForPermission()
        }
    }
    
    fileprivate func checkIfUserHasBeenAskedForPermission() {
        if CLLocationManager.authorizationStatus() == .notDetermined {
            manager.requestWhenInUseAuthorization()
        }else {
            manager.requestLocation()
        }
    }
    
    func requestLocation(){
        checkLocationServicesEnabled()
//        checkUserHadntPreviouslyDeniedLocationUse()
//        checkIfUserHasBeenAskedForPermission()
//        manager.requestLocation()
    }
    func stop(){
        manager.stopUpdatingLocation()
    }
    
    func pushUserToTurnLocationServicesOn(message: String){
        let noGPSPermissionAlert = UIAlertController.init(title: "Location Permission Needed", message: message, preferredStyle: .alert)
        
        let understood = UIAlertAction.init(title: "I understand.", style: .default, handler: nil)
//        let goToSettingsAction = UIAlertAction.init(title: "Go", style: .default, handler: {(alertAction) in
//            UIApplication.shared.open(URL(string:"App-Prefs:root=Privacy&path=LOCATION")!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]),
//                                      completionHandler: nil)
//        })
       // noGPSPermissionAlert.addAction(goToSettingsAction)
        noGPSPermissionAlert.addAction(understood)
        owner?.present(noGPSPermissionAlert, animated: true, completion: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            let elapsed = Date().timeIntervalSince(location.timestamp)
            if (elapsed <= within) && (location.horizontalAccuracy <= manager.desiredAccuracy) {
                manager.stopUpdatingLocation()
                performUIUpdatesOnMain {
                    self.owner?.locationDidUpdate(location: location)
                }
            }else {
                self.owner?.partialFix(location: location)
                requestCount += 1
                manager.requestLocation()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("location manager failed with error: \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            manager.requestLocation()
        }
        
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
