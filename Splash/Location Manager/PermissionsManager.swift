//
//  PermissionsManager.swift
//  Splash
//
//  Created by Simon Gardener on 16/10/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
import CoreLocation


class PermissionsManager {
    // needed a way to call location permission from BLTNBoard. This PM.shared allows the pop up to stay as the BLTNPage disappears to be replaced by the next card.
    
    static let shared = PermissionsManager()
    
    let locationManager = CLLocationManager()
    
    func requestWhenInUseLocation() {
        locationManager.requestWhenInUseAuthorization()
    }
    
}
