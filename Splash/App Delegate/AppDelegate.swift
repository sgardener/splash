//
//  AppDelegate.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 31/10/2017.
//  Copyright © 2017 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
    // need to increment seedVersion everytime we included an updated sqlite database

    var window: UIWindow?

    var dataModel = DataModel()
    
 
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setUserDefaults()
       // printStoreAndDirectoryLocations()
        let tbc = self.window?.rootViewController as! SplashTabBarController
        tbc.inject(dataModel)
        updateIfNeeded()
        return true
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        ensureNoBlankNamesOrCountry()
        dataModel.saveContext()
    }
    func ensureNoBlankNamesOrCountry(){
        if let facility = dataModel.facility {
            if facility.name == nil || facility.name?.isEmpty == true {
                facility.name = "Unknown"
            }
            if facility.country == nil || facility.country?.isEmpty == true {
                facility.country = "- Unknown -"
            }
        }
        if let site = dataModel.site {
            if site.name == nil || site.name?.isEmpty == true {
                site.name = "Unknown"
            }
            if site.country == nil || site.country?.isEmpty == true {
                site.country = "- Unknown -"
            }
        }
    }
    
    fileprivate func setUserDefaults() {
        if UserDefaults.haveBeenSet() == false {
            UserDefaults.setSeedVersion(1)
            UserDefaults.setAllMeasurement(units: .metric)
            UserDefaults.setBuddyAutofill(value: 2)
            UserDefaults.setClientDiverIsStudent(to: true)
            UserDefaults.setNickNameIsDisplayName(to: true)
            UserDefaults.setOtherAutofillItems(value: 1)
            UserDefaults.setRegardLastDiveAsMostRecentlyAdded(to: false)
            UserDefaults.setHaveBeenSet(to: true)
            UserDefaults.setOnboarded(to: false)
        }
    }
    
    fileprivate func printStoreAndDirectoryLocations() {
        let sqlitePath = Bundle.main.path(forResource: "SplashDiveLog", ofType: "sqlite") // used for logging
        print("""
            Preloaded store initial location: \(String(describing: sqlitePath))
            ****
            """ )
        print("""
            temporary directory location: \(SeedPersistentContainer.defaultDirectoryURL())
            *****
            """)
        print("""
            Normal Core Data store directory location: \(NSPersistentContainer.defaultDirectoryURL())
            *****
            """)
    }
    
    fileprivate func updateIfNeeded() {
        if ModelUpdater.updateIsNeeded() {
            TemporaryFileRemover.removeSqlFiles()
            let updater = ModelUpdater(dataModel)
            updater.update()

        }
    }
    
}

let solitaryCellTypeID = "cell"
