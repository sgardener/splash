//  TripDetails.swift
//  SplashSwift
//
//  Created by Simon Gardener on 10/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.

import UIKit

class TripDetails: UITableViewController {

    private let numberOfSections = 1
    private let rowsPerSection = 1
 
    let cellId = "cell"
    var mode : Mode!
    var trip: Trip!
    var dataModel : DataModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        setUpMode()
    }

    func setUpMode(){
        guard mode == .add else { return }
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel , target: self, action: #selector(cancelButtonPressed))
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonPressed))
        navigationItem.leftBarButtonItem = cancelButton
        navigationItem.rightBarButtonItem = doneButton
    }
    
    @objc private func cancelButtonPressed(){
        if let context = trip.managedObjectContext{
            context.delete(trip)
            presentingViewController?.dismiss(animated: true, completion: nil)
        }
        
    }
    
    @objc private func doneButtonPressed(){
        
        resignTextField()
        if trip.name == nil {
            showAlert(withTitle: "Missing Name", message: "A trip must have a name")
            if let cell =  tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? TextFieldTableViewCell {
                cell.textField?.becomeFirstResponder()
            }
        } else {
            presentingViewController?.dismiss(animated: true, completion: nil)
        }
    }
    func resignTextField(){
        if  let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? TextFieldTableViewCell {
            cell.textField.resignFirstResponder()
        }
        
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowsPerSection
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = (tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? TextFieldTableViewCell) else { fatalError("wrong type of cell for tripdetail cell")}
        
        cell.textField.text = trip.name
        cell.textField
            .becomeFirstResponder()
        
        return cell
    }
    //MARK:- Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let cell = tableView.cellForRow(at: indexPath) as? TextFieldTableViewCell, cell.textField.isFirstResponder == false else {return}
        cell.textField.becomeFirstResponder()
    }
}

extension TripDetails : Injectable {
    
    func inject(_ injected: (dataModel: DataModel, trip: Trip, title: String, mode: Mode)) {
        dataModel = injected.dataModel
        trip = injected.trip
        title = injected.title
        mode = injected.mode
    }
    
    func assertDependencies() {
        assert(dataModel != nil, "didn't get a datamodel")
        assert(trip != nil, "didn't get a trip")
        assert(mode != nil , "didn't get a mode")
        assert(title != nil, "didn't get a title")
    }
    
    typealias T = (dataModel: DataModel, trip: Trip, title: String, mode: Mode)
}

extension TripDetails : UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines ).capitalized
        if textField.text == ""  {
            showAlert(withTitle: "No Name", message: "Your trip will need a name!")
            textField.text = trip.name
        } else {
            trip.name = textField.text
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

