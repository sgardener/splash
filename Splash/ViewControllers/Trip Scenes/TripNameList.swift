//
//  TripNameList.swift
//  Splash
//
//  Created by Simon Gardener on 11/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class TripNameList: UIViewController, SegueHandlerType, NeedsDataModel {
    var dataModel: DataModel!
    
    
    enum Identifier: String {
        case addTrip
        case showTrip
    }
    var container: NSPersistentContainer!
    
    @IBOutlet var tripView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var context : NSManagedObjectContext!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        container = dataModel.container
        context = container.viewContext
        title = "Trips"
        setUpView()
        setUpNavBar()
        fetchTrips()
        updateView()
        
    }
    fileprivate func setUpNavBar(){
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    private func setUpView(){
        setUpMessageLabel()
        setUpTableView()
    }
    fileprivate func updateView() {
        tableView.isHidden = !hasTrips
        messageLabel.isHidden = hasTrips
    }
    private func setUpMessageLabel(){
        messageLabel.text = "Click the '+' button up top to add a trip."
    }
    private func setUpTableView() {
        tableView.tableFooterView = UIView()
    }
    private var hasTrips: Bool  {
        guard let fetchedTrips = frc.fetchedObjects else { return false }
        return fetchedTrips.count > 0
    }
    
    private func fetchTrips(){
        do{
            try frc.performFetch()
        }catch{
            print("\(error), \(error.localizedDescription)")
        }
    }
    
    fileprivate lazy var frc : NSFetchedResultsController<Trip> = {
        let tripRequest : NSFetchRequest<Trip>  = Trip.fetchRequest()
        tripRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(Trip.name), ascending: true )]
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: tripRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = self
        return fetchedResultsController
    }()
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let vc = segue.actualDestination() as? TripDetails else {fatalError("Wrong type of  viewcontroller - should be TripsDetail")}
        let segueId = segueIdentifierFor(segue: segue)
        
        switch segueId {
        case .addTrip:
            let trip = Trip(context: context)
            vc.inject((dataModel: dataModel, trip:trip, title: "Add Trip", mode: .add))
            
        case .showTrip :
            let trip = frc.object(at: tableView.indexPathForSelectedRow!)
            vc.inject((dataModel: dataModel, trip:trip, title: "Trip Name", mode: .show))
        }
    }
}
extension TripNameList : NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
        updateView()
    }
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        case .update:
            if let indexPath = indexPath  {
                tableView.reloadRows(at: [indexPath], with: .automatic)
            }
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .fade)
            }
        @unknown default:
            print("Futureproof: NSFetchedResultscontroller didChange - and has added a new enum type")
        }
    }
    
}

extension TripNameList : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sections = frc.sections else {return 0}
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = frc.sections?[section] else { return 0 }
        return section.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        configure(cell, at: indexPath)
        return cell
    }
    
    func configure(_ cell: UITableViewCell, at indexPath: IndexPath) {
        let trip = frc.object(at: indexPath)
        cell.textLabel?.text = trip.name
        
    }
}
extension TripNameList : UITableViewDelegate {
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let trip = frc.object(at: indexPath)
        if (trip.dive?.count == 0 ) {
            return true
        } else {
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete", handler: {action,indexPath in
            let tripToDelete = self.frc.object(at: indexPath)
            self.context.delete(tripToDelete)
        })
        return [deleteAction]
    }
}
extension TripNameList: Injectable  {
    
    func inject(_ dm: DataModel) {
        dataModel = dm
    }
    
    func assertDependencies() {
        assert(dataModel != nil, "Bugger, its nil")
    }
    
}
