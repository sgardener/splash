//
//  HomeViewController.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 07/11/2017.
//  Copyright © 2017 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import Solar
import MessageUI
import BLTNBoard

class HomeViewController: UIViewController, LocationUser, NeedsDataModel {
    
    lazy var bulletingManager : BLTNItemManager = {
        return BLTNItemManager.init(rootItem: BulletinDataSource.locationWasDeniedPage())
    }()
    
    
    func locationWasDenied() {
        bulletinManager.showBulletin(above: self)
    }
    
    func partialFix(location: CLLocation) {
        
    }
    
    var locationService: LocationService!
    var dataModel: DataModel!
    
    let settingsSegueId = "settings"
    let titleKey = "title"
    let logEntryNavCon = "LogEntryNavCon"
    var locationSevice: LocationService!
    var container: NSPersistentContainer!
    
    
    
    //MARK: Home Screen Labels
    @IBOutlet weak var mostRecentDateLabel: UILabel!
    @IBOutlet weak var mostRecentSiteLabel: UILabel!
    @IBOutlet weak var mostRecentSiteLocationLabel: UILabel!
    @IBOutlet weak var lastDiveLabel: UILabel!
    @IBOutlet weak var divesLoggedLabel: UILabel!
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var sunsetLabel: UILabel!
    
    
    @IBOutlet weak var noDataYetLabel: UILabel!
    
    @IBOutlet weak var submitDataLabel: UIButton!
    
    lazy var bulletinManager: BLTNItemManager = {
        var page = BulletinDataSource.welcomePage()
        return BLTNItemManager(rootItem: page)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        container = dataModel.container
        setUpNavBar()
        onboard()
        locationSevice = LocationService(owner: self, desiredAccuracy: kCLLocationAccuracyKilometer, within: 600)
        
    }
    fileprivate func onboard(){
        if UserDefaults.hasOnboarded() == false {
            bulletinManager.showBulletin(above: self)
            UserDefaults.setOnboarded(to: true)
        }
    }
    fileprivate  func setUpNavBar(){
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse { locationSevice.requestLocation() }
        updateLogBookDataDisplay()
        // logDataVersion()
    }
    @IBAction func LogDive(_ sender: UIButton) {
        if let index = indexForLogBookTab() {
            UserDefaults.setTriggerDive(bool: true)
            tabBarController?.selectedIndex = index
        }
    }
//
//    private func setAllMaxVizto51(){
//        let predicate = NSPredicate(format:"highSeasonVisibilityRangeHigh > %D || lowSeasonVisibilityRangeHigh >%D",50.0, 50.0)
//
//        let allSites = dataModel.fetchEntity(DiveSite.self, predicate: predicate)
//        let all =  dataModel.countEntity(DiveSite.self)
//        print("all sites count= \(all)")
//        for site in allSites {
//            if site.lowSeasonVisibilityRangeHigh > 50.0{
//                site.lowSeasonVisibilityRangeHigh = 51.0
//            }
//            if site.highSeasonVisibilityRangeHigh > 50.0 {
//                site.highSeasonVisibilityRangeHigh = 51.0
//            }
//        }
//    }
    private func indexForLogBookTab()-> Int? {
        if let vcs = tabBarController?.viewControllers{
            let index = vcs.enumerated().filter{(offset:Int, vc:UIViewController)in
                vc.title == logEntryNavCon}.map{$0.offset}
            return index.first
        }
        return nil
    }
    

    fileprivate func logDataVersion(){
        let versionNumber = dataModel.dataVersion()
        print("WORKING DATA STORE version \(versionNumber)")
    }

    fileprivate func updateLogBookDataDisplay(){
        showOrHideSubmitButton()
        let numberOfLoggedDives = dataModel.highestNumberedLogEntry()
        showNeededLogDetails(for: numberOfLoggedDives)
        show(numberOfLoggedDives)
        showLogDetails()
        showOrHideSubmitButton()
    }
    fileprivate func showOrHideSubmitButton(){
        
        if dataModel.numberOfItemsNeedingSubmitting() > 0 {
            submitDataLabel.isHidden = false
        } else {
            submitDataLabel.isHidden = true
        }
    }
    fileprivate func show(_ numberOfLoggedDives: Int) {
        switch numberOfLoggedDives {
      
        case 1:
            divesLoggedLabel.text = "1 dive logged"
        default:
            divesLoggedLabel.text = "\(String(dataModel.highestNumberedLogEntry())) dives logged"
        }

    }
    
    fileprivate func showLogDetails() {
        if let logEntry = dataModel.mostRecentDive() {
            setLabel(hidden: false)
            mostRecentSiteLabel.text = logEntry.diveSiteName ?? "missing site name"
            mostRecentDateLabel.text = Date.formattedDate(logEntry.timeIn!, timeZone: logEntry.timeZone!)
            mostRecentSiteLocationLabel.text = LocationDescriptions.locationStringFor(logEntry)
        }else {
            setLabel(hidden: true)
            noDataYetLabel.text = """
            No dives logged yet.
            
            When dives are logged,
            most recent dive data
            will appear here.
            """
        }
    }
    fileprivate func setLabel(hidden bool: Bool){
        noDataYetLabel.isHidden = !bool
        mostRecentDateLabel.isHidden = bool
        mostRecentSiteLabel.isHidden = bool
        mostRecentSiteLocationLabel.isHidden = bool
        lastDiveLabel.isHidden = bool
        divesLoggedLabel.isHidden = bool
    }

    fileprivate func showNeededLogDetails(for number: Int){
        if number == 0 {
            setTextFieldsHidden(to: true)
        }else {
            setTextFieldsHidden(to: false)
        }
    }

    fileprivate func setTextFieldsHidden(to hiddenBool :Bool) {
        lastDiveLabel.isHidden = hiddenBool
        mostRecentSiteLabel.isHidden = hiddenBool
        mostRecentDateLabel.isHidden = hiddenBool
        mostRecentSiteLocationLabel.isHidden = hiddenBool
        divesLoggedLabel.isHidden  = hiddenBool
    }
    
    //MARK:- location updated
    
    func locationDidUpdate(location: CLLocation){
        let solar = Solar(coordinate: location.coordinate)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        if let sunrise = solar?.sunrise{
            sunriseLabel.text = dateFormatter.string(from: sunrise)
        }
        if let sunset = solar?.sunset {
            sunsetLabel.text = dateFormatter.string(from: sunset)
        }
    }
    
    @IBAction func submittedDataTapped(_ sender: Any) {
        let dataSubmitter = Submitter()
        dataSubmitter.dataModel = dataModel
        let mailComposer = dataSubmitter.prepareCSVfiles()
        mailComposer.mailComposeDelegate = self
        
        present(mailComposer, animated: true, completion: nil)
    }
    //MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == settingsSegueId {
            if let vc = segue.actualDestination() as? SettingsListTableViewController {
                vc.inject(dataModel) 
            }
        }
    }
    @IBAction func shareButtonTapped(_ sender: Any) {
        print("Share tapped")
        let items = ["Try out Splash Dive Log, the location aware logbook for the iPhone", "https://itunes.apple.com/gb/app/splash-dive-log/id1455772941?mt=8"]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present (ac, animated: true)
    }
}


extension HomeViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if result == .sent{
            dataModel.resetSitesAfterSending()
            dataModel.resetFacilitiesAfterSending()
            dataModel.removeAllConfirmedValueObjects()
            dataModel.saveContext()
            showOrHideSubmitButton()
        }
        dismiss(animated: true , completion: nil)
    }
}
extension HomeViewController: Injectable {
    func inject(_ dm: DataModel) {
        dataModel = dm
    }
    
    func assertDependencies() {
        assert(dataModel != nil, "Bugger, its nil")
    }
    
}
