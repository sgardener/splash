//
//  BoatDetails.swift
//  SplashSwift
//
//  Created by Simon Gardener on 07/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData


class BoatDetails: UITableViewController {
 
    private let boatTypeSegue = "boatTypeChooser"
    private let numberOfSections = 2
    private let rowsPerSection = 1
    
    private let nameSection = 0
    private let boatTypeSection = 1
    
    private let textCellIdentifier = "textCell"
    private let boatTypeCellIdentifier = "cell"
    
    var dataModel: DataModel!
    var mode : Mode!
    var boat: DiveBoat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        setUpMode()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let ip = IndexPath(row: 0, section: 1)
        tableView.reloadRows(at: [ip], with: .automatic)
    }
    func setUpMode(){
        
        guard mode == .add else { return }
        
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel , target: self, action: #selector(cancelButtonPressed))
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonPressed))
        navigationItem.leftBarButtonItem = cancelButton
        navigationItem.rightBarButtonItem = doneButton
        
        
        
    }
    @objc private func cancelButtonPressed(){
        if let context = boat.managedObjectContext{
            context.delete(boat)
            presentingViewController?.dismiss(animated: true, completion: nil)
        }
        
    }
    @objc private func doneButtonPressed(){
        if let cell =  tableView.cellForRow(at: IndexPath(row: 0, section: nameSection)) as? TextFieldTableViewCell {
            cell.textField.resignFirstResponder()
        }
        if boat.name == nil {
            showAlert(withTitle: "Missing Name", message: "A boat must have a name")
            if let cell =  tableView.cellForRow(at: IndexPath(row: 0, section: nameSection)) as? TextFieldTableViewCell {
                cell.textField.becomeFirstResponder()   
            }
        } else {
            presentingViewController?.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowsPerSection
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        
        switch indexPath.section {
        case nameSection:
            guard let boatNameCell = (tableView.dequeueReusableCell(withIdentifier: textCellIdentifier    , for: indexPath) as? TextFieldTableViewCell) else { fatalError("Should have got a textFieldCell here")}
            
            boatNameCell.textField.text = boat.name
            if mode == .add {
                boatNameCell.textField.becomeFirstResponder()
            }
            cell = boatNameCell
        case boatTypeSection:
            cell = tableView.dequeueReusableCell(withIdentifier:"cell", for: indexPath)
            cell.textLabel?.text =  "Boat type: \(boat.type?.type  ?? "")"
        default:
            fatalError("Shouldnt be another type of cell on BoatDetail tableView")
            break
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case nameSection:
            return "NAME"
        case boatTypeSection:
            return "BOAT TYPE"
        default:
            fatalError("default section should not be called")
        }
    }
    //MARK: - TableView Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.section{
            
        case nameSection:
            
            guard indexPath.section == nameSection, let cell = tableView.cellForRow(at: indexPath) as? TextFieldTableViewCell, cell.textField.isFirstResponder == false else {return}
            cell.textField.becomeFirstResponder()
            
            
            
        default: break
        }
    }
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == boatTypeSegue {
        guard let vc = segue.destination as? BoatTypeChooser else{ fatalError("Should have been a BoatTypeChooser")}
            
        vc.inject(boat)
        }
    }
}

extension BoatDetails : UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines ).capitalized
        
        if textField.text == ""  {
            showAlert(withTitle: "No Name", message: "Your boat will need a name!")
            textField.text = boat.name
        } else {
            boat.name = textField.text
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension BoatDetails : Injectable {
    func inject(_ injected: (dataModel: DataModel, boat: DiveBoat, title: String, mode: Mode)) {
        dataModel = injected.dataModel
        boat = injected.boat
        title = injected.title
        mode = injected.mode
    }
    

    
    func assertDependencies() {
        assert(dataModel != nil, "Expected a datamodel")
        assert(mode != nil, "BoatDetails vc did not get passed")
        assert(boat != nil ,"BoatDetails vc did not get passed")
        assert(title != nil,"BoatDetails vc did not get passed")
    }
    
    
    typealias T = (dataModel: DataModel,boat: DiveBoat,title: String, mode: Mode)
    
    
}
