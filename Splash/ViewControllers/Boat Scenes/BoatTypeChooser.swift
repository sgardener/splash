//
//  BoatTypeChooser.swift
//  SplashSwift
//
//  Created by Simon Gardener on 07/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class BoatTypeChooser: UITableViewController, NSFetchedResultsControllerDelegate {
    private let numberOfSections = 1
    let cellIdentifier = "cell"
    var boat: DiveBoat!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        do {
            try frc.performFetch()
        } catch {
            print("\(error), \(error.localizedDescription)")
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = frc.sections else {fatalError("no sections in FRC")}
        let secInfo = sections[section]
        return secInfo.numberOfObjects
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier , for: indexPath)
        cell.textLabel?.text = (frc.object(at:indexPath)).type
        return cell
    }
    
    // MARK: - Table view delgate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        boat.type = frc.object(at: indexPath)
        navigationController?.popViewController(animated: true)
    }
    
    lazy var frc: NSFetchedResultsController<BoatType> = {
        let fetchRequest: NSFetchRequest<BoatType> = BoatType.fetchRequest()
        
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(BoatType.displayOrder), ascending: true)]
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: boat.managedObjectContext!,
                                                                  sectionNameKeyPath: nil,
                                                                  cacheName: nil)
        
        fetchedResultsController.delegate = self
        return fetchedResultsController
    }()
}

extension BoatTypeChooser: Injectable{
    func inject(_ boat: DiveBoat) {
        self.boat = boat
    }
    
    func assertDependencies() {
        assert(boat != nil, "no boat passed in")
    }
    
    typealias T = DiveBoat
    
    
}
