//
//  DiverDetails.swift
//  Splash
//
//  Created by Simon Gardener on 20/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData
import DeviceKit

class DiverDetails: UITableViewController {
    
    var viewModel: DiverViewModel!
    let labelValueCell  = "labelValueCell"
    let proNumberId = "proNumber"
    var newPhoneAdded = false
    var newEmailAdded = false
    var newSocialAdded = false
    
    enum DiverStatusRows: Int {
        case statusOptions
        case proNumber
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        title = "Diver Details"
        setUpMode()
        viewModel.updatePhonesEmailsSocials()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    
    
    func setUpMode(){
        guard viewModel.mode == .add else {return}
        let cancelButton =  UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelNewDiver))
        let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveNewDiver))
        navigationItem.leftBarButtonItem = cancelButton
        navigationItem.rightBarButtonItem = saveButton
        
    }
    @objc func cancelNewDiver() {
        viewModel.deleteNewDiver()
        presentingViewController?.dismiss(animated: true, completion: nil)    }
    @objc func saveNewDiver(){
        viewModel.save()
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    private func  removeEmptyPairValues(){
        if newPhoneAdded == true {
            removeEmptyPhoneValues()
        }
        if newSocialAdded == true{
            removeEmptyPhoneValues()
        }
        if newEmailAdded == true {
            removeEmptyEmailValues()
        }
    }
    private func removeEmptyPhoneValues(){
        
    }
    private func removeEmptyEmailValues(){
        
    }
    private func removeEmptySocialValues() {
        
    }
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return OptionOrder.social.rawValue + 1
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionName =  optionOrderFor(position: section)
        switch sectionName{
        case .phone:
            return viewModel.phoneCount()+1
        case .email:
            return viewModel.emailCount()+1
        case .social:
            return  viewModel.socialCount()+1
        case .diverStatus:
            return viewModel.diveProStatusRowCount()
        default: return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName{
            
        case .diverID:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.diverID.rawValue, for: indexPath) as? DiverIDCell else {fatalError("Didnt get a DiverIDCell")}
            cell.configure(with: viewModel.diver()!)
            return cell
            
        case .diverStatus:
            switch indexPath.row {
            case DiverStatusRows.statusOptions.rawValue:
                
                let device = Device.current
                var identifier = Identifier.diverStatus.rawValue
                if ((device == .iPhoneSE || device == .simulator(.iPhoneSE)) && traitCollection.preferredContentSizeCategory >= .extraLarge) ||
                    traitCollection.preferredContentSizeCategory >= .extraExtraExtraLarge
                {
                    identifier = identifier + "Larger"
                }
                guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? DiverStatusCell else {fatalError("didnt get a DiverStatusCell")}
                cell.configure(with: viewModel)
                return cell
                
            case  DiverStatusRows.proNumber.rawValue:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: proNumberId, for: indexPath) as? BasicWithHeaderTableViewCell  else { fatalError("Didnt get a basic header cell")}
                cell.configureWith(header: "Dive Pro Number", info: viewModel.proNumber() != nil ? viewModel.proNumber()!: "needs pro number"  )
                return cell
            default:
                fatalError("cant be anything but but 0 or 1")
            }
            
        case .certs:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.certs.rawValue, for: indexPath) as? CertListCell else{fatalError("didnt get a certsList cell")}
            cell.configureWith(viewModel: viewModel)
            return cell
            
        case .phone:
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "plusCell", for: indexPath)
                cell.textLabel?.text = "+ phone number"
                return cell
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: labelValueCell, for: indexPath) as? LabelDataTextPairCell else {fatalError("not a labelDataTextPairCell")}
                cell.labelTextField.text = viewModel.phoneLabel(at: indexPath.row)
                cell.dataTextField.text = viewModel.phoneData(at: indexPath.row)
                placeholdersFor(cell, indexPath)
                return cell
            }
            
            
        case .email:
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "plusCell", for: indexPath)
                cell.textLabel?.text = "+ email address"
                return cell
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: labelValueCell, for: indexPath) as? LabelDataTextPairCell
                    else { fatalError("not a labelDataTextPairCell ")}
                cell.labelTextField.text = viewModel.emailLabel(at: indexPath.row)
                
                cell.dataTextField.text = viewModel.emailData(at: indexPath.row)
                
                placeholdersFor(cell, indexPath)
                return cell
            }
            
        case.social:
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "plusCell", for: indexPath)
                cell.textLabel?.text = "+ social media account"
                return cell
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: labelValueCell, for: indexPath) as? LabelDataTextPairCell else { fatalError("not a labelDataTextPairCell ")}
                cell.labelTextField.text = viewModel.socialLabel(at: indexPath.row)
                cell.dataTextField.text = viewModel.socialData(at: indexPath.row)
                placeholdersFor(cell, indexPath)
                return cell
            }
            
        }
    }
    fileprivate func placeholdersFor(_ cell: LabelDataTextPairCell, _ indexPath: IndexPath) {
        cell.labelTextField.placeholder = viewModel.placeholderForPhoneEmailSocialLabel(at: indexPath.section, offsetBy: OptionOrder.phone.rawValue)
        cell.dataTextField.placeholder = viewModel.placeholderforPhoneEmailSocialValue(at: indexPath.section, offsetBy: OptionOrder.phone.rawValue)
    }
    
    //MARK:- TableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sectionName = optionOrderFor(position: indexPath.section)
        
        switch sectionName{
            
        case .phone:
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
                viewModel.addNewPhoneNumber()
                newPhoneAdded = true
                tableView.reloadSections(IndexSet.init(integer: indexPath.section)  , with: .automatic)
            }else {
                performSegue(withIdentifier: Identifier.phone.rawValue, sender: self)
            }
        case .email:
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
                viewModel.addNewEmail()
                newEmailAdded = true
                tableView.reloadSections(IndexSet.init(integer: indexPath.section)  , with: .automatic)
            }else {
                performSegue(withIdentifier: Identifier.email.rawValue, sender: self)
            }
        case .social:
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
                viewModel.addNewSocial()
                newSocialAdded = true
                tableView.reloadSections(IndexSet.init(integer: indexPath.section)  , with: .automatic)
            }else {
                performSegue(withIdentifier: Identifier.social.rawValue, sender: self)
            }
            
        case .diverID, .diverStatus, .certs:
            break
        }
    }
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let sectionName = optionOrderFor(position: indexPath.section)
        
        switch sectionName{
        case .phone, .email, .social :
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
                return false
            }else {
                return true
            }
        default: return false
        }
    }
    
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete", handler: {action,indexPath in
            let alertController = UIAlertController(title: "Confirm Delete", message: "Are you sure you want to delete this contact data?", preferredStyle: .actionSheet )
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {action in
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
            })
            let goAheadAction = UIAlertAction(title: "Yes", style: .destructive, handler: {(action) in
                self.viewModel.removeContactDataObject(at: indexPath, offsetBy: OptionOrder.phone.rawValue)
                
                self.tableView.reloadSections(IndexSet.init(integer: indexPath.section), with: .automatic)
                
            })
            
            alertController.addAction(goAheadAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        })
        return [deleteAction]
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let segueID = segueIdentifierFor(segue: segue)
        switch segueID {
        case .diverID:
            guard let vc = segue.actualDestination() as? DiverIDDetails else { fatalError("Didnt get a DiverIDDetails Scene") }
            vc.inject(viewModel)
            
        case .diverStatus:
            guard let vc = segue.actualDestination() as? DiverStatusOptions else {fatalError("didnt get a Diver Status Option Scene")}
            vc.inject(viewModel)
            
        case .certs:
            guard let vc = segue.actualDestination() as? CertificationsHeld else {fatalError("Expected a certsHeld scene")}
            vc.inject(viewModel.dataModel)
        case .phone:
            let indexPath = tableView.indexPathForSelectedRow!
            let object = viewModel.phones[indexPath.row]
            let type = "Phone Number"
            guard let vc = segue.actualDestination() as? DiverESPEditor else{ fatalError()}
            vc.inject((object, type))
        case .email:
            let indexPath = tableView.indexPathForSelectedRow!
            let object = viewModel.emails[indexPath.row]
            let type = "Email Address"
            guard let vc = segue.actualDestination() as? DiverESPEditor else{ fatalError()}
            vc.inject((object, type))
            break
        case .social:
            let indexPath = tableView.indexPathForSelectedRow!
            let object = viewModel.socials[indexPath.row]
            let type = "Social Media"
            guard let vc = segue.actualDestination() as? DiverESPEditor else{ fatalError()}
            vc.inject((object, type))
        case .proNumber:
            guard let vc = segue.destination as? ProNumberTableViewController else { fatalError()}
            vc.inject(viewModel)
        }
    }
    
}

extension DiverDetails : Injectable {
    func inject(_ vm: DiverViewModel) {
        viewModel = vm
    }
    
    func assertDependencies() {
        assert(viewModel != nil , "didnt get a viewModel")
    }
    
    typealias T = DiverViewModel
    
    
}

extension DiverDetails :  SegueHandlerType, OptionOrder {
    enum Identifier: String {
        case diverID, diverStatus, proNumber, certs, phone, email, social
    }
    
    enum OptionOrder: Int {
        case diverID, diverStatus, certs, phone, email, social
    }
}

