//
//  clientDivers.swift
//  Splash
//
//  Created by Simon Gardener on 17/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit


class ClientDiversList: DiversListsBaseVC{
   
    override func viewDidLoad() {
        frc = viewModel.frcForClients() 
        super.viewDidLoad()
        title = "Students & Guided"
    }
    override func addDiver(){
       super.addDiver()
    }
    
    override func addNewDiverWithDetailsFrom(nameElements: (first: String, middle: [String], last: String)?) {
        super.addNewDiverWithDetailsFrom(nameElements: nameElements)
        viewModel.setClientStatus()
    }
    override func prepareForAddDiver(_ vc: DiverDetails) {
        viewModel.addNewDiver()
        viewModel.setClientStatus()
        viewModel.mode = .add
        vc.inject(viewModel)
    }
}
