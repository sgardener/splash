//
//  ProNumberTableViewController.swift
//  Splash
//
//  Created by Simon Gardener on 12/04/2019.
//  Copyright © 2019 Simon Gardener. All rights reserved.
//

import UIKit

class ProNumberTableViewController: UITableViewController {
    
    var viewModel : DiverViewModel!
    let cellId = "cell"
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        title = "Dive Pro Number"
        tableView.tableFooterView = UIView()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? TextFieldTableViewCell else { fatalError()}
        
        cell.textField.text = viewModel.diver()?.proNumber
        return cell
    }
    
}
extension ProNumberTableViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if let text = textField.text?.trimmingCharacters(in: .whitespaces) , !text.isEmpty {
            viewModel.diver()?.proNumber = text
        } else {
            viewModel.diver()?.proNumber = nil
        }
    }
}

extension ProNumberTableViewController: Injectable {
    func inject(_ vm: DiverViewModel) {
        viewModel = vm
    }
    
    func assertDependencies() {
        assert(viewModel != nil, "")
    }
    
    typealias T = DiverViewModel
}
