//
//  DiverSummaryCell.swift
//  Splash
//
//  Created by Simon Gardener on 17/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class DiverSummaryCell: UITableViewCell {

    @IBOutlet weak var diverNameLabel: UILabel!
    @IBOutlet weak var proLabel: UILabel!
    @IBOutlet weak var buddyLabel: UILabel!
    @IBOutlet weak var clientLabel: UILabel!
    
    @IBOutlet weak var diverTypeStackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(with diver:Diver){
        diverNameLabel.text = StringWranglers.nameFor(diver)
        if diver.clientStatus > 1 {
            clientLabel.textColor = .black
        }else {
            clientLabel.textColor = UIColor.lightGray
        }
        
        if diver.buddyStatus > 1 {
            buddyLabel.textColor = .black
        }else{
            buddyLabel.textColor = .lightGray
        }
        
        if diver.proStatus > 1 {
            proLabel.textColor = .black
        }else{
            proLabel.textColor = .lightGray
        }
        
        if diverTypeStackView != nil {
            if traitCollection.preferredContentSizeCategory < .accessibilityLarge {
                diverTypeStackView.axis = .horizontal
                diverTypeStackView.alignment = .fill
                diverTypeStackView.distribution = .fillProportionally
            } else {
                diverTypeStackView.axis = .vertical
                
                diverTypeStackView.alignment = .leading
                diverTypeStackView.distribution = .fillProportionally
            }
        }
    }
}
