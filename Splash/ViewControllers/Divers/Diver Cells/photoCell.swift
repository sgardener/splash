//
//  photoCell.swift
//  Splash
//
//  Created by Simon Gardener on 21/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class photoCell: UITableViewCell {

    @IBOutlet weak var diverImage: RoundableImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configure(with viewModel:DiverViewModel){
        guard let image = viewModel.diverImage() else { return }
        diverImage.image = image
    
    }
}
