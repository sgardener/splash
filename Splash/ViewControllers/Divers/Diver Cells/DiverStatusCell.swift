//
//  DiverStatusCell.swift
//  Splash
//
//  Created by Simon Gardener on 20/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class DiverStatusCell: UITableViewCell {

    @IBOutlet weak var buddyLabel: UILabel!
    @IBOutlet weak var instructorLabel: UILabel!
    @IBOutlet weak var guideLabel: UILabel!
    @IBOutlet weak var studentLabel: UILabel!
    @IBOutlet weak var guidedDiverLabel: UILabel!
    
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configure(with vm: DiverViewModel){
        buddyLabel.textColor = vm.buddyLabelColour()
        instructorLabel.textColor = vm.instructorLabelColour()
        guideLabel.textColor = vm.guideLabelColour()
        studentLabel.textColor = vm.studentLabelColour()
        guidedDiverLabel.textColor = vm.guidedDiverLabelColour()
        
        buddyLabel.text = vm.typeOfBuddy()

        
    }
}
