//
//  DiverIDCell.swift
//  Splash
//
//  Created by Simon Gardener on 20/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class DiverIDCell: UITableViewCell {

//    @IBOutlet weak var givenNameTextField: UITextField!
//    @IBOutlet weak var familyNameTextField: UITextField!
//    @IBOutlet weak var nickNameTextField: UITextField!
    @IBOutlet weak var diverImage: UIImageView!
    
    @IBOutlet weak var givenNameLabel: UILabel!
    @IBOutlet weak var familyNameLabel: UILabel!
    @IBOutlet weak var nickNameLabel: UILabel!
    
    let givenPlaceholder = "given name"
    let familyPlaceholder = "family name"
    let nickPlaceholder = "nickname"
    
    let placeHolderColor = UIColor.lightGray
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
//    func configureOld(with diver: Diver){
//        givenNameTextField.text = diver.givenName
//        familyNameTextField.text = diver.familyName
//        nickNameTextField.text = diver.nickName
//
//        configurePhoto(with:diver)
//    }
    func configure(with diver: Diver){
        if let given = diver.givenName{
            set(givenNameLabel, with: given)
        }else { set(givenNameLabel, with: givenPlaceholder, textColor: placeHolderColor)
        }
        
        if let family = diver.familyName {
            set(familyNameLabel, with: family)
        } else {
            set(familyNameLabel, with: familyPlaceholder, textColor: placeHolderColor)
        }
        if let nick = diver.nickName{
            set(nickNameLabel, with: nick)
        }else {
            set(nickNameLabel, with: nickPlaceholder, textColor: placeHolderColor)
        }
        
        configurePhoto(with:diver)
    }
    fileprivate func configurePhoto(with diver: Diver){
        if let photo = diver.photo {
            diverImage.image = UIImage(data: photo)
        }
    }
    
    fileprivate func set(_ label: UILabel, with text: String, textColor : UIColor? = .darkText ){
        label.text = text
        label.textColor = textColor
    }
}


