//
//  DiverESPEditor.swift
//  Splash
//
//  Created by Simon Gardener on 08/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class DiverESPEditor: UITableViewController {
    
    var dataItem : ESPPair!
    var type: String!
    var header : String!
    
    let labelTag = 0
    let valueTag = 1
    let cellId = "labelValueCell"
    override func viewDidLoad() {
        super.viewDidLoad()
        title = type
        tableView.tableFooterView = UIView()
    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? LabelDataTextPairCell else { fatalError()}
        
        cell.labelTextField.text = dataItem.label
        cell.labelTextField.tag = labelTag
        cell.dataTextField.text = dataItem.value
        cell.dataTextField.tag = valueTag
        return cell
    }
}
extension DiverESPEditor : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = textField.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ."))
        
        if let theText = textField.text{
            if textField.tag == labelTag {
                dataItem.label = theText.isEmpty ? nil:theText
            }else {
                dataItem.value = theText.isEmpty ? nil:theText
                
            }
        }
    }
}

extension DiverESPEditor: Injectable  {
    func inject(_ injected: (item: ESPPair, type:String)) {
        dataItem = injected.item
        type = injected.type
        
    }
    
    func assertDependencies() {
        assert ( dataItem != nil, "dataItem is nil")
        assert(type != nil)
    }
    
    typealias T = (item:ESPPair, type:String)
    
    
    
}
