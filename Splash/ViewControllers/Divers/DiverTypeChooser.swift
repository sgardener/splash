//
//  DiverTypeChooser.swift
//  Splash
//
//  Created by Simon Gardener on 17/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class DiverTypeChooser: UITableViewController, NeedsDataModel {

    var container: NSPersistentContainer!
    var dataModel: DataModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavBar()
        tableView.tableFooterView = UIView()

    }
    func setUpNavBar(){
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        guard let vc = segue.actualDestination() as? DiversListsBaseVC else {fatalError("expecting a dlbvc subclass - didnt get one")}
        let viewModel = DiverViewModel.init(with: dataModel)
        vc.inject(viewModel)
    }
 
}
extension DiverTypeChooser: SegueHandlerType    {
    enum Identifier : String {
        case allDivers, clients, pros, buddies
    }
}
extension DiverTypeChooser: Injectable {
    func inject(_ injection: ((dataModel: DataModel, pContainer: NSPersistentContainer))) {
        dataModel = injection.dataModel
        container = injection.pContainer
    }
 
    func assertDependencies() {
        assert(dataModel != nil, "Expecting a dataModel. didnt get one")
        assert(container != nil, "Expecting a container. didnt get one")
    }
    
    typealias T = (dataModel:DataModel, pContainer: NSPersistentContainer)

    
    
    
}
