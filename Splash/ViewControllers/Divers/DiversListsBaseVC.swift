//  DiversListsBaseVC.swift
//  Splash
//
//  Created by Simon Gardener on 18/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.

import UIKit
import CoreData
import Contacts
import ContactsUI

class DiversListsBaseVC: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    var cellID = "diverSummary"
    var viewModel: DiverViewModel!
    var frc: NSFetchedResultsController<Diver>!
    let showDiverSegue = "showDiver"
    let addDiverSegue = "addDiver"
    var selectedContact : CNContact?
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
   
    //MARK:- Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()

        assertDependencies()
        setUpAddButton()
        frc.delegate = self
        fetchDivers()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateView()
    }
    override func viewDidAppear(_ animated: Bool) {
        dealWithSelectedContact()
    }
    func setUpAddButton(){
        let barButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addDiver))
        navigationItem.rightBarButtonItem = barButton
    }
    @objc func addDiver(){
        
        let whatKindOfAddition =  UIAlertController(title: "Add Diver", message: "How would you like to add diver(s)?", preferredStyle: .actionSheet )
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let addNewDiver = UIAlertAction(title: "Create new diver", style: .default, handler: {(alertAction) in
            self.performSegue(withIdentifier: self.addDiverSegue, sender: self)
        })
        let selectFromContacts = UIAlertAction(title: "Select from contacts", style: .default , handler: {(alertAction) in
            self.showContactPickerForSinglePick()})
      //  let selectGroupFromContacts =  UIAlertAction(title: "Select Group from contacts", style: .default , handler: {(alertAction) in     })
        
        whatKindOfAddition.addAction(selectFromContacts)
        whatKindOfAddition.addAction(addNewDiver)
        
        whatKindOfAddition.addAction(cancelAction)
        present(whatKindOfAddition, animated: true, completion: nil)
    }
    
    func showContactPickerForSinglePick(){
        let contactPicker = CNContactPickerViewController.init()
        contactPicker.delegate = self
        present(contactPicker, animated: true, completion: nil)
    }
    
    func updateView() {
        tableView.isHidden = !hasDivers()
        messageLabel.isHidden = hasDivers()
    }
    
    func hasDivers() -> Bool {
        guard let fetchedDivers = frc.fetchedObjects else { return false }
        return fetchedDivers.count > 0
    }
    
    func fetchDivers (){
        do{
            try frc.performFetch()
        }catch {
            print("\(error), \(error.localizedDescription)")
        }
    }
    func dealWithPotentialConflict(){
        let potentialAlert = UIAlertController(title: "Potential Name Issue", message: viewModel.messageForNameConflictIssue(contact: selectedContact!), preferredStyle: .actionSheet)
        let splitNameAction = UIAlertAction(title: "split name", style: .default , handler: {action in
            let nameElements = self.selectedContact?.splitGivenName()
            self.addDiverIfDoesntAlreadyExists(nameElements: nameElements)
        })
        let useNameAsIs = UIAlertAction(title: "use name as is", style: .default, handler: {action in
            self.addDiverIfDoesntAlreadyExists(nameElements: nil) })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        potentialAlert.addAction(splitNameAction)
        potentialAlert.addAction(useNameAsIs)
        potentialAlert.addAction(cancelAction)
        present(potentialAlert, animated: true, completion: nil)
    }
    fileprivate func dealWithSelectedContact() {
        
        guard selectedContact != nil else {return}
        if selectedContact!.hasPotentialCombinedName{
            dealWithPotentialConflict()
        }else {
            addDiverIfDoesntAlreadyExists(nameElements:nil)
        }
    }
    func addNewDiverWithDetailsFrom(nameElements:(first:String, middle :[String], last:String)?) {
        viewModel.addNewDiver()
        viewModel.addDetails(from: selectedContact!, with: nameElements)
        
        selectedContact = nil
    }
    
    fileprivate func addDiverIfDoesntAlreadyExists(nameElements:(first:String, middle :[String], last:String)?){
        
        if viewModel.alreadyExistsDiverWith(givenName: (selectedContact?.givenName)!, familyName: selectedContact?.familyName) {
            let message = "A diver with name \(selectedContact?.givenName ?? "") \(selectedContact?.familyName ?? "") already exists, so Splash is ignoring this."
            showAlert(withTitle: "Duplicate Diver", message: message)
            selectedContact = nil
        }else {
            addNewDiverWithDetailsFrom(nameElements:nameElements)
        }
    }
    
    fileprivate func checkForAlreadyExistingContact()->Bool{
        return viewModel.alreadyExistsDiverWith(givenName: selectedContact?.givenName, familyName: selectedContact?.familyName)
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let count = frc.sections?.count else {return 0}
        return count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = frc.sections?[section] else { return 0 }
        return section.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as? DiverSummaryCell else { fatalError("needed a DiverSummaryCell")}
        let diver = frc.object(at: indexPath)
        cell.configure(with: diver)
        return cell
        
    }
    //MARK:- TableView Delegate
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let diver = frc.object(at: indexPath)
       return viewModel.canDelete(diver)
      
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete", handler:{ action, indexPath in
            let diverToDelete = self.frc.object(at: indexPath)
            self.viewModel.set(diver: diverToDelete)
            let message = self.viewModel.deleteMessageFor(diverToDelete)
            let deleteAlert = UIAlertController(title: "Confirm Delete", message: message , preferredStyle: .actionSheet)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel , handler: {action in
                tableView.reloadRows(at: [indexPath], with: .automatic)
            })
            let deleteAction = UIAlertAction(title: "OK", style: .destructive, handler: {action in
                let context = self.frc.managedObjectContext
                context.delete(diverToDelete)
            })
            deleteAlert.addAction(deleteAction)
            deleteAlert.addAction(cancelAction)
            self.present(deleteAlert, animated: true, completion: nil)
        })
        return [deleteAction]
    }

    //MARK:- Navigation
    func prepareForAddDiver(_ vc: DiverDetails) {
        viewModel.addNewDiver()
        viewModel.mode = .add
        vc.inject(viewModel)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == showDiverSegue {
            guard let vc = segue.actualDestination() as? DiverDetails else { fatalError("didn't get a DiverDetails Scene")}
            let diver = frc.object(at: tableView.indexPathForSelectedRow!)
            viewModel.dataModel.diver = diver
            viewModel.mode = .show
            vc.inject(viewModel)
        }
        if segue.identifier == addDiverSegue{
            guard let vc = segue.actualDestination() as? DiverDetails else { fatalError("didnt get a DiverDetails Scene")}
            prepareForAddDiver(vc)
        }
    }
}
//MARK:- Extensions
extension DiversListsBaseVC : Injectable {
    func inject(_ vm: DiverViewModel) {
        viewModel = vm
    }
    func assertDependencies() {
        assert(viewModel != nil, "Expecting a datamodel")
    }
    typealias T = DiverViewModel
}

extension DiversListsBaseVC : NSFetchedResultsControllerDelegate{
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType){
        switch type {
        case .insert:
            let sectionIndexSet = NSIndexSet(index: sectionIndex)
            self.tableView.insertSections(sectionIndexSet as IndexSet, with: UITableView.RowAnimation.fade)
            updateView()
        case .delete:
            let sectionIndexSet = NSIndexSet(index: sectionIndex)
            self.tableView.deleteSections(sectionIndexSet as IndexSet, with: UITableView.RowAnimation.fade)
            updateView()
        default:
            break
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
            updateView()
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
            updateView()
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .automatic)
        @unknown default:
            print("Futureproof: NSFetchedResultscontroller didChange - and has added a new enum type")}
    }
}

extension DiversListsBaseVC : CNContactPickerDelegate{
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        selectedContact = contact
    }
}
