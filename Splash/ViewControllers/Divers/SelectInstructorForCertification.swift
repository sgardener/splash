//
//  SelectInstructorForCertification.swift
//  Splash
//
//  Created by Simon Gardener on 01/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class SelectInstructorForCertification: DiversListsBaseVC{
    override func viewDidLoad() {
        frc = viewModel.frcForInstructors()
        super.viewDidLoad()
        title = "Select Instructor"
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func addDiver(){
        
    }
    override func setUpAddButton(){
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pro = frc.object(at: indexPath)
        guard  let cell = tableView.cellForRow(at: indexPath) else {fatalError()}
        if cell.accessoryType == .checkmark {
            cell.accessoryType = .none
            viewModel.removeCertifyingInstructor(pro)
        }else if cell.accessoryType == .none {
            cell.accessoryType = .checkmark
            viewModel.makeCertifyingInstructor(pro)
            navigationController?.popViewController(animated: true)
        }
        
    }
    override  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as? DiverChoiceCell else { fatalError("needed a DiverChoice cell")}
        let diver = frc.object(at: indexPath)
        cell.configure(with: diver, isLogged: viewModel.certificationContains(diver))
        return cell
    }
    //MARK:- TableView Delegate
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}


