//
//  DiverListProtocol.swift
//  Splash
//
//  Created by Simon Gardener on 17/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
import UIKit
import CoreData
protocol DiversLists : ExpectingDataModel {
    var cellID : String {get set}
    var tableView: UITableView! { get set }
    var messageLabel : UILabel! { get set }
    var frc: NSFetchedResultsController<Diver>! {get set}
    func hasDivers()-> Bool
    func updateView()
    func fetchDivers ()
}
 extension DiversLists{
     func updateView() {
        tableView.isHidden = !hasDivers()
        messageLabel.isHidden = hasDivers()
    }

   func hasDivers() -> Bool {
        guard let fetchedDivers = frc.fetchedObjects else { return false }
        return fetchedDivers.count > 0
    }
     func fetchDivers (){
        do{
            try frc.performFetch()
        }catch {
            print("\(error), \(error.localizedDescription)")
        }
    }

}
