//
//  BuddiesList.swift
//  Splash
//
//  Created by Simon Gardener on 17/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData
import Contacts

class BuddiesList: DiversListsBaseVC {

    override func viewDidLoad() {
        frc = viewModel.frcForBuddies()
        super.viewDidLoad()
        title = "Buddies"
    }
    override func addNewDiverWithDetailsFrom(nameElements:(first:String, middle :[String], last:String)?) {
       super.addNewDiverWithDetailsFrom(nameElements: nameElements)
        viewModel.setBuddyStatus(to:BuddyStatus.regular.rawValue)
    }
 
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let dictionaryIndex = Int(frc.sections![section].name) else {fatalError("Wrong value for section name- should be 2 4 or 8")}
        return viewModel.buddyHeader(for: dictionaryIndex)
     
    }

   override func prepareForAddDiver(_ vc: DiverDetails) {
        viewModel.addNewDiver()
        viewModel.setBuddyStatus(to:BuddyStatus.regular.rawValue)
        viewModel.mode = .add
        vc.inject(viewModel)
    }
}
