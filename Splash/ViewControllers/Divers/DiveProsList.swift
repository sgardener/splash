//
//  DiveProsList.swift
//  Splash
//
//  Created by Simon Gardener on 17/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class DiveProsList: DiversListsBaseVC {
    
    override func viewDidLoad() {
        frc = viewModel.frcForPros()
        super.viewDidLoad()
        title = "Guides & Instructors"
    }

    override func addNewDiverWithDetailsFrom(nameElements:(first:String, middle :[String], last:String)?) {
        super.addNewDiverWithDetailsFrom(nameElements: nameElements)
        viewModel.setProStatus()
    }
    override func prepareForAddDiver(_ vc: DiverDetails) {
        viewModel.addNewDiver()
        viewModel.setProStatus()
        viewModel.mode = .add
        vc.inject(viewModel)
    }
}
