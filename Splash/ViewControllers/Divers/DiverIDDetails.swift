//
//  DiverIDDetails.swift
//  Splash
//
//  Created by Simon Gardener on 21/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.


import UIKit

class DiverIDDetails: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let textCellId = "textCell"
    let photoCellId = "photoCell"
    let displayChoiceCellId = "displayChoiceCell"
    var viewModel: DiverViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        title = "Identity"
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return OptionOrder.displayAs.rawValue + 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rowName = optionOrderFor(position: indexPath.row)
        switch rowName {
            
        case .photo :
            guard let cell = tableView.dequeueReusableCell(withIdentifier: photoCellId, for: indexPath) as? photoCell else { fatalError("not a photcell") }
            cell.configure(with: viewModel)
            return cell
            
        case .givenName:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: textCellId, for: indexPath ) as? TextFieldTableViewCell else { fatalError("not a textfield cell")}
            cell.textField.tag = indexPath.row
            cell.textField.text = viewModel.givenName()
            cell.textField.placeholder = viewModel.placeholderTextFor(indexPath.row)
            return cell
            
        case .familyName:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: textCellId, for: indexPath ) as? TextFieldTableViewCell else { fatalError("not a textfield cell")}
            cell.textField.tag = indexPath.row
            cell.textField.text = viewModel.familyName()
            cell.textField.placeholder = viewModel.placeholderTextFor(indexPath.row)
            
            return cell
        case .nickName:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: textCellId, for: indexPath ) as? TextFieldTableViewCell else { fatalError("not a textfield cell")}
            cell.textField.tag = indexPath.row
            cell.textField.text = viewModel.nickName()
            cell.textField.placeholder = viewModel.placeholderTextFor(indexPath.row)
            
            return cell
            
        case .displayAs:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: displayChoiceCellId, for: indexPath ) as? SwitchCell else { fatalError("not a switchcell")}
            cell.theSwitch.isOn = UserDefaults.useNickNameAsDisplayName()
            return cell
        }
    }
    
    //MARK:- TableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath:IndexPath) {
        if indexPath.row == OptionOrder.photo.rawValue {
            present(photoOptionAlertController(), animated: true, completion: {
                tableView.deselectRow(at: indexPath, animated: true)
            })
        }
    }
    
    fileprivate func photoOptionAlertController()-> UIAlertController{
        let alertController = UIAlertController(title:"Photo", message: "How would you like to add or change the photo?", preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "take photo", style: .default, handler:  {alert in self.showImagePicker(with: .camera)} )
        let choosePhotoAction = UIAlertAction(title: "choose photo", style: .default, handler: {alert in self.showImagePicker(with: .photoLibrary)})
        let cancelAction = UIAlertAction(title: "cancel", style: .cancel, handler:nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(choosePhotoAction)
        alertController.addAction(cancelAction)
        return alertController
    }
    
    @IBAction func displayNameSwitchSwitched(_ sender: UISwitch) {
        UserDefaults.setNickNameIsDisplayName(to: sender.isOn)
    }
    
    //MARK:- ImagePicker Stuff
    fileprivate func showImagePicker(with sourceType :UIImagePickerController.SourceType){
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = sourceType
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        if let selectedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage{
            viewModel.set(diverImage: selectedImage.shrink(to: 80.0))
        }
        picker.dismiss(animated: true, completion: {  self.tableView.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .automatic)})
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}

extension DiverIDDetails: Injectable{
    func inject(_ vm: DiverViewModel) {
        viewModel = vm
    }
    
    func assertDependencies() {
        assert(viewModel != nil, "Didnt get viewModel")
    }
    
    typealias T = DiverViewModel
}

extension DiverIDDetails: OptionOrder {
    enum OptionOrder: Int {
        case  photo,givenName,familyName,nickName,displayAs
    }
}

extension DiverIDDetails : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case OptionOrder.familyName.rawValue :
            viewModel.setFamilyName(textField.text)
        case OptionOrder.givenName.rawValue:
            viewModel.setGivenName(textField.text)
        case OptionOrder.nickName.rawValue:
            viewModel.setNickName(textField.text)
        default: break
        }
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
