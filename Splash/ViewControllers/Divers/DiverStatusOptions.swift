//
//  DiverStatusOptions.swift
//  Splash
//
//  Created by Simon Gardener on 21/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class DiverStatusOptions: UITableViewController {
    
    var viewModel: DiverViewModel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        title = "Status"
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return OptionOrder.clientStatus.rawValue + 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionName = optionOrderFor(position: section)
        switch sectionName {
        case .buddyStatus: return viewModel.numberOfBuddyOptions
        case .proStatus: return viewModel.numberOfProOptions
        case .clientStatus: return viewModel.numberOfClientOptions
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName {
        case .buddyStatus:
            cell.textLabel?.text = viewModel.buddyStatusType(row: indexPath.row)
            cell.accessoryType = viewModel.buddyStatusAccessoryTypeFor(row: indexPath.row)
        case .clientStatus:
            cell.textLabel?.text = viewModel.clientStatusType(row: indexPath.row)
            cell.accessoryType = viewModel.clientStatusAccessoryTypeFor(row: indexPath.row)
        case .proStatus:
            cell.textLabel?.text = viewModel.proStatusType(row: indexPath.row)
            cell.accessoryType = viewModel.proStatusAccessoryTypeFor(row: indexPath.row)
        }
        return cell
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.diverStatusHeader[section]
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName {
        case .buddyStatus:
            viewModel.setBuddyStatusWith(indexPath.row)
            tableView.reloadSections(IndexSet.init(integer: OptionOrder.buddyStatus.rawValue) , with: .automatic)
            break
        case .proStatus:
            viewModel.setProStatusWith(indexPath.row)
            tableView.reloadSections(IndexSet.init(integer: OptionOrder.proStatus.rawValue) , with: .automatic)
            break
        case .clientStatus:
            viewModel.setClientStatusWith(indexPath.row)
            tableView.reloadSections(IndexSet.init(integer: OptionOrder.clientStatus.rawValue) , with: .automatic)
            break
        }
    }
}
extension DiverStatusOptions: Injectable {
    func inject(_ vm: DiverViewModel) {
        viewModel = vm
    }
    
    func assertDependencies() {
        assert(viewModel != nil, "")
    }
    
    typealias T = DiverViewModel
}

extension DiverStatusOptions: OptionOrder {
    enum OptionOrder: Int {
        case buddyStatus , proStatus, clientStatus
    }
}
