//
//  NoteCell.swift
//  Splash
//
//  Created by Simon Gardener on 25/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class NoteCell: UITableViewCell {

    @IBOutlet weak var textView: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
