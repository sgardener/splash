//
//  AttributionCell.swift
//  Splash
//
//  Created by Simon Gardener on 21/12/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class AttributionCell: UITableViewCell {

     @IBOutlet weak var attributedTo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureWith(site: DiveSite) {
        attributedTo.text = site.noteAssocName
        if let assWeb = site.noteAssocWeb, !assWeb.isEmpty {
            accessoryType = .disclosureIndicator
        }else {
            accessoryType = .none
        }
    }

}
