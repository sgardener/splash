//
//  WebReferenceTableViewCell.swift
//  Splash
//
//  Created by Simon Gardener on 12/04/2019.
//  Copyright © 2019 Simon Gardener. All rights reserved.
//

import UIKit

class WebReferenceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var webReferenceLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureWith(site: DiveSite) {
        print("configuring")
        webReferenceLabel.text = site.webReference
    }
}
