//
//  MapViewCell.swift
//  Splash
//
//  Created by Simon Gardener on 25/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import MapKit


class MapViewCell: UITableViewCell{

    enum MapType: Int {
        case map
        case satellite
        case hybrid
    }
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var mapTypeSegmentedController: UISegmentedControl!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func changeMapType(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case MapType.map.rawValue:
            mapView.mapType = .standard
        case MapType.satellite.rawValue:
             mapView.mapType = .satellite
        case MapType.hybrid.rawValue:
            mapView.mapType = .hybrid
        default:
            break
        }
    }
    func centerMapOn(_ center :CLLocationCoordinate2D?,
                     annotateWithtitle title:String?,
                     andSubTitle subtitle: String?,
                    withColor colour: UIColor) {
        guard let center = center else { return }
        mapView.mapType = .standard
        mapView.showsScale = true
        let region = MKCoordinateRegion.init(center: center, latitudinalMeters: 10000, longitudinalMeters: 10000)
        mapView.setCenter(center, animated: true)
        mapView.setRegion(region, animated: true)
        mapView.showsUserLocation = true
        mapView.isZoomEnabled = true
       let point = MKPointAnnotation()
        point.coordinate = center
        point.title = title
        point.subtitle = subtitle
        
        mapView.addAnnotation(point)
        mapView.selectAnnotation(point, animated: true)
        
        
        
    }
}

