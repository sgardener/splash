//  SitesByCountry.swift
//  Splash
//
//  Created by Simon Gardener on 22/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.

import UIKit
import CoreData

class SitesByCountry: UITableViewController {
    
    
    var dataModel : DataModel!
    var countryName: String!
    let countryCellID = "cell"
    let segueID = "showSiteDetails"
    lazy var frc : NSFetchedResultsController<DiveSite> = {
        let fetchRequest: NSFetchRequest<DiveSite> = DiveSite.fetchRequest()
        let predicate = NSPredicate(format:"country == %@", countryName)
        fetchRequest.predicate = predicate
        
        let nameSortDescriptor = NSSortDescriptor(key: #keyPath(DiveSite.name), ascending: true)
        fetchRequest.sortDescriptors = [nameSortDescriptor]
        let countryFirstKeyPath = \DiveSite.country?.firstCharacter
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: (dataModel?.container.viewContext)!,
                                                                  sectionNameKeyPath: "firstLetterForIndex",
                                                                  cacheName: nil)
        fetchedResultsController.delegate = self
        return fetchedResultsController
    }()
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return  frc.sections![section].name
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return frc.sectionIndexTitles
    }
    override func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return frc.section(forSectionIndexTitle: title, at: index)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        frc.delegate = self
        fetchDiveSites()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchDiveSites()
    }
    private func fetchDiveSites(){
        do{
            try frc.performFetch()
        }catch {
            print("\(error), \(error.localizedDescription)")
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return frc.sections!.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let secInfo = frc.sections![section]
        return secInfo.numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: countryCellID, for: indexPath)
        let diveSite = frc.object(at: indexPath)
        cell.textLabel?.text = diveSite.name
        cell.detailTextLabel?.text = areaStringFor(diveSite)
        return cell
    }
    fileprivate func areaStringFor(_ diveSite: DiveSite)-> String {
        var siteAreaString = ""
        if diveSite.alternativeName != nil {
            siteAreaString = "\(diveSite.alternativeName!), "
        }
        if diveSite.islandAtoll != nil {
            siteAreaString = siteAreaString + diveSite.islandAtoll! + ", "
        }
        if diveSite.localAreaName != nil {
            siteAreaString = siteAreaString + diveSite.localAreaName! + ", "
        }
        if diveSite.bodyOfWater != nil {
            siteAreaString = siteAreaString + diveSite.bodyOfWater! + ", "
        }
        
        if siteAreaString != "" {
            siteAreaString = siteAreaString.trimmingCharacters(in: CharacterSet.init(charactersIn: ", "))
            siteAreaString += "."
        }
        return siteAreaString
    }
    
    
    //MARK:- UITableViewDelegate
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let site = frc.object(at: indexPath)
        if site.addedByUser == false { return false}
        if site.dive == nil || site.dive?.count == 0 { return true }
        return false
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete", handler: {action,indexPath in
            let siteToDelete = self.frc.object(at: indexPath)
            let alertController = UIAlertController(title: "Delete this dive site?", message: " You added this site.\n\nIt is safe to remove it as it is not referenced by any dives", preferredStyle: .actionSheet )
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {action in
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
            })
            let goAheadAction = UIAlertAction(title: "Delete", style: .destructive, handler: {(action) in
                let count = self.frc.fetchedObjects?.count
                self.dataModel.container.viewContext.delete(siteToDelete)
                self.dataModel.saveContext()
                
                if count == 1 {
                    self.navigationController?.popViewController(animated: true)
                }
            })
            alertController.addAction(goAheadAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        })
        return [deleteAction]
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType){
        
        switch type {
        case .insert:
            let sectionIndexSet = NSIndexSet(index: sectionIndex)
            self.tableView.insertSections(sectionIndexSet as IndexSet, with: UITableView.RowAnimation.fade)
            
        case .delete:
            let sectionIndexSet = NSIndexSet(index: sectionIndex)
            self.tableView.deleteSections(sectionIndexSet as IndexSet, with: UITableView.RowAnimation.fade)
            
        default:
            break
        }
    }
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .automatic)
        @unknown default:
            print("Futureproof: NSFetchedResultscontroller didChange - and has added a new enum type")
            
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.actualDestination() as? SiteDetails else {fatalError("expecting a SiteDetails Scene")}
        let indexPath = tableView.indexPathForSelectedRow
        dataModel.site = frc.object(at: indexPath!)
        vc.inject(dataModel)
 
    }
}
extension  SitesByCountry: NSFetchedResultsControllerDelegate{
    
}
extension SitesByCountry: Injectable {
    func inject(_ injected: (dateModel: DataModel?, countryName: String)) {
        dataModel = injected.dateModel!
        countryName = injected.countryName
    }
    
    func assertDependencies() {
        assert(dataModel != nil, "didnt get a datamodel ")
        assert(countryName != nil, "didnt get a countryname")
    }
    
    
    typealias T = (dateModel: DataModel?,countryName: String)
    
    
}
