//
//  WebViewController.swift
//  Splash
//
//  Created by Simon Gardener on 21/12/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    var urlString: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        if let url = URL(string: urlString){
            let request = URLRequest(url: url)
            webView.load(request)
        }
//        if !submitterAddress.contains("http"){
//            submitterAddress = "http://" + submitterAddress
//        }
//        if  let url = URL(string: submitterAddress){
//            let request = URLRequest(url: url)
//            webView.load(request)
//        }else {
//            showAlert(withTitle: "Bad URL", message: "The website in the database is not correctly formatted.")
//        }
    }
}

extension WebViewController : Injectable{
     typealias T = String
    
    func assertDependencies() {
        assert(urlString != nil , "no submittter address string passed in")
    }
    
    func inject(_ addressURL: String) {
        urlString = addressURL
    }
   

}
