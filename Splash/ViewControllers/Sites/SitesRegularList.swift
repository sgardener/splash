//
//  SitesRegularList.swift
//  Splash
//
//  Created by Simon Gardener on 28/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.

import UIKit

class SitesRegularList: UITableViewController {
    
    var dataModel : DataModel!
    var regularSites = [DiveSite]()
    let cellID = "cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Regular Sites"
        assertDependencies()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        regularSites = dataModel.regularDiveSites()
        tableView.reloadData()
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.numberOfSitesMarkedAsRegular()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)

        cell.textLabel?.text = regularSites[indexPath.row].name

        return cell
    }
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            guard let vc = segue.actualDestination() as? SiteDetails else { fatalError("Expecting a SiteDetails scene - didnt get one")}
            guard let indexPath = tableView.indexPathForSelectedRow else {fatalError("Didnt get a valid indexpath")}
        dataModel.site = regularSites[indexPath.row]
       vc.inject(dataModel)
    }
}

extension SitesRegularList : Injectable {
    func inject(_ dm : DataModel) {
        dataModel = dm
    }
    func assertDependencies() {
        assert(dataModel != nil , "Expected datamodel got nil")
    }
    typealias T = DataModel
}
