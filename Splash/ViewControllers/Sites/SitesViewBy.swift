//
//  SitesViewBy.swift
//  Splash
//
//  Created by Simon Gardener on 22/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.

import UIKit
import CoreData

class SitesViewBy: UITableViewController, NeedsDataModel {
    
    var container: NSPersistentContainer!
    var dataModel: DataModel!
    let CountryCellID = "countryCell"
    let addIdentifier = "addNewSite"
    let showByCountrySegue = "showSitesByCountry"
    let headerTitles = ["Nearby","Favourites","Regular","By country"]
    let sectionDisplayNames = ["List of nearby sites",
                               "List of your favourite sites",
                               "List of your regular sites"]
    lazy var frc : NSFetchedResultsController<DiveSite>  = dataModel.fetchedResultsControllerForDiveSitesWithSectionNameKeyPathCountry()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assert(dataModel != nil, "Datamodel not passed in")
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.rowHeight = UITableView.automaticDimension
        setUpNavBar()
    }
    func setUpNavBar(){
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchDiveSites()
        tableView.reloadData()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    private func fetchDiveSites() {
        do{
            try frc.performFetch()
        }catch {
            print("\(error), \(error.localizedDescription)")
        }
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int{
        return OptionOrder.countries.rawValue + 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionName = optionOrderFor(position: section)
        switch sectionName {
        case .nearby:
            return 1
        case .favourites:
            if dataModel.numberOfSitesMarkedAsFavourite() > 0 {
                return 1
            } else {return 0}
        case .regular:
            if  dataModel.numberOfSitesMarkedAsRegular() > 0 {
                return 1
            } else { return 0 }
        case .countries:
            return (frc.sections?.count)!
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName{
        case .nearby, .regular, .favourites:
            cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
            cell.textLabel?.text = sectionDisplayNames[indexPath.section]
            
        case .countries:
            let countryCell = tableView.dequeueReusableCell(withIdentifier: CountryCellID, for: indexPath)
            let secInfo = frc.sections?[indexPath.row]
            countryCell.textLabel?.text = secInfo?.name ?? "unknown"
            countryCell.detailTextLabel?.text = String("\((secInfo?.numberOfObjects)!) dive sites")
            cell = countryCell
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sectionName = optionOrderFor(position: section)
        switch sectionName{
        case .nearby, .countries:
            return headerTitles[section]
        case .regular:
            if dataModel.numberOfSitesMarkedAsRegular() > 0 {
                return headerTitles[section]
                
            } else { return nil }
        case .favourites:
            if dataModel.numberOfSitesMarkedAsFavourite() > 0 {
                return headerTitles[section]
            } else { return nil}
        }
    }
    
    //MARK: - TableView Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName{
        case.nearby:
            performSegue(withIdentifier: Identifier.nearby.rawValue, sender: self)
        case.favourites:
            performSegue(withIdentifier: Identifier.favourites.rawValue, sender: self)
        case .regular:
            performSegue(withIdentifier: Identifier.regular.rawValue, sender: self)
        case.countries:
            break
        }
    }
    
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == addIdentifier {
            
            guard let vc = segue.actualDestination() as? SiteBasicsEditor else {fatalError()}
            let viewModel = SiteBasicsViewModel.init(dataModel: dataModel)
            viewModel.mode = .add
            let _ = viewModel.newSite()
            vc.inject(viewModel)
            
        }else {
            let segueID = segueIdentifierFor(segue: segue)
            
            switch segueID{
            case .nearby:
                guard let vc = segue.actualDestination() as? SitesByNearby else { fatalError("didnt get a SitesByNearby scene")}
                vc.inject(dataModel)
            case.favourites:
                guard let vc = segue.actualDestination() as? SitesFavouritesList else { fatalError("didnt get a sitesFavouritesList scene")}
                vc.inject(dataModel)
                break
            case .regular:
                guard let vc = segue.actualDestination() as? SitesRegularList else { fatalError("didnt get a sitesRegularList scene")}
                vc.inject(dataModel)
                
            case .countries:
                guard let vc = segue.actualDestination() as? SitesByCountry else { fatalError("didnt get a sitesByCountry scene")}
                let indexPath = tableView.indexPathForSelectedRow!
                let secInfo = frc.sections?[indexPath.row]
                
                let countryName = secInfo?.name
                vc.countryName = countryName
                vc.title = countryName
                vc.dataModel = dataModel!
            }
        }
    }
    @IBAction  func addingSiteSavedUnwindsToHere(segue:UIStoryboardSegue){
    }
    @IBAction  func addingSiteCancelledUnwindsToHere(segue:UIStoryboardSegue){
    }
    
    //MARK:- Add New Site
    
    @IBAction func addNewDiveSite(_ sender: UIBarButtonItem) {
        
        let message  = "Some sites in the database still need location data and don't appear in the 'nearby' list.\n\nTo prevent duplication we recommend a manual search of the sites in your current country before adding a new one."
        let alertController = UIAlertController(title: "Confirm new site", message: message, preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
        let addNewAction = UIAlertAction(title: "Add New Dive Site", style: .default, handler: {(action) in
            self.performSegue(withIdentifier: self.addIdentifier , sender: self)
        })
        alertController.addAction(addNewAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
}
extension SitesViewBy: SegueHandlerType, OptionOrder{
    
    
    enum Identifier: String {
        case nearby
        case favourites
        case regular
        case countries
    }
    enum OptionOrder: Int {
        case nearby
        case favourites
        case regular
        case countries
        
    }
    
}

