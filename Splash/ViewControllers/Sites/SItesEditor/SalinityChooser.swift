//
//  SalinityChooser.swift
//  Splash
//
//  Created by Simon Gardener on 10/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class SalinityChooser: UITableViewController {
    typealias HandlerBlock =  (Int16)->()
    var site:DiveSite!
    var selectionHandler : HandlerBlock!

    let salinityTypes = ["salt","fresh","brackish","mixed layers","high salinity"];
    override func viewDidLoad() {
        
        super.viewDidLoad()
        assertDependencies()
        title = "Choose Salinity"
    }


    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return salinityTypes.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
        
        cell.textLabel?.text = salinityTypes[indexPath.row]
        return cell
    }


    //MARK:- TableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        site.salinity = Int16(pow(2, Double(indexPath.row)))
        selectionHandler(site.salinity)
        navigationController?.popViewController(animated: true)
    }
}

extension SalinityChooser : Injectable{
    func inject(_ injected : (site:DiveSite, handler:SalinityChooser.HandlerBlock)) {
        site = injected.site
        selectionHandler = injected.handler
    }

    func assertDependencies() {
        assert(site != nil, "salinityChoose didnt get a diveSite")
        assert(selectionHandler != nil , "didnt get a selectionHandler")
    }
    
    typealias T = (site:DiveSite, handler:HandlerBlock)
    
    
    
}
