//  SiteBasicsEditor.swift
//  Splash
//  Created by Simon Gardener on 09/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.

import UIKit
import CoreLocation
import BLTNBoard

class SiteBasicsEditor: UITableViewController, LocationUser {
    
    var locationService: LocationService!
    
    
    var viewModel: SiteBasicsViewModel!
    
    fileprivate  let locationTextId = "locationTextCell"
    fileprivate  let coordinatesID = "coordinatesCell"
    fileprivate  let salinityID = "salinity"
    fileprivate  let countryID = "country"
    fileprivate  let moreID = "more"
    fileprivate  let GPSIsID = "GPSIs"
    
    fileprivate var shouldSearchForGps = true
    fileprivate var isSearchingForGps = false
    fileprivate var locationUpdateCount = 0
    
    var capturedLocation : CLLocation?
    
    let gpsIndexPath = IndexPath(row: 0, section: OptionOrder.coordinates.rawValue)
    let countryIndexPath = IndexPath.init(row: 2, section: OptionOrder.location.rawValue)
    
    lazy var bulletinManager: BLTNItemManager = {
        var page = BulletinDataSource.locationWasDeniedPage()
        return BLTNItemManager(rootItem: page)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80
        assertDependencies()
        tableView.keyboardDismissMode = .interactive
    }
    fileprivate func initiateGPSLookup() {
        locationService = LocationService(owner: self, desiredAccuracy: 10, within: 30)
        locationService.requestLocation()
        isSearchingForGps = true
        coordinateCellStartIndicator()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if shouldSearchForGps == true {
            shouldSearchForGps = false
            initiateGPSLookup()
        }
    }
    
    // MARK: - Table view data source
    
    @IBAction func cancelEditingDive(_ sender: Any) {
        viewModel.deleteNewSite()
        viewModel.dataModel.saveContext()
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveNewDive(_ sender: Any) {
        tableView.endEditing(true)
        if   checkBeforeSaving() == true {
            viewModel.dataModel.saveContext()
            presentingViewController?.dismiss(animated: true, completion: nil)
        }
    }
    fileprivate func checkBeforeSaving()-> Bool {
        if viewModel.site().isNamed() == false {
            showAlert(withTitle: "Missing Name", message: "A site must have a name before you can save.")
            return false
        }
        if viewModel.site().hasCountry() == false {
            
            showAlert(withTitle: "Missing Country", message: "A site must have a country before you can save.")
            return false
        }
        if viewModel.site().hasLocation() == false{
            showAlert(withTitle: "Missing Coordinates", message: "A site must store coordinate. The app hasnt captured them yet.\n\nIf you are inside a boat, you might need to pop onto the deck to get the satellite signal.")
            return false
        }
        if viewModel.site().hasGPSIs() == false {
            showAlert(withTitle: "Missing 'GPS is'", message: "You must declare where the captured GPS location is before saving")
            return false
        }
        
       return true
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return OptionOrder.more.rawValue + 1

    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsIn(section: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName{
            
        case .location:
            return locationCellsforRow(at: indexPath)
            
        case .coordinates:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: coordinatesID, for: indexPath) as? CoordinateCell else { fatalError("didn't get coordnate cell") }
            
            cell.latitude.text = "banana"
            cell.longitude.text = "orange "
            if isSearchingForGps == true {
                cell.indicator.isHidden = false
                cell.indicator.startAnimating()
            } else {
                cell.indicator.isHidden = true
                cell.indicator.stopAnimating()
            }

            if let location = capturedLocation {
                cell.longitude.text = location.coordinate.longitude.toFourDecimalPlaces()
                cell.latitude.text = location.coordinate.latitude.toFourDecimalPlaces()
            }else {
                cell.longitude.text = viewModel.longitude()
                cell.latitude.text = viewModel.latitude()
            }
            return cell
            
        case .thisGPSIs:
            let cell = tableView.dequeueReusableCell(withIdentifier: GPSIsID, for: indexPath)
            cell.textLabel?.text = viewModel.gpsIsTextForRow(at: indexPath)
            cell.accessoryType = viewModel.gpsIsAccessoryForRow(at: indexPath)
            return cell
            
        case .salinity:
            let cell = tableView.dequeueReusableCell(withIdentifier: salinityID, for: indexPath)
            cell.textLabel?.text = viewModel.salinity()
            return cell
            
        case .more:
            let cell = tableView.dequeueReusableCell(withIdentifier: moreID, for: indexPath)
            return cell
        }
    }
    fileprivate func locationCellsforRow(at indexPath: IndexPath)-> UITableViewCell{
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: locationTextId, for: indexPath) as? TextFieldTableViewCell else { fatalError()}
        cell.textField.placeholder = viewModel.locationPlaceholder(at: indexPath.row)
        cell.selectionStyle = .none
        cell.accessoryType = .none
        cell.textField.isEnabled = true
        switch indexPath.row{
        case SiteBasicsLocation.name.rawValue:
            cell.textField.text = viewModel.siteName()
            cell.textField.tag = SiteBasicsLocation.name.rawValue
            return cell
        case SiteBasicsLocation.islandAtoll.rawValue:
            
            cell.textField.text = viewModel.islandAtoll()
            cell.textField.tag = SiteBasicsLocation.islandAtoll.rawValue
            return cell
            
        case SiteBasicsLocation.country.rawValue:
            cell.textField.isEnabled = false
            cell.accessoryType = .disclosureIndicator
            cell.textField.text = viewModel.country()
            cell.selectionStyle = .default
            return cell
        default:
            return  UITableViewCell()
        }
        
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.headerString(for:section)
    }
    
    //MARK:- Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName{
            
        case .location:
            if indexPath.row == SiteBasicsLocation.country.rawValue    {
                performSegue(withIdentifier: countryID, sender: self)
            }
        case .coordinates:
            if isSearchingForGps == false  {
                isSearchingForGps = true
                locationUpdateCount = 0
                tableView.reloadRows(at: [indexPath], with: .automatic)
                locationService.requestLocation()
            }
            tableView.deselectRow(at: indexPath, animated: true)
            
        case .thisGPSIs :
            viewModel.flipGpsOption(at: indexPath.row)
            tableView.reloadSections(IndexSet.init(integer: indexPath.section), with: .automatic)
            
        case .more:
            tableView.deselectRow(at: indexPath, animated: true)
            if checkBeforeSaving() == true {
                viewModel.dataModel.saveContext()
                performSegue(withIdentifier: Identifier.more.rawValue, sender: self)
            }
            
        default: break
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == countryID {
            guard let vc = segue.actualDestination() as? CountryChooser else { fatalError("didn't get a countrychooser")}
            vc.handleSelection = { (country:String) in
                self.viewModel.setCountry(country)
                self.tableView.reloadRows(at: [ self.countryIndexPath ], with: .none)
            }
        }
        if segue.identifier == "salinity"{
            guard let vc = segue.actualDestination() as? SalinityChooser  else { fatalError("didn't get a salinity chooser")}
            let handler = {(salinity:Int16) in
                self.viewModel.setSalinity(value: salinity)
                self.tableView.reloadSections(IndexSet.init(integer: OptionOrder.salinity.rawValue) , with: .none)
            }
            vc.inject((viewModel.site(),handler))
        }
        if segue.identifier == moreID {
            viewModel.dataModel.saveContext()
            guard let vc = segue.actualDestination() as? SiteDetailedEditor else { fatalError()}
            vc.inject((viewModel.dataModel, mode: .add))
            
            locationService.stop()
        }
    }
    
    
    //MARK:- Location
    func locationDidUpdate(location: CLLocation) {
        guard locationUpdateCount == 0 else {
           // print ("discarding locationUpdate")// throw away first value - often old data
            return
        }
        locationUpdateCount += 1
        capturedLocation = location
        viewModel.save(location: location)
        isSearchingForGps = false
        reloadCoordinateSection()
        let handler = {(placemarks:[CLPlacemark]?,error: Error?) in
            guard error == nil else {
                print (" we got a reverse geo error \(String(describing: error))")
                return}
            
            let alreadyStoredKeys = GeocoderService.keysForStoredValues(in: self.viewModel.site())
            if alreadyStoredKeys == nil {
                GeocoderService.insertAll(placemarks: placemarks, into: self.viewModel.site())
            }else{
                let warning = UIAlertController(title: "Data Found", message: "Some location data has been found for your location.\n\n What action should Splash take ", preferredStyle: .actionSheet)
                let overwrite = UIAlertAction(title: "Overwrite all values", style: .destructive, handler: {action in
                    GeocoderService.insertAll(placemarks: placemarks, into: self.viewModel.site())
                    self.reloadLocationSection()
                })
                let insert = UIAlertAction(title: "Insert new Values", style: .default, handler: {action in
                    GeocoderService.insertNew(placemarks: placemarks, into: self.viewModel.site())
                    self.reloadLocationSection()
                })
                let cancel = UIAlertAction(title: "Cancel", style: .cancel , handler: nil)
                warning.addAction(insert)
                warning.addAction(overwrite)
                warning.addAction(cancel)
                self.present(warning, animated: true, completion: nil)
            }
            self.reloadLocationSection()
        }
        GeocoderService.lookUp(location: location, with: handler)
        
    }
    fileprivate func save(location loc:CLLocation){
        
    }
    fileprivate func reloadLocationSection() {
        tableView.reloadSections(IndexSet.init(integer: OptionOrder.location.rawValue), with: .automatic)
    }
    fileprivate func reloadCoordinateSection(){
        tableView.reloadSections(IndexSet.init(integer: OptionOrder.coordinates.rawValue), with: .automatic)
    }
    func partialFix(location: CLLocation) {
        capturedLocation = location
        reloadCoordinateSection()
    }
    func locationWasDenied(){
        bulletinManager.showBulletin(above: self)
    }
    
    func getCoordinatesCell()-> CoordinateCell{
        guard let cell = tableView.cellForRow(at: gpsIndexPath) as? CoordinateCell else { fatalError()}
        
        return cell
    }
    func coordinateCellStartIndicator(){
        reloadCoordinateSection()
    }
    func coordinateCellStopIndicator(){
        reloadCoordinateSection()
    }
    
}
extension SiteBasicsEditor: Injectable{
    func inject(_ vm: SiteBasicsViewModel) {
        viewModel = vm
    }
    func assertDependencies() {
        assert(viewModel != nil, "No viewModel passed to SiteBasics")
    }
    
    typealias T = SiteBasicsViewModel
    
}
extension SiteBasicsEditor {
    enum SiteBasicsLocation : Int {
        case name, islandAtoll, country
    }
}
extension  SiteBasicsEditor: SegueHandlerType, OptionOrder{
    enum Identifier: String {
        case location
        case coordinates
        case thisGPSIs
        case salinity
        case more
    }
    
    enum OptionOrder: Int{
        case location
        case coordinates
        case thisGPSIs
        case salinity
        case more
    }
}

extension SiteBasicsEditor : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        viewModel.handleSiteBasics(textField)
    }
}
