//
//  SiteDetailedEditor.swift
//  Splash
//
//  Created by Simon Gardener on 11/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreLocation
import BLTNBoard

class SiteDetailedEditor: UIViewController, LocationUser {
    var locationService: LocationService!
    var mode: Mode!
    var dataModel: DataModel!
    
    let locationTableLocationSection =  IndexSet.init(integer: 0)
    let locationTableCoordinationSection = IndexSet.init(integer: 1)
    let gpsIndexPath = IndexPath(row: 0, section: 1)
    
    let countryRow = 8
    
    let cancellingWhileLoggingSegueID = "cancellingWhileLogging"
    let savingWhileLoggingSegueId = "savingWhileLogging"
    let siteEditsSavedSegueId = "siteEditsSaved"
    let siteEditsCancelledSegueId = "siteEditsCancelled"
    let siteAddedSavedSegueId = "siteAddedSaved"
    let siteAddedCancelledSegueId = "siteAddedCancelled"
    let siteAddedWhileLoggingSegueId = "siteAddedWhileLogging"
    
    fileprivate enum activeTable: Int {
        case location, data, environment, notes
    }
    
    @IBOutlet weak var locationTable: SiteLocationTable!
    @IBOutlet weak var dataTable: SiteDataTable!
    @IBOutlet weak var environmentTable: SiteEnvironmentTable!
    @IBOutlet weak var noteTable: SiteNoteTable!
    
    @IBOutlet weak var tableSwitcher: UISegmentedControl!
    
    //  fileprivate var isSearchingForGPS = false
    
    lazy var bulletinManager: BLTNItemManager = {
        var page = BulletinDataSource.locationWasDeniedPage()
        return BLTNItemManager(rootItem: page)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        setUpButtons()  
        setUpTableViewModels()
        setTableViewDelegatesAndDataSources()
        setParentViewOnTables()
        showLocationTable()
        if mode == .addedWhileLogging {
            locationTable.startGpsIfNeeded()
        }
    }
    fileprivate func setUpButtons(){
        let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveSiteChanges))
        let cancelButton =
            UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelSiteChanges))
        navigationItem.rightBarButtonItem = saveButton
        navigationItem.leftBarButtonItem = cancelButton
    }
    @objc fileprivate func saveSiteChanges(){
        locationTable.endEditing(true)
        if checkBeforeSaving() == true {
            switch mode!{
            case .edit:
                if dataModel.site?.changedValues().count == 0{
                    navigationController?.popViewController(animated: true)
                }else{
                    ChangeChecker.noteChangesTo(dataModel.site!)
                    dataModel.saveContext()
                    //TODO:- mark for sending new details
                    performSegue(withIdentifier: siteEditsSavedSegueId, sender: self)
                }
            case .add:
                dataModel.saveContext()
                //TODO:- mark for sending new details
                performSegue(withIdentifier: siteAddedSavedSegueId, sender: self)
            case .addedWhileLogging:
                dataModel.addDiveSiteToLogEntry()
                //dont save here - that will be done when the logEntry is saved
                performSegue(withIdentifier: siteAddedWhileLoggingSegueId, sender: self)
            case .show: break
            }
        }
    }
    fileprivate func checkBeforeSaving()-> Bool {
        if dataModel.site?.isNamed() == false {
            showAlert(withTitle: "Missing Name", message: "A site must have a name before you can save.")
            return false
        }
        if dataModel.site?.hasCountry() == false {
            
            showAlert(withTitle: "Missing Country", message: "A site must have a country before you can save.")
            return false
        }
//        if dataModel.site?.hasLocation() == false{
//            showAlert(withTitle: "Missing Coordinates", message: "A site must store coordinate. The app hasnt captured them yet.\n\nIf you are inside a boat, you might need to pop onto the deck to get the satellite signal.")
//            return false
//        }
        if dataModel.site?.hasGPSIs() == false && dataModel.site?.hasLocation() == true {
            showAlert(withTitle: "Missing 'GPS is'", message: "You must declare where the captured GPS location is before saving")
            return false
        }
        if dataModel.site?.hasLocation() == false &&  dataModel.site?.hasGPSIs() == true {
            dataModel.site?.gpsFix = 0
        }
        
        dataModel.site?.needsSubmitting = true
        return true
    }
    @objc fileprivate func cancelSiteChanges(){
        
        switch mode!{
            
        case .edit:
            dataModel.site?.cancelChanges()
            // dataModel.site?.revert()
            navigationController?.popViewController(animated: true)
        case .add:
            dataModel.container.viewContext.delete(dataModel.site!)
            dataModel.saveContext()
            performSegue(withIdentifier: siteAddedCancelledSegueId, sender: self)
        case .addedWhileLogging:
            dataModel.container.viewContext.delete(dataModel.site!)
            performSegue(withIdentifier: cancellingWhileLoggingSegueID, sender: self)
            
        case .show:break
        }
    }
    
    fileprivate func setUpTableViewModels(){
        locationTable.viewModel = SiteLocationViewModel(with: dataModel)
        dataTable.viewModel = SiteDataViewModel(with: dataModel)
        environmentTable.viewModel = SiteEnvironmentViewModel(with: dataModel)
        noteTable.viewModel = SiteNoteViewModel(with: dataModel)
    }
    fileprivate func setTableViewDelegatesAndDataSources(){
        dataTable.delegate = dataTable
        dataTable.dataSource = dataTable
        locationTable.delegate = locationTable
        locationTable.dataSource = locationTable
        environmentTable.delegate = environmentTable
        environmentTable.dataSource = environmentTable
        noteTable.delegate = noteTable
        noteTable.dataSource = noteTable
    }
    fileprivate func setKeyboardDismiss(){
        locationTable.keyboardDismissMode = .interactive
        noteTable.keyboardDismissMode = .interactive
        
    }
    fileprivate func setParentViewOnTables(){
        dataTable.parentView = self
        locationTable.parentView = self
        noteTable.parentView = self
        environmentTable.parentView = self
    }
    
    fileprivate func showLocationTable() {
        locationTable.isHidden = false
        dataTable.isHidden = true
        environmentTable.isHidden = true
        noteTable.isHidden = true
    }
    fileprivate func showDataTable() {
        locationTable.isHidden = true
        dataTable.isHidden = false
        environmentTable.isHidden = true
        noteTable.isHidden = true
    }
    fileprivate func showEnvironmentTable() {
        locationTable.isHidden = true
        dataTable.isHidden = true
        environmentTable.isHidden = false
        noteTable.isHidden = true
    }
    fileprivate func showNotesTable() {
        locationTable.isHidden = true
        dataTable.isHidden = true
        environmentTable.isHidden = true
        noteTable.isHidden = false
    }
    
    /// Turns the isHidden property on and off to display the appropiate table to the user
    
    @IBAction func tableSwitcherTapped(_ sender: UISegmentedControl) {
        view.endEditing(true)
        switch sender.selectedSegmentIndex{
            
        case activeTable.location.rawValue:
            showLocationTable()
        case activeTable.data.rawValue:
            showDataTable()
        case activeTable.environment.rawValue:
            showEnvironmentTable()
        case activeTable.notes.rawValue:
            showNotesTable()
        default: break
        }
    }
    //MARK:- Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let segueName = segueIdentifierFor(segue: segue)
        
        switch segueName{
        case .country:
            guard let vc = segue.actualDestination()  as? CountryChooser else {fatalError("not a country site chooser")}
            vc.handleSelection = { (country:String) in
                self.dataModel.site?.country = country
                self.locationTable.reloadRows(at: [ IndexPath.init(row: self.countryRow, section: 0)], with: .none)}
            
            
        case .canBePublic:
            guard let vc = segue.actualDestination() as? SecrecyChooser else { fatalError(" expected of a secrecy chooser")}
            vc.site = dataModel.site
            vc.block = {self.locationTable.reloadSections(IndexSet.init(integer: 3), with: .automatic)}
            
        case .credit:
            guard let vc = segue.actualDestination() as? UserCredit else { fatalError("expected a user credit scene")}
            vc.userChangedValues = {[unowned self] in self.noteTable.reloadSections(IndexSet.init(integer: 1), with: .none)}
            
        case .cancellingWhileLogging, .siteAddedWhileLogging , .siteEditsSaved, .siteAddedSaved, .siteAddedCancelled:
            break
        }
    }
    
    //MARK:- Location Stuff
    
    func locationDidUpdate(location: CLLocation) {
        guard locationTable.locationUpdateCount == 0 else {
            return
        }
        locationTable.locationUpdateCount += 1
        locationTable.capturedLocation = location
        locationTable.viewModel.save(location: location)
        locationTable.isSearchingForGPS = false
        
        reloadCoordinateSection()
        let handler = {(placemarks:[CLPlacemark]?,error: Error?) in
            guard error == nil else {
                print (" we got a reverse geo error \(String(describing: error))")
                return}
            let alreadyStoredKeys = GeocoderService.keysForStoredValues(in: self.locationTable.viewModel.site!)
            if alreadyStoredKeys == nil {
                GeocoderService.insertAll(placemarks: placemarks, into: self.locationTable.viewModel.site!)
            }else{
                let warning = UIAlertController(title: "Data Found", message: "New GPS values have been save.\n\nSome relevevant location infomation has been found.\n\n What action should Splash take ", preferredStyle: .actionSheet)
                let overwrite = UIAlertAction(title: "Overwrite all values", style: .destructive, handler: {action in
                    GeocoderService.insertAll(placemarks: placemarks, into: self.locationTable.viewModel.site!)
                    self.reloadLocationSection()
                })
                let insert = UIAlertAction(title: "Only insert missing values", style: .default, handler: {action in
                    GeocoderService.insertNew(placemarks: placemarks, into: self.locationTable.viewModel.site!)
                    self.reloadLocationSection()
                })
                let cancel = UIAlertAction(title: "Cancel", style: .cancel , handler: nil)
                warning.addAction(insert)
                warning.addAction(overwrite)
                warning.addAction(cancel)
                self.present(warning, animated: true, completion: nil)
            }
            self.reloadLocationSection()
        }
        GeocoderService.lookUp(location: location, with: handler)
    }
    
    fileprivate func reloadLocationSection() {
        locationTable.reloadSections(locationTableLocationSection, with: .automatic)
    }
    fileprivate func reloadCoordinateSection(){
        locationTable.reloadSections(locationTableCoordinationSection, with: .automatic)
    }
    func partialFix(location: CLLocation) {
        locationTable.capturedLocation = location
        reloadCoordinateSection()
    }
    func locationWasDenied(){
        bulletinManager.showBulletin(above: self)
    }
    
    func getCoordinatesCell()-> CoordinateCell{
        guard let cell = locationTable.cellForRow(at: gpsIndexPath) as? CoordinateCell else { fatalError()}
        
        return cell
    }
    func coordinateCellStartIndicator(){
        let coordinateCell = getCoordinatesCell()
        coordinateCell.indicator.startAnimating()
    }
    func coordinateCellStopIndicator(){
        let coordinateCell = getCoordinatesCell()
        coordinateCell.indicator.stopAnimating()
    }
}

extension SiteDetailedEditor : SegueHandlerType {
    enum Identifier : String  {
        case country
        case canBePublic
        case credit
        case cancellingWhileLogging
        case siteAddedWhileLogging
        case siteEditsSaved
        case siteAddedSaved
        case siteAddedCancelled
    }
}

let cancellingWhileLoggingSegueID = "cancellingWhileLogging"
let savingWhileLoggingSegueId = "savingWhileLogging"
let siteEditsSavedSegueId = "siteEditsSaved"
let siteEditsCancelledSegueId = "siteEditsCancelled"
let siteAddedSavedSegueId = "siteAddedSaved"
let siteAddedCancelledSegueId = "siteAddedCancelled"
let siteAddedWhileLoggingSegueId = "siteAddedWhileLogging"

extension SiteDetailedEditor: Injectable{
    func inject(_ injected: (dataModel: DataModel, mode: Mode)) {
        dataModel = injected.dataModel
        mode = injected.mode
    }
    
    
    func assertDependencies() {
        assert(dataModel != nil, "didnt get a datamodel")
        assert(dataModel.site != nil, " should always get a site in the passed in dataModel")
        assert(mode != nil,"didnt get a mode")
    }
    
    typealias T = (dataModel: DataModel, mode :Mode)
    
    
}
