//
//  UserCredit.swift
//  Splash
//
//  Created by Simon Gardener on 12/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class UserCredit: UIViewController {

    @IBOutlet weak var webField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    var userChangedValues : ( ()->() )!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameField.text = UserDefaults.associatedName()
        webField.text = UserDefaults.associateWeb()
    }
}

extension UserCredit : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        let text = StringWranglers.cleanedUpText(from: textField)
        if textField.placeholder == "enter name" {
            UserDefaults.setAssociatedName(name: text)
        }else {
            UserDefaults.setAssociatedWeb(web: text)
        }
        userChangedValues()
    }
}
