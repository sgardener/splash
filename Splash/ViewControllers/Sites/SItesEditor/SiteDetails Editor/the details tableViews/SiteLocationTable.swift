//
//  SiteLocationTable.swift
//  Splash
//
//  Created by Simon Gardener on 11/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.

import UIKit
import CoreLocation

class SiteLocationTable: UITableView, UITableViewDelegate, UITableViewDataSource {
    
    var viewModel: SiteLocationViewModel!
    weak var parentView : SiteDetailedEditor!
    var capturedLocation : CLLocation?
    
    var locationService: LocationService!
    var isSearchingForGPS = false
    var locationUpdateCount = 0
    
    func numberOfSections(in tableView: UITableView) -> Int {

        return viewModel.numberOfSections()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsIn(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName {
            
        case .nameLocation:
            guard let cell = dequeueReusableCell(withIdentifier: Identifier.nameLocation.rawValue, for: indexPath) as? TextFieldTableViewCell else { fatalError()}
            cell.textField.placeholder = viewModel.placeholder(for: indexPath)
            cell.textField.text = viewModel.value(for: indexPath)
            cell.textField.tag = indexPath.row
            cell.textField.delegate = self
            cell.accessoryType = .none
            cell.textField.isEnabled = true
            cell.selectionStyle = .none
            if indexPath.row == locationIdentifierOrder.country.rawValue {
                cell.accessoryType = .disclosureIndicator
                cell.textField.isEnabled = false
                cell.selectionStyle = .default
            }
            if indexPath.row == locationIdentifierOrder.tapToUseLast.rawValue {
                cell.textField.isEnabled = false
                cell.selectionStyle = .default
            }
            return cell
            
        case .canBePublic:
            let cell = dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
            cell.accessoryType = .disclosureIndicator
            cell.textLabel?.text = viewModel.secrecyString()
            return cell
            
        case .coordinates:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.coordinates.rawValue, for: indexPath) as? CoordinateCell else { fatalError("didn't get coordnate cell") }
            
            if isSearchingForGPS == true {
                cell.indicator.isHidden = false
                cell.indicator.startAnimating()
            } else {
                cell.indicator.isHidden = true
                cell.indicator.stopAnimating()
            }
            if let location = capturedLocation {
                cell.longitude.text = location.coordinate.longitude.toFourDecimalPlaces()
                cell.latitude.text = location.coordinate.latitude.toFourDecimalPlaces()
            }else {
                cell.longitude.text = viewModel.longitude()
                cell.latitude.text = viewModel.latitude()
                }
            return cell
            
        case .thisGPSIs:
            let cell = dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
            cell.textLabel?.text = viewModel.GPSIsOption(for: indexPath.row)
            cell.accessoryType = viewModel.gpsIsAccessoryForRow(at: indexPath)
            return cell
            
        case .accessToSite:
            let cell = dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
            cell.textLabel?.text = viewModel.accessOption(for: indexPath.row)
            cell.accessoryType = viewModel.accessOptionAccessory(for: indexPath.row)
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.headerStringFor(section)
    }
    
    func startGpsIfNeeded() {
        if isSearchingForGPS == false  {
            isSearchingForGPS = true
            locationUpdateCount = 0
            if locationService != nil {
                locationService.requestLocation()
                reloadCoordinateSection()
            }else {
                initiateGPSLookup()
            }
        }
    }
    
    fileprivate func getConfirmationAlert() -> UIAlertController {
        let message = "This will use your current GPS for this site in this app's database.\n\nYou should only do this if you are actually above the dive site, at the shore entry point or at nearby mooring."
        
        let confirmationAlert = UIAlertController(title: "Capture Coordinates?", message: message , preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Capture GPS Data", style: .destructive , handler: {action in
            self.startGpsIfNeeded()
        })
        let noAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        confirmationAlert.addAction(action)
        confirmationAlert.addAction(noAction)
        return confirmationAlert
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        deselectRow(at: indexPath, animated: true)
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName{
        case .nameLocation:
            if indexPath.row == locationIdentifierOrder.country.rawValue {
                parentView.performSegue(withIdentifier: locationIdentifier.country.rawValue , sender: self)
            } else if indexPath.row == locationIdentifierOrder.tapToUseLast.rawValue{
                //TODO: write method for using last data
                print("use last tapped")
                
            }
        case .coordinates:
            
            let confirmAlert = getConfirmationAlert()
            self.parentView.present(confirmAlert, animated: true, completion: nil)
            tableView.deselectRow(at: indexPath, animated: true)
            
        case .accessToSite:
            viewModel.flipAccessOrderOption(at: indexPath)
            reloadRows(at: [indexPath], with: .automatic)
        case .thisGPSIs :
            viewModel.flipGpsOption(at: indexPath.row)
            reloadSections(IndexSet.init(integer: indexPath.section), with: .automatic)
        case .canBePublic:
            parentView.performSegue(withIdentifier: Identifier.canBePublic.rawValue , sender: self)

        }
    }
  
}
//MARK:- Location Stuff
extension SiteLocationTable {
    
    func initiateGPSLookup() {
        locationService = LocationService(owner: parentView, desiredAccuracy: 10, within: 30)
        locationService.requestLocation()
        isSearchingForGPS = true
        reloadCoordinateSection()

    }

    func reloadCoordinateSection(){
        let section =  OptionOrder.coordinates.rawValue
        reloadRows(at: [IndexPath.init(row: 0, section: section)], with: .automatic)
    }
}
extension SiteLocationTable {
    enum locationIdentifier: String  {
        case name, alternativeName, bodyOfWater, islandAtoll, localAreaName, cityTown, county, stateProvince, country, tapToUseLast
    }
    enum locationIdentifierOrder: Int {
        case name, alternativeName, bodyOfWater, islandAtoll, localAreaName, cityTown, county, stateProvince, country, tapToUseLast
    }
    
}
extension SiteLocationTable: SegueHandlerType , OptionOrder{
    enum Identifier: String {
        case nameLocation, coordinates, thisGPSIs, canBePublic, accessToSite
    }
    enum OptionOrder: Int {
        case nameLocation, coordinates, thisGPSIs, canBePublic, accessToSite
    }
}

extension SiteLocationTable: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        viewModel.process(textField)
    }
    
}

