//
//  SiteEnvironmentTable.swift
//  Splash
//
//  Created by Simon Gardener on 11/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class SiteEnvironmentTable: UITableView, UITableViewDelegate, UITableViewDataSource  {
    
    var viewModel: SiteEnvironmentViewModel!
    weak var parentView : SiteDetailedEditor!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
              return viewModel.rows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
        cell.textLabel?.text = viewModel.label(at: indexPath)
        cell.accessoryType = viewModel.checkMark(at: indexPath) ? .checkmark: .none
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
          return viewModel.numberOfSections()
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.headerStringFor(section)
    }
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    viewModel.flipSelection(at: indexPath)
    reloadData()
    }
}
