//
//  SecrecyChooser.swift
//  Splash
//
//  Created by Simon Gardener on 12/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class SecrecyChooser: UITableViewController {
    
    enum SecrecyOrder: Int {
        case canBePublic , competitive, fishing
    }
 
    let canBePublic:Int16 = 0
    let competetitive:Int16  = 1
    let fishing:Int16  = 2
    let both:Int16 = 3

    var site : DiveSite!
    var block: (()->())!
    
    let secrecyOptions = ["Yes, site is generally know","No, unknown to competition","No, unknown to fishermen"]
    let header = "select if GPS can be displayed"
    let footer = "'No' will hide location in app."
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return secrecyOptions.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
        let secrecyValue = Int(site.keepLocationSecret)
        cell.textLabel?.text = secrecyOptions[indexPath.row]
        cell.accessoryType = .none
        switch indexPath.row{
        case SecrecyOrder.canBePublic.rawValue: if secrecyValue == 0 {
            cell.accessoryType = .checkmark
        }
        case SecrecyOrder.competitive.rawValue:
            
        if (secrecyValue == 1 || secrecyValue == 3) {
            cell.accessoryType = .checkmark
        }
        case SecrecyOrder.fishing.rawValue:
            if (secrecyValue == 2 || secrecyValue == 3) {
                cell.accessoryType = .checkmark
            }
            
        default:
            break
        }
        return cell

    }
    
   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        
        case Int(canBePublic):
            site.keepLocationSecret = canBePublic
            block()
            navigationController?.popViewController(animated: true)
        case Int(competetitive):
            if site.keepLocationSecret == canBePublic{
                site.keepLocationSecret = competetitive
            }else if site.keepLocationSecret == competetitive {
                site.keepLocationSecret = canBePublic
            }else if site.keepLocationSecret == both {
                site.keepLocationSecret = fishing
            }else if site.keepLocationSecret == fishing {
                site.keepLocationSecret = both
            }
            
        case Int(fishing):
            if site.keepLocationSecret == canBePublic {
                site.keepLocationSecret = fishing
            }else if site.keepLocationSecret == fishing {
                site.keepLocationSecret = canBePublic
            }else if site.keepLocationSecret == both{
                site.keepLocationSecret = competetitive
            }else if site.keepLocationSecret == competetitive {
                site.keepLocationSecret = both
            }

        default:
            break
    }
    tableView.reloadSections(IndexSet.init(integer: 0), with: .automatic)
    block()
    }
    
}

