//
//  SiteNoteTable.swift
//  Splash
//
//  Created by Simon Gardener on 11/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class SiteNoteTable: UITableView, UITableViewDelegate, UITableViewDataSource {
    
    var viewModel: SiteNoteViewModel!
    weak var parentView : SiteDetailedEditor! 
    
 func numberOfSections(in tableView: UITableView) -> Int {
              return viewModel.numberOfSections()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return viewModel.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionName =  optionOrderFor(position: indexPath.section)
        switch sectionName{
            
        case .note:
            guard let cell = dequeueReusableCell(withIdentifier: Identifier.note.rawValue, for: indexPath) as? NoteCell else { fatalError()}
            cell.textView.text = viewModel.note()
            return cell
        case .credit:
            let cell = dequeueReusableCell(withIdentifier: Identifier.credit.rawValue, for: indexPath)
            cell.textLabel?.text = viewModel.personToCredit()
            cell.detailTextLabel?.text = viewModel.webToCredit()
            return cell
        case .animals:
            guard let cell = dequeueReusableCell(withIdentifier: Identifier.animals.rawValue, for: indexPath) as? AnimalSliderCell else { fatalError()}
            cell.animalLabel.text = viewModel.animalString(at: indexPath)
            cell.slider.tag = indexPath.row
            cell.slider.value = viewModel.possibility(at: indexPath)
            return cell
        }

    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.headerStringFor(section)
    }
    //MARK:- Slider
    @IBAction func sliderChanges(_ sender: UISlider) {
        viewModel.setPossibility(value: sender.value, tag: sender.tag)
        let indexPath = IndexPath.init(row: sender.tag, section: OptionOrder.animals.rawValue)
        let cell = cellForRow(at: indexPath) as! AnimalSliderCell
        cell.animalLabel.text = viewModel.animalString(at: indexPath)
    }
}
extension SiteNoteTable:  SegueHandlerType, OptionOrder {
    enum Identifier: String {
        case note, credit, animals
    }
    enum OptionOrder: Int {
        case note, credit, animals
    }

}
 extension  SiteNoteTable : UITextViewDelegate{
    func textViewDidEndEditing(_ textView: UITextView) {
        viewModel.setNote(with: textView.text)
        textView.text = viewModel.note()
}
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

