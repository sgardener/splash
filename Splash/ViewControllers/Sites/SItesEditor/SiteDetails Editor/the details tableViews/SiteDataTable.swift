//
//  SiteDataTable.swift
//  Splash
//
//  Created by Simon Gardener on 11/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.

import UIKit
import RangeUISlider

class SiteDataTable: UITableView, UITableViewDelegate, UITableViewDataSource  {
    
    var viewModel: SiteDataViewModel!
    weak var parentView : SiteDetailedEditor!
    let currentID = "current"
    let oldRangeId = "rangeCell"
    let rangeID = "rangeUICell"
    let rangeCellHeight : CGFloat = 84.0
    let currentCellHeight :CGFloat = 72.0
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRow(in: section)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName{
        case .current: return currentCellHeight
        default: return rangeCellHeight
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName{
        case .current:
            guard let cell = dequeueReusableCell(withIdentifier: currentID, for: indexPath) as? CurrentCell else { fatalError()}
            cell.nameLabel.text = viewModel.currentNameLabel(for: indexPath)
            cell.valueLabel.text = viewModel.currentValue(for: indexPath)
            cell.currentSlider.value = Float(viewModel.currentSliderValue(for: indexPath))
            cell.currentSlider.tag = indexPath.row
            
            return cell
        default:
            guard let cell = dequeueReusableCell(withIdentifier: rangeID, for: indexPath) as? RangeUITableViewCell else { fatalError() }
                        
            cell.rangeUISlider.delegate = self
            cell.rangeUISlider.scaleMinValue =  CGFloat(viewModel.minimumValue(at: indexPath))
            cell.rangeUISlider.scaleMaxValue =  CGFloat(viewModel.maximumValue(at: indexPath))
            cell.rangeUISlider.defaultValueLeftKnob =  CGFloat(viewModel.lowerValue(at: indexPath))
            cell.rangeUISlider.defaultValueRightKnob =  CGFloat(viewModel.upperValue(at: indexPath))
            cell.rangeUISlider.tag = indexPath.section
            cell.lower.text = viewModel.labelTextForValue(at: indexPath.section, lower: true)
            cell.upper.text = viewModel.labelTextForValue(at: indexPath.section, lower: false)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.headerStringFor(section)
    }

    @IBAction func currentSliderUpdated(_ sender: UISlider) {
        viewModel.setCurrentValue(for: sender.tag, with: sender.value)
        let indexPath = IndexPath(row: sender.tag, section: OptionOrder.current.rawValue)
        let cell = cellForRow(at: indexPath) as! CurrentCell
        cell.valueLabel.text = viewModel.currentValue(for: indexPath)
    }

}
extension SiteDataTable :SegueHandlerType, OptionOrder{
    enum Identifier: String {
        case depthRange,typicalDepthRange,highSeasonViz, lowSeasonViz, waterTemp, current
        
    }
    enum OptionOrder : Int {
        case depthRange,typicalDepthRange,highSeasonViz, lowSeasonViz, waterTemp, current
    }
}
extension SiteDataTable : RangeUISliderDelegate{
    func rangeChangeFinished(minValueSelected: CGFloat, maxValueSelected: CGFloat, slider: RangeUISlider) {
        self.isScrollEnabled = true
    }
    
    func rangeIsChanging(minValueSelected: CGFloat, maxValueSelected: CGFloat, slider: RangeUISlider) {
        let indexPath = IndexPath(row: 0, section: slider.tag)
        let cell = cellForRow(at: indexPath) as! RangeUITableViewCell
        viewModel.setUpperValue(at: slider.tag  , with: Double(maxValueSelected))
        viewModel.setLowerValue(at: slider.tag, with: Double(minValueSelected))
        let label = viewModel.labelTextForValue(at: slider.tag, lower: true)
        cell.lower.text = label
        cell.upper.text = viewModel.labelTextForValue(at: slider.tag, lower: false)
        print("min: \(minValueSelected) -  max: \(maxValueSelected) - identifier: \(slider.tag)")
    }
    
    func rangeChangeStarted() {
        self.isScrollEnabled = false
    }
}
