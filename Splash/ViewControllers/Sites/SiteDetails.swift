//
//  SiteDetails.swift
//  Splash
//
//  Created by Simon Gardener on 25/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class SiteDetails: UITableViewController {
    
    var dataModel: DataModel!
    var viewModel : SiteDetailsViewModel!
    var diveSite: DiveSite!
    let switcherCellId = "SwitcherCell"
    let editSegueId = "editSite"
    let visitWebId = "visitWeb"
    let visitWebRefId = "visitWebRef"
    private enum cellId: String {
        case note = "noteCell"
        case map = "mapCell"
        case attribution = "attributionCell"
        case webReference = "webReferenceCell"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        diveSite = dataModel.site
        viewModel = SiteDetailsViewModel(with: diveSite)
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        addEditButton()
        title = diveSite.name ?? "unknown"
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print(diveSite as Any )// just debugging
    }
    
    fileprivate func addEditButton(){
        let barButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editSite))
        navigationItem.rightBarButtonItem = barButton
    }
    @objc fileprivate func editSite(){
        performSegue(withIdentifier: editSegueId, sender: self)
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return OptionOrder.webReference.rawValue + 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionPosition = optionOrderFor(position: section)
        switch sectionPosition{
        case  .markAsRegular, .markAsUserFavourite : return 1
        case .name:
            guard diveSite.name != nil else { return 0}
            return 1
        case .altName:
            return viewModel.numberOfRowsForAlternativeName()
            
        case .country:
            guard diveSite.country != nil else { return 0}
            return 1
        case .map:
            guard diveSite.coordinate != nil else { return 0}
            return 1
        case .note :
            guard diveSite.note != nil , diveSite.note != "" else {return 0}
            return 1
        case .noteAttribution :
            if diveSite.needsAttribution == true {
                return 1
            }else {
                return 0
            }
            
        case.location :
            return 1
        case .depthRange:
            return viewModel.hasDepthRange() ? 1:0
        case .typicalDepthRange:
            return viewModel.hasTypicalDepthRange() ? 1:0
        case .siteLocated:
            return viewModel.numberofRowsForSiteLocated()
        case .siteEnvironment:
            return viewModel.numberOfRowsForSiteEnvironment()
        case .siteFeatures:
            return viewModel.numberOfRowsForSiteFeatures()
        case .temperatureRange:
            return viewModel.numberOfRowsForTemperature()
        case .highSeasonViz:
            return viewModel.numberOfRowsForHighSeasonVisibility()
        case .lowSeasonViz:
            return viewModel.numberOfRowsForLowSeasonVisability()
        case .current:
            return viewModel.numberOfRowsForCurrent()
        case .bottomCompostion:
            return viewModel.numberofRowsForBottomComposition()
        case .salinity:
            return viewModel.numberIfRowsForSalinity()
        case .suitableFor:
            return viewModel.numberOfRowsForSuitable()
        case .accessible:
            return viewModel.numberOfRowsForAccessible()
        case .webReference:
            print(diveSite.webReference as Any)
            print("has webrefence \(diveSite.webReference == nil ? 0:1)")
            return diveSite.webReference == nil ? 0:1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let whichCell = optionOrderFor(position: indexPath.section)
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicHeaderCell", for: indexPath) as! BasicWithHeaderTableViewCell
        cell.selectionStyle = .none
        cell.accessoryType = .none
        
        switch whichCell {
            
        case .name:
            if let name = diveSite.name {
                cell.configureWith(header: "site name", info: name)
            }
            
        case .country:
            if let country = diveSite.country{
                cell.configureWith(header: "country", info: country)
            }
            
        case.location:
            cell.configureWith(header: "location", info:  viewModel.locationAsString())
            
        case .note:
            guard let noteCell = tableView.dequeueReusableCell(withIdentifier: cellId.note.rawValue, for: indexPath) as? NoteCell else {fatalError("Expecting a NoteCell- didnt get one")}
            noteCell.textView.isEditable = false
            noteCell.textView.text = diveSite.note
            
            //  noteCell.textView.text = "\(diveSite.note!)\n\n\(attributeString(for: diveSite))"
            return noteCell
            
            
        case .map:
            guard  let mapCell = tableView.dequeueReusableCell(withIdentifier: cellId.map.rawValue, for: indexPath) as? MapViewCell else {fatalError("didnt get a mapcell - expecteed one")}
            mapCell.centerMapOn(diveSite.coordinate, annotateWithtitle: diveSite.name, andSubTitle: diveSite.localAreaName, withColor: .blue)
            return mapCell
            
        case .markAsRegular:
            guard let switchCell = tableView.dequeueReusableCell(withIdentifier: switcherCellId, for: indexPath) as? SwitcherTableViewCell else { fatalError("Didnt get a switcherCell")}
            switchCell.markSwitch.tag = indexPath.section
            switchCell.selectionStyle = .none
            switchCell.configureWith("regular sites", bool: diveSite.regularSite)
            return switchCell
            
        case .markAsUserFavourite:
            guard let switchCell = tableView.dequeueReusableCell(withIdentifier: switcherCellId, for: indexPath) as? SwitcherTableViewCell else { fatalError("Didnt get a switcherCell")}
            switchCell.markSwitch.tag = indexPath.section
            switchCell.selectionStyle = .none
            switchCell.configureWith("favourite sites", bool: diveSite.userFavourite)
            return switchCell
            
        case .noteAttribution:
            guard let attributionCell = tableView.dequeueReusableCell(withIdentifier: cellId.attribution.rawValue) as? AttributionCell else {fatalError("Expecting an Attribution cell - didnt get one")}
            attributionCell.configureWith(site: diveSite)
            return attributionCell
        case .depthRange:
            cell.configureWith(header: "site depth range", info: viewModel.depthRangeString())
            
        case .siteLocated:
            cell.configureWith(header: "located", info:  viewModel.siteLocatedString())
        case .siteEnvironment:
            cell.configureWith(header: "environment", info:  viewModel.siteEnvironmentString())
        case .siteFeatures:
            cell.configureWith(header: "site features", info:  viewModel.siteFeaturesString())
        case .temperatureRange:
            cell.configureWith(header: "temperature range", info:  viewModel.temperatureString())
        case .highSeasonViz:
            cell.configureWith(header: "gigh season visibility", info:  viewModel.highSeasonVisibilityString())
        case .lowSeasonViz:
            cell.configureWith(header: "low season visibility", info:  viewModel.lowSeasonVisibility())
        case .current:
            cell.configureWith(header: "current", info:  viewModel.currentString())
        case .altName:
            cell.configureWith(header: "alternative name", info: viewModel.alternativeNameString())
        case .typicalDepthRange:
            cell.configureWith(header: "typical dive depth range", info: viewModel.typicalDepthRangeString())
        case .suitableFor:
            cell.configureWith(header: "suitable for", info: viewModel.suitableFor())
        case .salinity:
            cell.configureWith(header: "salinity", info: viewModel.salinity())
        case .bottomCompostion:
            cell.configureWith(header: "bottom composition", info: viewModel.bottomComposition())
        case .accessible:
            cell.configureWith(header: "accessible by", info: viewModel.accessibleBy())
        case .webReference:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId.webReference.rawValue, for: indexPath) as? WebReferenceTableViewCell else { fatalError("didnt get a attributioncell")}
            cell.configureWith(site: diveSite)
            return cell
        }
        return cell
    }
    
    private func attributeString(for site: DiveSite)-> String {
        
        var attributeString: String = ""
        if let noteAssociatedName = site.noteAssocName, !noteAssociatedName.isEmpty{
            attributeString = "Submitted by : \(noteAssociatedName)\n"
        }
        if let noteAssociatedWeb = site.noteAssocWeb, !noteAssociatedWeb.isEmpty {
            if attributeString.isEmpty == true {
                attributeString = "Submitted by : \(noteAssociatedWeb)"
            }else {
                attributeString = attributeString + "\(noteAssociatedWeb)"
            }
        }
        return attributeString
    }
    
    //TableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let whichCell = optionOrderFor(position: indexPath.section)
        
        switch whichCell {
        case .markAsRegular:
            diveSite.regularSite = !diveSite.regularSite
            tableView.reloadRows(at:[tableView.indexPathForSelectedRow!] , with: .automatic)
            
        case .markAsUserFavourite:
            diveSite.userFavourite = !diveSite.userFavourite
            tableView.reloadRows(at:[tableView.indexPathForSelectedRow!] , with: .automatic)
            
        default :
            break
        }
    }
    // switch action
    @IBAction func switchChanged(_ sender: UISwitch) {
        let whichCell = optionOrderFor(position: sender.tag)
        switch whichCell  {
        case .markAsRegular :
            diveSite.regularSite = sender.isOn
            
        case .markAsUserFavourite:
            diveSite.userFavourite = sender.isOn
            
        default: break
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == editSegueId {
            guard let vc = segue.actualDestination() as? SiteDetailedEditor else { fatalError("didnt get a SiteDetailed Editor scene")}
            
            vc.inject((dataModel:dataModel,mode: .edit ))
        }
        if segue.identifier == visitWebId {
            guard let vc = segue.destination as? WebViewController else {fatalError("didnt get a webviewcontroller")}
            vc.inject(diveSite.noteAssocWeb!)
        }
        if segue.identifier == visitWebRefId {
            guard let vc = segue.destination as? WebViewController else {fatalError("didnt get a webviewcontroller")}
            vc.inject(diveSite.webReference!)
        }
    }
    
    @IBAction func siteEditsSaved(segue:UIStoryboardSegue){
        tableView.reloadData()
    }
    @IBAction func siteEditsCancelled(segue: UIStoryboardSegue){
        //dont do anything here
    }
}
extension SiteDetails: OptionOrder {
    
    enum OptionOrder: Int {
        case name
        case altName
        case location
        case country
        case accessible
        case suitableFor
        case depthRange
        case typicalDepthRange
        case temperatureRange
        case highSeasonViz
        case lowSeasonViz
        case current
        case siteLocated
        case salinity
        case siteEnvironment
        case siteFeatures
        case bottomCompostion
        case markAsRegular
        case markAsUserFavourite
        case map
        case note
        case noteAttribution
        case webReference
        
    }
}

extension SiteDetails: Injectable{
    func inject(_ dm: DataModel) {
        dataModel = dm
    }
    
    func assertDependencies() {
        assert(dataModel != nil, "didnt get a datamodel")
        assert(dataModel.site != nil, "expected a divesite in dataModel")
    }
    
    typealias T = DataModel
}
