//
//  SitesByNearby.swift
//  Splash
//
//  Created by Simon Gardener on 26/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import BLTNBoard

class SitesByNearby: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var dataModel: DataModel!
    var locationService: LocationService!
    var searchComplete = false
    var coordinateRange: CoordinateRange!
    var knownDiveSites = [DiveSite]()
    var unknownDiveSites = [DiveSite]()
    
    let headerTitles = ["Sites with known location","Sites known to be nearby"]
    let cellID = "cell"
    
    lazy var bulletinManager: BLTNItemManager = {
        var page = BulletinDataSource.locationWasDeniedPage()
        return BLTNItemManager(rootItem: page)
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        title = "Nearby Sites"
        setUpView()
        activityIndicator.startAnimating()
        locationService = LocationService(owner: self, desiredAccuracy: 50, within: 100)
        locationService.requestLocation()
    }
    
    func setUpView(){
        setUpMessageLabel()
        setUpTableView()
        updateView()
    }
    
 func setUpMessageLabel(){
        if searchComplete == false {
            activityIndicator.isHidden = false
            activityIndicator.startAnimating()
            messageLabel.text = "Waiting on GPS. Are you inside ?"
        }else {
            activityIndicator.stopAnimating()
            activityIndicator.isHidden = true
            
            messageLabel.text = "Splash doesn't know any nearby dive sites.\n\n If you are at a site, please add it so it will show up next time."
        }
    }
    
    private func setUpTableView() {
        tableView.tableFooterView = UIView()
    }
    
    func updateView() {
        tableView.isHidden = !hasSites
        messageLabel.isHidden = hasSites
        setUpMessageLabel()
        tableView.reloadData()
    }
    
    private var hasSites: Bool {
        if knownDiveSites.count + unknownDiveSites.count == 0 {
            return false
        }else {
            return true
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.actualDestination() as? SiteDetails else { fatalError("Expecting a SiteDetails scene - didnt get one")}
        guard let indexPath = tableView.indexPathForSelectedRow else {fatalError("Didnt get a valid indexpath")}
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName{
        case .knownLocations:
           dataModel.site =  knownDiveSites[indexPath.row]
           vc.inject(dataModel)
        case .unknownLocations:
           dataModel.site = unknownDiveSites[indexPath.row]
           vc.inject(dataModel)
        }
    }
    
}
//MARK: - TableView Delegate
extension SitesByNearby: UITableViewDelegate {
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
       let site = siteAt(indexPath)
        if site.addedByUser == false {
            return false }
        if site.dive == nil || site.dive?.count == 0 {
            return true }
        return false
    }
    
    fileprivate func siteAt(_ indexPath:IndexPath)-> DiveSite {
        var site : DiveSite
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName {
        case .knownLocations:
            site = knownDiveSites[indexPath.row]
        case .unknownLocations:
            site = unknownDiveSites[indexPath.row]
        }
        return site
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete", handler: {action,indexPath in
            let siteToDelete = self.siteAt(indexPath)
            let alertController = UIAlertController(title: "Delete this dive site?", message: " You added this site.\n\nIt is safe to remove it as it is not referenced by any dives", preferredStyle: .actionSheet )
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {action in
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
            })
            let goAheadAction = UIAlertAction(title: "Delete", style: .destructive, handler: {(action) in
                let count = self.knownDiveSites.count + self.unknownDiveSites.count
                let sectionName = self.optionOrderFor(position: indexPath.section)
                switch sectionName {
                case .knownLocations:
                    self.knownDiveSites.remove(at: indexPath.row)
                case .unknownLocations:
                    self.unknownDiveSites.remove(at: indexPath.row)
                }

                self.dataModel.container.viewContext.delete(siteToDelete)
                self.dataModel.saveContext()
                
                if count == 1 {
                    self.navigationController?.popViewController(animated: true)
                } else {
                    self.tableView.reloadData()
                }
                
            }
            )
            alertController.addAction(goAheadAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        })
        return [deleteAction]
    }
}
//MARK: - TableView DataSource
extension SitesByNearby: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionName = optionOrderFor(position:section)
        switch sectionName {
        case .knownLocations:
            //print("known: \(knownDiveSites.count)")
            return knownDiveSites.count
        case .unknownLocations:
          //  print("unknonw: \(unknownDiveSites.count)")
            return unknownDiveSites.count
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sectionName = optionOrderFor(position: section)
        switch sectionName {
        case .knownLocations:
            if knownDiveSites.count > 0 {
                return headerTitles[section]
            }
        case .unknownLocations:
            if unknownDiveSites.count > 0 {
                return headerTitles[section]
            }
        }
        return nil
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName {
        case .knownLocations:
            cell.textLabel?.text = knownDiveSites[indexPath.row].name
        case .unknownLocations:
            cell.textLabel?.text = unknownDiveSites[indexPath.row].name
            
        }
        return cell
    }
}
//MARK: - Needs
extension SitesByNearby: LocationUser {
    func locationWasDenied(){
        bulletinManager.showBulletin(above: self)
    }
    func partialFix(location: CLLocation) {
    }

    func locationDidUpdate(location: CLLocation) {
        
        coordinateRange = CoordinateRange.init(withCenter: location, offset: 0.5)
        let rangePredicate = coordinateRange.searchPredicates()
        
        if var sitesInRange = dataModel.allDiveSitesIn(range: rangePredicate){
            Splash.sortFetched(nearbySites: &sitesInRange, byDistanceFrom: location)
            knownDiveSites = sitesInRange
        }

       if let generalAreas = dataModel.allGeneralAreasIn(range: rangePredicate), let nearestArea = nearestAreaIn(generalAreas, sortedByDistanceFrom: location), let unknownSitesInArea = dataModel.unknownLocationDiveSitesIn(generalArea: nearestArea) {
            unknownDiveSites = unknownSitesInArea
        }
        searchComplete = true
        updateView()
    }
    
    private func nearestAreaIn(_ generalAreas :[GeneralArea],sortedByDistanceFrom location: CLLocation)-> GeneralArea? {
        for generalArea in generalAreas {
            generalArea.distanceFromLocation = location.distance(from: generalArea.location) as NSNumber
        }
        let generalAreas = generalAreas.sorted { Int(truncating: $0.distanceFromLocation!) < Int(truncating: $1.distanceFromLocation! )}
        return generalAreas.first
    }
}
//MARK: - Injectable
extension SitesByNearby: Injectable{
    func inject(_ dm: DataModel) {
        dataModel = dm
    }
    
    func assertDependencies() {
        assert(dataModel != nil, "expected a dataModel - didnt get one")
    }
    
    typealias T = DataModel
}
//MARK: -
extension SitesByNearby: SegueHandlerType, OptionOrder{
    enum OptionOrder: Int {
        case knownLocations
        case unknownLocations
    }
    enum Identifier: String {
        case knownLocations
        case unknownLocations
    }
}
