//
//  SitesFavouritesList.swift
//  Splash
//
//  Created by Simon Gardener on 28/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class SitesFavouritesList: UITableViewController {
    
    var dataModel: DataModel!
    var favouriteSites : [DiveSite]!
    let cellID = "cell"
   
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        favouriteSites = dataModel.favouriteDiveSites()
        tableView.reloadData()
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return favouriteSites.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)

        cell.textLabel?.text = favouriteSites[indexPath.row].name

        return cell
    }




    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.actualDestination() as? SiteDetails else { fatalError("Expecting a SiteDetails scene - didnt get one")}
        guard let indexPath = tableView.indexPathForSelectedRow else {fatalError("Didnt get a valid indexpath")}
       dataModel.site = favouriteSites[indexPath.row]
        vc.inject(dataModel)
    }


}
extension SitesFavouritesList : Injectable {
    func inject(_ dm : DataModel) {
        dataModel = dm
    }
    
    func assertDependencies() {
        assert(dataModel != nil , "Expected datamodel got nil")
    }
    
    typealias T = DataModel
    
}
