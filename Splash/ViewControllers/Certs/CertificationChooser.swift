//
//  CertificationChooser.swift
//  Splash
//
//  Created by Simon Gardener on 19/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class CertificationChooser: UITableViewController {

    var dataModel: DataModel!
    var agency: String!
    
    lazy var frc: NSFetchedResultsController<CertificationType> = {
       return dataModel.fetchedResultsControllerForCertsTypeFor(agency)
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchCerts()
        
    }

    func fetchCerts(){
        do{
            try frc.performFetch()
        }catch {
            print("\(error), \(error.localizedDescription)")
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        guard let count = frc.sections?.count else { return 0 }
        return count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = frc.sections?[section] else { return 0 }
        return section.numberOfObjects
    }

  
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID , for: indexPath)
        let cert = frc.object(at: indexPath)
        cell.textLabel?.text = cert.name
        cell.detailTextLabel?.text = ""
        cell.accessoryType = .none
        cell.selectionStyle = .default
        if let diverCertSet  = dataModel.diver?.holdsCertification, let certArray = Array(diverCertSet) as? [Certification] {
            let matchingArray = certArray.filter(){$0.type?.name == cert.name }
            if matchingArray.count > 0 {
                cell.selectionStyle = .none
                cell.detailTextLabel?.text = "already added"
            }
        }
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        if cell?.selectionStyle == .default {
            let certType = frc.object(at: indexPath)
            let newCert = dataModel.newCertification()
            newCert.type = certType
            dataModel.theDiver?.addToHoldsCertification(newCert)
            performSegue(withIdentifier: "certAdded", sender: self)
        }
    }

}
extension CertificationChooser  :Injectable{ 
    
    func inject(_ injected: (dataModel: DataModel,agency: String)) {
        dataModel = injected.dataModel
        agency = injected.agency
    }
    
    func assertDependencies() {
        assert(dataModel != nil, "didnt get a datamodel")
        assert(agency != nil)
    }
    
    typealias T = (dataModel:DataModel, agency:String)
    
    
    
}
