//
//  CertificationsHeld.swift
//  Splash
//
//  Created by Simon Gardener on 19/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class CertificationsHeld: UIViewController {
    
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var dataModel: DataModel!
    var editMode: Mode!
    var certsHeld = [Certification]()
    lazy var frc : NSFetchedResultsController<Certification> = {
        return dataModel.fetchedResultsControllerForDiversCerts()
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        setUpView()
        frc.delegate = self
        fetchCerts()
        updateView()
    }
    
    private func setUpView(){
        setUpMessageLabel()
    }
    private func updateView() {
        tableView.isHidden = !hasCerts
        messageLabel.isHidden = hasCerts
    }
    private func setUpMessageLabel(){
        messageLabel.text = "To add certifications click that little '+' button up top."
        
    }
    
    
    private var hasCerts : Bool {
        guard let certs = frc.fetchedObjects else { return false }
        return certs.count > 0
    }
    private func fetchCerts (){
        do{
            try frc.performFetch()
        }catch {
            print("\(error), \(error.localizedDescription)")
        }
    }
    
    @IBAction func addButtonTapped(_ sender: UIBarButtonItem) {
        
        performSegue(withIdentifier: Identifier.selectAgency.rawValue, sender: self)
    }
    
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let segueName = segueIdentifierFor(segue: segue)
        switch segueName{
        case .selectAgency:
            guard let vc = segue.actualDestination() as? CertificationAgencyList else { fatalError("didnt get a certification agency list")}
            vc.inject(dataModel)
        case .certificationDetails:
            guard let vc = segue.actualDestination() as? CertificationDetails else { fatalError("Expected a cert details scene")}
            let cert = frc.object(at: tableView.indexPathForSelectedRow!)
            vc.inject((dataModel, cert))
        }
    }
    
    @IBAction func certWasAdded(segue:UIStoryboardSegue){
        //  the unwindSeque method -no action here
    }
}

extension CertificationsHeld : UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
        let cert = frc.object(at: indexPath)
        cell.textLabel?.text = cert.type?.name
        cell.detailTextLabel?.text = cert.type?.agency
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = frc.sections?[section] else { return 0 }
        return section.numberOfObjects
    }
}

extension CertificationsHeld : UITableViewDelegate{
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete", handler: {action,indexPath in
            let certToDelete = self.frc.object(at: indexPath)
            let alertController = UIAlertController(title: "Delete Certification?", message: "Are you sure you want to delete this certification?", preferredStyle: .actionSheet )
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {action in
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
            })
            let goAheadAction = UIAlertAction(title: "Yes", style: .destructive, handler: {(action) in
                self.dataModel.container.viewContext.delete(certToDelete)})
        alertController.addAction(goAheadAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        })
        return [deleteAction]
    }
}
extension CertificationsHeld : NSFetchedResultsControllerDelegate{

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    tableView.beginUpdates()
    }
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
            updateView()
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
            updateView()
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .automatic)
        @unknown default:
            print("Futureproof: NSFetchedResultscontroller didChange - and has added a new enum type")
        }
    }
}

extension CertificationsHeld : Injectable {
    func inject(_ dm: DataModel) {
        dataModel = dm
    }
    
    func assertDependencies() {
        assert(dataModel != nil, "Expected a data model , didnt get one")
    }
    
    typealias T = DataModel
    
}
extension CertificationsHeld : SegueHandlerType{
    enum Identifier: String {
        case selectAgency
        case certificationDetails
    }
}
