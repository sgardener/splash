//
//  CertificationDetails.swift
//  Splash
//
//  Created by Simon Gardener on 19/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class CertificationDetails: UITableViewController  {
    
    var dataModel: DataModel!
    var cert: Certification!
    var certDateIndexPath = IndexPath(row: OptionOrder.certDate.rawValue, section: 0)
    let instructorIndexPath = IndexPath(row: OptionOrder.certifyingInstructor.rawValue, section: 0)
    var certDateCellId = "certDate"
    let instructorCellId = "instructorCell"
    var datePicker : UIDatePicker!
    var toolbar: UIToolbar!
    let certNumberTag = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        datePicker = setUpDatePicker()
        toolbar = toolbarForPicker()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadRows(at: [instructorIndexPath], with: .automatic)
    }
    
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return OptionOrder.certifyingInstructor.rawValue + 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rowName = optionOrderFor(position: indexPath.row)
        switch rowName {
            
        case .certLevel:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID) else { fatalError("no cell")}
            cell.selectionStyle = .none
            cell.textLabel?.text = cert.type?.name
            return cell
            
        case .certAgency:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID)  else { fatalError("no cell")}
            cell.selectionStyle = .none
            cell.textLabel?.text = cert.type?.agency
            return cell
            
        case .certDate:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.certDate.rawValue, for: indexPath) as? TextFieldTableViewCell else { fatalError("no TFcell")}
            datePicker.sizeToFit()
            cell.textField.inputView = datePicker
            cell.textField.inputAccessoryView = toolbar
            cell.textField.placeholder = "certification date"
            cell.textField.text = dateString()
            return cell
            
        case .certNumber:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.certNumber.rawValue, for: indexPath) as? TextFieldTableViewCell else { fatalError("no TFcell")}
            cell.textField.tag = certNumberTag
            cell.textField.placeholder = "certification number"
            if let certNumber = cert.certnumber {
                cell.textField.text = certNumber
            }
            return cell
            
        case .certifyingInstructor:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: instructorCellId) else { fatalError("no cell")}
            if let instructor = cert.certifiedBy
            {
                
                cell.detailTextLabel?.text = "instructor number:\(instructor.proNumber ?? "unknown")"
                
                cell.textLabel?.text = instructor.displayName 
            }
            return cell
        }
    }
    fileprivate func dateString()->String? {
        if let date = cert.dateQualified{
            let timeZone = cert.timeZone ?? TimeZone.current.identifier
            return "certified: \(Date.formattedDate(date, timeZone: timeZone))"
        }else{
            return  nil
        }
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Identifier.certifyingInstructor.rawValue{
            guard let vc = segue.actualDestination()  as? SelectInstructorForCertification else { fatalError("expected a selectInstructorForCertifiation scene")}
            dataModel.cert = cert
            vc.inject(DiverViewModel(with: dataModel))
        }
    }
    
    fileprivate func setUpDatePicker()-> UIDatePicker{
        let dp = UIDatePicker()
        dp.sizeToFit()
        dp.datePickerMode = .date
        if let tz = cert.timeZone {
            dp.timeZone = TimeZone(identifier: tz)} else {
            dp.timeZone = TimeZone.current
        }
        dp.date = cert.dateQualified != nil ?cert.dateQualified! : Date()
        dp.addTarget(self, action: #selector(dateChanged), for: .valueChanged)
        return dp
    }
    //MARK:- Picker Stuff
    @objc fileprivate func dateChanged(){
        
        cert.dateQualified = datePicker.date
        if let  cell = tableView.cellForRow(at: certDateIndexPath) as? TextFieldTableViewCell{
            cell.textField.text = dateString()
        }
    }
    private func toolbarForPicker()-> UIToolbar  {
        let pickerToolbar = UIToolbar()
        pickerToolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(pickerDoneTapped))
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        pickerToolbar.setItems([flexible,doneButton], animated: false)
        return pickerToolbar
    }
    @objc func pickerDoneTapped() {
        view.endEditing(true)
    }
}

extension CertificationDetails : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == certNumberTag {
            
            cert.certnumber = textField.text
            // could do some cleanup of the textfield prior to setting the cert value
        }
    }
}
extension CertificationDetails: Injectable{
    func inject(_ injected: (dataModel: DataModel, cert: Certification)) {
        dataModel = injected.dataModel
        cert = injected.cert
    }
    
    func assertDependencies() {
        assert(dataModel != nil, "no datamodel")
        assert(cert != nil, "no cert")
    }
    
    typealias T = (dataModel:DataModel, cert:Certification)
    
    
}
extension CertificationDetails: SegueHandlerType, OptionOrder {
    enum Identifier: String {
        case certLevel, certAgency, certDate, certNumber, certifyingInstructor
    }
    enum OptionOrder: Int {
        case certLevel, certAgency, certDate, certNumber, certifyingInstructor
    }
}
