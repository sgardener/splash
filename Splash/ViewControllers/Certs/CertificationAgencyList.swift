//
//  CertificationAgencyList.swift
//  Splash
//
//  Created by Simon Gardener on 19/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class CertificationAgencyList: UITableViewController{
    
    
    var dataModel: DataModel!
    lazy var frc: NSFetchedResultsController<CertificationType> = {
        return dataModel.fetchedResultsControllerForCertificationTypes()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        fetchCertificationTypes()
    }
    fileprivate func fetchCertificationTypes(){
        do{
            try frc.performFetch()
        }catch{
            print("\(error), \(error.localizedDescription)")
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = frc.sections else { return 0 }
        return sections.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
        cell.textLabel?.text = frc.sections![indexPath.row].name
        return cell
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.actualDestination() as? CertificationChooser else { fatalError("Didnt get a certification Chooser")}
        guard let indexPath = tableView.indexPathForSelectedRow else {fatalError("if this fails I have a logic error to fix before shipping :)")}
        let agencyName = frc.sections![(indexPath.row)].name
        vc.inject((dataModel: dataModel, agency:agencyName ))
    }
}
extension CertificationAgencyList :Injectable{
    func inject(_ dm : DataModel) {
        dataModel = dm
    }
    
    func assertDependencies() {
        assert(dataModel != nil, "didnt get a datamodel")
    }
    
    typealias T = DataModel
    
}


