//
//  ProModeSettings.swift
//  Splash
//
//  Created by Simon Gardener on 27/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class ProModeSettings: UITableViewController {
    
    fileprivate  enum ProMode :Int {
        case guiding
        case instructing
        case assisting
    }
    fileprivate enum  ClientsMode: Int {
        case student
        case guidedDiver
    }
    let proModeSection = 0
    let newClientSection = 1
    let numberOfSections = 2
    
    let proModes = ["guiding","instructing","assisting"]
    let newClientsListedAs = ["students","guided divers"]
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case proModeSection:
            return proModes.count
            
        case newClientSection:
            return newClientsListedAs.count
        default: return 0
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
        
        cell.accessoryType = .none
        
        switch indexPath.section {
            
        case proModeSection:
            cell.textLabel?.text = proModes[indexPath.row]
            switch indexPath.row{
                
            case ProMode.guiding.rawValue:
                cell.accessoryType = UserDefaults.newLogEntryUsesGuideMode() == true ? .checkmark: .none
            case ProMode.instructing.rawValue:
                cell.accessoryType = UserDefaults.newLogEntryUsesInstructingMode() == true ? .checkmark: .none
                
            case ProMode.assisting.rawValue:
                cell.accessoryType = UserDefaults.newLogEntryUsesAssistingMode() == true ? .checkmark: .none
                
            default : break
            }
        case newClientSection:
            cell.textLabel?.text = newClientsListedAs[indexPath.row]
            switch indexPath.row {
            case ClientsMode.student.rawValue:
                cell.accessoryType = UserDefaults.newClientDiversIsStudent() == true ? .checkmark: .none
            case ClientsMode.guidedDiver.rawValue:
                cell.accessoryType = UserDefaults.newClientDiverIsGuidedDiver() == true ? .checkmark: .none
            default : break
            }
            
        default: break
        }
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section{
            
        case  proModeSection:
            switch indexPath.row{
            case ProMode.guiding.rawValue:
                UserDefaults.setGuideMode(to: !UserDefaults.newLogEntryUsesGuideMode())
                
            case ProMode.instructing.rawValue:
                UserDefaults.setInstuctingMode(to: !UserDefaults.newLogEntryUsesInstructingMode())
                
            case ProMode.assisting.rawValue:
                UserDefaults.setAssistingMode(to: !UserDefaults.newLogEntryUsesAssistingMode())
            default :  break
            }
        case newClientSection:
            switch indexPath.row{
            case ClientsMode.student.rawValue:
                UserDefaults.setClientDiverIsStudent(to: !UserDefaults.newClientDiversIsStudent())
                
            case ClientsMode.guidedDiver.rawValue:
                UserDefaults.setClientDiverIsGuidedDiver(to: !UserDefaults.newClientDiverIsGuidedDiver())
            default: break
            }
            
        default: break
        }
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case proModeSection:
            return " Tag new log entries as..."
            
        case newClientSection:
            return " Create new client divers as..."
        default: return nil
        }
    }
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        switch section {
        case proModeSection:
            return "If a log entry is tagged with 'guiding' or 'instucting' then 'guided diver' or 'student' fields are shown. The dive pro field is not displayed. When assisting all fields are shown."
            
        default: return nil
        }
        
    }
}
