//
//  InsurancePolicyList.swift
//  Splash
//
//  Created by Simon Gardener on 02/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.


import UIKit
import CoreData
class InsurancePolicyList: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var message: UILabel!
    var viewModel :OwnerViewModel!
    lazy var frc : NSFetchedResultsController<InsurancePolicy> = {
        return viewModel.frcForInsurancePolicies()
    }()
    let showSegueId = "showInsuranceDetails"
    let addSegueId = "addInsuranceDetails"
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        frc.delegate = self
        fetchPolicies()
        addAddButton()
        updateView()
    }
    
    fileprivate func fetchPolicies (){
        do{
            try frc.performFetch()
        }catch {
            print("\(error), \(error.localizedDescription)")
        }
    }
    fileprivate func addAddButton() {
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addPolicy))
        navigationItem.rightBarButtonItem  = addButton
    }
    private func updateView(){
        tableView.isHidden = !viewModel.hasPolicies()
        message.isHidden = viewModel.hasPolicies()
    }
    
    @objc private func addPolicy(){
        
        performSegue(withIdentifier: addSegueId, sender: self)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.actualDestination() as? PolicyDetails else { fatalError()}
        if segue.identifier == showSegueId {
            let indexPath = tableView.indexPathForSelectedRow!
            let policy = frc.object(at: indexPath)
            viewModel.set(policy)
            viewModel.mode = .show
            vc.inject(viewModel)
            
        }else if segue.identifier == addSegueId {
            viewModel.newInsurancePolicy()
            viewModel.mode = .add
            vc.inject(viewModel)
            
        }
    }
    
    fileprivate func prepare(for segue: UIStoryboardSegue,withMode mode:Mode, andPolicy policy: InsurancePolicy){
        
    }
    
}
extension InsurancePolicyList: UITableViewDelegate{
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete", handler: {action,indexPath in
            let objectToDelete = self.frc.object(at: indexPath)
            let alertController = UIAlertController(title: "Delete Policy?", message: "Are you sure you want to delete this insurance policy?", preferredStyle: .actionSheet )
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {action in
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
            })
            let goAheadAction = UIAlertAction(title: "Yes", style: .destructive, handler: {(action) in
                self.viewModel.delete(objectToDelete)})
            alertController.addAction(goAheadAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        })
        return [deleteAction]
    }
}


extension InsurancePolicyList: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return frc.sections![section].numberOfObjects
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
        let policy = frc.object(at: indexPath)
        cell.textLabel?.text = policy.name ?? "needs policy name"
        cell.detailTextLabel?.text = policy.number ?? "needs policy number"
        return cell
    }
}

extension InsurancePolicyList : Injectable{
    func inject(_ ovm: OwnerViewModel) {
        viewModel = ovm
    }
    
    func assertDependencies() {
        assert(viewModel != nil, "No View Model")
    }
    
    typealias T = OwnerViewModel
    
    
    
}
extension InsurancePolicyList: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
            updateView()
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
            updateView()
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .automatic)
        @unknown default:
            print("Futureproof: NSFetchedResultscontroller didChange - and has added a new enum type")
            
        }
    }
}
