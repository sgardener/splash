//
//  PolicyDetails.swift
//  Splash
//
//  Created by Simon Gardener on 02/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit


class PolicyDetails: UITableViewController {
    
    var viewModel :OwnerViewModel!
    let policyDatesId = "policyDates"
    let policyTypeId = "policyType"
    let policyLabelDataCellId = "policyLabelDataCell"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Details"
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.rowHeight = UITableView.automaticDimension
        if viewModel.mode == .add {
            setUpBarButtons()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        /// Reloads the 2 sections that could have changed when the user comes back to this view from
        /// either the policy type or the policy validity dates
        if !isMovingToParent{
            let sectionIndex = IndexSet([OptionOrder.policyDates.rawValue, OptionOrder.policyType.rawValue])
            //retuning from datePickers
            tableView.reloadSections(sectionIndex, with: .automatic)
        }
    }
    func setUpBarButtons(){
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelNewPolicy))
        let saveButton =  UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveNewPolicy))
        navigationItem.leftBarButtonItem = cancelButton
        navigationItem.rightBarButtonItem = saveButton
    }
    @objc fileprivate func cancelNewPolicy(){
        viewModel.cancelNewPolicy()
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    @objc fileprivate func saveNewPolicy(){
        viewModel.save()
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return OptionOrder.note.rawValue + 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionName = optionOrderFor(position: section)
        switch sectionName {
        case .expiring: return 0
        default : return 1
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let sectionName = optionOrderFor(position: indexPath.section)
        
        switch sectionName{
        case .expiring:
            guard let switchCell = tableView.dequeueReusableCell(withIdentifier: Identifier.expiring.rawValue, for: indexPath) as? SwitchCell else { fatalError()}
            return switchCell
        case .policyName,.insurerName,.policyNumber,.emergencyPhoneNumber,.depthLimit,.emailContact,.website:
            return policyLabelDataCell(for: indexPath)
            
        case .policyType:
            guard let policyTypeCell =  tableView.dequeueReusableCell(withIdentifier: policyTypeId, for: indexPath) as? PolicyTypeTableViewCell else {fatalError()}
            policyTypeCell.configure(with: viewModel.currentPolicy)
            return policyTypeCell
            
        case .policyDates:
            guard let datesCell = tableView.dequeueReusableCell(withIdentifier: policyDatesId, for: indexPath) as? PolicyDateTableViewCell else {fatalError("not a policy dateCell")}
            datesCell.configure(with: viewModel.currentPolicy )
            return datesCell
            
        case .note:
            guard let noteCell = tableView.dequeueReusableCell(withIdentifier: Identifier.note.rawValue, for: indexPath) as? NoteCell else { fatalError()}
            noteCell.textView.text = viewModel.textForInsuranceNote()
            return noteCell
        }
    }
    
    private func policyLabelDataCell(for indexPath:IndexPath)-> PolicyLabelDataCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: policyLabelDataCellId, for: indexPath) as? PolicyLabelDataCell else { fatalError()}
        cell.configure(with: viewModel, at: indexPath)
        return cell
    }
    
    //MARK:- TableView Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName{
        case .policyName,.insurerName,.policyNumber,.emergencyPhoneNumber,.depthLimit,.emailContact,.website:
            let cell = tableView.cellForRow(at: indexPath) as! PolicyLabelDataCell
            cell.textField.becomeFirstResponder()

        default: break
        }
    }
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == policyDatesId {
            let policyDatesVC = segue.actualDestination() as! PolicyDateViewController
            policyDatesVC.inject(viewModel.currentPolicy)
        }
        if segue.identifier == policyTypeId {
            let policyTypeVC = segue.actualDestination() as! PolicyTypeChooser
            policyTypeVC.inject(viewModel)
        }
    }
}
extension PolicyDetails: SegueHandlerType, OptionOrder{
    
    
    
    enum Identifier: String {
        case policyName
        case insurerName
        case policyNumber
        case policyType
        case emergencyPhoneNumber
        case policyDates
        case depthLimit
        case emailContact
        case website
        case expiring
        case note
    }
    enum OptionOrder: Int {
        case policyName
        case insurerName
        case policyNumber
        case policyType
        case emergencyPhoneNumber
        case policyDates
        case depthLimit
        case emailContact
        case website
        case expiring
        case note
    }
}

extension PolicyDetails : Injectable{
    func inject(_ ovm: OwnerViewModel){
        viewModel = ovm
    }
    func assertDependencies() {
        assert(viewModel != nil , "expected a viewModel")
        assert(viewModel.mode != nil, "mode not set")    }
    
    typealias T = (OwnerViewModel)
    
}

extension PolicyDetails :UITextFieldDelegate, UITextViewDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        viewModel.store(data: textField.text, at: textField.tag)
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        viewModel.storeNote(textView.text)
       
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        else {
            return true
        }
    }
}


