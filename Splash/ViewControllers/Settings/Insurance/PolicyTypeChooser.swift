//
//  PolicyTypeChooser.swift
//  Splash
//
//  Created by Simon Gardener on 02/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData
class PolicyTypeChooser: UITableViewController {

    var viewModel: OwnerViewModel!
    var frc: NSFetchedResultsController<InsurancePolicyType>!
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        frc = viewModel.frcForPolicyType()
        fetchPolicyTypes()
    }

    private func fetchPolicyTypes(){
        do{
            try frc.performFetch()
        }catch {
            print("\(error), \(error.localizedDescription)")
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return frc.sections![0].numberOfObjects
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
        let policyType = frc.object(at: indexPath)
        cell.textLabel?.text = policyType.type
        cell.accessoryType = viewModel.accessoryTypeFor(policyType)
        return cell
    }
    
    //MARK: TableView Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let policyType = frc.object(at: indexPath)
        viewModel.userChangedStatus(of: policyType)
        tableView.reloadData()
    }
}
extension PolicyTypeChooser: Injectable{
    func inject(_ vm: OwnerViewModel) {
        viewModel = vm
    }
    
    func assertDependencies() {
        assert(viewModel != nil, "PolicyTypeChooser expected a viewmodel ")
        assert(viewModel.currentPolicy != nil)
    }
    
    typealias T = OwnerViewModel
}

