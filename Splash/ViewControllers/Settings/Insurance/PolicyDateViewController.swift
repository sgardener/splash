//
//  PolicyDateViewController.swift
//  Splash
//
//  Created by Simon Gardener on 04/02/2019.
//  Copyright © 2019 Simon Gardener. All rights reserved.
//

import UIKit

class PolicyDateViewController: UIViewController {

    var policy : InsurancePolicy!
    let aYearInSeconds : TimeInterval =  31536000
    var existingStartDate : Date?
    var existingEndDate: Date?
    
    @IBOutlet weak var startDatePicker: UIDatePicker!
    @IBOutlet weak var endDatePicker: UIDatePicker!
    override func viewDidLoad() {

        super.viewDidLoad()
        
       
        assertDependencies()
        addCancelButton()
        captureExistingDates()
        setDatesOnPickers()
    }
    
    fileprivate func captureExistingDates(){
        existingStartDate = policy.startDate
        existingEndDate = policy.endDate
    }
    fileprivate func setDatesOnPickers() {
        if let startDate = policy.startDate {
            startDatePicker.setDate(startDate, animated: true)
        }else { startDatePicker.setDate(Date(), animated: true)
            
        }
        if let endDate = policy.endDate {
            endDatePicker.setDate(endDate, animated: true)
        }else {
            endDatePicker.setDate(startDatePicker.date.addingTimeInterval(aYearInSeconds),animated: true)
        }
    }
    fileprivate func addCancelButton(){
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .undo, target: self, action: #selector(undoDateChanges))
        self.navigationItem.rightBarButtonItem = cancelButton
    }
    @objc fileprivate func undoDateChanges(){
        policy.startDate = existingStartDate
        policy.endDate = existingEndDate
        setDatesOnPickers()
    }

    @IBAction func startDateChanged(_ sender: UIDatePicker) {
        policy.startDate = sender.date
    }
    
    
    @IBAction func endDateChanged(_ sender: UIDatePicker) {
        policy.endDate = sender.date
    }
    
    
}
extension PolicyDateViewController : Injectable {
    func inject(_ insurancePolicy : PolicyDateViewController.T) {
        policy = insurancePolicy
        
    }
    func assertDependencies() {
        assert(policy != nil, "valid Insurance policy not passed in")
    }
    
    typealias T  = InsurancePolicy
}
