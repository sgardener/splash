//
//  EmergencyContactsDetails.swift
//  Splash
//
//  Created by Simon Gardener on 03/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class EmergencyContactsDetails: UITableViewController {
    
    var viewModel : EmergencyContactViewModel!
    var toolbar: UIToolbar!
    var relationshipPicker = UIPickerView()
    var orderPicker = UIPickerView()
    let relativeDSD = RelationshipPickerDataSourceDelegate()
    let contactOrderDSD = ContactOrderPickerDataSourceDelegate()
    let labelValueCell  = "labelValueCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if viewModel.mode == .add {
            setUpBarButtons()
        }
        setUpPickersAndToolBar()
        viewModel.updatePhonesEmailsSocials()
        tableView.keyboardDismissMode = .interactive
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    func setUpBarButtons(){
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelNewContact))
        let saveButton =  UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveNewContact))
        navigationItem.leftBarButtonItem = cancelButton
        navigationItem.rightBarButtonItem = saveButton
    }
    @objc fileprivate func cancelNewContact(){
        viewModel.cancelNewContact()
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    @objc fileprivate func saveNewContact(){
        viewModel.save()
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    fileprivate func setUpPickersAndToolBar(){
        toolbar = toolbarForPicker()
        setUpRelationshipPicker()
        setUpOrderPicker()
    }
    fileprivate func setUpRelationshipPicker(){
        // need to pass a block the
        // a) assigns the value from the picker
        // B) reloads the row
        let handler = {(_ relationship: String) in
            self.viewModel.setEmergencyContact(relationship: relationship)
            self.tableView.reloadRows(at: [IndexPath.init(row: 0, section: OptionOrder.relationship.rawValue)] , with: .automatic)
        }
        relativeDSD.handler = handler
        relationshipPicker.sizeToFit()
        relationshipPicker.tag = OptionOrder.relationship.rawValue
        relationshipPicker.delegate = relativeDSD
        relationshipPicker.dataSource = relativeDSD
        
        
        
    }
    fileprivate func setUpOrderPicker(){
        let handler = {(_ row: Int) in
            self.viewModel.setEmergencyContact(contactOrder: row)
            self.tableView.reloadRows(at: [IndexPath.init(row: 0, section: OptionOrder.contactOrder.rawValue)], with: .automatic)
        }
        contactOrderDSD.handler = handler
        orderPicker.sizeToFit()
        orderPicker.tag = OptionOrder.contactOrder.rawValue
        orderPicker.delegate = contactOrderDSD
        orderPicker.dataSource = contactOrderDSD
    }
    fileprivate func toolbarForPicker()-> UIToolbar  {
        let pickerToolbar = UIToolbar()
        pickerToolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(pickerDoneTapped))
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        pickerToolbar.setItems([flexible,doneButton], animated: false)
        return pickerToolbar
    }
    @objc func pickerDoneTapped() {
        view.endEditing(true)
        deselectPickerRow()
        
    }
    func deselectPickerRow(){
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return OptionOrder.note.rawValue + 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionName = optionOrderFor(position: section)
        switch sectionName{
        case .contactID, .contactOrder, .relationship, .note: return 1
        case .phone: return viewModel.phoneCount() + 1
        case .email: return viewModel.emailCount() + 1
        case .social: return viewModel.socialCount() + 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName{
            
        case .contactID:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.contactID.rawValue, for: indexPath) as? DiverIDCell else {fatalError()}
            cell.configure(with: viewModel.currentContact)
            
            return cell
        case .relationship:
            let cell = pickerCell(for: indexPath)
            cell.triggeringTextField.inputView = relationshipPicker
            
            cell.label.text = viewModel.relationshipString()
            return cell
        case .contactOrder:
            let cell = pickerCell(for: indexPath)
            cell.triggeringTextField.inputView = orderPicker
            cell.label.text = viewModel.orderString()
            return cell
            
        case .note:
            guard let cell =  tableView.dequeueReusableCell(withIdentifier: Identifier.note.rawValue, for: indexPath) as? NoteCell else { fatalError("No noteCell for EcDetails")}
            cell.textView.text = viewModel.emergencyContactNote()
            return cell
            
        //MARK: email phone social cells
        case .phone:
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "plusCell", for: indexPath)
                cell.textLabel?.text = "+ phone number"
                return cell
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: labelValueCell, for: indexPath) as? LabelDataTextPairCell else {fatalError("not a labelDataTextPairCell")}
                cell.labelTextField.text = viewModel.phoneLabel(at: indexPath.row)
                cell.dataTextField.text = viewModel.phoneData(at: indexPath.row)
                placeholdersFor(cell, indexPath)
                return cell
            }
            
            
        case .email:
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "plusCell", for: indexPath)
                cell.textLabel?.text = "+ email address"
                return cell
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: labelValueCell, for: indexPath) as? LabelDataTextPairCell
                    else { fatalError("not a labelDataTextPairCell ")}
                cell.labelTextField.text = viewModel.emailLabel(at: indexPath.row)
                
                cell.dataTextField.text = viewModel.emailData(at: indexPath.row)
                
                placeholdersFor(cell, indexPath)
                return cell
            }
            
        case.social:
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "plusCell", for: indexPath)
                cell.textLabel?.text = "+ social media account"
                return cell
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: labelValueCell, for: indexPath) as? LabelDataTextPairCell else { fatalError("not a labelDataTextPairCell ")}
                cell.labelTextField.text = viewModel.socialLabel(at: indexPath.row)
                cell.dataTextField.text = viewModel.socialData(at: indexPath.row)
                placeholdersFor(cell, indexPath)
                return cell
            }
            
        }
      
    }
    
    fileprivate func placeholdersFor(_ cell: LabelDataTextPairCell, _ indexPath: IndexPath) {
        cell.labelTextField.placeholder = viewModel.placeholderForPhoneEmailSocialLabel(at: indexPath.section, offsetBy: OptionOrder.phone.rawValue)
        cell.dataTextField.placeholder = viewModel.placeholderforPhoneEmailSocialValue(at: indexPath.section, offsetBy: OptionOrder.phone.rawValue)
    }
    fileprivate func pickerCell(for indexPath: IndexPath)-> PickerCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "pickerCell", for: indexPath) as? PickerCell else { fatalError("No picker cell")}
        cell.triggeringTextField.tag = indexPath.section
        cell.triggeringTextField.inputAccessoryView = toolbar
        return cell
    }
    
    //MARK: TableView Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sectionName = optionOrderFor(position: indexPath.section)
        
        switch sectionName{
        case .contactOrder, .relationship:
            guard let cell = tableView.cellForRow(at: indexPath) as? PickerCell else { fatalError("Didnt get right type of cell")}
            if cell.triggeringTextField.isFirstResponder == true {
                cell.triggeringTextField.resignFirstResponder()
                tableView.deselectRow(at: indexPath, animated: true)
            }else{
                cell.triggeringTextField.becomeFirstResponder()
            }
        case .phone:
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
                viewModel.addNewPhoneNumber()
                tableView.reloadSections(IndexSet.init(integer: indexPath.section)  , with: .automatic)
            }else {
                performSegue(withIdentifier: Identifier.phone.rawValue, sender: self)
            }
        case .email:
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
                viewModel.addNewEmail()
                tableView.reloadSections(IndexSet.init(integer: indexPath.section)  , with: .automatic)
            }else {
                performSegue(withIdentifier: Identifier.email.rawValue, sender: self)
            }
        case .social:
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
                viewModel.addNewSocial()
                tableView.reloadSections(IndexSet.init(integer: indexPath.section)  , with: .automatic)
            }else {
                performSegue(withIdentifier: Identifier.social.rawValue, sender: self)
            }
        case .contactID , .note:
            break
        }
    }
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let sectionName = optionOrderFor(position: indexPath.section)
        
        switch sectionName{
        case .phone, .email, .social :
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
                return false
            }else {
                return true
            }
        default: return false
        }
    }
    
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete", handler: {action,indexPath in
            let alertController = UIAlertController(title: "Confirm Delete", message: "Are you sure you want to delete this contact data?", preferredStyle: .actionSheet )
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {action in
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
            })
            let goAheadAction = UIAlertAction(title: "Yes", style: .destructive, handler: {(action) in
                self.viewModel.removeContactDataObject(at: indexPath, offsetBy: OptionOrder.phone.rawValue)
                
                self.tableView.reloadSections(IndexSet.init(integer: indexPath.section), with: .automatic)
                
            })
            
            alertController.addAction(goAheadAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        })
        return [deleteAction]
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let segueID = segueIdentifierFor(segue: segue)
        
        switch segueID {
        case .contactID:
            guard let vc = segue.destination as? DiverIDDetails else { fatalError()}
            vc.inject(viewModel)
            
        case .phone:
            let indexPath = tableView.indexPathForSelectedRow!
            let object = viewModel.phones[indexPath.row]
            let type = "Phone Number"
            guard let vc = segue.actualDestination() as? DiverESPEditor else{ fatalError()}
            vc.inject((object, type))
        case .email:
            let indexPath = tableView.indexPathForSelectedRow!
            let object = viewModel.emails[indexPath.row]
            let type = "Email Address"
            guard let vc = segue.actualDestination() as? DiverESPEditor else{ fatalError()}
            vc.inject((object, type))
            
        case .social: let indexPath = tableView.indexPathForSelectedRow!
        let object = viewModel.socials[indexPath.row]
        let type = "Social Media"
        guard let vc = segue.actualDestination() as? DiverESPEditor else{ fatalError()}
        vc.inject((object, type))
            
        case .relationship ,.contactOrder: break
            
        case .note:
            break
        }
        
    }
}

extension EmergencyContactsDetails: SegueHandlerType,OptionOrder{
    enum Identifier : String{
        case contactID
        case relationship
        case contactOrder
        case phone
        case email
        case social
        case note
        
    }
    
    enum OptionOrder : Int {
        case contactID
        case relationship
        case contactOrder
        case phone
        case email
        case social
        case note
    }
}

extension EmergencyContactsDetails : Injectable{
    func inject(_ vm: EmergencyContactViewModel) {
        viewModel = vm
    }
    
    func assertDependencies() {
        assert(viewModel != nil, "No viewModel for EmergencyContactDetails")
        //        assert(viewModel. ,  "No viewModel  for EmergencyContactDetails")
    }
    
    typealias T = EmergencyContactViewModel
    
}

extension EmergencyContactsDetails : UITextViewDelegate{
    func textViewDidEndEditing(_ textView: UITextView) {
        viewModel.storeNote(textView)
    }
}
extension EmergencyContactsDetails : UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
}

