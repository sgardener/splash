//
//  EmergencyContactsList.swift
//  Splash
//
//  Created by Simon Gardener on 03/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData
import ContactsUI

class EmergencyContactsList: UIViewController {
    
    var viewModel : EmergencyContactViewModel!
    var frc : NSFetchedResultsController<EmergencyContact>!
    let messageText = """
        You don't have any emergency contacts.
        
        Who you gonna call ?
        """
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        frc = viewModel.frcForEmergencyContacts()
        frc.delegate = self
        fetchEmergencyContacts()
        updateView()
        // Do any additional setup after loading the view.
    }
    
    fileprivate func fetchEmergencyContacts(){
        do{
            try frc.performFetch()
        }catch {
            print("\(error), \(error.localizedDescription)")
        }
    }
    
    fileprivate func updateView(){
        message.text = messageText
        tableView.isHidden = !viewModel.hasEmergencyContacts()
        message.isHidden = viewModel.hasEmergencyContacts()
    }
    
    @IBAction func addButtonTapped(_ sender: UIBarButtonItem) {
        let whatKindOfAddition =  UIAlertController(title: "Add Diver", message: "How would you like to add a new emergency contact(s)?", preferredStyle: .actionSheet )
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let addNewDiver = UIAlertAction(title: "Create new contact", style: .default, handler: {(alertAction) in
            self.performSegue(withIdentifier: Identifier.addContact.rawValue, sender: self)
        })
        let selectFromContacts = UIAlertAction(title: "Select from contacts", style: .default , handler: {(alertAction) in
            self.showContactPickerForSinglePick()})
        
        
        whatKindOfAddition.addAction(selectFromContacts)
        whatKindOfAddition.addAction(addNewDiver)
        
        whatKindOfAddition.addAction(cancelAction)
        present(whatKindOfAddition, animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.actualDestination() as? EmergencyContactsDetails else { fatalError("ECList didn't get a segue to ECDetails") }
        
        let segueId = segueIdentifierFor(segue: segue)
        switch segueId{
        case .addContact:
            viewModel.newEmergencyContact()
            viewModel.mode = .add
          vc.inject(viewModel)
   
        case .showContact:
            guard let indexPath = tableView.indexPathForSelectedRow else { fatalError("No selected indexPath")}
            let contact = frc.object(at: indexPath)
            viewModel.set(emergencyContact: contact)
            viewModel.mode = .show
            vc.inject(viewModel)
        }
    }
    
    @objc func addEmergencyContactPrompt(){
        
      
    }
    func showContactPickerForSinglePick(){
        let contactPicker = CNContactPickerViewController.init()
        contactPicker.delegate = self
        present(contactPicker, animated: true, completion: nil)
    }
}
extension EmergencyContactsList : CNContactPickerDelegate{
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        viewModel.addSelected(contact)
    }
}
extension EmergencyContactsList : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return frc.sections![section].numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
        let emergencyContact = frc.object(at: indexPath)
        
        cell.textLabel?.text = viewModel.displayName(for: emergencyContact)
        cell.detailTextLabel?.text = viewModel.contactAndRelationship(for: emergencyContact)
        return cell
    }
}

extension EmergencyContactsList : UITableViewDelegate{
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete", handler: {action,indexPath in
            let objectToDelete = self.frc.object(at: indexPath)
            let alertController = UIAlertController(title: "Delete Emergency Contact?", message: "Are you sure you want to delete this emergency Contact?", preferredStyle: .actionSheet )
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {action in
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
            })
            let goAheadAction = UIAlertAction(title: "Yes", style: .destructive, handler: {(action) in
                self.viewModel.delete(objectToDelete)})
            alertController.addAction(goAheadAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        })
        return [deleteAction]
    }
}
extension EmergencyContactsList : NSFetchedResultsControllerDelegate{
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType){
        
        switch type {
        case .insert:
            let sectionIndexSet = NSIndexSet(index: sectionIndex)
            self.tableView.insertSections(sectionIndexSet as IndexSet, with: UITableView.RowAnimation.fade)
            updateView()
        case .delete:
            let sectionIndexSet = NSIndexSet(index: sectionIndex)
            self.tableView.deleteSections(sectionIndexSet as IndexSet, with: UITableView.RowAnimation.fade)
            updateView()
        default:
            break
        }
    }
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
            updateView()
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
            updateView()
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .automatic)
        @unknown default:
            print("Futureproof: NSFetchedResultscontroller didChange - and has added a new enum type")
            
        }
    }
}
extension EmergencyContactsList : Injectable{
    func inject(_ vm: EmergencyContactViewModel) {
        viewModel = vm
    }
    func assertDependencies() {
        assert(viewModel != nil , "expected a emergencyCOntacts ViewModel")
    }
    typealias T = EmergencyContactViewModel
}
extension  EmergencyContactsList : SegueHandlerType, OptionOrder {
    enum Identifier: String {
        case showContact
        case addContact
    }
    enum OptionOrder: Int {
        case showContact
        case addContact
    }
}


