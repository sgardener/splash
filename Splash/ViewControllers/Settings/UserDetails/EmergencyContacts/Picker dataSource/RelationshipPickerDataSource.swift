//
//  RelationshipPickerDataSource.swift
//  Splash
//
//  Created by Simon Gardener on 04/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class RelationshipPickerDataSourceDelegate :NSObject, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var handler : ((String)->())!
    
    let titles = ["not specified","Partner","Mother","Father","Brother","Sister",  "Son","Daughter","Uncle","Aunt","Grandfather","Grandmother","Grandson","Granddaughter","Cousin","In-Law","Girlfriend","BoyFriend", "Friend", "Boss","Co-worker" ]
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return  1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return titles.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return titles[row]
    }
  
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return 300
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        handler(titles[row])
    }
}

