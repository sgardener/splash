//
//  ContactOrderPickerDataSource.swift
//  Splash
//
//  Created by Simon Gardener on 04/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class ContactOrderPickerDataSourceDelegate :NSObject, UIPickerViewDataSource, UIPickerViewDelegate{
    
    var handler : ((Int)->())!
    let titles  = [ "Primary Contact", "Secondary Contact","Contact of last resort","contact order undeclared"]
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return titles.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return titles[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        handler(row)
    }
}

