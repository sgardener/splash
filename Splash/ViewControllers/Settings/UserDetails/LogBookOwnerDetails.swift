//
//  LogBookOwnerDetails.swift
//  Splash
//
//  Created by Simon Gardener on 29/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class LogBookOwnerDetails: UITableViewController {
    
    var viewModel : OwnerViewModel!
    var owner: LBOwner!
    
    let labelValueCell  = "labelValueCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        owner = viewModel.owner
        viewModel.updatePhonesEmailsSocials()
        tableView.keyboardDismissMode = .interactive
        setUpNavBar()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if isMovingFromParent {
            viewModel.dataModel.saveContext()
        }
    }
    
    func setUpNavBar(){
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 9
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionName =  optionOrderFor(position: section)
        switch sectionName{
        case .phoneNumber:
            return viewModel.phoneCount()+1
        case .email: return viewModel.emailCount()+1
        case .social: return viewModel.socialCount()+1
            
        default: return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.ownerDetailsSectionHeaders(for: section)
    }
//    fileprivate func placeholdersFor(_ cell: LabelDataTextPairCell, _ indexPath: IndexPath) {
//        cell.labelTextField.placeholder = viewModel.placeholderForPhoneEmailSocialLabel(at: indexPath.section, offsetBy: OptionOrder.phoneNumber.rawValue)
//        cell.dataTextField.placeholder = viewModel.placeholderforPhoneEmailSocialValue(at: indexPath.section, offsetBy: OptionOrder.phoneNumber.rawValue)
//    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName{
            
        case .diverID:
            guard let diverIdCell = tableView.dequeueReusableCell(withIdentifier: Identifier.diverID.rawValue, for: indexPath) as? DiverIDCell else { fatalError("didnt get a diverIdCell")}
            diverIdCell.configure(with: viewModel.owner!)
            return diverIdCell
            
        case .certs:
            guard let certCell = tableView.dequeueReusableCell(withIdentifier: Identifier.certs.rawValue, for: indexPath) as? CertListCell else { fatalError("didnt get a certsListCell")}
            certCell.configureWith(viewModel: viewModel)
        
            return certCell
            
        case .numberOfDives:
            guard let cell  = tableView.dequeueReusableCell(withIdentifier: Identifier.numberOfDives.rawValue, for: indexPath) as? TextFieldTableViewCell else {fatalError()}

               cell.textField?.text = viewModel.loggedDives()
            return cell
        case .emergencyContact:
            let cell  = tableView.dequeueReusableCell(withIdentifier: Identifier.emergencyContact.rawValue, for: indexPath)
            cell.textLabel?.text = "Emergency contacts"
            return cell
        case .insurance:
            let cell  = tableView.dequeueReusableCell(withIdentifier: Identifier.insurance.rawValue, for: indexPath)
            cell.textLabel?.text = "Insurance: "
            return cell
        case .bloodType:
            let cell  = tableView.dequeueReusableCell(withIdentifier: Identifier.bloodType.rawValue, for: indexPath)
            cell.textLabel?.text = "BloodType: \(owner.bloodType!) "
            return cell
            
        //MARK: email phone social cells
        case .phoneNumber:
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "plusCell", for: indexPath)
                cell.textLabel?.text = "+ phone number"
                return cell
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: labelValueCell, for: indexPath) as? LabelValuePairCell else {fatalError("not a labelDataTextPairCell")}
               cell.configure(withLabel: viewModel.phoneLabel(at: indexPath.row), data: viewModel.phoneData(at: indexPath.row), placeHolderLabel: viewModel.placeholderPESLabels[PairOrder.phone.rawValue], placeHolderData: viewModel.placeholderPESValues[PairOrder.phone.rawValue])
//
//                cell.labelTextField.text = viewModel.phoneLabel(at: indexPath.row)
//                cell.dataTextField.text = viewModel.phoneData(at: indexPath.row)
//                placeholdersFor(cell, indexPath)
                return cell
            }
            
            
        case .email:
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "plusCell", for: indexPath)
                cell.textLabel?.text = "+ email address"
                return cell
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: labelValueCell, for: indexPath) as? LabelValuePairCell
                    else { fatalError("not a labelDataTextPairCell ")}
                cell.configure(withLabel: viewModel.emailLabel(at: indexPath.row) , data: viewModel.emailData(at: indexPath.row), placeHolderLabel: viewModel.placeholderPESLabels[PairOrder.email.rawValue], placeHolderData: viewModel.placeholderPESValues[PairOrder.email.rawValue])
//                cell.labelTextField.text = viewModel.emailLabel(at: indexPath.row)
//
//                cell.dataTextField.text = viewModel.emailData(at: indexPath.row)
//
//                placeholdersFor(cell, indexPath)
                return cell
            }
            
        case.social:
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "plusCell", for: indexPath)
                cell.textLabel?.text = "+ social media account"
                return cell
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: labelValueCell, for: indexPath) as? LabelValuePairCell else { fatalError("not a labelDataTextPairCell ")}
                cell.configure(withLabel: viewModel.socialLabel(at: indexPath.row), data: viewModel.socialData(at: indexPath.row) , placeHolderLabel: viewModel.placeholderPESLabels[PairOrder.social.rawValue], placeHolderData: viewModel.placeholderPESValues[PairOrder.social.rawValue])
//                cell.labelTextField.text = viewModel.socialLabel(at: indexPath.row)
//                cell.dataTextField.text = viewModel.socialData(at: indexPath.row)
//                placeholdersFor(cell, indexPath)
                return cell
            }
            
        }
        
    }
    //MARK:- TableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sectionName = optionOrderFor(position: indexPath.section)
        
        switch sectionName{
            
        case .numberOfDives:
            if let numberCell = tableView.cellForRow(at: indexPath) as? TextFieldTableViewCell {
                numberCell.textField.becomeFirstResponder()
            }
        case .phoneNumber:
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
                viewModel.addNewPhoneNumber()
                tableView.reloadSections(IndexSet.init(integer: indexPath.section)  , with: .automatic)
            }else {
                performSegue(withIdentifier: Identifier.phoneNumber.rawValue, sender: self)
            }
        case .email:
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
                viewModel.addNewEmail()
                tableView.reloadSections(IndexSet.init(integer: indexPath.section)  , with: .automatic)
            }else {
                performSegue(withIdentifier: Identifier.email.rawValue, sender: self)
            }
        case .social:
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
                viewModel.addNewSocial()
                tableView.reloadSections(IndexSet.init(integer: indexPath.section)  , with: .automatic)
            }else {
                performSegue(withIdentifier: Identifier.social.rawValue, sender: self)
            }
        case .diverID, .certs, .emergencyContact, .insurance, .bloodType:
            break
            
        }
    }
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        let sectionName = optionOrderFor(position: indexPath.section)
        
        switch sectionName{

        case .phoneNumber, .email, .social :
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
                return false
            }else {
                return true
            }
            
        default: return false
        }
    }
    
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete", handler: {action,indexPath in
            let alertController = UIAlertController(title: "Confirm Delete", message: "Are you sure you want to delete this contact data?", preferredStyle: .actionSheet )
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {action in
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
            })
            let goAheadAction = UIAlertAction(title: "Yes", style: .destructive, handler: {(action) in
                self.viewModel.removeContactDataObject(at: indexPath, offsetBy: OptionOrder.phoneNumber.rawValue)
                
                self.tableView.reloadSections(IndexSet.init(integer: indexPath.section), with: .automatic)
                
            })
            
            alertController.addAction(goAheadAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        })
        return [deleteAction]
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let segueName = segueIdentifierFor(segue: segue)
        switch segueName {
            
        case .diverID:
            guard let vc = segue.destination as? DiverIDDetails else { fatalError()}
            vc.inject(viewModel)
        case .certs:
            guard let vc = segue.actualDestination() as? CertificationsHeld else { fatalError("LGBODetails Expected a CertificationsHeld scene")}
            vc.inject(viewModel.dataModel)
            
        case .numberOfDives:
            break
        case .emergencyContact:
            guard let vc = segue.actualDestination() as? EmergencyContactsList else { fatalError("LGBODetails Expected a emergency contacts list scene" )}
            vc.inject(EmergencyContactViewModel.init(with: viewModel.dataModel, lbo: owner, mode: .show))
            
        case .insurance:
            guard let vc = segue.actualDestination() as? InsurancePolicyList else { fatalError()}
            vc.inject(viewModel)
            
        case .bloodType:
            guard let vc = segue.actualDestination() as? BloodGroupChooser else { fatalError()}
            vc.inject(owner)
        case .phoneNumber:
            let indexPath = tableView.indexPathForSelectedRow!
            let object = viewModel.phones[indexPath.row]
            let type = "Phone Number"
            guard let vc = segue.actualDestination() as? DiverESPEditor else{ fatalError()}
            vc.inject((object, type))
        case .email:
            let indexPath = tableView.indexPathForSelectedRow!
            let object = viewModel.emails[indexPath.row]
            let type = "Email Address"
            guard let vc = segue.actualDestination() as? DiverESPEditor else{ fatalError()}
            vc.inject((object, type))
            break
        case .social: let indexPath = tableView.indexPathForSelectedRow!
        let object = viewModel.socials[indexPath.row]
        let type = "Social Media"
        guard let vc = segue.actualDestination() as? DiverESPEditor else{ fatalError()}
        vc.inject((object, type))
        }
    }
    
}
extension LogBookOwnerDetails    : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        viewModel.setLoggedDives(text: textField.text)
    }
}
extension LogBookOwnerDetails : Injectable {
    func inject(_ vm: OwnerViewModel) {
        viewModel = vm
    }
    
    func assertDependencies() {
        assert(viewModel != nil, "didnt get a viewModel")
    }
    
    typealias T = OwnerViewModel
    
}
extension LogBookOwnerDetails  {
    enum PairOrder : Int{
        case phone
        case email
        case social
    }
}
extension LogBookOwnerDetails : SegueHandlerType,OptionOrder{
    enum Identifier: String {
        case diverID
        case certs
        case numberOfDives
        case emergencyContact
        case insurance
        case bloodType
        case phoneNumber
        case email
        case social
    }
    
    enum OptionOrder: Int {
        case diverID
        case certs
        case numberOfDives
        case emergencyContact
        case insurance
        case bloodType
        case phoneNumber
        case email
        case social
        
    }
}
