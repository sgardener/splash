//
//  BloodGroupChooser.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 07/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class BloodGroupChooser: UITableViewController {
    let numberOfSections = 1
    
    
    let bloodTypes = ["unknown","A","B","AB","O","A positive ","B positive","AB positive","O positive","A negative","B negative","AB negative","O negative"];
    
    var logBookOwner : LBOwner!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        title = "Blood Type"
    }
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bloodTypes.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
        
        cell.textLabel?.text = bloodTypes[indexPath.row]
        if bloodTypes[indexPath.row] == logBookOwner.bloodType {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        return cell
    }
    //MARK: - TableView Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        logBookOwner.bloodType = bloodTypes[indexPath.row]
        tableView.reloadData()
    }
}
extension BloodGroupChooser: Injectable {
    func inject(_ logBookOwner: LBOwner) {
        self.logBookOwner = logBookOwner
    }
    func assertDependencies() {
        assert(logBookOwner != nil, "Didnt get a logBookOwner")
    }
    typealias T = LBOwner
}
