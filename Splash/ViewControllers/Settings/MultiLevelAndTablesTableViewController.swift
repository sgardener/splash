//
//  MultiLevelAndTablesTableViewController.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 07/11/2017.
//  Copyright © 2017 Simon Gardener. All rights reserved.
//

import UIKit

class MutliLevelAndTablesSettingsTableViewController: UITableViewController, OptionOrder {
    
    enum OptionOrder : Int {
        case usesTables
        case logAsMultiLevel
    }
    var options = ["...use tables","...are multi-level dive"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Tables & Multilevel"
        tableView.tableFooterView = UIView()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        cell.textLabel?.text = options[indexPath.row]
    
        let option = optionOrderFor(position: indexPath.row)

        switch option {
        case .logAsMultiLevel:
            cell.accessoryType =  UserDefaults.newLogIsMultiLevel() ? .checkmark: .none
        
        case .usesTables:
            cell.accessoryType = UserDefaults.newLogUsesTables() ? .checkmark: .none
            
        }
        
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let option = optionOrderFor(position: indexPath.row)
        
        switch option {
        case .logAsMultiLevel:
            UserDefaults.setNewLogIsMultiLevel(bool: !(UserDefaults.newLogIsMultiLevel()))
            
        case .usesTables:
            UserDefaults.setNewLogUsesTables(bool: !(UserDefaults.newLogUsesTables()))
            
        }
        tableView.reloadRows(at: [indexPath], with: .automatic)
        
        
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "New log entries..."
    }
    
    
}

