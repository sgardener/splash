//
//  BuddyAutofillSettingsTableViewController.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 07/11/2017.
//  Copyright © 2017 Simon Gardener. All rights reserved.
//

import UIKit

class BuddyAutofillSettingsTableViewController: UITableViewController  {


    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Buddy Autofill"
        tableView.tableFooterView = UIView()

    }

    var options = ["don't autofill buddy.", "use default buddy if set.", "use default. Not set? use last", "use last dive's buddy." ];
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = options[indexPath.row]
        
        let buddyAutofillValue = UserDefaults.buddyAutofillValue()
        
        if  buddyAutofillValue == indexPath.row {
            cell.accessoryType = .checkmark
        }else {
            cell.accessoryType = .none
        }
        
        return cell
    }


    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UserDefaults.setBuddyAutofill(value: indexPath.row)
        tableView.reloadData()
    }
}
