//
//  TankConfigurationList.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 02/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.


import UIKit
import CoreData

class TankConfigurationList: UITableViewController, NSFetchedResultsControllerDelegate, SegueHandlerType {
    
    
    enum Identifier: String {
        case showConfiguration
        case addConfiguration
    }
    
    var context : NSManagedObjectContext!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        fetchConfiguration()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isMovingFromParent == true {
            print ("context.hasChanges\(context.hasChanges)")
            if context.hasChanges{
                do{
                   try context.save()
                }catch{
                    print("Unable to Perform save")
                    print("\(error), \(error.localizedDescription)")
                }
            }
            print ("context.hasChanges\(context.hasChanges)")

        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = frc.sections else { fatalError("No sections")}
        let secInfo = sections[section]
        return secInfo.numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let configuration = frc.object(at: indexPath)
        cell.textLabel?.text = configuration.name ?? "needs Name"
        cell.detailTextLabel?.text = configuration.defaultConfiguration == true ? "Default configuration" : ""
        return cell
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }    
     }
     */
    

 
    
    @IBAction func newConfiguration(_ sender: UIBarButtonItem) {
        let newConfig = Configuration(context: context!)
        newConfig.name = "testingButton"
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let segueId = segueIdentifierFor(segue: segue)
        
        switch segueId{
        
        case .addConfiguration :
            guard  let vc = segue.actualDestination() as? ConfigurationDetails else{ fatalError("THis should have been a ConfigurationDetail tvc")}
            let configuration = Configuration(context: context)
            vc.inject((context: context, configuration: configuration, mode: .add))

            
        case .showConfiguration :
            guard let vc = segue.destination as? ConfigurationDetails else{ fatalError("THis should have been a ConfigurationDetail tvc")}
            let configuration = frc.object(at: tableView.indexPathForSelectedRow!)
            vc.inject((context: context, configuration: configuration, mode: .show))
        }
    }
    //MARK: -  UITableViewDelegate
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteRowAction = UITableViewRowAction(style: .destructive, title: "delete", handler: {action,indexPath in
            let configurationToDelete = self.frc.object(at: indexPath)
            self.context.delete(configurationToDelete)
            //TODO - throw up a confirm delete button here ?
        })
        return [deleteRowAction]
    }
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let config = frc.object(at: indexPath)
        return config.canDelete
        
    }
    
    //MARK: - FetchedResultsController + Delegate Stuff
    
    fileprivate func fetchConfiguration() {
        do {
            try frc.performFetch()
        } catch {
            print("\(error), \(error.localizedDescription)")
        }
    }
    private lazy var frc:NSFetchedResultsController<Configuration> = {
        
        let fetchRequest: NSFetchRequest<Configuration> = Configuration.fetchRequest()
        
        let defaultSort = NSSortDescriptor(key: #keyPath(Configuration.defaultConfiguration), ascending: false)
        let nameSort = NSSortDescriptor(key: #keyPath(Configuration.name), ascending: true)
        fetchRequest.sortDescriptors = [defaultSort,nameSort]
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: context!,
                                                                  sectionNameKeyPath: nil,
                                                                  cacheName: nil)
        
        fetchedResultsController.delegate = self
        
        return fetchedResultsController
    }()
    
    
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .automatic)
        @unknown default:
            print("Futureproof: NSFetchedResultscontroller didChange - and has added a new enum type")
        }
    }
}

extension TankConfigurationList : Injectable{
    func inject(_ context: NSManagedObjectContext) {
        self.context = context
    }
    
    func assertDependencies() {
        assert(context != nil , "no context passed in")
    }
    
    typealias T = NSManagedObjectContext
    
    
}
