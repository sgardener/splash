//
//  AirInOutPickerView.swift
//  Splash
//
//  Created by Simon Gardener on 18/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class AirInOutPickerView: UIPickerView , UIPickerViewDelegate,UIPickerViewDataSource {
    
    var airInOrOut: InOrOut = .airIn
    var gasIn :BarOrPSI = .bar
    var componentsNumber = 5
    private let digitsArray = ["0","1","2","3","4","5","6","7","8","9"]
    private let unitsArray = ["Bar","PSI"]
    let barOrPSIWidth: CGFloat =  90.0
    let digitWidth: CGFloat = 32
    
    enum ComponentPosition : Int {
        case thousands, hundreds, tens, ones, barOrPsi
    }
    let psiRows = [5,10,10,10,2]
    let barRows = [0,4,10,10,2]
    
    
    var pressure : Int16! {
        didSet{
            updatePickerWithPressure()
        }
    }
    
    private func updatePickerWithPressure(){
        let thousands =  pressure / 1000
        let thousandsValue = thousands * 1000
        let hundreds = (pressure - thousandsValue) / 100
        let hundredsValue = hundreds * 100
        let tens = (pressure - thousandsValue - hundredsValue)/10

        let tensValue = tens * 10
        let ones = pressure - thousandsValue - hundredsValue - tensValue
        setRowFor(thousands: Int(thousands), hundreds: Int(hundreds), tens: Int(tens), ones: Int(ones))
        
    }
    private func setRowFor(thousands:Int, hundreds:Int,tens:Int,ones:Int){
        selectRow(thousands, inComponent: ComponentPosition.thousands.rawValue, animated: true)
        selectRow(hundreds, inComponent: ComponentPosition.hundreds.rawValue, animated: true)
        selectRow(tens, inComponent: ComponentPosition.tens.rawValue, animated: true)
        selectRow(ones, inComponent: ComponentPosition.ones.rawValue, animated: true)
        let gasRow = gasIn.rawValue
        selectRow(gasRow, inComponent: ComponentPosition.barOrPsi.rawValue, animated: true)
    }
    
    
    //MARK: - PickerView DataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return componentsNumber
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch gasIn{
        case .bar:
            return barRows[component]
        case .psi:
            return psiRows[component]
        }
    }
    //MARK: - PickerView Delegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case ComponentPosition.barOrPsi.rawValue:
            return unitsArray[row]
        default:
            return digitsArray[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        if component == ComponentPosition.barOrPsi.rawValue{
            return  barOrPSIWidth
        }else {
            return digitWidth
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("hello picker didselectrow")
        if component == ComponentPosition.barOrPsi.rawValue {
            if row == BarOrPSI.bar.rawValue{
                gasIn = .bar
            }else{
                gasIn = .psi
            }
            pressure = valueOnPickerWheels()
            pickerView.reloadComponent(ComponentPosition.thousands.rawValue)
            pickerView.reloadComponent(ComponentPosition.hundreds.rawValue)
        }else {
            pressure = valueOnPickerWheels()
        }
    }
    func valueOnPickerWheels()->Int16 {
        var thousands = selectedRow(inComponent: ComponentPosition.thousands.rawValue)
        let hundreds = 100 * selectedRow(inComponent: ComponentPosition.hundreds.rawValue)
        let tens = 10 * selectedRow(inComponent:ComponentPosition.tens.rawValue)
        let ones = selectedRow(inComponent:ComponentPosition.ones.rawValue)
        if gasIn == .psi {
            thousands = thousands * 1000
        }else {
            thousands = 0
        }
        
        return Int16(thousands + hundreds + tens + ones )
        
    }
    func isInBar() -> Bool {
        if selectedRow(inComponent: ComponentPosition.barOrPsi.rawValue) == BarOrPSI.bar.rawValue{
            return true
            
        }else { return false }
    }
}
enum InOrOut {
    case airIn
    case airOut
}
enum BarOrPSI:Int {
    case bar
    case psi
    
}
