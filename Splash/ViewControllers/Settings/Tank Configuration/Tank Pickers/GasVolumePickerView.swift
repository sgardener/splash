//
//  GasVolumePickerView.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 05/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class GasVolumePickerView: UIPickerView ,UIPickerViewDelegate, UIPickerViewDataSource   {
    
    let numberOfElementsForLtrs = 5
    let numberOfElementsForCubicFeet = 4
    var  numberOfElements = 5
    
    let tensPosition = 0
    let unitsPosition = 1
    let decimalPosition = 2
    let tenthsPosition = 3
    
    let cuFtHundredsPosition = 0
    let cuFtTensPosition = 1
    let cuFtUnitsPosition = 2
    
    let ltrsRowsInComponent = [2,10,1,10,2]
    let cuFtRowsInComponent = [2,10,10,2]
    
    let ltrsCuFtWidth: CGFloat =  60.0
    let digitWidth: CGFloat = 32
    let decimalPointWidth: CGFloat = 12
    
    func ltrsCuFtComponentPosition()->Int  {
        if volumeIsInLitres == true{
            return 4
        }else{
            return 3
        }
    }
    
    
    var volumeIsInLitres : Bool = true {
        didSet{
            numberOfElements = volumeIsInLitres == true ? numberOfElementsForLtrs : numberOfElementsForCubicFeet
            self.reloadAllComponents()
            setUpGasVolumeValues()  
        }
    }
    
    var volume: Double! {
        didSet{
            setUpGasVolumeValues()
        }
    }
    
    private let digitsArray = ["0","1","2","3","4","5","6","7","8","9"]
    private let unitsArray = ["Ltrs","CuFt"]
    
    //MARK: - PickerView DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return numberOfElements
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch volumeIsInLitres {
        case true:
            return ltrsRowsInComponent[component]
            
        case false:
            return cuFtRowsInComponent[component]
        }
    }
    
    //MARK: - PickerView Delegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == ltrsCuFtComponentPosition() {
            return unitsArray[row]
        }
        switch volumeIsInLitres {
        case true:
            if component == decimalPosition {
                return "."
            } else if component == tensPosition || component == unitsPosition || component == tenthsPosition {
                return digitsArray[row]
            }
            
        case false:
            if component >= 0 && component < 3 {
                return digitsArray[row]
            }
        }
        return "Should never get here but the compiler wants a return"
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        if component == ltrsCuFtComponentPosition() {
            return CGFloat(ltrsCuFtWidth)
        }
        switch volumeIsInLitres {
        case true:
            if component == decimalPosition {
                return decimalPointWidth
            } else {
                return digitWidth
            }
            
        case false :
            return digitWidth
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == ltrsCuFtComponentPosition(){
            if  row == 0 {
                volumeIsInLitres = true
            } else if row == 1 {
                volumeIsInLitres = false
            }

        }
    }
    
    func setUpGasVolumeValues(){
        selectRow(volumeIsInLitres == true ? 0 : 1  , inComponent: ltrsCuFtComponentPosition(), animated: false)
        let hundreds = Int(volume / 100.0)
        let tens = Int(    ( volume - Double(hundreds*100)) / 10.0   )
        let units = Int(volume - Double(hundreds*100) - (Double(tens*10)) )
        let tenths = Int( (volume - Double(hundreds*100) - Double(tens*10) - Double(units)) * 10)
        
        switch volumeIsInLitres {
        case true:
            setLtrsRowsfor(tens: tens, units: units, tenths: tenths)
        case false:
            setCuFtRowsfor(hundreds: hundreds, tens: tens, units: units)
        }
    }
    
    private func setLtrsRowsfor(tens: Int, units: Int, tenths: Int){
        selectRow(tens, inComponent: tensPosition, animated: false)
        selectRow(units, inComponent: unitsPosition, animated: false)
        selectRow(tenths, inComponent: tenthsPosition, animated: false)
    }
    private func setCuFtRowsfor(hundreds: Int, tens: Int, units:Int){
        selectRow(hundreds, inComponent: cuFtHundredsPosition, animated: false)
        selectRow(tens, inComponent: cuFtTensPosition, animated: false)
        selectRow(units, inComponent: cuFtUnitsPosition, animated: false)
    }
    func volumeAndMeasurementfromPickerValues()-> (volume:Double, volumeIsInLtrs:Bool) {
        var newVolume = 0.0
        switch volumeIsInLitres{
        case true:
            newVolume += Double(selectedRow(inComponent: tensPosition) * 10)
            newVolume += Double(selectedRow(inComponent: unitsPosition))
            newVolume += Double(selectedRow(inComponent: tenthsPosition)/10)
            
        case false:
            newVolume += Double(selectedRow(inComponent: cuFtHundredsPosition)*100)
            newVolume += Double(selectedRow(inComponent: cuFtTensPosition) * 10)
            newVolume += Double(selectedRow(inComponent: cuFtUnitsPosition))
        }
        return(newVolume,volumeIsInLitres)
    }
}
