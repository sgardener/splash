//
//  GasPercentagePickerView.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 04/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class GasPercentagePickerView: UIPickerView, UIPickerViewDataSource,UIPickerViewDelegate {
    let numberOfElements = 4
    
    let tensPosition = 0
    let unitsPosition = 1
    let decimalPosition = 2
    let tenthsPosition = 3
    
    let digitWidth: CGFloat = 32
    let decimalPointWidth: CGFloat = 12
    
    var percentage :Double! {
        didSet{
            setUpGasPercentageValues()  
        }
    }
    
    private let digitsArray = ["0","1","2","3","4","5","6","7","8","9"]
    
    //MARK: - PickerView DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return numberOfElements
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == decimalPosition {
            return 1
        }else {
            return digitsArray.count
        }
    }
    
    //MARK: - PickerView Delegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == decimalPosition {
            return "."
        } else {
            return digitsArray[row]
        }
    }
   
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        if component == decimalPosition {
            return decimalPointWidth
        } else {
            return digitWidth
        }
    }
    
    func setUpGasPercentageValues() {
        let firstDigit = Int(percentage / 10.0)
        let secondDigit = Int( percentage - Double(firstDigit*10))
        let tenths = Int((percentage - Double(firstDigit*10) - Double(secondDigit)) * 10)
        selectRow(firstDigit, inComponent: tensPosition, animated: true)
        selectRow(secondDigit, inComponent: unitsPosition, animated: true)
        selectRow(tenths, inComponent: tenthsPosition, animated: true)
    }
    
    func calculatePercentageFromPickerValues()-> Double{
        let tens = selectedRow(inComponent: tensPosition)
        let unit = selectedRow(inComponent: unitsPosition)
        let tenths = selectedRow(inComponent: tenthsPosition)
        return Double(tens*10 + unit) + (Double(tenths))/10.0
    }
}
