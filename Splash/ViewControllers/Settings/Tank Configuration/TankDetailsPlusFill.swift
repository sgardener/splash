//
//  TankDetailsPlusFill.swift
//  Splash
//
//  Created by Simon Gardener on 18/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class TankDetailsPlusFill: TankDetails {
    
    weak var delegate : AnotherTank!
    
    @IBOutlet weak var airOutButton: UIButton!
    @IBOutlet weak var airInButton: UIButton!
   
    lazy var airInOutPicker = AirInOutPickerView()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    fileprivate func setEndPressureLabel() {
        airOutButton.setTitle(title(with: tank.endPressure), for: .normal)
    }
    
    override func setInitailDisplayforTankValues() {
        super.setInitailDisplayforTankValues()
        setStartPressureLabel()
        setEndPressureLabel()
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    @IBAction func addTankPressed(_ sender: Any) {
        let confirm = UIAlertController(title: "Add Tank?", message: "Confirm additional tank", preferredStyle: .actionSheet)
        let goAheadAction = UIAlertAction(title: "Yes", style: .default, handler: {action in
            self.delegate.addTank()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        confirm.addAction(goAheadAction)
        confirm.addAction(cancelAction)
        present(confirm, animated: true)
    }
    
    @IBAction func airInTapped(_ sender: UIButton) {
        prepareInOutPicker(pressure: tank.startPressure)
        presentPressurePicker(with: #selector(updateStartPressure), andTitle: "AirIn")
    }
    
    @IBAction func airOutTapped(_ sender: UIButton) {
        prepareInOutPicker(pressure: tank.endPressure)
        presentPressurePicker(with: #selector(updateEndPressure), andTitle: "Air Out")
    }
    
    func prepareInOutPicker(pressure:Int16){
        airInOutPicker.sizeToFit()
        airInOutPicker.delegate = airInOutPicker
        airInOutPicker.dataSource = airInOutPicker
        airInOutPicker.gasIn = tank.pressureIsInBar == true ? .bar: .psi
        airInOutPicker.pressure = pressure
        
    }
    
    func presentPressurePicker(with selector: Selector, andTitle title:String){
        hiddenTriggeringTextField.inputView = airInOutPicker
        hiddenTriggeringTextField .inputAccessoryView = toolbarForPicker(doneMethod: selector,title: title)
        hiddenTriggeringTextField.becomeFirstResponder()
    }
    
    fileprivate func setStartPressureLabel() {
        airInButton.setTitle(title(with: tank.startPressure), for: .normal)
    }
    
    @objc func updateStartPressure(){
        tank.startPressure = airInOutPicker.pressure
        tank.pressureIsInBar = airInOutPicker.isInBar()
        setStartPressureLabel()
        setEndPressureLabel()
        hiddenTriggeringTextField.resignFirstResponder()
    }
    
    @objc func updateEndPressure(){
        tank.endPressure = airInOutPicker.pressure
        tank.pressureIsInBar = airInOutPicker.isInBar()
        setEndPressureLabel()
        setStartPressureLabel()
        hiddenTriggeringTextField.resignFirstResponder()
    }
    
    func title(with pressure: Int16)->String{
        let pressureUnit = tank.pressureIsInBar == true ? "Bar":"PSI"
        return "\(pressure) \(pressureUnit)"
    }
}
