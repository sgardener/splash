//
//  TankDetailTableViewCell.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 03/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
//import CoreData

class TankDetailTableViewCell: UITableViewCell {
    
    let tankMaterialNameArray = ["Aluminium","Steel","Carbon"]
    let gasTypeArray = ["Air","Nitrox","Trimix","Heliox","Oxygen","Helium"];
    private enum SupplyType: Int {
        case open, closed, semi
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func populateWithData(for tank : Tank){
        
        textLabel?.text = tankDescription(for: tank)
        
        detailTextLabel?.text = systemTypeDescription(for: tank)
    }
    private func tankDescription(for tank: Tank) -> String {
        
        return "\(tankMaterialNameArray[Int(tank.tankMaterial)]) - \(tank.volume.toOneDecimalPlaceString()) \(tank.volumeIsinLiters == true ? "Ltrs":"CuFt") - \(gasTypeArray[Int(tank.gasType)])"
    }
    private func systemTypeDescription(for tank: Tank)-> String{
        switch Int(tank.supplyType){
        case SupplyType.open.rawValue:
            if tank.manifoldedtwins == true {
                return "Manifolded twinset"
            }else {return ""}
        case SupplyType.closed.rawValue:
            return "Rebreather - closed-circuit."
        case SupplyType.semi.rawValue:
            return "Rebreather - Semi-closed."
        default:
            return ""
        }
    }
    
}
