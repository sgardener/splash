//
//  ConfigurationDetails.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 03/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.

import UIKit
import CoreData

enum Mode {
    case show
    case edit
    case add
    case addedWhileLogging
}

class ConfigurationDetails: UITableViewController{
    
    let nameCellId = "NameCell"
    let makeDefaultCellId = "MakeDefaultCell"
    
    let tankDescriptionCellID = "TankCell"
    let addTankCellId = "AddTankCell"
    
    let tankMaterialNameArray = ["Aluminium","Steel","Carbon"]
    let gasTypeArray = ["Air","Nitrox","Trimix","Heliox","Oxygen","Helium"];
    
    private enum ConfigurationSection : Int {
        case nameAndDefault
        case theTanks
        case addTank
    }
    
    enum Section0 : Int {
        case name
        case makeDefault
    }
    
    var mode: Mode!
    var context :NSManagedObjectContext!
    var configuration :Configuration!
    
    var tanks = [Tank]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tanks = configuration.has?.allObjects as! [Tank]
        if mode == .add{
            setUpAsAddMode()
        }
    }
    func setUpAsAddMode(){
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelNewConfig))
        let doneButton = UIBarButtonItem    (barButtonSystemItem: .done, target: self, action: #selector(doneAdding))
        navigationItem.leftBarButtonItem = cancelButton
        navigationItem.rightBarButtonItem = doneButton
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    @objc func cancelNewConfig (){
        context.delete(configuration)
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    @objc func doneAdding(){
        view.endEditing(true)
        if configuration.name == nil{
            showAlert(withTitle: "Missing Name", message: "A configuration needs a name")
        }else if tanks.count == 0 {
            showAlert(withTitle: "Missing Tank(s)", message: "A configurations needs at least one tank")
        }else {
            presentingViewController?.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return tanks.count + 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 2 //name and default
            
        default: return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        switch indexPath.section{
            
        case 0:
            if indexPath.row == Section0.name.rawValue {
                let nameCell = (tableView.dequeueReusableCell(withIdentifier: nameCellId, for: indexPath) as!  TextFieldTableViewCell)
                nameCell.textField?.text = configuration.name
                return nameCell
            }else {
                cell = tableView.dequeueReusableCell(withIdentifier: makeDefaultCellId, for: indexPath)
                cell.accessoryType = configuration.defaultConfiguration ? .checkmark : .none
            }
            
        case (tableView.numberOfSections - 1):
            cell = tableView.dequeueReusableCell(withIdentifier: addTankCellId, for: indexPath)
            
        default:
            let tankCell = tableView.dequeueReusableCell(withIdentifier: tankDescriptionCellID, for: indexPath) as! TankDetailTableViewCell
            let tank = tanks[indexPath.section - 1]
            tankCell.populateWithData(for: tank)
            return tankCell
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard (section != 0 && section != numberOfSections(in: tableView)-1)  else { return nil }
        
        return "TANK NO. \(section)"
        
    }
    
    func  describeTank(at indexPath: IndexPath){
        let tank = tanks[indexPath.section - 1]
        print("*******tank description*********")
        print("tank material:\(tankMaterialNameArray[Int(tank.tankMaterial)])")
        print("tank volume\(tank.volume)")
        print("in \(tank.volumeIsinLiters == true ? "Ltrs": "CuFt")")
        print("*****************************")
    }
    
    @IBAction private func createNewTank() {
        let tank = Tank(context: context)
        configuration.addToHas(tank)
        tanks.append(tank)
        let thisMany = configuration.has?.count
        tank.displayOrder = Int16(thisMany! - 1)
        if UserDefaults.pressureUnits() != .metric {
            tank.volumeIsinLiters = false
            tank.pressureIsInBar = false
            tank.workingPressure = 3000
            tank.volumeIsinLiters = false
            tank.volume = 80
        }
        tableView.reloadData()
    }
    //MARK: -  UITableViewDelegate
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteRowAction = UITableViewRowAction(style: .destructive, title: "delete", handler: {action,indexPath in
            let tankToDelete = self.tanks.remove(at: indexPath.section - 1)
            self.context.delete(tankToDelete)
            tableView.reloadData()
            //TODO - throw up a confirm delete button here ?
        })
        return [deleteRowAction]
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        //only allow tanks to be deleted and only allow tanks to be deleted if there is more than one tank
        if indexPath.section == ConfigurationSection.nameAndDefault.rawValue  || indexPath.section == numberOfSections(in: tableView) - 1 {
            return false
        }
        if numberOfSections(in: tableView) == 3 {
            return false
        }
        return true
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == ConfigurationSection.nameAndDefault.rawValue && indexPath.row == Section0.makeDefault.rawValue /* is default*/ {
            if configuration.defaultConfiguration  == false {
                removeDefaultOnDefaultConfiguration()
                configuration.defaultConfiguration = true
                tableView .reloadRows(at: [IndexPath.init(row: 1, section: 0)], with: .automatic)
                
            }else {
                //TODO: maybe put up a message to say there must always be a default . remove this as default by selecting another configuration as default
            }
        }
    }
    private func removeDefaultOnDefaultConfiguration(){
        let fetchRequest : NSFetchRequest<Configuration> = Configuration.fetchRequest()
        let predicate = NSPredicate(format: "defaultConfiguration == true")
        fetchRequest.predicate = predicate
        var theDefault:[Configuration]
        do {
            theDefault = (try context.fetch(fetchRequest))
            let config = theDefault.first
            config?.defaultConfiguration = false
        }catch{
            print("\(error), \(error.localizedDescription)")
        }
    }
    
    //MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.destination as? TankDetails else {fatalError("This should be a TankDetails view Controller")}
        let indexPath =  tableView.indexPathForSelectedRow
        if let index = indexPath?.section{
            vc.inject(tanks[index-1])
        }
    }
}

//MARK: - Extensions
//MARK: Textfield Delegate
extension ConfigurationDetails : UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines ).capitalized
        configuration.name = textField.text
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
//MARK: Injection Protocol
extension ConfigurationDetails: Injectable{
    func inject(_ injection: (context: NSManagedObjectContext, configuration: Configuration, mode: Mode)) {
        context = injection.context
        configuration = injection.configuration
        mode = injection.mode
    }
    
    func assertDependencies() {
        assert(context != nil, "Context is nil in configurationDetails")
        assert(mode != nil, " Mode has no value")
        assert(configuration != nil ,"config has no value ")
    }
    
    typealias T = (context: NSManagedObjectContext, configuration: Configuration, mode: Mode)
}
