//
//  TankDetails.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 03/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class TankDetails: UIViewController {
   
    var tank: Tank!
    
   lazy var gasPicker = GasPercentagePickerView()
   lazy var volumePicker = GasVolumePickerView ()
    @IBOutlet weak var hiddenTriggeringTextField: UITextField!
    
    
    @IBOutlet weak var tankMaterialsSegmentedController: UISegmentedControl!
    @IBOutlet weak var gasSupplyTypeSegmentedController: UISegmentedControl!
    @IBOutlet weak var singleOrTwinSetSegmentedController: UISegmentedControl!
    @IBOutlet weak var gasTypeSegmentedController: UISegmentedControl!
    
    @IBOutlet weak var gasVolumeButton: UIButton!
    
    @IBOutlet weak var heliumButton: UIButton!
    
    @IBOutlet weak var oxygenButton: UIButton!
    
    @IBOutlet weak var nitrogenButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        setInitailDisplayforTankValues()

    }


    func setInitailDisplayforTankValues() {
        tankMaterialsSegmentedController.selectedSegmentIndex = Int(tank.tankMaterial)
        gasSupplyTypeSegmentedController.selectedSegmentIndex = Int(tank.supplyType)
        singleOrTwinSetSegmentedController.selectedSegmentIndex = tank.manifoldedtwins == false ? 0 : 1
        gasTypeSegmentedController.selectedSegmentIndex = Int(tank.gasType)
        changeGasType(gasTypeSegmentedController)
         setGasVolumeButtonTitle()
    }

    
    //MARK: - Tank Type stuff
    
    @IBAction func changeTankMaterials(_ sender: UISegmentedControl) {
        tank.tankMaterial = Int16(sender.selectedSegmentIndex)
    }
    
    @IBAction func changeGasSupplyType(_ sender: UISegmentedControl) {
        tank.supplyType = Int16(sender.selectedSegmentIndex)
        singleOrTwinSetSegmentedController.isEnabled = sender.selectedSegmentIndex == GasSupplyType.openCircuit.rawValue ? true : false
    }
    
    @IBAction func changeSingleOrTwinSet(_ sender: UISegmentedControl) {
        tank.manifoldedtwins = sender.selectedSegmentIndex == SingleOrTwin.manifolded.rawValue ? true : false
    }
    //MARK: - Gas Type Functions
   @IBAction func changeGasType(_ sender: UISegmentedControl) {
        tank.gasType =  Int16(sender.selectedSegmentIndex)
        switch sender.selectedSegmentIndex {
            case GasType.air.rawValue: setToAir()
            case GasType.nitrox.rawValue: setToNitrox()
            case GasType.trimix.rawValue: setToTrimix()
            case GasType.heliox.rawValue: setToHeliox()
            case GasType.O2.rawValue: setToO2()
            case GasType.He.rawValue: setToHelium()
        default: break
        }
    }
    func nitrogenPercent()-> Double {
        return 100.0 - tank.oxygenPercent - tank.heliumPercent
    }
    func setToAir(){
        disableAllGasButtons()
        tank.oxygenPercent = 21.0
        tank.heliumPercent = 0.0
        updateGasButtons()
    }
   
    func setToNitrox(){
        disableAllGasButtons()
        oxygenButton.isEnabled = true
        tank.heliumPercent = 0.0
        updateGasButtons()
    }
    func setToTrimix(){
        enableHeliumAndOxygenButtons()
        if tank.oxygenPercent + tank.heliumPercent > 100.0 {
            tank.heliumPercent = 100.0 - tank.oxygenPercent
        }
        updateGasButtons()
    }
    func setToHeliox(){
        enableHeliumAndOxygenButtons()
        tank.heliumPercent = 100 - tank.oxygenPercent
        updateGasButtons()
    }
    
    func setToO2(){
        disableAllGasButtons()
        tank.oxygenPercent = 100.0
        tank.heliumPercent = 0.0
        updateGasButtons()
    }
    func setToHelium(){
        disableAllGasButtons()
        tank.heliumPercent = 100.0
        tank.oxygenPercent = 0.0
        updateGasButtons()
    }
    func updateGasButtons(){
        oxygenButton.setTitle(tank.oxygenPercent.asOneDecimalPlaceWithPercentString(), for: .normal)
        heliumButton.setTitle(tank.heliumPercent.asOneDecimalPlaceWithPercentString(), for: .normal)
        nitrogenButton.setTitle(nitrogenPercent().asOneDecimalPlaceWithPercentString(), for: .normal)
    }
    func enableHeliumAndOxygenButtons(){
        oxygenButton.isEnabled = true
        heliumButton.isEnabled = true
    }
    func disableAllGasButtons() {
        oxygenButton.isEnabled = false
        heliumButton.isEnabled = false
        nitrogenButton.isEnabled = false
    }
    private func setGasVolumeButtonTitle() {
        if tank.volumeIsinLiters == true {
            gasVolumeButton.setTitle(tank.volume.toOneDecimalPlaceString() + " ltrs",  for: .normal)
        }else {
        gasVolumeButton.setTitle(String(Int(tank.volume)) + " cuFt.", for: .normal)
    }
    }
    
    //MARK:- User Actions requiring Pickers

    @IBAction func setVolumeButtonTapped(_ sender: UIButton) {
        prepareVolumePicker()
        presentVolumePicker()
    }
    
    @IBAction fileprivate func oxygenButtonTapped(_ sender: UIButton) {
        prepareGasPicker()
        gasPicker.percentage = tank.oxygenPercent
        hiddenTriggeringTextField.inputAccessoryView = toolbarForPicker(doneMethod: #selector(updateOxygenFromGasPicker), title: "Oxygen %")
        hiddenTriggeringTextField.becomeFirstResponder()
    }


    @IBAction fileprivate func heliumButtonTapped(_ sender: UIButton) {
        prepareGasPicker()
        gasPicker.percentage = tank.heliumPercent
        hiddenTriggeringTextField.inputAccessoryView = toolbarForPicker(doneMethod: #selector(updateHeliumFromGasPicker), title: "Helium %")
        hiddenTriggeringTextField.becomeFirstResponder()
    }
    //MARK: - Picker & Toolbar prep
    
    func toolbarForPicker(doneMethod: Selector, title:String)-> UIToolbar  {
        let pickerToolbar = UIToolbar()
        pickerToolbar.sizeToFit()
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelGasPercentChange) )
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: doneMethod)
        let titleItem =  UIBarButtonItem(title: title, style: .plain, target: nil, action: nil)
        titleItem.isEnabled = false
        titleItem.setTitleTextAttributes([
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 23.0),
            NSAttributedString.Key.foregroundColor : UIColor.black], for: UIControl.State.disabled)
        
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        pickerToolbar.setItems([cancelButton,flexible,titleItem,flexible,doneButton], animated: false)
        return pickerToolbar
    }
    
    fileprivate func prepareVolumePicker(){
        volumePicker.sizeToFit()
        volumePicker.dataSource = volumePicker
        volumePicker.delegate = volumePicker
        volumePicker.volume = tank.volume
        volumePicker.volumeIsInLitres = tank.volumeIsinLiters
    }
    fileprivate func presentVolumePicker(){
        hiddenTriggeringTextField.inputView = volumePicker
        hiddenTriggeringTextField.inputAccessoryView = toolbarForPicker(doneMethod: #selector(updateVolumeFromPicker),title:"Tank Volume")
        hiddenTriggeringTextField.becomeFirstResponder()
    }
    
    fileprivate func prepareGasPicker() {
        hiddenTriggeringTextField.inputView = gasPicker
        gasPicker.sizeToFit()
        gasPicker.dataSource = gasPicker
        gasPicker.delegate = gasPicker
    }
    
    //MARK: - Picker Toolbar response
    @objc private func cancelGasPercentChange(){
        hiddenTriggeringTextField.resignFirstResponder()
    }
    @objc private func updateOxygenFromGasPicker(){
        useRegularColorForPercentages()
        tank.oxygenPercent = gasPicker.calculatePercentageFromPickerValues()
        if tank.gasType == GasType.heliox.rawValue {
            tank.heliumPercent = 100 - tank.oxygenPercent
        }else {
            if tank.heliumPercent + tank.oxygenPercent > 100.0{
                useWarningColorForPercentages()
            }
        }
        updateGasButtons()
        hiddenTriggeringTextField.resignFirstResponder()
    }
    @objc private func updateHeliumFromGasPicker(){
        useRegularColorForPercentages()
        tank.heliumPercent = gasPicker.calculatePercentageFromPickerValues()
        if tank.gasType == GasType.heliox.rawValue {
            tank.oxygenPercent = 100  - tank.heliumPercent
        } else {
            if tank.heliumPercent + tank.oxygenPercent > 100.0{
             useWarningColorForPercentages()
            }
        }
        updateGasButtons()
        hiddenTriggeringTextField.resignFirstResponder()
    }
    @objc private func updateVolumeFromPicker(){
        let newValues =  volumePicker.volumeAndMeasurementfromPickerValues()
        tank.volumeIsinLiters = newValues.volumeIsInLtrs
        tank.volume = newValues.volume
        setGasVolumeButtonTitle()
        hiddenTriggeringTextField.resignFirstResponder()
    }

    func useWarningColorForPercentages(){
        oxygenButton.tintColor = .red
        heliumButton.tintColor = .red
    }
    func useRegularColorForPercentages() {
        oxygenButton.tintColor = view.tintColor
        heliumButton.tintColor = view.tintColor
    }
}
extension TankDetails : Injectable{
    func inject(_ tank: Tank) {
        self.tank = tank
    }
    
    func assertDependencies() {
        assert( tank != nil,"Tank details not passed a valid tank")
    }
    typealias T = Tank
}
