//
//  MeasurementsSettingsViewController.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 07/11/2017.
//  Copyright © 2017 Simon Gardener. All rights reserved.
//

import UIKit
import DeviceKit

class MeasurementsSettingsViewController: UIViewController {
    //MARK: properties
    @IBOutlet weak var allMeasurement: UISegmentedControl!
    @IBOutlet weak var depthSystem: UISegmentedControl!
    @IBOutlet weak var pressureSystem: UISegmentedControl!
    @IBOutlet weak var temperatureSystem: UISegmentedControl!
    @IBOutlet weak var weightSystem: UISegmentedControl!
    @IBOutlet weak var distanceSystem: UISegmentedControl!


    @IBOutlet weak var selectMeasurementSystemLabel: UILabel!
    
        //MARK: LifeCycle
        override func viewDidLoad() {
            super.viewDidLoad()
            title = "Measures"
            showOrHideMeasurementLabel()
            UpdateControls()
        }
    
    func showOrHideMeasurementLabel(){
        let device = Device.current
        
        if device == .iPhoneSE || device == .iPhone5s {
            selectMeasurementSystemLabel.isHidden = true
        }else{
            selectMeasurementSystemLabel.isHidden = false
        }
    }
        //MARK: Update Segmented Controls
        func UpdateControls(){
            updateAllValueSegmentedControl()
            updateIndividualSegmentedControls()
        }
        
        func updateAllValueSegmentedControl(){
            allMeasurement.selectedSegmentIndex = UserDefaults.allMeasures().rawValue
        }
        
        func updateIndividualSegmentedControls(){
            depthSystem.selectedSegmentIndex = UserDefaults.depthUnits().rawValue
            pressureSystem.selectedSegmentIndex = UserDefaults.pressureUnits().rawValue
            temperatureSystem.selectedSegmentIndex = UserDefaults.temperatureUnits().rawValue
            weightSystem.selectedSegmentIndex = UserDefaults.weightUnits().rawValue
            distanceSystem.selectedSegmentIndex = UserDefaults.distanceUnits().rawValue
        }
        //MARK: Segments Change
        
        @IBAction func allUnitsDidChange(_ sender: UISegmentedControl) {
            UserDefaults.setAllMeasurement(units: UnitsNotation(rawValue: sender.selectedSegmentIndex)!)
            updateIndividualSegmentedControls()
            
        }
        
        @IBAction func depthUnitDidChange(_ sender: UISegmentedControl) {
            UserDefaults.setDepth(units: UnitsNotation(rawValue: sender.selectedSegmentIndex)!)
            updateAllValueSegmentedControl()
            
        }
        
        @IBAction func pressureUnitDidChange(_ sender: UISegmentedControl) {
            UserDefaults.setPressure(units: UnitsNotation(rawValue: sender.selectedSegmentIndex)!)
            updateAllValueSegmentedControl()
        }
        
        @IBAction func temperatureUnitDidChange(_ sender: UISegmentedControl) {
            UserDefaults.setTemperature(units: UnitsNotation(rawValue: sender.selectedSegmentIndex)!)
            updateAllValueSegmentedControl()
            
        }
        
        @IBAction func weightUnitDidChange(_ sender: UISegmentedControl) {
            UserDefaults.setWeight(units: UnitsNotation(rawValue: sender.selectedSegmentIndex)!)
            updateAllValueSegmentedControl()
            
        }
        @IBAction func distanceUnitDidChange(_ sender: UISegmentedControl) {
            UserDefaults.setDistance(units: UnitsNotation(rawValue: sender.selectedSegmentIndex)!)
            updateAllValueSegmentedControl()
            
        }
}

