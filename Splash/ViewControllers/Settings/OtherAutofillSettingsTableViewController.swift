//
//  OtherAutofillSettingsTableViewController.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 07/11/2017.
//  Copyright © 2017 Simon Gardener. All rights reserved.
//

import UIKit

class OtherAutofillSettingsTableViewController: UITableViewController {
    
 private enum sectionOrder: Int {
        case forwardItems
        case regardLast
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Autofill Options"
        tableView.tableFooterView = UIView()

    }
     let firstRow = 0
    
    let headings =  ["Select items to autofill", "Regard last dive as most..."]
    let regardLastFooter = "Use 'recently added dive' if adding a series of dives from the past"
    let autoFillItems = ["do not use last dive details", "weights", "exposure suit", "tank", "diveguide/instructor", "dive operation", "trip", "boat"]
    let regardLastAs = ["...recent dive","...recently added dive"]
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return headings.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case sectionOrder.forwardItems.rawValue:
            return autoFillItems.count
        case sectionOrder.regardLast.rawValue:
            return regardLastAs.count
        default: fatalError("No valid Section")
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
      
        switch indexPath.section {
            
        case sectionOrder.forwardItems.rawValue:
              cell.textLabel?.text = autoFillItems[indexPath.row]
              let carryForwardItemsValue = UserDefaults.otherAutofillItemsValue()
              let rowCompareValue = Int( pow(2, Double(indexPath.row)) )
              cell.accessoryType = (carryForwardItemsValue & rowCompareValue == rowCompareValue)  ?  .checkmark : .none
    
        case sectionOrder.regardLast.rawValue:
            cell.textLabel?.text = regardLastAs[indexPath.row]
            if indexPath.row == 0 {
                cell.accessoryType = UserDefaults.regardLastDiveAsMostRecentlyAdded() ? .none : .checkmark
            }else {
                cell.accessoryType = UserDefaults.regardLastDiveAsMostRecentlyAdded() ? .checkmark : .none
            }
            
        default :
            fatalError("No such section")
        }
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        switch indexPath.section {

        case sectionOrder.forwardItems.rawValue:
            var carryForwardItemsValue = UserDefaults.otherAutofillItemsValue()
            let rowValue = Int(pow(2,Double(indexPath.row)))
            if indexPath.row == 0 {
                UserDefaults.setOtherAutofillItems(value: 1)
                tableView.reloadSections([indexPath.section], with: .automatic)
            }else {
                if carryForwardItemsValue == 1 {
                    carryForwardItemsValue = rowValue
                } else {
                    if carryForwardItemsValue & rowValue == 0 {
                        carryForwardItemsValue = carryForwardItemsValue + rowValue
                    } else {
                        carryForwardItemsValue = carryForwardItemsValue - rowValue
                    }
     
                }
                UserDefaults.setOtherAutofillItems(value: carryForwardItemsValue)
                tableView.reloadRows(at: [indexPath,IndexPath(row:0, section: 0)], with: .automatic)
            }
            
        case sectionOrder.regardLast.rawValue:
            
            if indexPath.row == 0 {
                UserDefaults.setRegardLastDiveAsMostRecentlyAdded(to: false)
            }else {
                UserDefaults.setRegardLastDiveAsMostRecentlyAdded(to: true)
            }
            tableView.reloadSections([indexPath.section], with: .automatic)
            
        default :
            fatalError("No such section")
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return headings[section]
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if section == sectionOrder.regardLast.rawValue{
            return regardLastFooter;
        } else {
            return nil
        }
    }
}
