//
//  SettingsListTableViewController.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 07/11/2017.
//  Copyright © 2017 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData
import Contacts
import ContactsUI

class SettingsListTableViewController: UITableViewController{
    
    
    var dataModel: DataModel!
    
    var settings = ["Personal data","Measurement in...", "Autofill buddy options","Autofill from last dive","MultiLevel and Tables","Tank Configuration","DivePro Mode","Thanks To"];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Settings"
        assertDependencies()
        setUpNavBar()
        tableView.tableFooterView = UIView()

    }
    func setUpNavBar(){
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settings.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = settings[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedRow = optionOrderFor(position: indexPath.row)
        switch selectedRow {
        case .measurements:
            performSegueWith(identifier: .measurements, sender: self)
        case .multiLevelAndTables :
            performSegueWith(identifier: .multiLevelAndTables, sender: self)
        case .autofillBuddy:
            performSegueWith(identifier: .autofillBuddy, sender: self)
        case .autofillFromLast :
            performSegueWith(identifier: .autofillFromLast, sender: self)
        case .personalData:
            handlePersonalDataSelection()
        case .tankConfiguration :
            performSegueWith(identifier: .tankConfiguration, sender: self)
        case .proMode:
            performSegueWith(identifier: .proMode , sender: self)
        case .credit:
            performSegueWith(identifier: .credit, sender: self)
        }
    }
    private func  handlePersonalDataSelection(){
        if dataModel.hasLogBookOwner() == false  {
            let status = CNContactStore.authorizationStatus(for: .contacts)
            switch status {
            case .notDetermined:
                let contactStore = CNContactStore()
                contactStore.requestAccess(for: .contacts, completionHandler: {granted,error in
                    if error != nil {
                        print("error\(error!)")
                    }
                    else if granted == true {
                        performOnMain {
                            self.createAnOwner()
                        }
                    }else {
                        performOnMain {
                            self.addNewLBOwnerOption()
                        }
                    }
                })
                
            case .restricted, .denied:
                let _ = dataModel.newLBOwner()
                self.performSegue(withIdentifier: Identifier.personalData.rawValue , sender: self)
            case .authorized:
                createAnOwner()
            @unknown default:
                print("Futureproof: CNAuthorizationStatus - and has added a new enum type")
            }
        } else{
           performSegue(withIdentifier: Identifier.personalData.rawValue, sender: self)
        }
    }
    // MARK: - Navigation
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let segueIdentifier = segueIdentifierFor(segue: segue)
        switch  segueIdentifier{
        case .tankConfiguration:
            print("Must do the tank configuration action")
            if let vc = segue.actualDestination() as? TankConfigurationList {
                vc.context = dataModel.container.viewContext
            } else {
                fatalError("was expecting a TankConfigurationList")
            }
        case .personalData:
            guard let vc = segue.actualDestination()  as? LogBookOwnerDetails else { fatalError("Didnt get a LogBookOwnerScene")}
            
            let viewModel = OwnerViewModel(with: dataModel)
           
            vc.inject(viewModel)
            
        default:
            break
        }
    }
    //MARK:- owner stuff
    
    fileprivate func createAnOwner() {
        if let ownerName = UIDevice.ownersName(), tryandCreateProfileFrom(ownerName) == true {
                        performSegue(withIdentifier: Identifier.personalData.rawValue, sender: self)
        }else{
            addNewLBOwnerOption()
        }
    }
    
    private func tryandCreateProfileFrom(_ ownersName: String)-> Bool{
        let nameArray = ownersName.components(separatedBy: " ")
        guard nameArray.count > 1 else { return false }

        let contactStore = CNContactStore()
        let predicate = CNContact.predicateForContacts(matchingName: ownersName)
        let keysToFetch = [ CNContactFormatter.descriptorForRequiredKeys(for:.fullName ), CNContactEmailAddressesKey,CNContactNoteKey,CNContactThumbnailImageDataKey,CNContactImageDataKey ,CNContactNicknameKey,CNContactPhoneNumbersKey,CNContactSocialProfilesKey] as! [CNKeyDescriptor]
        let matchingContact = try? contactStore.unifiedContacts(matching: predicate, keysToFetch: keysToFetch )
        if let contact = matchingContact?.first{
            let lBOwner =  dataModel.newLBOwner()
            contact.copyDetails(to: lBOwner)
            return true
        }
        return false
    }
    
    func addNewLBOwnerOption(){
        
        let whatKindOfAddition =  UIAlertController(title: "Create Profile", message: "How would you like to create your profile ", preferredStyle: .actionSheet )
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let addNewDiver = UIAlertAction(title: "Create from scratch", style: .default, handler: {(alertAction) in
            let _ = self.dataModel.newLBOwner()
            self.performSegue(withIdentifier: Identifier.personalData.rawValue , sender: self)
            
        })
        let selectFromContacts = UIAlertAction(title: "Select from contacts", style: .default , handler: {(alertAction) in
            self.showContactPickerForSinglePick()})
        
        whatKindOfAddition.addAction(selectFromContacts)
        whatKindOfAddition.addAction(addNewDiver)
        
        whatKindOfAddition.addAction(cancelAction)
        present(whatKindOfAddition, animated: true, completion: nil)
    }
    func showContactPickerForSinglePick(){
        let contactPicker = CNContactPickerViewController.init()
        contactPicker.delegate = self
        present(contactPicker, animated: true, completion: nil)
    }
    
    @IBAction func doneTapped(_ sender: UIBarButtonItem) {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }

    
}

extension SettingsListTableViewController : CNContactPickerDelegate{
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        let newLBOwner = dataModel.newLBOwner()
        contact.copyDetails(to: newLBOwner)
        performSegue(withIdentifier: Identifier.personalData.rawValue , sender: self)
    }
}

extension SettingsListTableViewController :SegueHandlerType, OptionOrder {
    enum Identifier: String {
        case personalData
        case measurements
        case autofillBuddy
        case autofillFromLast
        case multiLevelAndTables
        case tankConfiguration
        case proMode
        case credit
    }
    
    enum OptionOrder: Int {
        case personalData = 0
        case measurements
        case autofillBuddy
        case autofillFromLast
        case multiLevelAndTables
        case tankConfiguration
        case proMode
        case credit
        
    }
}
extension SettingsListTableViewController : Injectable {
    
    func inject(_ dm: DataModel) {
        dataModel = dm
    }
    
    func assertDependencies() {
        assert(dataModel != nil, "No datamodel")
    }
    
    typealias T = DataModel
    
}

