//
//  CountryChooser.swift
//  Splash
//
//  Created by Simon Gardener on 10/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class CountryChooser: UITableViewController {
    
    typealias CountryChooserBlock =  (String)->()
    var handleSelection : CountryChooserBlock!
    
    fileprivate typealias countriesSectioned = [[String]]

    fileprivate var countries: [String] = {
        var allCountries: [String] = []
        for code in NSLocale.isoCountryCodes {
            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            let name = NSLocale(localeIdentifier: "en_UK").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
            allCountries.append(name)
        }
        return allCountries
    }()
    
   fileprivate var countriesInSections: countriesSectioned!
   fileprivate var sectionHeaders = ["A", "B", "C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W", "Y", "Z"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
       countriesInSections =  createCountriesInSections()
    }
    fileprivate func createCountriesInSections()->countriesSectioned{
        var countriesByAlphabeticalGrouping = countriesSectioned()
        for initialLetter in sectionHeaders{
            var section = [String]()
            section = countries.filter{$0.firstChar() == initialLetter}
            countriesByAlphabeticalGrouping.append(section)
        }
        return countriesByAlphabeticalGrouping
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return countriesInSections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countriesInSections[section].count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
        cell.textLabel?.text = countriesInSections[indexPath.section][indexPath.row]
        return cell
    }

    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return sectionHeaders
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       let country = countriesInSections[indexPath.section][indexPath.row]
       handleSelection(country)
        navigationController?.popViewController(animated: true)
    }
}
