//  LogPage.swift
//  Splash
//
//  Created by Simon Gardener on 11/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.

import UIKit
import CoreLocation
import CoreData
import BLTNBoard

class LogPage: UITableViewController, LocationUser {
    
    let cellID = ["date", "location", "tripBoatOp", "profile", "tank", "equipment", "conditions", "tags", "student", "guidedDiver",  "buddy", "divePro", "note", "signature" ]
    
    var suggestedSite: DiveSite? = nil
    var dataModel : DataModel!
    var logEntry: LogEntry!
    var mode: Mode!
    var locationService: LocationService!
    var isRequestingLocation = false
    var indexPathsForCellsToReload : [IndexPath]? = nil
    var indexSetForDiverSectionsToReload : IndexSet? = nil
    var tankArray = [Tank](){
        didSet{
            tableView.reloadSections(IndexSet.init(integer: OptionOrder.tank.rawValue), with: .none)
        }
    }
    
    var wasAddedAutomatically = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()

        logEntry = dataModel.logEntry
        setTankArray()
        setUpView()
        if mode == .add {
            locationService = LocationService(owner: self, desiredAccuracy: 50, within: 60)
            isRequestingLocation = true
            locationService.requestLocation()
        }
    }
    
    /// prepare for segue sets  values so we know which sections to change on return
    fileprivate func performTableViewUpdatesUponReturn() {
        tableView.beginUpdates()
        // beginUpdates()- endUpdates()  used to prevent error about number of rows in section being trigger
        // also will work without begin/end if swapped the order of the two changes below so sections updates first
        if let ip = indexPathsForCellsToReload {
            tableView.reloadRows(at: ip, with: .automatic)
        }
        if let sections = indexSetForDiverSectionsToReload {
            tableView.reloadSections(sections, with: .automatic)
        }
        tableView.endUpdates()
        indexPathsForCellsToReload = []
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        if !(self.isMovingToParent || self.isBeingPresented){
//            tableView.reloadData()
//        }
        performTableViewUpdatesUponReturn()
    }
    override func viewWillDisappear(_ animated: Bool) {
        if isMovingFromParent  {
            if logEntry.isUpdated {
                classifyDiveTime()
            }
            dataModel.saveContext()
        }
        super .viewWillDisappear(animated)
    }
    private func setUpView(){
        if mode == .add {
            let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButtonPressed))
            let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveButtonPressed))
            navigationItem.leftBarButtonItem = cancelButton
            navigationItem.rightBarButtonItem = saveButton
        }
    }
    
    @objc private func cancelButtonPressed(){
        logEntry.managedObjectContext?.delete(logEntry)
        dismiss(animated: true, completion: nil)
    }
    @objc private func saveButtonPressed(){
        //TODO: lots of checks in here to make sure a valid save
        if mode == .add && logEntry.diveSite?.addedByUser == false {
            captureLocationIfNecessary()
        } else {
            goAheadAndSave()
        }
    }
    fileprivate func captureLocationIfNecessary(){
        if (logEntry.diveSite?.userAlreadyConfirmedLocation == false) && (logEntry.diveSite!.locationConfirms < 11), let _ = logEntry.latitude, let _ = logEntry.longitude {
            present(alertForLocationConfirms(), animated: true)
        }else {
            captureAccessByIfNecessary()
        }
    }
    
    fileprivate func alertForLocationConfirms()-> UIAlertController{
        let message = """
When you started this log entry were you...

(Choose "not sure" if not sure.)
"""
        let alert = UIAlertController(title: "Confirm Location", message: message, preferredStyle: .actionSheet)
        let notAtAction = UIAlertAction(title: "NOT at dive site", style: .default, handler: {action  in self.handleAlertLocation(fix: .isNowhereNear)})
        let shoreAction = UIAlertAction(title: "at shore entry point", style: .default, handler: { action in self.handleAlertLocation(fix: .isShoreEntryPoint)})
        let aboveAction = UIAlertAction(title: "above or moored to site", style: .default, handler: {action in self.handleAlertLocation(fix: .isAboveSite)})
        let nearbyMooringAction = UIAlertAction(title: "at nearby mooring", style: .default, handler: {action in self.handleAlertLocation(fix: .isNearbyMooring)})
        let notSureAction =  UIAlertAction(title: "not sure", style: .cancel , handler: {action in self.handleAlertLocation(fix: .isNowhereNear)})
        alert.addAction(notAtAction)
        alert.addAction(shoreAction)
        alert.addAction(aboveAction)
        alert.addAction(nearbyMooringAction)
        alert.addAction(notSureAction)
        return alert
    }
    
    fileprivate func handleAlertLocation(fix : GPSFix){
        if fix != .isNowhereNear  {
            let message = "You chose \(fix.gpsFixString()!).\n\nWas that correct?\n\nWARNING : This will update your database as well as provide feedback for the developer."
                let alert = UIAlertController(title: "Confirm Choice", message: message, preferredStyle: .alert)
                let confirm = UIAlertAction(title: "Yes", style: .default, handler: {action in self.updatesForConfirmedLocation(fix: fix)})
                let hellNo = UIAlertAction(title: "No", style: .destructive, handler: {action in self.captureLocationIfNecessary() })
            alert.addAction(confirm)
            alert.addAction(hellNo)
            present(alert, animated: true)
        }else {
            captureAccessByIfNecessary()
        }
    }
    
    fileprivate func updatesForConfirmedLocation(fix:GPSFix){
    if let siteID = logEntry.diveSiteID, let lat = logEntry.latitude, let long = logEntry.longitude {
        let confirmValue =  dataModel.confirmValue(matching: siteID)
        confirmValue.siteName = logEntry.diveSite?.name
        confirmValue.latitude =  lat.doubleValue
        confirmValue.longitude = long.doubleValue
        confirmValue.gpsFix = Int16(fix.rawValue)
        logEntry.diveSite?.gpsFix = Int16(fix.rawValue)
        logEntry.diveSite?.latitude = lat.doubleValue as NSNumber
        logEntry.diveSite?.longitude = long.doubleValue as NSNumber
        logEntry.diveSite?.userAlreadyConfirmedLocation = true
    }
        captureAccessByIfNecessary()
}
    
    fileprivate func captureAccessByIfNecessary(){
        if logEntry.diveSite?.userAlreadyConfirmedAccessBy == false  {
            let alert = alertForAccessByConfirms()
            present(alert, animated: true)
        }else {
            goAheadAndSave()
        }
    }
    
    fileprivate func alertForAccessByConfirms()-> UIAlertController {
        let alert = UIAlertController(title: "Confirm Site Access ", message: "I got to this site by..", preferredStyle: .actionSheet)
        let shoreAction = UIAlertAction(title: "shore entry", style: .default, handler: {action in self.handleAccessBy(.shore)})
        let liveaboardAction = UIAlertAction(title: "liveaboard", style: .default, handler: { action in  self.handleAccessBy(.liveaboard)})
        let dayboatAction = UIAlertAction(title: "day boat", style: .default, handler: { action in self.handleAccessBy(.dayboat)})
        let speedboatAction = UIAlertAction(title: "speed boat", style: .default, handler: {action in self.handleAccessBy(.speedboat)})
        let outriggerAction = UIAlertAction(title: "outrigger", style: .default, handler: { action in self.handleAccessBy(.outrigger )})
        let ribAction = UIAlertAction(title: "RIB", style: .default, handler: {action in self.handleAccessBy(.rib)})
        let noneAction = UIAlertAction(title: "none of the above", style: .cancel, handler: {action in self.handleAccessBy(.unknown)})
        
        alert.addAction(shoreAction)
        alert.addAction(liveaboardAction)
        alert.addAction(dayboatAction)
        alert.addAction(speedboatAction)
        alert.addAction(outriggerAction)
        alert.addAction(ribAction)
        alert.addAction(noneAction)
        return alert
    }
    
    fileprivate func handleAccessBy(_ access : AccessBy) {
        if access != .unknown {
            if let siteID = logEntry.diveSiteID {
                let confirmValue = dataModel.confirmValue(matching: siteID)
                if confirmValue.siteName == nil { confirmValue.siteName = logEntry.diveSite?.name }
                confirmValue.accessBy = access.rawValue
                logEntry.diveSite?.userAlreadyConfirmedAccessBy = true
            }
        }
        goAheadAndSave()
    }
    
    fileprivate func goAheadAndSave(){
        do{
            classifyDiveTime()
            try logEntry.managedObjectContext?.save()
            SplashSound.play()
        }catch {
            print("unable to save the context")
            print("\(error), \(error.localizedDescription)")
        }
        dismiss(animated: true)
    }
    
    fileprivate func classifyDiveTime(){
        
        let classifier = DiveTimeClassifier()
        let timeClassification = classifier.classify(dataModel.logEntry!)
        dataModel.setTimeTagsfor(classification: timeClassification)
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return cellID.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionOrder = optionOrderFor(position: section)
        switch sectionOrder {
        case .date, .siteName, .note, .conditions, .tripBoatOp, .profile, .tags, .buddy, .equipment, .signature :
            return 1

        case .tank:
            return tankArray.count

        case .divePro:
            if logEntry.isInstructing{
                return 0
            }else {
                return 1
            }
        case .student:
            if logEntry.isInstructing == true || logEntry.isAssisting == true {
                return 1
            }else {
                return 0
            }

        case .guidedDiver :
            if logEntry.isGuiding == true || logEntry.isAssisting == true {
                return 1
            } else {
                return 0
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section = optionOrderFor(position: indexPath.section)
        
        var cell = UITableViewCell()
        
        switch section {
        case .date :
            guard let dateCell = tableView.dequeueReusableCell(withIdentifier: cellID[indexPath.section], for: indexPath) as? DiveDateNumberCell else{ break}
            dateCell.configureWith(logEntry)
            cell = dateCell
        case .siteName :
            guard let locationCell = tableView.dequeueReusableCell(withIdentifier: cellID[indexPath.section]
                , for: indexPath) as? DiveLocationCell else {fatalError("didnt get a locationCell")}
            locationCell.configure(withLogEntry: logEntry, shouldSpin: isRequestingLocation, addedAutomatically: wasAddedAutomatically)
            cell = locationCell
            
        case .tripBoatOp :
            guard let tBOcell = tableView.dequeueReusableCell(withIdentifier: cellID[indexPath.section], for: indexPath) as? DiveOpBoatTripCell else {fatalError("didnt get a DiveOpBoatTripCelll")}
            tBOcell.configure(with: logEntry)
            cell = tBOcell
            
        case .conditions:
            guard let conditionsCell = tableView.dequeueReusableCell(withIdentifier: cellID[indexPath.section], for: indexPath) as? DiveConditionsCell else {fatalError(" did not get condition cell")}
            conditionsCell.configure(with: logEntry)
            cell = conditionsCell
            
        case .tank:
            var idForCell = cellID[indexPath.section]
            if traitCollection.preferredContentSizeCategory > .accessibilityMedium {
                idForCell = "tankLargertext"
            }
            guard let tankCell = tableView.dequeueReusableCell(withIdentifier: idForCell, for: indexPath) as? DiveTankCell  else {fatalError("Didnt get a diveTankCell")}
            tankCell.populateTankCellWith(tvm: TankDataViewModel(tank: tankArray[indexPath.row]))
            cell = tankCell
            
        case .note:
            guard let diveNoteCell = tableView.dequeueReusableCell(withIdentifier: cellID[indexPath.section]) as? DiveNoteCell else { fatalError("Expecting a dive note cell")}
            diveNoteCell.populateWithData(logEntry)
            cell = diveNoteCell
            
        case .profile:
            var cellIdentifier = "profileSingle"
            if logEntry.logAsMultilevel == true {
                cellIdentifier = "profileMultiLevel"
            }
            guard let profileCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier , for: indexPath) as? DiveProfileCell else {fatalError("didnt get a profile cell")}
            let viewModel = ProfileViewModel(with: dataModel)
            profileCell.populateWith(viewModel)
            cell = profileCell
            
        case .tags:
            guard let tagCell = tableView.dequeueReusableCell(withIdentifier: cellID[indexPath.section], for: indexPath) as? DiveTagCell else {fatalError("didnt get a diveTagCell")}
            tagCell.configure(with:logEntry)
            cell = tagCell
            
        case .buddy:
            guard let diverCell = tableView.dequeueReusableCell(withIdentifier: cellID[indexPath.section], for: indexPath) as? DiveLogDiverGenericCell else { fatalError("didnt get a divercell")}
            diverCell.configure(with: logEntry, attribute: "buddiedBy" )
            cell = diverCell

        case .student:
            guard let diverCell = tableView.dequeueReusableCell(withIdentifier: cellID[indexPath.section], for: indexPath) as? DiveLogDiverGenericCell else { fatalError("didnt get a divercell")}
            diverCell.configure(with: logEntry, attribute: "instructed")
            cell = diverCell
            
        case .guidedDiver:
            guard let diverCell = tableView.dequeueReusableCell(withIdentifier: cellID[indexPath.section], for: indexPath) as? DiveLogDiverGenericCell else { fatalError("didnt get a divercell")}
            diverCell.configure(with: logEntry, attribute: "supervised" )
            cell = diverCell
            
        case .divePro:
            guard let diverCell = tableView.dequeueReusableCell(withIdentifier: cellID[indexPath.section], for: indexPath) as? DiveLogDiverGenericCell else { fatalError("didnt get a divercell")}
            diverCell.configure(with: logEntry, attribute: "ledBy" )
            cell = diverCell
            
        case .equipment:
            guard let equipmentCell = tableView.dequeueReusableCell(withIdentifier: cellID[indexPath.section], for: indexPath) as? EquipmentCell else {
                fatalError("problem")
            }
            equipmentCell.configure(with: logEntry)
            cell = equipmentCell
        
        case .signature:
            guard let signatureCell =  tableView.dequeueReusableCell(withIdentifier: cellID[indexPath.section], for: indexPath) as? SignatureTableViewCell  else { fatalError("didnt get a signaturecell") }
            signatureCell.configure(with: logEntry)
            return signatureCell
        }
        return cell
    }
    
    // MARK: - TableView Delegate
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == OptionOrder.tank.rawValue{
            guard let tankCount = logEntry.gasSupply?.count, tankCount > 1 else { return false }
            return true
        }else {
            return false
        }
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete Tank", handler: {action, indexPath in
            
            let alertController = UIAlertController(title: "Delete Tank?", message: "Are you sure you want to delete this tank?", preferredStyle: .actionSheet )
            let goAheadAction = UIAlertAction(title: "Yes, delete tank", style: .destructive, handler: {action in
                let tank = self.tankArray.remove(at: indexPath.row)
                self.logEntry.removeFromGasSupply(tank)
                self.dataModel.container.viewContext.delete(tank)
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {action in
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
            })
            alertController.addAction(goAheadAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        })
        return [deleteAction]
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        setIndexPathForReloadingData()
        let segueId = segueIdentifierFor(segue: segue)
        switch  segueId {
        case .date:
            guard let vc = segue.actualDestination() as? NumberAndDateChooser else { fatalError("didn't get a number and date chooser tvc")}
            vc.inject(logEntry)
            
        case .siteName :
            guard let vc = segue.actualDestination() as? LocationOptions else  { fatalError("didnt get a LocationOptions tvc")}
            vc.inject(dataModel)
            if isRequestingLocation == true{
                isRequestingLocation = false
                locationService.stop()
            }
            
        case .conditions:
            guard let vc = segue.actualDestination() as? ConditonsChooser  else { fatalError("didnt get a ConditionsChooser")}
            vc.inject(logEntry)
            isRequestingLocation = false

        case .tripBoatOp:
            guard let vc = segue.actualDestination() as? TripBoatOperator  else { fatalError("didnt Get a tripboatOperator scene")}
            vc.inject(dataModel)
            
        case .tank:
            guard let vc = segue.destination as? TankDetailsPlusFill else {fatalError("This should be a TankDetailsPlusFill view Controller")}
            let indexPath =  tableView.indexPathForSelectedRow
            if let index = indexPath?.row{
                vc.inject(tankArray[index])
                vc.delegate = self
            }
            
        case .note:
            guard let vc = segue.actualDestination() as? NoteEditor else { fatalError("Expected a Note Editor Scene")}
            vc.inject(dataModel)
            
        case .profile:
            guard let vc = segue.actualDestination() as? ProfileDataSetter else { fatalError("Expected a profileDtat Setter scene")}
            let viewModel = ProfileViewModel(with: dataModel)
            vc.viewModel = viewModel
            
        case .tags:
            setIndexSetForReloadingDivers()
            guard let vc = segue.actualDestination() as? TagsChooser else  {fatalError(" expected a tag chooser scene")}
            vc.inject(dataModel)
        
        case .buddy:
            guard let vc = segue.actualDestination() as? BuddyChooser else {fatalError("Expecting BVC ")}
            let viewModel = DiverViewModel(with: dataModel)
            vc.inject(viewModel)
            
        case .divePro:
            guard let vc = segue.actualDestination() as? ProChooser else { fatalError("Expecting PVC ")}
            let viewModel = DiverViewModel(with: dataModel)
            vc.inject(viewModel)
            
        case .guidedDiver:
            guard let vc = segue.actualDestination() as? SupervisedChooser else { fatalError("Expecting guidedDiver")}
            let viewModel = DiverViewModel(with: dataModel)
            vc.inject(viewModel)
            
        case .student:
            guard let vc = segue.actualDestination() as? StudentChooser else { fatalError("Expecting StudentChoose ")}
            let viewModel = DiverViewModel(with: dataModel)
            vc.inject(viewModel)
        case .equipment:
            guard let vc = segue.actualDestination() as? EquipmentChoosers else {fatalError("Expected an EquipmentChooser")}
            vc.inject(dataModel.logEntry! )
        
        case .signature:
            guard let vc = segue.actualDestination() as? SignatureViewController else { fatalError("Expected a signature of view controller")}
            vc.inject(dataModel)
        break
        }
    }
    func setIndexPathForReloadingData(){
        indexPathsForCellsToReload = [tableView.indexPathForSelectedRow!]
    }
    func setIndexSetForReloadingDivers(){
        let lowerIndex = OptionOrder.student.rawValue
        let upperIndex = OptionOrder.divePro.rawValue
        
        indexSetForDiverSectionsToReload = IndexSet.init(integersIn: lowerIndex...upperIndex)
        /*OptionOrder.student.rawValue...OptionOrder.divePro.rawValue*/
    }
    @IBAction func siteChoosenUnwindToLogPage(segue: UIStoryboardSegue){
        wasAddedAutomatically = false
    }
    @IBAction func facilityChoosenUnwindToLogPage(segue:UIStoryboardSegue){
    }
    
    //MARK:- LocationUser Conformance
    
    func locationDidUpdate(location: CLLocation) {
        isRequestingLocation = false
 
        let coordinateRange = CoordinateRange(withCenter: location, offset: 0.6)
        let searchPredicates = coordinateRange.searchPredicates()
        
        let fetchRequest: NSFetchRequest<DiveSite> = DiveSite.fetchRequest()
        fetchRequest.predicate = searchPredicates
        
        do {
            let siteNameIndexPath = IndexPath(row: 0, section: OptionOrder.siteName.rawValue)
            let conditiondsIndexPath = IndexPath(row: 0, section: OptionOrder.conditions.rawValue)
            var diveSites = try dataModel.container.viewContext.fetch(fetchRequest)
            guard diveSites.count > 0 else {
                tableView.reloadRows(at: [siteNameIndexPath], with: .automatic)
                return }
            if diveSites.count > 1 {
                sortFetched(nearbySites: &diveSites, byDistanceFrom: location)
            }
            SiteAdder.add(diveSites[0], to: logEntry)
            wasAddedAutomatically = true
            logEntry.wasAddedAutomatically = true
            suggestedSite = logEntry.diveSite
            //capture where the user is when we get an automatically filled in site
            logEntry.latitude = location.coordinate.latitude as NSNumber
            logEntry.longitude = location.coordinate.longitude as NSNumber
            tableView.reloadRows(at:[siteNameIndexPath,conditiondsIndexPath ], with: .automatic)
        } catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
    }
    
    func locationWasDenied(){
        bulletinManager.showBulletin(above: self)
    }
    func partialFix(location: CLLocation) {
    }
    lazy var bulletinManager: BLTNItemManager = {
        var page = BulletinDataSource.locationWasDeniedPage()
        return BLTNItemManager(rootItem: page)
    }()
}

func sortFetched(nearbySites: inout [DiveSite], byDistanceFrom location: CLLocation) {
    for site in nearbySites {
        site.distanceFromLocation = location.distance(from: site.location!) as NSNumber
    }
    nearbySites = nearbySites.sorted {Int(truncating: $0.distanceFromLocation!) < Int(truncating: $1.distanceFromLocation!)}
}

func sortFetched(nearbyFacilities: inout [Facility], byDistanceFrom location:CLLocation){
    for facility in nearbyFacilities {
        facility.distanceFromLocation = location.distance(from: facility.location!) as NSNumber
    }
    nearbyFacilities = nearbyFacilities.sorted {Int(truncating: $0.distanceFromLocation!) < Int(truncating: $1.distanceFromLocation!)}
}

extension LogPage : Injectable {
    
    func inject(_ injected: (dataModel: DataModel ,title: String, mode: Mode) ){
        dataModel = injected.dataModel
        mode = injected.mode
        title = injected.title
    }
    
    func assertDependencies() {
        assert(mode != nil, "mode is nil")
        assert(title != nil, "title is nil")
        assert(dataModel != nil)
    }
    typealias T = (dataModel :DataModel, title:String, mode :Mode)
}

extension LogPage /* helpers*/ {
    func  setTankArray() {
        tankArray = dataModel.logEntry?.gasSupply?.allObjects as! [Tank]
        tankArray = tankArray.sorted{$0.displayOrder < $1.displayOrder}
    }
}

extension LogPage : AnotherTank {
    func addTank() {
        let newTank = dataModel.newTank()
        TankHelper.prepNew(newTank, with: dataModel)
        logEntry.addToGasSupply(newTank)
        tankArray.append(newTank)
    }
}
extension LogPage : OptionOrder, SegueHandlerType {
    enum Identifier: String {
        case date
        case siteName
        case tripBoatOp
        case profile
        case tank
        case equipment
        case conditions
        case tags
        case student
        case guidedDiver
        case buddy
        case divePro
        case note
        case signature
    }
    
    enum OptionOrder :Int {
        case date
        case siteName
        case tripBoatOp
        case profile
        case tank
        case equipment
        case conditions
        case tags
        case student
        case guidedDiver
        case buddy
        case divePro
        case note
        case signature
    }
}
