//
//  WeightPickerView.swift
//  Splash
//
//  Created by Simon Gardener on 11/06/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
class WeightPicker: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource{
    private let all10Digits = 10
    private let fiveComponents = 5
    private let digits = [0,1,2,3,4,5,6,7,8,9]
    private let unitSymbols = ["kg","lb"]
    
    private let tensComponents = 4
    private let unitsComponents = 2
    private let pointComponents = 1
    private let width = 35
    
    private let tensPosition = 0
    private let singlePosition = 1
    private let pointPosition = 2
    private let tenthsPosition = 3
    private let unitsPosition = 4
    private let metricRow = 0
    
    var logEntry: LogEntry!
    weak var cell :  LogWeightCell!
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return fiveComponents
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component{
        case tensPosition: return tensComponents
        case unitsPosition: return unitsComponents
        case pointPosition: return pointComponents
        default : return all10Digits
        }
    }
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return CGFloat(width)
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        //  print("requesting \(row), component\(component))")
        switch component{
        case unitsPosition:
            return unitSymbols[row]
        case pointPosition:
            return "."
        default:
            return String(digits[row])
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("row selected \(row) in component \(component )")
        let weight = valueOnPickerWheel()
        if selectedRow(inComponent: unitsPosition) != metricRow {
            logEntry.weight = weight.convertPoundsToKilos() as NSNumber
        }else {
            logEntry.weight = weight as NSNumber
        }
        let weightToDisplay = Double(truncating: logEntry.weight!).weightInUserPreference()
        cell.textLabel?.text = weightToDisplay
        
    }
    
    func valueOnPickerWheel() -> Double{
        print("going to calculate pickerwheel value")
        let newValue = Double(
            selectedRow(inComponent: tensPosition) * 10 + selectedRow(inComponent: singlePosition)) + Double(selectedRow(inComponent: tenthsPosition))/10.0
        print("newValue\(newValue)")
        return newValue
        
    }
    func setPickerWithInitialValues(){
        guard let weight = logEntry.weight, weight != 0 else {
            return
        }
        var weightAsDouble = Double(truncating: weight)
        if UserDefaults.weightUnits() == .imperial {
            weightAsDouble = weightAsDouble * 2.2
        }
        let digits = weightAsDouble.digitsAtPositions()
        selectRow(digits.tens , inComponent: tensPosition, animated: false)
        selectRow(digits.singles, inComponent: singlePosition, animated: false  )
        selectRow(digits.tenths, inComponent: tenthsPosition, animated: false)
        if UserDefaults.weightUnits() == .metric {
            selectRow(0, inComponent: unitsPosition, animated: false)
        }else {
            selectRow(1, inComponent: unitsPosition, animated: false)
        }
    }

}
