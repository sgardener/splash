//
//  PressureGroupPairPicker.swift
//  Splash
//
//  Created by Simon Gardener on 13/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class PressureGroupPairPicker: UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate  {
    
    let componentNumber = 5
    let componentLabelNumber = 1
    let componentPGNumber = 27
    let pressureGroups = ["-","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    let blankComponent = 1
    
    let defaultWidth :CGFloat = 35
    let widerComponent :CGFloat = 50
    
    let startPosition = 1
    let endPosition = 3
    
    var cell : PressureGroupCell!
    var logEntry: LogEntry!
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return componentNumber
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let componentName = optionOrderFor(position: component)
        switch componentName {
        case .pgInLabel, .pgOutLabel:
            return componentLabelNumber
        case .pgIn, .pgOut:
            return componentPGNumber
        case .blank:
            return blankComponent
        }
    }
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat{
        let optionName = optionOrderFor(position: component)
        switch optionName {
        case .pgIn,.pgOut:
            return defaultWidth
        default:
            return widerComponent
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let optionName = optionOrderFor(position: component)
        switch optionName {
        case .blank: return " "
        case .pgInLabel : return "In"
        case .pgOutLabel : return "Out"
        case .pgIn, .pgOut : return  pressureGroups[row]
        }
    }
    func setInitialPickerValues() {
        if let startIndex = pressureGroups.firstIndex(of:logEntry.pressureGroupIn!){
            selectRow(startIndex, inComponent: startPosition, animated: false)
        }
        if let endIndex = pressureGroups.firstIndex(of: logEntry.pressureGroupOut!){
            selectRow(endIndex, inComponent: endPosition, animated: false)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == OptionOrder.pgIn.rawValue {
            logEntry.pressureGroupIn = pressureGroups[row]
            cell.startPGLabel.text = pressureGroups[row]
        }else if component == OptionOrder.pgOut.rawValue{
            logEntry.pressureGroupOut = pressureGroups[row]
            cell.endPGLabel.text = pressureGroups[row]
        }
    }
}
extension PressureGroupPairPicker : OptionOrder{
    enum OptionOrder: Int{
        case pgInLabel, pgIn, blank, pgOutLabel, pgOut
    }
}
