//  SafetyStopPicker.swift
//  Splash
//  Created by Simon Gardener on 13/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.

import UIKit

class SafetyStopPicker: UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate{
    private let totalComponents = 6
    private let all10Digits = 10
    private let limitedDigits = 4
    private let atComponents = 1
    private let numberOfUnitsComponents = 2
    private let digits = [0,1,2,3,4,5,6,7,8,9]
    private let unitsArray = ["m","ft"]
    private let componentWidth = 35
    private let timeTensPosition = 0
    private let timeSinglePosition = 1
    private let safetyTensPosition = 3
    private let safetySinglePosition = 4
    private let unitsPosition = 5
    private let metricRow = 0
    
    var logEntry: LogEntry!
    weak var cell :  LogAttributeDataCell!
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return totalComponents
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let componentName = optionOrderFor(position: component)
        switch componentName {
        case .timeTens,.timeSingle, .depthSingle:
            return all10Digits
        case .depthTens:
            return limitedDigits
        case .at:
            return atComponents
        case.depthUnit :
            return numberOfUnitsComponents
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return CGFloat(componentWidth)
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let componentName = optionOrderFor(position: component)
        switch componentName {
        case .timeTens,.timeSingle, .depthSingle, .depthTens :
            return String(digits[row])
        case .at:
            return "@"
        case.depthUnit :
            return unitsArray[row]
        }
    }
    func setPickerWithInitialValues(times:(tens:Int, single:Int), safetyDepth:( tens: Int, singles:Int)){
        
        selectRow(times.tens, inComponent: timeTensPosition, animated: false)
        selectRow(times.single, inComponent: timeSinglePosition, animated: false)
        selectRow(safetyDepth.tens, inComponent: safetyTensPosition, animated: false)
        selectRow(safetyDepth.singles, inComponent: safetySinglePosition, animated: false)
        if UserDefaults.depthUnits() == .metric {
            selectRow(0, inComponent: unitsPosition, animated: false)
        }else {
            selectRow(1, inComponent: unitsPosition, animated: false)
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let time = selectedRow(inComponent: timeTensPosition) * 10 + selectedRow(inComponent: timeSinglePosition)
        let depth = selectedRow(inComponent: safetyTensPosition) * 10 + selectedRow(inComponent: safetySinglePosition)
        if selectedRow(inComponent: unitsPosition) != metricRow{
            logEntry.safetyStopDepth = Double(depth).convertFeetToMeters()
        }else {
            logEntry.safetyStopDepth = Double(depth)
        }
        logEntry.safetyStopTime = Int16(time)
        cell.dataLabel.text = "\(logEntry.safetyStopTime) @ \(logEntry.safetyStopDepth.depthInUserPreference())"

    }
}

extension  SafetyStopPicker :OptionOrder{
    enum OptionOrder: Int {
        case timeTens, timeSingle, at, depthTens, depthSingle, depthUnit
        
    }
}
