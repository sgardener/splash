//
//  MultiLevelPicker.swift
//  Splash
//
//  Created by Simon Gardener on 13/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class MultiLevelPicker: UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate  {

    private let totalComponents = 10
    private let noPGComponents = 8
    private let blankPosition1 = 4
    private let blankPosition2 = 8
    private let all10Digits = 10
    private let unitsComponents = 2
    private let unitsPosition = 3
    private let pressurePosition = 9
    private let digits = [0,1,2,3,4,5,6,7,8,9]
    private let pressureGroups = ["-","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    private let unitSymbols = ["m","ft"]
    private let isMetric = 0
    
    var depthAttribute : String!
    var timeAttribute : String!
    var pgAttribute : String!
    var cell : MultiLevelCell!
    var logEntry: LogEntry!
    var includesPGs : Bool!
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if includesPGs == true { print(" ALL COMPONENT ")
            return totalComponents }
        else { print(" SOME COMPONENT ")
            return noPGComponents }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component{
        case unitsPosition:
            return unitsComponents
        case pressurePosition:
            return pressureGroups.count
        case blankPosition1,blankPosition2: return 1
        default:return all10Digits
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case unitsPosition: return unitSymbols[row]
        case pressurePosition: return pressureGroups[row]
        case blankPosition1 , blankPosition2: return " "
            
        default: return String(digits[row])
        }
    }

    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        switch component{
        case blankPosition2,blankPosition1: return 15
        default: return 30
            
        }
    }
   // func setInitialPickerValues(depth:(d100s: Int,dTens: Int, d1s), time:(t100s: Int,Int ,epg:String)){
    func setInitialPickerValues(){
        let depth = logEntry.value(forKey: depthAttribute) as! Double
        let depthValues = depth.digitValues()
        let time = logEntry.value(forKey: timeAttribute) as! Int16
        let timeValues = time.digitValues()
        let pressureGroup = logEntry.value(forKey: pgAttribute)  as! String?
        selectRow(depthValues.hundreds, inComponent: OptionOrder.depthTens
            .rawValue, animated: false)
        selectRow(depthValues.tens, inComponent: OptionOrder.depthTens.rawValue, animated: false)
        selectRow(depthValues.singles, inComponent: OptionOrder.depthSingle.rawValue, animated: false)
        
        if UserDefaults.depthUnits() == .metric {
            selectRow(0, inComponent: OptionOrder.units.rawValue, animated: false)
        }else{
            selectRow(1, inComponent: OptionOrder.units.rawValue, animated: false)
        }
        
        selectRow(timeValues.hundreds, inComponent: OptionOrder.timeHundreds.rawValue, animated: false)
        selectRow(timeValues.tens, inComponent: OptionOrder.timeTens.rawValue, animated: false)
        selectRow(timeValues.units, inComponent: OptionOrder.timeSingle.rawValue, animated: false)
        guard includesPGs == true else {return}
        if let pg = pressureGroup, let pgIndex = pressureGroups.firstIndex(of: pg){
            selectRow(pgIndex, inComponent: OptionOrder.pG.rawValue, animated: false)
        } else{
            selectRow(0, inComponent: OptionOrder.pG.rawValue, animated: false)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let componentName = optionOrderFor(position: component)
        switch componentName{
        case .depthHundreds, .depthSingle, .depthTens, .units :
        var depth = Double(selectedRow(inComponent: OptionOrder.depthHundreds.rawValue)*100 + selectedRow(inComponent: OptionOrder.depthTens.rawValue)*10 + selectedRow(inComponent: OptionOrder.depthSingle.rawValue))
        if selectedRow(inComponent: OptionOrder.units.rawValue) != isMetric{
            depth = depth.convertFeetToMeters()
            }
            cell.depthLabel.text = depth.depthInUserPreference()
            logEntry.setValue(depth, forKey: depthAttribute)
        
        case .timeHundreds, .timeTens, .timeSingle:
            let time = Int16(selectedRow(inComponent: OptionOrder.timeHundreds.rawValue)*100 + selectedRow(inComponent: OptionOrder.timeTens.rawValue)*10 + selectedRow(inComponent: OptionOrder.timeSingle.rawValue))
            logEntry.setValue(time, forKey: timeAttribute)
            cell.timeLabel.text = "\(time)"
            
        case .pG:
            let pgString = pressureGroups[selectedRow(inComponent: component)]
            logEntry.setValue(pgString , forKey: pgAttribute)
            cell.endPGLabel?.text = pgString
        
        case .blank1, .blank2 :
            break
       
       
        }
    }
}
extension MultiLevelPicker :OptionOrder{
    enum OptionOrder :Int{
        case depthHundreds,depthTens,depthSingle,units,blank1,timeHundreds,timeTens,timeSingle,blank2, pG
    }
}

