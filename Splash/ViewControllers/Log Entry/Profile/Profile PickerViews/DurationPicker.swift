//
//  DurationPicker.swift
//  Splash
//
//  Created by Simon Gardener on 13/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class DurationPicker: UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate {
    private let all10Digits = 10
    private let threeComponents = 3
    private let hundredsComponentPosition = 0
    private let tensComponentPosition = 1
    private let unitsComponentPosition = 2
    private let digits = [0,1,2,3,4,5,6,7,8,9]
    let componentWidth : CGFloat = 35
   
    var logEntry: LogEntry!
    weak var cell :  LogAttributeDataCell!
    var durationAttribute : String!
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return threeComponents
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return all10Digits
    }
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return componentWidth
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return String(digits[row])
        
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    print("value on wheel - \(valueOnPickerWheels())")
    
    }
    
    func setPickerWheelsTo(rowValue:(hundreds :Int, tens: Int, units: Int)){

        selectRow(rowValue.hundreds, inComponent: hundredsComponentPosition, animated: false)
        selectRow(rowValue.tens, inComponent: tensComponentPosition, animated: false)
        selectRow(rowValue.units, inComponent: unitsComponentPosition, animated: false)
     
    }

    func valueOnPickerWheels()-> Int{
        let hundreds = selectedRow(inComponent: hundredsComponentPosition)
        let tens = selectedRow(inComponent: tensComponentPosition)
        let units = selectedRow(inComponent: unitsComponentPosition)
        return hundreds*100 + tens*10 + units
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let time = valueOnPickerWheels()
        logEntry.setValue(time, forKey: durationAttribute)
        cell.dataLabel.text =  Int16(time).durationString()
    }
}
