//
//  SurfaceIntervalPicker.swift
//  Splash
//
//  Created by Simon Gardener on 13/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class SurfaceIntervalPicker: UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate  {
    private let sixComponents = 6
    private let all10Digits = 10
    private let unitsCount = 1
    private let hourTensPosition = 0
    private let hourSinglesPosition = 1
    private let hourPosition = 2
    private let minuteTenPosition = 3
    private let minuteSinglePosition = 4
    private let minutePosition = 5
    private let digits = [0,1,2,3,4,5,6,7,8,9]
    private let hourSymbol = "hr"
    private let minuteSymbol = "min"
    private let defaultComponentWidth :CGFloat = 35
    private let widerComponentWidth :CGFloat = 50
    
    var logEntry: LogEntry!
    weak var cell :  LogAttributeDataCell!
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return sixComponents
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component{
        case hourPosition, minutePosition:
            return unitsCount
        default: return all10Digits
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        if component == minutePosition{
            return widerComponentWidth
        } else {
            return defaultComponentWidth
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component{
        case minutePosition:
            return minuteSymbol
        case hourPosition:
            return hourSymbol
        default:
            return String(digits[row])
        }
    }
    
    func setInitialPickerValues(interval:(hourTen: Int, hourSingle: Int, minuteTen: Int, minuteSingle: Int)){
        selectRow(interval.hourTen, inComponent: hourTensPosition, animated: false)
        selectRow(interval.hourTen, inComponent: hourSinglesPosition, animated: false)
        selectRow(interval.minuteTen, inComponent: minuteTenPosition, animated: false)
        selectRow(interval.minuteSingle, inComponent: minuteSinglePosition, animated: false)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let hours = selectedRow(inComponent: hourTensPosition) * 10 + selectedRow(inComponent: hourSinglesPosition)
        let minutes = selectedRow(inComponent: minuteTenPosition) * 10 + selectedRow(inComponent: minuteSinglePosition)
        let time = hours * 60 + minutes
        logEntry.surfaceInterval = Int16(time)
        cell.dataLabel.text = logEntry.surfaceInterval.durationString()
        
    }
}
