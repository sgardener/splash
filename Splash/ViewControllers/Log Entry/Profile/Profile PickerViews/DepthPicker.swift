//
//  DepthPicker.swift
//  Splash
//
//  Created by Simon Gardener on 13/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class DepthPicker: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource{
    private let all10Digits = 10
    private let sixComponents = 6
    private let digits = [0,1,2,3,4,5,6,7,8,9]
    private let unitSymbols = ["m","ft"]
    private let unitsComponents = 2
    private let unitsPosition = 5
    private let pointPosition = 3
    private let pointComponents = 1
    private let width = 35
    private let hundredsPosition = 0
    private let tensPosition = 1
    private let singlePosition = 2
    private let tenthsPosition = 4
    private let metricRow = 0
    
    var logEntry: LogEntry!
    weak var cell :  LogAttributeDataCell!
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
       return sixComponents
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component{
        case unitsPosition: return unitsComponents
        case pointPosition: return pointComponents
        default : return all10Digits
        }
    }
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return CGFloat(width)
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
      //  print("requesting \(row), component\(component))")
        switch component{
        case unitsPosition:
            return unitSymbols[row]
        case pointPosition:
            return "."
        default:
            return String(digits[row])
        }
    }
 
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("row selected \(row) in component \(component )")
        let depth = valueOnPickerWheel()
        if selectedRow(inComponent: unitsPosition) != metricRow {
            logEntry.depthMax = Double(depth).convertFeetToMeters()
        }else {
            logEntry.depthMax = Double(depth)
        }
        cell.dataLabel.text = "\(logEntry.depthMax.depthInUserPreference(withUnits: false))"
        
    }
   
    func valueOnPickerWheel() -> Double{
        print("going to calculate pickerwheel value")
        let newValue = Double( selectedRow(inComponent: hundredsPosition) * 100 +
            selectedRow(inComponent: tensPosition) * 10 + selectedRow(inComponent: singlePosition)) + Double(selectedRow(inComponent: tenthsPosition))/10.0
        
        print("newValue\(newValue)")
        
        return newValue
        
    }
    func setPickerWithInitialValues(rowValues:(hundreds:Int, tens:Int, singles: Int, tenths:Int)){
        selectRow(rowValues.hundreds, inComponent: hundredsPosition, animated: false)
        selectRow(rowValues.tens, inComponent: tensPosition, animated: false)
        selectRow(rowValues.singles, inComponent: singlePosition, animated: false  )
        selectRow(rowValues.tenths, inComponent: tenthsPosition, animated: false)
        if UserDefaults.depthUnits() == .metric {
            selectRow(0, inComponent: unitsPosition, animated: false)
        }else {
            selectRow(1, inComponent: unitsPosition, animated: false)
        }
    }
}
