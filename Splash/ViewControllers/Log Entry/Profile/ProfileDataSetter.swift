//
//  ProfileDataSetter.swift
//  Splash
//
//  Created by Simon Gardener on 12/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class ProfileDataSetter: UITableViewController {
    
    @IBOutlet var pickerTrigger: UITextField!
    
    var viewModel: ProfileViewModel!
    
    var datePicker = UIDatePicker()
    var durationPicker = DurationPicker()
    var depthPicker = DepthPicker()
    var safetyStopPicker = SafetyStopPicker()
    var surfaceIntervalPicker = SurfaceIntervalPicker()
    var pressureGroupPairPicker = PressureGroupPairPicker()
    var multiLevelPicker = MultiLevelPicker()
    let multiLevelDeepest = 3
    let multiLevelMiddle = 2
    let multiLevelShallow = 1
    var toolbar : UIToolbar!
    
    private enum cellIdentifiers: String{
        case AttributeData, SwitchCell, PGCell, LevelCell, LevelCellWithPG
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        toolbar =  toolbarForPicker(doneMethod: #selector(doneButtonPressed))
        title = "Profile"
        
        preparePickers()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.updateTimeOut()
    }
    
    //MARK:- toolbar and picker Prep
    
    private func toolbarForPicker(doneMethod: Selector)-> UIToolbar  {
        let pickerToolbar = UIToolbar()
        pickerToolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: doneMethod)
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        pickerToolbar.setItems([flexible,doneButton], animated: false)
        return pickerToolbar
    }
    @objc private func doneButtonPressed() {
        print("pressedDone")
        let indexPath = tableView.indexPathForSelectedRow!
        let cell = tableView.cellForRow(at: indexPath ) as? ResignableCell
        cell?.hiddenTextField.resignFirstResponder()
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    fileprivate func preparePickers(){
        
        prepareDatePicker()
        durationPicker.sizeToFit()
        depthPicker.sizeToFit()
        safetyStopPicker.sizeToFit()
        surfaceIntervalPicker.sizeToFit()
        pressureGroupPairPicker.sizeToFit()
        multiLevelPicker.sizeToFit()
        multiLevelPicker.sizeToFit()
        
        durationPicker.delegate = durationPicker
        durationPicker.dataSource = durationPicker
        
        depthPicker.delegate = depthPicker
        depthPicker.dataSource = depthPicker
        
        safetyStopPicker.delegate = safetyStopPicker
        safetyStopPicker.dataSource = safetyStopPicker
        
        surfaceIntervalPicker.delegate = surfaceIntervalPicker
        surfaceIntervalPicker.dataSource = surfaceIntervalPicker
        
        pressureGroupPairPicker.delegate = pressureGroupPairPicker
        pressureGroupPairPicker.dataSource = pressureGroupPairPicker
        
        multiLevelPicker.delegate = multiLevelPicker
        multiLevelPicker.dataSource = multiLevelPicker
        multiLevelPicker.delegate = multiLevelPicker
        multiLevelPicker.dataSource = multiLevelPicker
        
    }
    fileprivate func prepareDatePicker(){
        datePicker.sizeToFit()
        datePicker.timeZone = viewModel.timeZone()
        datePicker.date = viewModel.timeIn()
        datePicker.datePickerMode = .time
        datePicker.addTarget(self, action: #selector(dateChanged), for: .valueChanged)
    }
    @objc fileprivate func dateChanged(){
        let date = datePicker.date
        viewModel.userUpdated(date: date)
        let cell = tableView.cellForRow(at: tableView.indexPathForSelectedRow!) as! LogAttributeDataCell
        cell.dataLabel.text = viewModel.timeIn()
    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return viewModel.numberOfRowsIn(section)
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell
        let sectionName = optionOrderFor(position: indexPath.section)
        
        switch sectionName {
        case .timeIn, .diveTime, .maxDepth, .bottomTime, .safetyStop, .surfaceInterval :
            let attributeDataCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifiers.AttributeData.rawValue, for: indexPath) as! LogAttributeDataCell
            attributeDataCell.attributeLabel.text = viewModel.attributeLabelForSection(section: indexPath.section)
            attributeDataCell.hiddenTextField.inputAccessoryView = toolbar
            
            
            switch sectionName {
            case.timeIn:
                attributeDataCell.hiddenTextField.inputView = datePicker
                attributeDataCell.dataLabel.text = viewModel.timeIn()
            case .diveTime:
                attributeDataCell.hiddenTextField.inputView = durationPicker
                attributeDataCell.dataLabel.text = viewModel.totalDiveTime()
            case.maxDepth:
                attributeDataCell.hiddenTextField.inputView = depthPicker
                attributeDataCell.dataLabel.text = viewModel.maxDepth()
            case.bottomTime:
                attributeDataCell.hiddenTextField.inputView = durationPicker
                attributeDataCell.dataLabel.text = viewModel.bottomTime()
            case.safetyStop:
                attributeDataCell.hiddenTextField.inputView = safetyStopPicker
                attributeDataCell.dataLabel.text = viewModel.safetyStop()
            case.surfaceInterval:
                attributeDataCell.hiddenTextField.inputView = surfaceIntervalPicker
                attributeDataCell.dataLabel.text = viewModel.surfaceInterval()
            default:break
            }
            
            cell = attributeDataCell
            
        case .usesDiveTables:
            if indexPath.row == 0{
                let switchCell = getSwitchCellFor(indexPath: indexPath)
                switchCell.theSwitch.tag = indexPath.section
                switchCell.theSwitch.isOn = viewModel.usesTables
                
                cell = switchCell
            }  else {
                
                guard  let pgCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifiers.PGCell.rawValue, for: indexPath) as? PressureGroupCell else {fatalError()}
                print("Pgcell: \(pgCell)")
                pgCell.startPGLabel.text  = viewModel.startPG()
                pgCell.endPGLabel.text = viewModel.endPG()
                pgCell.hiddenTextField.inputView = pressureGroupPairPicker
                pgCell.hiddenTextField.inputAccessoryView = toolbar
                cell = pgCell
            }
        case .multiLevelDive:

            if indexPath.row == 0 {
                let switchCell = getSwitchCellFor(indexPath: indexPath)
                switchCell.theSwitch.tag = indexPath.section
                switchCell.theSwitch.isOn = viewModel.isMultiLevel
                cell = switchCell
            }else {
                var levelCell : MultiLevelCell
                if viewModel.usesTables == true{
                    levelCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifiers.LevelCellWithPG.rawValue , for: indexPath) as! MultiLevelCell
                }else {
                    levelCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifiers.LevelCell.rawValue, for: indexPath) as! MultiLevelCell
                }
                
                levelCell.hiddenTextField.inputView = multiLevelPicker
                levelCell.hiddenTextField.inputAccessoryView = toolbar
                
                let units = traitCollection.preferredContentSizeCategory <= .accessibilityMedium
                switch indexPath.row{
                case multiLevelDeepest:
                    levelCell.depthLabel.text = viewModel.maxDepth(withUnits: units )
                    levelCell.endPGLabel?.text = viewModel.firstIntermediatePG()
                    levelCell.timeLabel.text = viewModel.bottomTimeShort()
                    levelCell.depthNumber.text = "D1:"
                    levelCell.timeNumber.text = "T1:"
                case multiLevelMiddle:
                    levelCell.depthLabel.text = viewModel.secondDepth(withUnits: units)
                    levelCell.endPGLabel?.text = viewModel.secondIntermediatePG()
                    levelCell.timeLabel.text = viewModel.secondTimeShort()
                    levelCell.depthNumber.text = "D2:"
                    levelCell.timeNumber.text = "T2:"
                case multiLevelShallow:
                    levelCell.depthLabel.text = viewModel.thirdDepth(withUnits: units)
                    levelCell.endPGLabel?.text = viewModel.endPG()
                    levelCell.timeLabel.text = viewModel.thirdTimeShort()
                    levelCell.depthNumber.text = "D3:"
                    levelCell.timeNumber.text = "T3:"
                default : break
                }
                cell = levelCell
            }
            
        case .decoStops:
            let switchCell =  getSwitchCellFor(indexPath: indexPath)
            switchCell.theSwitch.isOn = viewModel.isDecoDive
            cell = switchCell
        }
        
        return cell
    }
    
    private func getSwitchCellFor(indexPath:IndexPath) -> SwitchCell{
        let switchCell =  tableView.dequeueReusableCell(withIdentifier: cellIdentifiers.SwitchCell.rawValue, for: indexPath) as! SwitchCell
        switchCell.attributeLabel.text = viewModel.attributeLabelForSection(section: indexPath.section)
        return switchCell
        
    }
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        
        switch sender.tag {
            
        case OptionOrder.usesDiveTables.rawValue:
            viewModel.usesTables = sender.isOn
            tableView.reloadSections(IndexSet.init(integer:sender.tag), with: .automatic)
            if viewModel.isMultiLevel == true {
                tableView.reloadSections(IndexSet.init(integer:sender.tag + 1), with: .none)
            }
        case OptionOrder.multiLevelDive.rawValue:
            viewModel.isMultiLevel = sender.isOn
            tableView.reloadSections(IndexSet.init(integer:sender.tag), with: .automatic)
            
        case OptionOrder.decoStops.rawValue:
            viewModel.isDecoDive = sender.isOn
        default : break
        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName{
            
        case .timeIn:
            guard let cell = tableView.cellForRow(at: tableView.indexPathForSelectedRow!) as? LogAttributeDataCell else { fatalError("wrong kind of cell") }
            cell.hiddenTextField.becomeFirstResponder()
            
        case .bottomTime:
            guard let cell = tableView.cellForRow(at: tableView.indexPathForSelectedRow!) as? LogAttributeDataCell else { fatalError("wrong kind of cell") }
            let rowValues = (viewModel.dataModel.logEntry?.bottomTime)!.digitValues()
            cell.hiddenTextField.becomeFirstResponder()
            durationPicker.cell = cell
            durationPicker.logEntry = viewModel.dataModel.logEntry
            durationPicker.durationAttribute = "bottomTime"
            durationPicker.setPickerWheelsTo(rowValue: rowValues)
            
        case .diveTime:
            guard let cell = tableView.cellForRow(at: tableView.indexPathForSelectedRow!) as? LogAttributeDataCell else { fatalError("wrong kind of cell") }
            let rowValues = (viewModel.dataModel.logEntry?.duration)!.digitValues()
            cell.hiddenTextField.becomeFirstResponder()
            durationPicker.cell = cell
            durationPicker.logEntry = viewModel.dataModel.logEntry
            durationPicker.durationAttribute = "duration"
            durationPicker.setPickerWheelsTo(rowValue: rowValues)
            
        case .safetyStop:
            guard let cell = tableView.cellForRow(at: tableView.indexPathForSelectedRow!) as? LogAttributeDataCell else { fatalError("wrong kind of cell") }
            cell.hiddenTextField.becomeFirstResponder()
            safetyStopPicker.cell = cell
            safetyStopPicker.logEntry = viewModel.dataModel.logEntry
            
        case.maxDepth:
            guard let cell = tableView.cellForRow(at: tableView.indexPathForSelectedRow!) as? LogAttributeDataCell else { fatalError("wrong kind of cell")}
            cell.hiddenTextField.becomeFirstResponder()
            let rowValues = (viewModel.dataModel.logEntry?.depthMax)!.digitValues()
            depthPicker.cell = cell
            depthPicker.logEntry = viewModel.dataModel.logEntry
            depthPicker.setPickerWithInitialValues(rowValues: rowValues)
            
        case .surfaceInterval:
            guard let cell = tableView.cellForRow(at: tableView.indexPathForSelectedRow!) as? LogAttributeDataCell else { fatalError("wrong kind of cell")}
            cell.hiddenTextField.becomeFirstResponder()
            surfaceIntervalPicker.cell = cell
            surfaceIntervalPicker.logEntry = viewModel.dataModel.logEntry
            // surfaceIntervalPicker.setInitialPickerValues(interval: <#T##(hourTen: Int, hourSingle: Int, minuteTen: Int, minuteSingle: Int)#>)
            
        case .multiLevelDive:
            guard indexPath.row > 0 else {break}
            
            guard let cell = tableView.cellForRow(at: tableView.indexPathForSelectedRow!) as? MultiLevelCell else {fatalError("didnt get right kind of cell")}
            multiLevelPicker.includesPGs = viewModel.usesTables
            print("MMultipicker pg set to \(viewModel.usesTables)")
            multiLevelPicker.cell = cell
            multiLevelPicker.logEntry = viewModel.dataModel.logEntry
            switch indexPath.row {
            case multiLevelShallow:
                multiLevelPicker.depthAttribute = "depthThirdLevel"
                multiLevelPicker.timeAttribute = "bottomTimeThirdLevel"
                multiLevelPicker.pgAttribute = "pressureGroupOut"
            case multiLevelMiddle:
                multiLevelPicker.depthAttribute = "depthSecondLevel"
                multiLevelPicker.timeAttribute = "bottomTimeSecondLevel"
                multiLevelPicker.pgAttribute = "pressureGroupSecondLevel"
            case multiLevelDeepest:
                multiLevelPicker.depthAttribute = "depthMax"
                multiLevelPicker.timeAttribute = "bottomTime"
                multiLevelPicker.pgAttribute = "pressureGroupFirstLevel"
            default: break
            }
            cell.hiddenTextField.becomeFirstResponder()
            
        case .usesDiveTables:
            guard indexPath.row > 0  else {break}
            guard let cell = tableView.cellForRow(at: tableView.indexPathForSelectedRow!) as? PressureGroupCell else {fatalError("didnt get right kind of cell")}
            pressureGroupPairPicker.cell = cell
            pressureGroupPairPicker.logEntry = viewModel.dataModel.logEntry
            pressureGroupPairPicker.setInitialPickerValues()
            cell.hiddenTextField.becomeFirstResponder()
            
        case .decoStops:
            break
        }
        
    }
    
}

extension ProfileDataSetter  : OptionOrder{
    
    enum OptionOrder : Int {
        case timeIn
        case diveTime
        case maxDepth
        case bottomTime
        case safetyStop
        case surfaceInterval
        case usesDiveTables
        case multiLevelDive
        case decoStops
    }
}
extension ProfileDataSetter : Injectable{
    func inject(_ vm: ProfileViewModel) {
        viewModel = vm
    }
    
    func assertDependencies() {
        assert(viewModel != nil, "Expected a viewModel for ProfileData")
    }
    
    typealias T = ProfileViewModel
    
    
    
}

