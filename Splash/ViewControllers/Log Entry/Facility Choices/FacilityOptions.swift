//
//  FacilityOptions.swift
//  Splash
//
//  Created by Simon Gardener on 19/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import BLTNBoard

class FacilityOptions: UITableViewController {
    private let indexPathForNearest = IndexPath(row: 0, section: OptionOrder.nearestFacility.rawValue)
    var locationService: LocationService!
    var logEntry: LogEntry!
    var dataModel: DataModel!
    var previousLogEntry : LogEntry?
    var nearestFacility : Facility? {
        didSet{
            tableView.reloadRows(at:[indexPathForNearest], with: .automatic)
        }
    }
    
    let cellID = "cell"
    let sectionNames = ["Current Facility","Last Facility","Nearest Facility","Nearby Facilities","Regular Facilities","Manual Search","Add New Facility"]
    override func viewDidLoad() {
        super.viewDidLoad()
        previousLogEntry = dataModel.previousLogEntry()
        logEntry = dataModel.logEntry
        locationService = LocationService(owner: self, desiredAccuracy: 50, within: 60)
        locationService.requestLocation()
    }
    
    lazy var bulletinManager: BLTNItemManager = {
        var page = BulletinDataSource.locationWasDeniedPage()
        return BLTNItemManager(rootItem: page)
    }()
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return OptionOrder.addNewFacility.rawValue + 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInTable = optionOrderFor(position: section)
        switch sectionInTable {
        case .currentFacility:
            guard logEntry.facility != nil else { return 0 }
            return 1
        case .lastFacility:
            guard (previousLogEntry?.facility) != nil else { return 0}
            return 1
        case .regularFacilities:
            guard (dataModel.numberOfFacilitiesMarkedAsRegular() > 0) else {return 0}
            return 1
        default: return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sectionName =  optionOrderFor(position: section)
        switch sectionName {
        case .currentFacility:
            if logEntry.facility == nil {
                return nil
            }else{
                return "Current Dive Operation"
            }
        case .nearestFacility: 
            return "Use Nearest Dive Operation"
        case .lastFacility:
            if previousLogEntry?.facility == nil {
                return nil
            }else{
                return "Use Last Logged dive Operation"
            }
            
        case .nearbyFacilities: return "Or Choose From"
            
        case .addNewFacility: return "if All Else Fails..."
        default:
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        cell.selectionStyle = .default
        cell.isUserInteractionEnabled = true
        let rowName = optionOrderFor(position: indexPath.section)
        switch rowName {
        case .currentFacility:
            cell.textLabel?.text = logEntry.facilityName
            cell.accessoryType = .checkmark
        case .lastFacility:
            cell.accessoryType = .none
            cell.textLabel?.text = previousLogEntry?.facility?.name
            
        case.nearestFacility:
            cell.accessoryType = .none
            if let nearest = nearestFacility {
                cell.textLabel?.text = nearest.name
                cell.isUserInteractionEnabled = true
            } else {
                cell.textLabel?.text = "waiting on GPS"
                cell.selectionStyle =   .none
                cell.isUserInteractionEnabled = false
            }
        default:
            cell.textLabel?.text = sectionNames[indexPath.section]
        }
        
        
        return cell
    }
    //MARK: - TableView Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName {
        case .currentFacility:
            logEntry.facilityName = nil
            logEntry.facility = nil
            self.tableView.reloadSections(IndexSet(indexPath), with: .automatic)

        case .manualSearch:
            performSegue(withIdentifier: Identifier.manualSearch.rawValue, sender: self)
        case .regularFacilities:
            performSegue(withIdentifier: Identifier.regularFacilities.rawValue, sender: self)
        case.nearbyFacilities:
            performSegue(withIdentifier: Identifier.nearbyFacilities.rawValue, sender: self)
        case .lastFacility:
            logEntry.facility = previousLogEntry?.facility
            logEntry.facilityName = previousLogEntry?.facilityName
            performSegue(withIdentifier: Identifier.lastFacility.rawValue , sender: self) //unwind on choosing
        case .nearestFacility:
            logEntry.facility = nearestFacility
            logEntry.facilityName = nearestFacility?.name
            performSegue(withIdentifier: Identifier.nearestFacility.rawValue, sender: self)
            
        case .addNewFacility:
            performSegue(withIdentifier: Identifier.addNewFacility.rawValue, sender: self)
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let segueID = segueIdentifierFor(segue: segue)  
        switch segueID {
        case .currentFacility: break
        case .nearbyFacilities :
            guard let vc = segue.actualDestination() as? NearbyFacilityChooser else { fatalError("expected a NearbyFacilityChooser - didn't get one")}
            vc.inject(dataModel)
            
        case .regularFacilities:
            guard let vc = segue.actualDestination() as? RegularFacilitiesChooser else { fatalError("expected a RegularFacilityChooser - didn't get one")}
            vc.inject(dataModel)
            
        case .manualSearch:
            guard let vc = segue.actualDestination() as? ChooseFacilityCountryTVC else { fatalError("expected a ChooseFacilityCountry scene - didnt get one")}
            vc.inject(dataModel)
            
        case .nearestFacility: break
        case .lastFacility: break
            
        case .addNewFacility:
            guard let vc = segue.actualDestination() as? FacilityDetailsEditor else { fatalError("expected a FacilityDetailsEditor scene - didnt get one ")}
            let _ = dataModel.newFacility() //not added to logEntry TBD if new facility is saved.
            vc.inject((dm:dataModel, mode: .addedWhileLogging))
            
            
        }
        
    }
    /// unwind segue - deletes the facility that was added to the conext
    @IBAction func addingNewFacilityWasCancelled(segue:UIStoryboardSegue){
        // an unwind segue
        dataModel.container.viewContext.delete(dataModel.facility!)
    }
}

extension FacilityOptions : LocationUser{
    
    func locationWasDenied(){
        bulletinManager.showBulletin(above: self)
    }
    func partialFix(location: CLLocation) {
    }
    
    
    func locationDidUpdate(location: CLLocation) {
        print("in did update location")
        let coordinateRange = CoordinateRange(withCenter: location, offset: 0.5)
        let searchPredicates = coordinateRange.searchPredicates()
        
        let fetchRequest: NSFetchRequest<Facility> = Facility.fetchRequest()
        fetchRequest.predicate = searchPredicates
        
        do {
            var facilities = try dataModel.container.viewContext.fetch(fetchRequest)
            print("facilities.count = \(facilities.count)")
            switch facilities.count{
            case 0: break
            case 1 : nearestFacility = facilities[0]
                
            default:
                sortFetched(nearbyFacilities: &facilities, byDistanceFrom: location)
                nearestFacility = facilities.first
            }
            tableView.reloadRows(at: [indexPathForNearest], with: .none)
        } catch {
            let fetchError = error as NSError
            print("Unable to Execute Fetch Request")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
    }
    
}
extension FacilityOptions: Injectable {
    func inject(_ dm : DataModel) {
        dataModel = dm
    }
    
    func assertDependencies() {
        assert( dataModel != nil , "Didnt get a dataModel passed in")
    }
    
    typealias T = DataModel
    
    
}
extension FacilityOptions : SegueHandlerType, OptionOrder {
    enum Identifier : String {
        case currentFacility
        case lastFacility
        case nearestFacility
        case nearbyFacilities
        case regularFacilities
        case manualSearch
        case addNewFacility
    }
    
    enum OptionOrder : Int {
        case currentFacility
        case lastFacility
        case nearestFacility
        case nearbyFacilities
        case regularFacilities
        case manualSearch
        case addNewFacility
        
    }
}
