//
//  NearbyFacilityChooser.swift
//  Splash
//
//  Created by Simon Gardener on 05/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class NearbyFacilityChooser: FacilitiesByNearbyVC {
    let unwindIdentifier = "fromNearbyFacility"
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedFacility = facilities[indexPath.row]
        dataModel.logEntry?.facility = selectedFacility
        dataModel.logEntry?.facilityName = selectedFacility.name
        performSegue(withIdentifier: unwindIdentifier, sender: self)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}
