//
//  FacilityFromCountryChooser.swift
//  Splash
//
//  Created by Simon Gardener on 06/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class FacilityFromCountryChooserTVC: FacilitiesByCountryTVC {


    fileprivate let unwindSegueIdentifier =  "unwindFromManualSearch"
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
    }
   
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("HELLO THERE")
        let facility = frc.object(at: indexPath)
        dataModel.logEntry?.facility = facility
        dataModel.logEntry?.facilityName = facility.name
        performSegue(withIdentifier: unwindSegueIdentifier, sender: self)
    }
}
