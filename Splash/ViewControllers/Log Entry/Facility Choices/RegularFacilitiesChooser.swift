//
//  RegularFacilitiesChooser.swift
//  Splash
//
//  Created by Simon Gardener on 05/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class RegularFacilitiesChooser: FacilitiesByRegularTVC {

let unwindSegue = "fromRegularFacility"
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedFacility = regularFacilities[indexPath.row]
        dataModel.logEntry?.facility = selectedFacility
        dataModel.logEntry?.facilityName = selectedFacility.name
        
        performSegue(withIdentifier: unwindSegue, sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }

}
