//
//  ChooseFacilityCountryTVC.swift
//  Splash
//
//  Created by Simon Gardener on 06/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class ChooseFacilityCountryTVC: UITableViewController {

    fileprivate let segueIdentifier = "chooseFacilityFromCountry"
    var dataModel : DataModel!
    let CountryCellID = "countryCell"
    let showByCountrySegue = "showSitesByCountry"
    lazy var frc : NSFetchedResultsController<Facility>  = dataModel.fetchedResultsControllerForFacilityWithSectionNameKeyPathCountry()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        fetchDiveSites()
    }

    private func fetchDiveSites() {
        do{
            try frc.performFetch()
        }catch {
            print("Unable to Perform Fetch Request")
            print("\(error), \(error.localizedDescription)")
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return (frc.sections?.count)!
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CountryCellID, for: indexPath)
        let secInfo = frc.sections?[indexPath.row]
        cell.textLabel?.text = secInfo?.name
        cell.detailTextLabel?.text = String("\((secInfo?.numberOfObjects)!) dive facilities")
        return cell
    }
  
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard segue.identifier == "chooseFacilityFromCountry", let vc = segue.actualDestination() as? FacilitiesByCountryTVC else { fatalError("didnt get a FacilitiesByCountry scene")}
        let indexPath = tableView.indexPathForSelectedRow!
        let secInfo = frc.sections?[indexPath.row]
        
        let countryName = secInfo?.name
        vc.countryName = countryName
        vc.title = countryName
        vc.dataModel = dataModel!
    }
 

}
extension ChooseFacilityCountryTVC : Injectable{
    func inject(_ dm: DataModel) {
        dataModel = dm
    }
    
    func assertDependencies() {
        assert(dataModel != nil, "Expected a DataModel object - didnt get one")
    }
    
    typealias T = DataModel
    
    
    
}
