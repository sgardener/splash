//
//  SignatureViewController.swift
//  Splash
//
//  Created by Simon Gardener on 08/02/2019.
//  Copyright © 2019 Simon Gardener. All rights reserved.
//

import UIKit
import SignaturePad
class SignatureViewController: UIViewController{
    
    @IBOutlet weak var signaturePad: SignaturePad!
    var dataModel : DataModel!
    var logEntry: LogEntry!
    var previousSignature : Data?
    @IBOutlet weak var cancel: UIButton!
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var clear: UIButton!
    
    @IBOutlet weak var signThisWay: UILabel!
    
    fileprivate func rotateViews() {
        rotateView(signThisWay)
        rotateView(cancel)
        rotateView(save)
        rotateView(clear)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        previousSignature = logEntry.signature?.image
        rotateViews()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func savePressed(_ sender: Any) {
        if let signature = signaturePad.getSignature() {
            let rotatedSig = signature.rotate(radians: .pi / 2.0)
            if let sigData = rotatedSig?.jpegData(compressionQuality: 0.1){
                saveSignature(sigData: sigData)
            }
        }
        self.navigationController?.popViewController(animated: true)
    }
    func saveSignature(sigData: Data){
        if let signature = logEntry.signature {
            signature.image = sigData
        }else {
            let newSignature = dataModel.newSignature()
            newSignature.image = sigData
            logEntry.signature = newSignature
        }
    }
   
    @IBAction func cancelPressed(_ sender: Any) {
        logEntry.signature?.image = previousSignature
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func clearPressed(_ sender: Any) {
        logEntry.signature = nil
        signaturePad.clear()
    }

    fileprivate func rotateView(_ view : UIView)
    {
        let orig = view.frame
        view.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 1.5))
        view.frame = orig
    }
}
extension SignatureViewController: Injectable{
    func inject(_ dm: DataModel) {
        dataModel = dm
        logEntry = dataModel.logEntry
        
    }
    func assertDependencies() {
        assert(dataModel != nil)
        assert(logEntry != nil, "logentry is nil")
        
    }
    
    typealias T = DataModel
    
    
}
