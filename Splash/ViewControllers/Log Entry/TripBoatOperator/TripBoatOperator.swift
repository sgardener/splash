//
//  TripBoatOperator.swift
//  Splash
//
//  Created by Simon Gardener on 15/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class TripBoatOperator: UITableViewController ,SegueHandlerType, OptionOrder {
    
    enum Identifier: String {
        case trip, boat, diveOp
    }
    enum OptionOrder: Int {
        case trip, boat, diveOp
    }
    var sectionToReload :IndexPath!
    var dataModel: DataModel!
    var logEntry: LogEntry!
    let cellID = "cell"
    let numberOfSections = 3
    let headerTitles = ["Trip","Boat","Operator"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Trip Details"
        tableView.tableFooterView = UIView()

        assertDependencies()
        logEntry = dataModel.logEntry

    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !isMovingToParent {
            tableView.reloadRows(at: [sectionToReload], with: .automatic)
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        let order = optionOrderFor(position: indexPath.section)
        switch order {
        case .trip:
            cell.textLabel?.text = logEntry.trip?.name ?? "choose trip"
            cell.textLabel?.textColor = textColour(for: logEntry.trip?.name)
            
        case.boat:
            cell.textLabel?.text = logEntry.divedFromBoat?.name ?? "choose boat"
            cell.textLabel?.textColor = textColour(for: logEntry.divedFromBoat?.name)
        case.diveOp:
            cell.textLabel?.text = logEntry.facility?.name ?? "choose dive operation"
            cell.textLabel?.textColor = textColour(for: logEntry.facility?.name)
        }

        return cell
    }
    fileprivate func textColour(for string: String?)-> UIColor {
        if let string = string , string.isEmpty == false {
            return .black
        }else {
            return .lightGray
        }
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return headerTitles[section]
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let position = optionOrderFor(position: indexPath.section)
        sectionToReload = indexPath
        switch position {
        case .trip:
            performSegue(withIdentifier: Identifier.trip.rawValue , sender: self)
        case .boat:
            performSegue(withIdentifier: Identifier.boat.rawValue, sender: self)
        case .diveOp:
            performSegue(withIdentifier: Identifier.diveOp.rawValue, sender: self)
            break
            
        }
    }
    //MARK:- Navigation section
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let segueId = segueIdentifierFor(segue: segue)
        
        switch segueId {
        case .trip:
            guard let vc = segue.actualDestination() as? TripChooser else { fatalError("Expected a Trip Chooser scene . didnt get one.")}
            vc.inject(dataModel)
            
        case .boat:
            guard let vc =  segue.actualDestination() as? BoatChooser else { fatalError("Expected a Boat Chooser scene . didn't get one.")}
        vc.inject(dataModel)
            
        case.diveOp:
            guard let vc = segue.actualDestination() as? FacilityOptions else { fatalError("Expected a Facility Options scene . didnt get one.")}
            vc.inject(dataModel)
            
        }
    }
    //x
    
    @IBAction func tripChoosen(segue:UIStoryboardSegue){}
    @IBAction func boatChoosen(segue:UIStoryboardSegue){}
    @IBAction func facilityChoosenForLogEntry(segue:UIStoryboardSegue){}
    
}

extension TripBoatOperator : Injectable {
    func inject(_ dm: DataModel) {
       dataModel = dm
    }
    
    func assertDependencies() {
        assert(dataModel != nil, "didnt get a datamodel object")
    }
    
    typealias T = DataModel
    
    
    
    
    
}
