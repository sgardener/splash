//
//  SupervisedChooser.swift
//  Splash
//
//  Created by Simon Gardener on 28/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class SupervisedChooser: DiversListsBaseVC {

    override func viewDidLoad() {
        frc = viewModel.frcForGuidedDivers()
        super.viewDidLoad()

    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let diver = frc.object(at: indexPath)
        guard let cell = tableView.cellForRow(at: indexPath) else { fatalError()}
        if cell.accessoryType == .checkmark {
            cell.accessoryType = .none
            viewModel.removeSupervised(diver)
        }else if cell.accessoryType == .none {
            cell.accessoryType = .checkmark
            viewModel.addSupervised(diver)
        }
    }
    override  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as? DiverChoiceCell else { fatalError("needed a DiverChoice cell")}
        let diver = frc.object(at: indexPath)
        cell.configure(with: diver, isLogged: viewModel.logEntryGuidedDiversContains(diver))
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    override func prepareForAddDiver(_ vc: DiverDetails) {
        viewModel.addNewDiver()
        viewModel.setGuidedStatus()
        viewModel.mode = .add
        vc.inject(viewModel)
    }
    override func addNewDiverWithDetailsFrom(nameElements:(first:String, middle :[String], last:String)?) {
        super .addNewDiverWithDetailsFrom(nameElements: nameElements)
        viewModel.setGuidedStatus()
        
    }
}
