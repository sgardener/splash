//
//  BVC.swift
//  Splash
//
//  Created by Simon Gardener on 17/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class BuddyChooser: BuddiesList {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let buddy = frc.object(at: indexPath)
        guard let cell = tableView.cellForRow(at: indexPath) else { fatalError()}
        if cell.accessoryType == .checkmark {
            cell.accessoryType = .none
            viewModel.removeBuddy(buddy)
        }else if cell.accessoryType == .none {
            cell.accessoryType = .checkmark
            viewModel.addBuddy(buddy)
        }
    }
    
    override  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as? DiverChoiceCell else { fatalError("needed a DiverSummaryCell")}
        let diver = frc.object(at: indexPath)
        cell.configure(with: diver, isLogged: viewModel.logEntryBuddiesContain(diver))
        return cell
    }
    //MARK:- TableView Delegate
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }

}
