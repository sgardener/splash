//
//  DiveLogList.swift
//  Splash
//
//  Created by Simon Gardener on 11/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class DiveLogList: UIViewController,SegueHandlerType, NeedsDataModel {
    var dataModel: DataModel!
    
    var traitSize : UIContentSizeCategory!
    
    enum Identifier: String {
        case addDive
        case showDive
    }
    private let cellId = "DiveSummaryCell"
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageLabel: UILabel!
    
    private var context: NSManagedObjectContext!
    var container: NSPersistentContainer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        traitSize = traitCollection.preferredContentSizeCategory
        container = dataModel.container
        context = container.viewContext
        registerForNotifications()
        setUpView()
        fetchDives()
        setUpNavBar()   
        tableView.tableFooterView = UIView()
        updateView()
    }
    func setUpNavBar(){
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserDefaults.shouldTriggerDive() == true {
            UserDefaults.setTriggerDive(bool: false)
            performSegue(withIdentifier: Identifier.addDive.rawValue, sender: self)
        }
        if traitSize != traitCollection.preferredContentSizeCategory {
            tableView.reloadData()
        }
        traitSize = traitCollection.preferredContentSizeCategory
    }
    private func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(userUpdatedMeasurementPreferences), name: Notification.Name.init("depthUnitsChanged"), object: nil)
    }
    private func setUpView(){
        setUpMessageLabel()
        setUpTableView()
    }
    fileprivate func updateView() {
        tableView.isHidden = !hasDives
        messageLabel.isHidden = hasDives
    }
    private func setUpMessageLabel(){
        messageLabel.text = """
You have no logged dives.

See that little '+' button up there...
"""
    }
    private func setUpTableView() {
        tableView.estimatedRowHeight = 70
    }
    private var hasDives: Bool  {
        guard let fetchedTrips = frc.fetchedObjects else { return false }
        return fetchedTrips.count > 0
    }
    
    private func fetchDives(){
        do{
            try frc.performFetch()
        }catch{
            print("\(error), \(error.localizedDescription)")
        }
    }
    @objc fileprivate func userUpdatedMeasurementPreferences(){
        tableView.reloadData()
    }
    
    fileprivate lazy var frc : NSFetchedResultsController<LogEntry> = {
        let tripRequest : NSFetchRequest<LogEntry>  = LogEntry.fetchRequest()
        tripRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(LogEntry.timeIn), ascending: false)]
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: tripRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = self
        return fetchedResultsController
    }()
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let segueId = segueIdentifierFor(segue: segue)
        guard  let vc = segue.actualDestination() as? LogPage else { fatalError("should have got a DiveLogEntry tvc") }
    
        
        switch segueId{
        case .addDive:
            
            let _ = dataModel.newLogEntry()
   
            vc.inject((dataModel: dataModel , title: "Add Dive", mode: .add))
            
            
        case .showDive:
            dataModel.logEntry = frc.object(at: tableView.indexPathForSelectedRow!)
            vc.inject((dataModel: dataModel, title:"Dive Details", mode: .show))
         
            tableView.deselectRow(at: tableView.indexPathForSelectedRow!, animated: true)
            
        }
        
    }
}

extension DiveLogList : NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
            updateView()
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
            updateView()
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .automatic)
        @unknown default:
            print("Futureproof: NSFetchedResultscontroller didChange - and has added a new enum type")
        }
    }
}

extension DiveLogList : UITableViewDelegate {

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete", handler: {action,indexPath in
            let logToDelete = self.frc.object(at: indexPath)
            let alertController = UIAlertController(title: "Delete this Log?", message: "If you delete this log its gone permanently.", preferredStyle: .actionSheet )
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {action in
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
            })
            let goAheadAction = UIAlertAction(title: "Delete", style: .destructive, handler: {(action) in
                let count = self.frc.fetchedObjects?.count
                self.dataModel.container.viewContext.delete(logToDelete)
                self.dataModel.saveContext()
                if count == 1 {
                    self.navigationController?.popViewController(animated: true)
                }
            })
            alertController.addAction(goAheadAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        })
        return [deleteAction]
    }
}

extension DiveLogList : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = frc.sections?[section] else { return 0 }
        return section.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? DiveSummaryCell else {fatalError("should have been a Dive SummaryCell")}
        let logEntry = frc.object(at: indexPath)
        cell.configureWith(logEntry)
        return cell
    }
}
extension DiveLogList : Injectable {
    func inject(_ dm: DataModel) {
        dataModel = dm
    }
    
    func assertDependencies() {
        assert(dataModel != nil, "Bugger, its nil")
    }
    
}

