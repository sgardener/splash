//
//  ConditonsChooser.swift
//  Splash
//
//  Created by Simon Gardener on 13/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class ConditonsChooser: UITableViewController ,OptionOrder {
    
    enum OptionOrder : Int {
        case water, air, season, weather, visability, current, salinity, surface
    }
    let keys = ["waterTemperature","airTemperature", "season","weatherConditions","visibilityRelative", "currentStrength", "salinity", "surfaceConditions"]

    var attributeLabels = ConditionData.conditionLabels
    let cellId = "cell"
    let picker = ConditionsPicker()
    let temperaturePicker = TemperaturePicker()
    var toolBar : UIToolbar!
    var logEntry : LogEntry!
    
    fileprivate func preparePicker() {
        picker.sizeToFit()
        picker.delegate = picker
        picker.dataSource = picker
    }
    fileprivate func prepareTemperaturePicker() {
    temperaturePicker.sizeToFit()
        temperaturePicker.delegate = temperaturePicker
        temperaturePicker.dataSource = temperaturePicker
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        title = "Conditions"
        toolBar = toolbarForPicker(doneMethod: #selector(doneButtonPressed))
        preparePicker()
        prepareTemperaturePicker()
    }
    
    fileprivate func toolbarForPicker(doneMethod: Selector)-> UIToolbar  {
        let pickerToolbar = UIToolbar()
        pickerToolbar.sizeToFit()
        let removeButton = UIBarButtonItem(title: "Remove value", style: .plain, target: self, action: #selector(removeButtonPressed))
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: doneMethod)
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        pickerToolbar.setItems([removeButton, flexible,doneButton], animated: false)
        return pickerToolbar
    }
    @objc private func removeButtonPressed() {
        let indexPath = tableView.indexPathForSelectedRow!
        logEntry.setValue(nil, forKey: keys[indexPath.row])
        let cell = tableView.cellForRow(at: indexPath ) as? LogAttributeDataCell

        cell?.hiddenTextField.resignFirstResponder()
        tableView.reloadRows(at: [indexPath], with: .automatic)
        
    }
    @objc private func doneButtonPressed() {
        let indexPath = tableView.indexPathForSelectedRow!
        let cell = tableView.cellForRow(at: indexPath ) as? LogAttributeDataCell
        cell?.hiddenTextField.resignFirstResponder()
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return attributeLabels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? LogAttributeDataCell else { fatalError("got wrong cell type")}
       
        cell.attributeLabel.text = attributeLabels[indexPath.row] + ":"
        cell.hiddenTextField.inputView =  picker
        cell.hiddenTextField.inputAccessoryView = toolBar
     
        let cellType = optionOrderFor(position: indexPath.row)

        switch cellType {
            
        case .air :
            cell.hiddenTextField.inputView = temperaturePicker
            cell.dataLabel.text = Temperature.stringFor(temperature: logEntry.airTemperature )
        case .current :
            cell.dataLabel.text = ConditionData.current[Int(logEntry.currentStrength)]
        case .salinity :
            cell.dataLabel.text = ConditionData.salinity[Int(logEntry.salinity)]
        case .season :
            cell.dataLabel.text = ConditionData.season[Int(logEntry.season)]
        case .surface :
            cell.dataLabel.text = ConditionData.surface[Int(logEntry.surfaceConditions)]
        case .visability:
            cell.dataLabel.text = ConditionData.visibility[Int(logEntry.visibilityRelative)]
        case .water :
            cell.hiddenTextField.inputView = temperaturePicker
            cell.dataLabel.text = Temperature.stringFor(temperature: logEntry.waterTemperature )
        case .weather :
            cell.dataLabel.text = ConditionData.weather[Int(logEntry.weatherConditions)]
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView,didSelectRowAt indexPath: IndexPath) {
        
        guard  let cell = tableView.cellForRow(at: indexPath) as? LogAttributeDataCell else { fatalError("not a logAttributeData cell")}
  

        let cellType = optionOrderFor(position: indexPath.row)
        switch cellType {
            
        case .air :
            temperaturePicker.setUpWithCell(cell, key: "airTemperature", logEntry: logEntry)
            break
        case .water :
            temperaturePicker.setUpWithCell(cell, key: "waterTemperature", logEntry: logEntry)
            break
        case .current :
            picker.setUpWithCell(cell, key: "currentStrength", data: ConditionData.current ,logEntry: logEntry)
           
        case .salinity :
            picker.setUpWithCell(cell, key: "salinity", data: ConditionData.salinity, logEntry: logEntry)
        
        case .season :
            picker.setUpWithCell(cell, key: "season", data: ConditionData.season, logEntry: logEntry)
            
        case .surface :
            picker.setUpWithCell(cell, key: "surfaceConditions", data: ConditionData.surface, logEntry: logEntry)

        case .visability:
            picker.setUpWithCell(cell, key: "visibilityRelative", data: ConditionData.visibility, logEntry: logEntry)
       
        case .weather :
            picker.setUpWithCell(cell, key: "weatherConditions", data: ConditionData.weather, logEntry: logEntry)
        }
        
        picker.reloadComponent(0)
        
        cell.hiddenTextField.becomeFirstResponder()
        
    }
    private func changePickerToTemperaturePicker(){
        
    }
    
}
extension ConditonsChooser  : Injectable {
    func inject(_ log: LogEntry) {
        logEntry = log
    }
    func assertDependencies() {
        assert(logEntry != nil, "didnt get a logEntry passed in")
    }
    typealias T = LogEntry
}
