//
//  ConditionsPicker.swift
//  Splash
//
//  Created by Simon Gardener on 13/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class ConditionsPicker: UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate{
   
    var cell: LogAttributeDataCell!
    var key : String!
    var displayChoice : [String]!
    var logEntry: LogEntry!
 
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return displayChoice.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return  displayChoice[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
    
    return 260.0
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        cell.dataLabel.text = displayChoice[row]
        logEntry.setValue(row, forKey: key )
       
    }

    
    func setUpWithCell(_ cell :LogAttributeDataCell, key: String, data: [String],logEntry:LogEntry) {
    
        self.cell = cell
        self.key = key
        displayChoice = data
        self.logEntry = logEntry
        
    }
}
