//
//  TemperaturePicker.swift
//  Splash
//
//  Created by Simon Gardener on 13/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class TemperaturePicker: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    let hundredsComponent = 0
    let tensComponent = 1
    let singlesComponent = 2
    let tempSystemComponent = 3
    
    let metric = 0
    let imperial = 1
    
    let metricHundredsRowCount = 2
    let imperialHundredsRowCount = 3
    let metricTensRowCount = 5
    let allTenDigits = 10
    let temperatureSystemsCount = 2
    
    let firstComponent = ["-","0","+1"]
    let digitComponents = ["0","1","2","3","4","5","6","7","8","9"]
    let temperatureComponent = ["°C","°F"]
    
    var cell: LogAttributeDataCell!
    var key : String!
    var logEntry: LogEntry!
    var temperature : Int16?
    var temperatureUnits = UserDefaults.temperatureUnits()
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 4
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component  {
        case hundredsComponent :
            if  temperatureUnits == .metric {
                return metricHundredsRowCount
            }else {
                return imperialHundredsRowCount
            }
        case tensComponent:
            if  temperatureUnits == .metric {
                return metricTensRowCount
            }else {
                return allTenDigits
            }
        case singlesComponent:
            return  allTenDigits
        case tempSystemComponent:
            return temperatureSystemsCount
        default: return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case hundredsComponent:
            return firstComponent[row]
        case tensComponent,singlesComponent:
            return digitComponents[row]
        case tempSystemComponent:
            return  temperatureComponent[row]
        default:
            return "x"
            
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return 35
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch component {
        case tempSystemComponent:
            if (row == metric &&  temperatureUnits == .metric) || (row == imperial && temperatureUnits == .imperial) {
              //  print("No Change")
            } else {
                if row == metric {
                    temperatureUnits = .metric
                }else {
                    temperatureUnits = .imperial
                }
            }
        default:
            break
        }
      updateUnderlyingTemperatureOnPicker()
    }
    
    func updateUnderlyingTemperatureOnPicker() {
        logEntry.setValue(temperatureOnPicker(), forKey: key)
        cell.dataLabel.text = Temperature.stringFor(temperature: (logEntry.value(forKey: key) as? NSNumber) )
    }
    
    func setUpWithCell(_ cell: LogAttributeDataCell,  key: String, logEntry: LogEntry){
        self.cell = cell
        self.key = key
        self.logEntry = logEntry
        temperature = (logEntry.value(forKey: key) as! Int16?)
        setPickerTo(temperature)
    }
    
    func setPickerTo(_ temperature: Int16?){
        if let setToTemp = temperature{
            var setToTemp = Int(setToTemp)
            selectRow(rowForUnitsComponent(), inComponent: tempSystemComponent, animated: true)
            if temperatureUnits == .imperial{
                setToTemp = setToTemp.celciusToFahrenheit
            }
            let rowValues = rowValuesFor(temperature:setToTemp)
            selectRow(rowValues.hundredsRow, inComponent: hundredsComponent, animated: true)
            selectRow(rowValues.tensRow , inComponent: tensComponent, animated: true)
            selectRow(rowValues.singlesRow, inComponent: singlesComponent, animated: true)
        }
        else {
            selectRow(1 , inComponent: hundredsComponent, animated: true)
            selectRow(0, inComponent: tensComponent, animated: true)
            selectRow(0, inComponent: singlesComponent, animated: true)
        }
    }
    
    private func rowValuesFor(temperature temp: Int)-> (hundredsRow: Int, tensRow: Int, singlesRow: Int) {
        var number = temp
        var hundreds = 1
        if number > 99 {
            hundreds = 2
            number = number - 100
        }else if number < 0 {
            hundreds = 0
            number = number * -1
        }
        let tens = number / 10
        let singles = number - ( tens*10)
        return (hundreds, tens, singles)
    }
    
    private func rowForUnitsComponent ()-> Int{
        if temperatureUnits == .metric{
            return metric
        }else {
            return imperial
        }
    }
    
    // returns a centigrade value for temperature as Int16 so can be saved to logEntry
    func temperatureOnPicker()-> Int16 {
        var newTemperature :Int = 0
        newTemperature = newTemperature + selectedRow(inComponent: singlesComponent) + 10 * selectedRow(inComponent: tensComponent)
        switch selectedRow(inComponent: hundredsComponent){
        case 0:
            newTemperature = newTemperature * -1
        case 2:
            newTemperature += 100
        default: break
        }
        if temperatureUnits == .imperial {
            newTemperature = newTemperature.fahrenheitToCelcius
        }
        return Int16(newTemperature)
    }
    
}

extension Int {
    var fahrenheitToCelcius :Int {
       return  Int(round(( Double(self) - 32.0) * 5.0 / 9.0))
    }
    var celciusToFahrenheit : Int {
        return Int(round( Double(self) * 1.8 + 32.0))
    }
}

extension Int16 {
    var celciusToFahrenheit : Int16 {
        return  Int16(round(( Double(self) - 32.0) * 5.0 / 9.0))
    }
    var fahrenheitToCelcius : Int16  {
        return  Int16(round(( Double(self) - 32.0) * 5.0 / 9.0))
    }
}
//
//-(int) convertCelsiusToFahrenheit:(int)temperature{
//    return round(temperature*1.8 +32) ;
//
//}
//-(int) convertFahrenheitToCelcius:(int) temperature{
//    return round((temperature-32)*5/9);
//
