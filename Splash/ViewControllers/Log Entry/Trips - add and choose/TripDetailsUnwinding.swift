//
//  TripDetailsUnwinding.swift
//  Splash
//
//  Created by Simon Gardener on 07/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class TripDetailsUnwinding: TripDetails {

    private let unwindSegue = "unwindFromNewTrip"
    
    override func setUpMode(){
        guard mode == .add else { return }
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel , target: self, action: #selector(cancelButtonPressedOverride))
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonPressedOverride))
        navigationItem.leftBarButtonItem = cancelButton
        navigationItem.rightBarButtonItem = doneButton
    }
    
    @objc private func cancelButtonPressedOverride(){
        if let context = trip.managedObjectContext{
            context.delete(trip)
            presentingViewController?.dismiss(animated: true, completion: nil)
        }
        
    }
    
    @objc private func doneButtonPressedOverride(){
        
        resignTextField()
        if trip.name == nil {
            showAlert(withTitle: "Missing Name", message: "A trip must have a name")
            if let cell =  tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? TextFieldTableViewCell {
                cell.textField?.becomeFirstResponder()
            }
        } else {
            dataModel.logEntry?.trip = trip
            performSegue(withIdentifier: unwindSegue, sender: self)
        }
    }
}
