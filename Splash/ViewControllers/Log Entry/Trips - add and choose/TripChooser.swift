//
//  TripChooser.swift
//  Splash
//
//  Created by Simon Gardener on 15/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData
class TripChooser: UIViewController, SegueHandlerType {
    let unwindSegue = "tripChosen"
    enum Identifier: String {
        case addTrip
        case tripChosen
       // case showTrip
    }
    var logEntry : LogEntry!
    var dataModel : DataModel!
    var context : NSManagedObjectContext!
    
    @IBOutlet var tripView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logEntry = dataModel.logEntry
        context = dataModel.container.viewContext
        title = "Choose Trip"
        setUpView()
        fetchTrips()
        tableView.tableFooterView = UIView()
        updateView()

    }
    
    private func setUpView(){
        setUpMessageLabel()
        setUpTableView()
    }
    fileprivate func updateView() {
        tableView.isHidden = !hasTrips
        messageLabel.isHidden = hasTrips
    }
    private func setUpMessageLabel(){
        messageLabel.text = "Click the '+' button up top to add a trip."
    }
    private func setUpTableView() {
        
    }
    private var hasTrips: Bool  {
        guard let fetchedTrips = frc.fetchedObjects else {
            return false }
        return fetchedTrips.count > 0
    }
    
    private func fetchTrips(){
        do{
            try frc.performFetch()
        }catch{
            print("\(error), \(error.localizedDescription)")
        }
    }
    
    fileprivate lazy var frc : NSFetchedResultsController<Trip> = {
        let tripRequest : NSFetchRequest<Trip>  = Trip.fetchRequest()
        tripRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(Trip.name), ascending: true )]
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: tripRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = self
        return fetchedResultsController
    }()
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
      
        let segueId = segueIdentifierFor(segue: segue)
        
        switch segueId {
        case .addTrip:
              guard let vc = segue.actualDestination() as? TripDetails else {fatalError("Wrong type of  viewcontroller - should be TripsDetail")}
            let trip = Trip(context: context)
            vc.inject((dataModel: dataModel, trip:trip, title: "Add Trip", mode: .add))
        case .tripChosen: break
            
//        case .showTrip :
//              guard let vc = segue.actualDestination() as? TripDetails else {fatalError("Wrong type of  viewcontroller - should be TripsDetail")}
  //          let trip = frc.object(at: tableView.indexPathForSelectedRow!)
    //        vc.inject((trip: trip, title: "Show Trip", mode: .show))
        }
    }
}



extension TripChooser : NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
        updateView()
    }
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        case .update:
            if let indexPath = indexPath  {
               tableView.reloadRows(at: [indexPath], with: .automatic)
            }
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .fade)
            }
        @unknown default:
            print("Futureproof: NSFetchedResultscontroller didChange - and has added a new enum type")
        }
    }
    
}

extension TripChooser : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sections = frc.sections else {return 0}
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = frc.sections?[section] else { return 0 }
        return section.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        configure(cell, at: indexPath)
        return cell
    }
    
    func configure(_ cell: UITableViewCell, at indexPath: IndexPath) {
        let trip = frc.object(at: indexPath)
        if trip == logEntry.trip {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        cell.textLabel?.text = trip.name
    }
}
extension TripChooser : UITableViewDelegate {
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let trip = frc.object(at: indexPath)
        if (trip.dive?.count == 0 ) {
            return true
        } else {
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete", handler: {action,indexPath in
            let tripToDelete = self.frc.object(at: indexPath)
            self.context.delete(tripToDelete)
        })
        return [deleteAction]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if logEntry.trip == frc.object(at: indexPath){
            logEntry.trip = nil
        }else {
            logEntry.trip = frc.object(at: indexPath)
            performSegue(withIdentifier: unwindSegue, sender: self)
        }
        tableView.reloadRows(at: [indexPath], with: .automatic)
      //  performSegue(withIdentifier: unwindSegue, sender: self)
    }
}

extension TripChooser : Injectable{

    func inject(_ dm: DataModel) {
        dataModel = dm
    }
    
    func assertDependencies() {
        assert(dataModel  != nil, "didnt get a log entry passed into boat chooser ")
    }
    
    typealias T = DataModel
    
    
}
