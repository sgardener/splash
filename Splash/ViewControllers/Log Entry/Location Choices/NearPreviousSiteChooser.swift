//
//  NearPreviousSiteChooser.swift
//  Splash
//
//  Created by Simon Gardener on 03/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit


class NearPreviousSiteChooser: SitesByNearby {
    let unwindSegueId = "siteChoosen"
    var previousDiveHadLocation = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        turnOffUnneededLocationStuff()
        
        if let previousSite = dataModel.previousLogEntry(), let site = previousSite.diveSite, let location = site.location {
            previousDiveHadLocation = true
            locationDidUpdate(location: location)
        } else {
            if let previousSite = dataModel.previousLogEntry(), let site = previousSite.diveSite, let location = site.generalArea?.location {
                previousDiveHadLocation = true
                locationDidUpdate(location: location)
            }
        }
        updateView()
        title = "Choose Site"
    }
    
    func turnOffUnneededLocationStuff(){
        locationService.stop()
        locationService = nil
        activityIndicator.stopAnimating()
        activityIndicator.isHidden  = true
    }
    override func setUpMessageLabel() {
        messageLabel.text =  "The previous logged dive did not have a known location to search from."
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName{
        case .knownLocations:
            SiteAdder.add(knownDiveSites[indexPath.row], to: dataModel.logEntry!)
        case .unknownLocations:
            SiteAdder.add(unknownDiveSites[indexPath.row    ], to: dataModel.logEntry!)
        }
        dataModel.logEntry?.wasAddedAutomatically = false
        performSegue(withIdentifier: unwindSegueId, sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    
}
