//
//  SiteFromCountryChooser.swift
//  Splash
//
//  Created by Simon Gardener on 22/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class SiteFromCountryChooser: SitesByCountry {

    let unwindSegueId = "siteChoosen"


    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let site = frc.object(at: indexPath)
         SiteAdder.add(site, to: dataModel.logEntry!)
        dataModel.logEntry?.wasAddedAutomatically = false
        performSegue(withIdentifier: unwindSegueId, sender: self)
    }


}
