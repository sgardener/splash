//
//  RegularSiteChooser.swift
//  Splash
//
//  Created by Simon Gardener on 22/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class RegularSiteChooser: SitesRegularList {

    let unwindSegueId = "siteChoosen"

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let site = regularSites[indexPath.row]
        SiteAdder.add(site, to: dataModel.logEntry!)
        dataModel.logEntry?.wasAddedAutomatically = false
        performSegue(withIdentifier: unwindSegueId, sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}
