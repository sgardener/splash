//
//  NearbySiteChooser.swift
//  Splash
//
//  Created by Simon Gardener on 03/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class NearbySiteChooser: SitesByNearby {

 let unwindSegueId = "siteChoosen"
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName{
        case .knownLocations:
            SiteAdder.add(knownDiveSites[indexPath.row], to: dataModel.logEntry!)
        case .unknownLocations:
            SiteAdder.add(unknownDiveSites[indexPath.row    ], to: dataModel.logEntry!)
        }
        dataModel.logEntry?.wasAddedAutomatically = false
        performSegue(withIdentifier: unwindSegueId, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }


}
