//
//  FavoriteSitesChooser.swift
//  Splash
//
//  Created by Simon Gardener on 02/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class FavoriteSitesChooser: SitesFavouritesList {

    let unwindSegueId = "siteChoosen"
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let site = favouriteSites[indexPath.row]
        SiteAdder.add(site, to: dataModel.logEntry!)
        dataModel.logEntry?.wasAddedAutomatically = false
        performSegue(withIdentifier: unwindSegueId, sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }

}
