//  LocationOptions.swift
//  Splash
//
//  Created by Simon Gardener on 16/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.

import UIKit
import CoreLocation
import CoreData
import BLTNBoard

class LocationOptions: UITableViewController,LocationUser {
    
    func partialFix(location: CLLocation) {
    }
    func locationWasDenied(){
        bulletinManager.showBulletin(above: self)
    }
    
    var dataModel: DataModel!
    var locationService: LocationService!
    var isRequestingLocation = false
    var nearestSite : DiveSite? {
        didSet{
            tableView.reloadSections(nearestIndexSet
                , with: .automatic)

        }
    }
    let nearestIndexSet = IndexSet(arrayLiteral: 0)
    let nearestSiteIndexPath = IndexPath.init(item: 0, section: 0)
    var previousLogEntry: LogEntry?
    
    var optionStrings = ["waiting for location...","last site","nearby sites","near last site","favourite sites","regular sites","manual search","Add new site"]
    let numberOfSections =  8
    var logEntry: LogEntry!
    
    lazy var bulletinManager: BLTNItemManager = {
        var page = BulletinDataSource.locationWasDeniedPage()
        return BLTNItemManager(rootItem: page)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        previousLogEntry = dataModel.previousLogEntry()
        title = "Site Options"
        tableView.tableFooterView = UIView()

        locationService = LocationService(owner: self, desiredAccuracy: 50, within: 60)
        isRequestingLocation = true
        locationService.requestLocation()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionOrder = optionOrderFor(position: section)
        switch sectionOrder {
        case .nearestSite:
            if nearestSite == nil { return 0 }
            return 1
        case .addNewSite, .manualSearch, .nearbySites:
            return 1
        case .lastSite:
            if previousLogEntry?.diveSite?.name != nil{
                return 1
            } else {
                return 0
            }
        case .regularSites :
            if dataModel.numberOfSitesMarkedAsRegular() == 0{
                return 0
            }else {
                return 1
            }
        case .favouriteSites :
            if dataModel.numberOfSitesMarkedAsFavourite() == 0 {
                return 0
            }else {
                return 1
            }
        case .nearLastLoggedSite :
            if dataModel.previousLogEntry()?.diveSite != nil {
             return 1 }
            else {
                return 0
            }
        
            
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let sectionName = optionOrderFor(position: indexPath.section)
        setAccessoryOnCell(cell, inSection: sectionName)
        switch sectionName {
        case .nearestSite:
            
            cell.textLabel?.text = nearestSite != nil ? nearestSite?.name : "waiting for location"
            cell.selectionStyle = nearestSite != nil ? .default : .none
        case .lastSite:
            cell.textLabel?.text = dataModel.previousLogEntry()?.diveSiteName
        default:
            cell.textLabel?.text = optionStrings[indexPath.section]
        }
        
        return cell
    }
    func setAccessoryOnCell(_ cell: UITableViewCell, inSection section :LocationOptions.OptionOrder){
        switch section{
        case .nearestSite, .lastSite:
            cell.accessoryType = .none
        default:
            cell.accessoryType = .disclosureIndicator
            
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sectionName = optionOrderFor(position: section)
        switch sectionName {
        case .nearestSite: return  nearestSite == nil ? nil : "Use Nearest Site"
        case.lastSite:
            return previousLogEntry?.diveSite?.name != nil ? "Use Last Site" :  nil
        case .nearbySites : return "Or Choose From..."
        case .addNewSite : return "If All Else Fails..."
        default :
            return nil
        }
    }
    //MARK:- table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName {
        case .nearestSite:
            tableView.deselectRow(at:indexPath, animated: true)
            guard let nearestSite = nearestSite else {return}
            SiteAdder.add(nearestSite, to: logEntry)
            navigationController?.popViewController(animated: true)
        case .lastSite:
            if let site = dataModel.previousLogEntry()?.diveSite{
                SiteAdder.add(site, to: logEntry)
            navigationController?.popViewController(animated: true)
            }
            tableView.deselectRow(at: indexPath, animated: true)
            
        case .favouriteSites:
            performSegue(withIdentifier: Identifier.favouriteSites.rawValue, sender: self)
        case.regularSites:
            performSegue(withIdentifier: Identifier.regularSites.rawValue, sender: self)
        
        case .nearLastLoggedSite:
            performSegue(withIdentifier: Identifier.nearLastLoggedSite.rawValue, sender: self)
        case .nearbySites:
            performSegue(withIdentifier: Identifier.nearbySites.rawValue, sender: self)
        case .addNewSite:
                performSegue(withIdentifier: Identifier.addNewSite.rawValue,sender: self)
        case .manualSearch:
            performSegue(withIdentifier: Identifier.manualSearch.rawValue, sender: self)
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let segueID = segueIdentifierFor(segue: segue)
        switch segueID {
        case .regularSites :
            guard let  vc = segue.actualDestination() as? RegularSiteChooser else { fatalError("Didnt get a regularSiteChooser scene")}
            vc.inject(dataModel)
    
        case .favouriteSites:
            guard let vc = segue.actualDestination() as? FavoriteSitesChooser else { fatalError("expected a favouriteSitesChooser didnt get one")}
                vc.inject(dataModel)
    
        case.manualSearch:
            guard let vc = segue.actualDestination() as? ChooseSiteCountry else { fatalError("expected a ChooseSiteCountry scene - didnt get one ")}
            vc.inject(dataModel)
            
        case .nearbySites:
            guard let vc = segue.actualDestination() as? NearbySiteChooser else { fatalError("expected a NearbySiteChooser scene - didnt get one")}
            vc.inject(dataModel)
            
        case .nearLastLoggedSite:
            guard let vc = segue.actualDestination() as? NearPreviousSiteChooser else { fatalError("Expected a Near Previous Site Chooser but didnt get one")}
            vc.inject(dataModel)
            
        case .addNewSite:
            guard let vc = segue.actualDestination() as? SiteDetailedEditor else { fatalError()}
            
            let _ = dataModel.newDiveSite()
            vc.inject((dataModel, mode: .addedWhileLogging ))
        default :
            break
        }
    }
    @IBAction func didntAddNewSite(segue:UIStoryboardSegue){
        
    }
    
    
    // MARK: - location stuff
    
    func locationDidUpdate(location: CLLocation) {
        print("in did update location")
        let coordinateRange = CoordinateRange(withCenter: location, offset: 0.5)
        let searchPredicates = coordinateRange.searchPredicates()
        
        let fetchRequest: NSFetchRequest<DiveSite> = DiveSite.fetchRequest()
        fetchRequest.predicate = searchPredicates
        
        do {
            var diveSites = try dataModel.container.viewContext.fetch(fetchRequest)
            print("diveSites.count = \(diveSites.count)")
            if diveSites.count > 1 {
                sortFetched(nearbySites: &diveSites, byDistanceFrom: location)
            }
            guard diveSites.count > 0 else { return }
            nearestSite = diveSites[0]
        } catch {
            let fetchError = error as NSError
            print("Unable to Execute Fetch Request")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
    }

}
extension LocationOptions : Injectable {
    
    func inject(_ dm: DataModel) {
        dataModel = dm
        logEntry = dm.logEntry
    }
    func assertDependencies() {
        assert(dataModel != nil, "didnt get a dataModel passed in")
        assert( logEntry != nil, "dataModel didnt have a logEntry!")
       
    }
    typealias T = DataModel
}

extension LocationOptions : SegueHandlerType, OptionOrder{
    
    enum Identifier : String {
        case nearestSite
        case lastSite
        case nearbySites
        case nearLastLoggedSite
        case favouriteSites
        case regularSites
        case manualSearch
        case addNewSite
    }
    
    enum OptionOrder : Int {
        case nearestSite
        case lastSite
        case nearbySites
        case nearLastLoggedSite
        case favouriteSites
        case regularSites
        case manualSearch
        case addNewSite
    }
    
}
