//
//  ChooseSiteCountry.swift
//  Splash
//
//  Created by Simon Gardener on 22/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class ChooseSiteCountry: UITableViewController {
    
    var dataModel: DataModel!
    
    lazy var frc : NSFetchedResultsController<DiveSite>  = dataModel.fetchedResultsControllerForDiveSitesWithSectionNameKeyPathCountry()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = " Choose Country"
        assertDependencies()
        fetchDiveSites()
        
    }
    private func fetchDiveSites() {
        do{
            try frc.performFetch()
        }catch {
            print("Unable to Perform Fetch Request")
            print("\(error), \(error.localizedDescription)")
        }
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (frc.sections?.count)!
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID , for: indexPath)
        let countrySecInfo = frc.sections?[indexPath.row]
        cell.textLabel?.text = countrySecInfo?.name
        cell.detailTextLabel?.text = String("\((countrySecInfo?.numberOfObjects)!) dive sites")
        return cell
    }
    
  
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.actualDestination() as? SiteFromCountryChooser else { fatalError("didnt get a sitesByCountry scene")}
        let indexPath = tableView.indexPathForSelectedRow!
        let secInfo = frc.sections?[indexPath.row]
        let countryName = secInfo?.name
        vc.countryName = countryName
        vc.title = countryName
        vc.dataModel = dataModel!
    }
}

extension ChooseSiteCountry: Injectable {
    func inject(_ dm: DataModel) {
        dataModel = dm
    }
    
    func assertDependencies() {
        assert(dataModel != nil , "expected a dataModel - didnt get one ")
    }
    
    typealias T = DataModel
    
    
}
