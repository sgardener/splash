//
//  BoatDetailsUnwinding.swift
//  Splash
//
//  Created by Simon Gardener on 06/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class BoatDetailsUnwinding: BoatDetails {
    private let unwindSegue = "newBoatChosen"
    

    @objc private func doneAndUnwindButtonPressed(){
        if let cell =  tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? TextFieldTableViewCell {
            cell.textField.resignFirstResponder()
        }
        if boat.name == nil {
            showAlert(withTitle: "Missing Name", message: "A boat must have a name")
            if let cell =  tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? TextFieldTableViewCell {
                cell.textField.becomeFirstResponder()
            }
        } else {
            dataModel.logEntry?.divedFromBoat = boat
            performSegue(withIdentifier: unwindSegue, sender: self)
        }
    }
    @objc  func cancelButtonPressed2(){
        if let context = boat.managedObjectContext{
            context.delete(boat)
            presentingViewController?.dismiss(animated: true, completion: nil)
        }
        
    }
    override func setUpMode(){
        
        guard mode == .add else { return }
        
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel , target: self, action: #selector(cancelButtonPressed2))
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneAndUnwindButtonPressed ))
        navigationItem.leftBarButtonItem = cancelButton
        navigationItem.rightBarButtonItem = doneButton
        
        
        
    }
}
