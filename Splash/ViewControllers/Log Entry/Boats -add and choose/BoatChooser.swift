//
//  BoatChooser.swift
//  Splash
//
//  Created by Simon Gardener on 15/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//


import UIKit
import CoreData

class BoatChooser: UIViewController, SegueHandlerType {
    
    enum Identifier: String {
        //   case showBoatDetails
        case addBoat
        case unwindFromBoatChoosen
    }
    
    
    @IBOutlet var boatList: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
 
    var dataModel: DataModel!
    var logEntry: LogEntry!
    var context: NSManagedObjectContext!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        logEntry = dataModel.logEntry
        context = dataModel.container.viewContext
        title = "Choose Boat"
        setUpView()
        fetchBoats()
        tableView.tableFooterView = UIView()
        updateView()
        
    }
    
    private func setUpView(){
        setUpMessageLabel()
        setUpTableView()
    }
    private func updateView() {
        tableView.isHidden = !hasBoats
        messageLabel.isHidden = hasBoats
    }
    private func setUpMessageLabel(){
        messageLabel.text = "Click the '+' button up top to add a boat."
    }
    private func setUpTableView() {
        
    }
    
    private var hasBoats : Bool {
        guard let fetchedBoats = frc.fetchedObjects else { return false }
        return fetchedBoats.count > 0
    }
    private func fetchBoats (){
        do{
            try frc.performFetch()
        }catch {
            print("Unable to Perform Fetch Request")
            print("\(error), \(error.localizedDescription)")
        }
    }
    
    lazy var frc: NSFetchedResultsController<DiveBoat> = {
        let fetchRequest: NSFetchRequest<DiveBoat> = DiveBoat.fetchRequest()
        
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(DiveBoat.name), ascending: true)]
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: context,
                                                                  sectionNameKeyPath: nil,
                                                                  cacheName: nil)
        
        fetchedResultsController.delegate = self
        
        return fetchedResultsController
    }()
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let segueId = segueIdentifierFor(segue: segue)
        switch segueId {
            //    case .showBoatDetails:
            //        guard let vc = segue.destination as? BoatDetails else { fatalError("Expecting a BoatDetails view controller - didnt get that one!")}
            //        let boat = frc.object(at: (tableView.indexPathForSelectedRow)!)
            //        vc.inject((boat:boat, title:"Boat", mode: .show))
            //
        case.unwindFromBoatChoosen: break
            
        case .addBoat:
            guard let vc = segue.actualDestination() as? BoatDetails else { fatalError("Expecting a BoatDetails view controller - didnt get that one!")}
            let boat = DiveBoat(context: context)
            vc.inject((dataModel: dataModel, boat: boat, title: "Add Boat", mode: .add))
           
        }
    }
    
}

extension BoatChooser : NSFetchedResultsControllerDelegate{
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
        
        updateView()
    }
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
        case .delete:
            
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        case .update:
            if let indexPath = indexPath {
                tableView.reloadRows(at: [indexPath], with: .automatic)
            }
            
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .fade)
            }
        @unknown default:
            print("Futureproof: NSFetchedResultscontroller didChange - and has added a new enum type")
        }
    }
}

extension BoatChooser : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sections = frc.sections else {return 0}
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = frc.sections?[section] else { return 0 }
        return section.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        configure(cell, at: indexPath)
        return cell
    }
    
    func configure(_ cell: UITableViewCell, at indexPath: IndexPath) {
        let boat = frc.object(at: indexPath)
        cell.textLabel?.text = boat.name
        if boat == logEntry.divedFromBoat {
            cell.accessoryType = .checkmark
        }else {
            cell.accessoryType = .none
        }
    }
}
extension BoatChooser : UITableViewDelegate {
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let boat = frc.object(at: indexPath)
        if (boat.divesDoneFromBoat?.count == 0 ) {
            return true
        } else {
            return false
        }
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete", handler: {action,indexPath in
            let boatToDelete = self.frc.object(at: indexPath)
            self.context.delete(boatToDelete)
        })
        return [deleteAction]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let boat = frc.object(at: indexPath)
        if boat == logEntry.divedFromBoat {
            logEntry.divedFromBoat = nil
        }else {
            logEntry.divedFromBoat = boat
            performSegue(withIdentifier: Identifier.unwindFromBoatChoosen.rawValue, sender: self)
        }
    }
}

extension BoatChooser : Injectable{
    
    
    func inject(_ dm: DataModel) {
        dataModel = dm
    }
    
    func assertDependencies() {
        assert(dataModel  != nil, "didnt get a log entry passed into boat chooser ")
    }
    
    typealias T = DataModel
    
    
}

