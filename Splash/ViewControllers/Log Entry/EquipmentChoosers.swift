//
//  EquipmentChoosers.swift
//  Splash
//
//  Created by Simon Gardener on 11/06/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class EquipmentChoosers: UITableViewController , UITextFieldDelegate {
    
    let header = ["weigth","wetsuit description"]
    let cellID = ["weight","suit"]
    let weightSection = 0
    let suitSection = 1

    var logEntry: LogEntry!
    var weightPicker: WeightPicker!
    var toolbar: UIToolbar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        title = "Weights & suit"
        weightPicker = setUpWeightPicker()
        toolbar = setUpToolBar()
        tableView.keyboardDismissMode = .interactive
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setUpToolBar ()-> UIToolbar {
        let tb = UIToolbar()
        tb.sizeToFit()
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let removeButton = UIBarButtonItem(title: "Remove value", style: .plain , target: self, action: #selector(removeButtonPressed))
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done , target: self, action: #selector(doneButtonPressed))
        tb.setItems([removeButton, flexibleSpace,doneButton], animated: true)
        return tb
    }
    func setUpWeightPicker() -> WeightPicker {
        let picker = WeightPicker()
        picker.dataSource = picker
        picker.delegate = picker
        picker.sizeToFit()
        picker.logEntry = logEntry
        picker.setPickerWithInitialValues()
        picker.sizeToFit()
        return picker
    }
    @objc private func removeButtonPressed(){
       logEntry.weight = nil
        let indexPath = IndexPath(row: 0, section: 0)
        if let cell = tableView.cellForRow(at: indexPath) as? LogWeightCell {
            cell.triggeringTextField.resignFirstResponder()
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    @objc private func doneButtonPressed(){
        
        let indexPath = IndexPath(row: 0, section: 0)
        if let cell = tableView.cellForRow(at: indexPath) as? LogWeightCell {
            cell.triggeringTextField.resignFirstResponder()
        }
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return header[section]    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case weightSection:
            guard let weightCell = tableView.dequeueReusableCell(withIdentifier: cellID[indexPath.section], for: indexPath) as? LogWeightCell else { fatalError("Didnt get a logWeightCell")}
            weightCell.configure(with: logEntry, toolbar: toolbar, picker: weightPicker)

            return weightCell
        case suitSection:
            guard let suitCell = tableView.dequeueReusableCell(withIdentifier: cellID[indexPath.section], for: indexPath) as? LogSuitCell else { fatalError("Didnt get a logSuit Cell")}
            suitCell.configure(with: logEntry)
            return suitCell
        default : return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row{
        case weightSection:
            let weightCell = tableView.cellForRow(at: indexPath) as! LogWeightCell
            weightPicker.cell = weightCell
            weightCell.triggeringTextField.becomeFirstResponder()
            break
        case suitSection:
            let suitCell = tableView.cellForRow(at: indexPath) as! LogSuitCell
        suitCell.suitTextField.becomeFirstResponder()
            break
        default: break
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case suitSection:
            logEntry.exposureSuit = textField.text
        default : break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


extension EquipmentChoosers : Injectable {
    
    func inject(_ log: LogEntry) {
        logEntry = log
  
    }
    
    func assertDependencies() {
        assert(logEntry != nil , "got nil for logentry in equipment chooser")
        
    }
    typealias T = LogEntry
    
}
