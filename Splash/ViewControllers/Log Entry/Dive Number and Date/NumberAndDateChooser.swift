//
//  NumberAndDateChooser.swift
//  Splash
//
//  Created by Simon Gardener on 14/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class NumberAndDateChooser: UITableViewController, OptionOrder {
    
    enum OptionOrder : Int {
        case number, date, timeZone
    }
    
    let cellID = "cell"
    let numberOfRows = 3
    var logEntry : LogEntry!
    
    var timeZonePicker : TimeZonePicker!
    var diveNumberPicker : DiveNumberPicker!
    var datePicker = UIDatePicker()
    var toolbar : UIToolbar!
    var datePickerToolBar : UIToolbar!
    var attributeNames = ["Dive number","Date of dive","Timezone"]
    
   var dateFormatter : DateFormatter {
    let df = DateFormatter()
        df.dateStyle = .medium
        df.timeZone = TimeZone(identifier: logEntry.timeZone!)

        return df
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Number & Date"
     toolbar = setUpToolBar()
        datePickerToolBar = setUpDatePickerToolbar()
        diveNumberPicker = setUpNumberPicker()
        timeZonePicker   = setUpTimeZonePicker()
        datePicker = setUpDatePicker()
        assertDependencies()
    
    }
    
    func setUpToolBar ()-> UIToolbar {
        let tb = UIToolbar()
        tb.sizeToFit()
        let flexibleSPace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done , target: self, action: #selector(doneButtonPressed))
        tb.setItems([flexibleSPace, doneButton], animated: true)
        return tb
    }
    func setUpDatePickerToolbar () -> UIToolbar{
        let tb = UIToolbar()
        tb.sizeToFit()
        let flexibleSPace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done , target: self, action: #selector(dismissDatePicker))
        tb.setItems([flexibleSPace, doneButton], animated: true)
        return tb
    }
    
    func setUpNumberPicker() -> DiveNumberPicker {
        let picker = DiveNumberPicker()
        picker.dataSource = picker
        picker.delegate = picker
        picker.sizeToFit()
        picker.logEntry = logEntry
        picker.sizeToFit()
        return picker
    }
    func setUpTimeZonePicker () -> TimeZonePicker {
        let tzPicker = TimeZonePicker()
        tzPicker.dataSource = tzPicker
        tzPicker.delegate = tzPicker
        tzPicker.sizeToFit()
        tzPicker.logEntry = logEntry
        return tzPicker
    }
    
    func setUpDatePicker () -> UIDatePicker{
        
        let dp = UIDatePicker()
        dp.sizeToFit()
        
        if let timeZone = logEntry.timeZone{
            dp.timeZone = TimeZone(identifier: timeZone)
        }else {
            dp.timeZone = TimeZone.current
        }
        
        if let date = logEntry.timeIn {
            dp.setDate(date, animated: true)
        }else {
            dp.setDate(Date.init(), animated: true)
        }
        dp.datePickerMode = .date
        
        return dp
    }
    @objc func doneButtonPressed(){
        let indexPath = tableView.indexPathForSelectedRow!
        let cell = tableView.cellForRow(at: indexPath ) as? LogAttributeDataCell
        cell?.hiddenTextField.resignFirstResponder()
        tableView.deselectRow(at: indexPath, animated: true)
    }
   
    @objc func dismissDatePicker(){
         // updates timein date AND timeout date by same amount.
        // reloads cell and disnmisses firstresponder
        var changedBy : TimeInterval = 0
        
        if let timeIn = logEntry.timeIn{
            changedBy = datePicker.date .timeIntervalSince(timeIn)
        }
        logEntry.timeIn = datePicker.date
        
        if let timeOut = logEntry.timeOut , changedBy != 0 {
            logEntry.timeOut = timeOut.addingTimeInterval(changedBy)
        }
        let indexPath = tableView.indexPathForSelectedRow!
        let cell = tableView.cellForRow(at: indexPath) as! LogAttributeDataCell
        cell.hiddenTextField.resignFirstResponder()
        tableView.reloadRows(at: [indexPath], with: .automatic)
        
    }
    
//    func datePickerChangedValue() {
//        logEntry.
//        var dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
//        var strDate = dateFormatter.stringFromDate(datePicker.date)
//        self.selectedDate.text = strDate
//    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfRows
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as? LogAttributeDataCell
        cell?.attributeLabel.text = attributeNames[indexPath.row] + ":"
        let order = optionOrderFor(position: indexPath  .row)
        
        switch  order {
        case .number:
            cell?.dataLabel.text = String(logEntry.diveNumber)
            cell?.hiddenTextField.inputView = diveNumberPicker
            cell?.hiddenTextField.inputAccessoryView = toolbar
            diveNumberPicker.cell = cell
            
        case .date:
            cell?.dataLabel.text = dateFormatter.string(from: logEntry.timeIn!)
            cell?.hiddenTextField.inputAccessoryView = datePickerToolBar
            cell?.hiddenTextField.inputView = datePicker
            
        case .timeZone:
            cell?.dataLabel.text = logEntry.timeZone
            cell?.hiddenTextField.inputView = timeZonePicker
            cell?.hiddenTextField.inputAccessoryView = toolbar
            timeZonePicker.cell = cell
            
        }
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? LogAttributeDataCell
      cell?.hiddenTextField.becomeFirstResponder()
        
    }
}
extension NumberAndDateChooser : Injectable {
    func inject(_ log: LogEntry) {
        logEntry = log
    }
    func assertDependencies() {
        assert(logEntry != nil, "didnt get a logEntry passed in")
    }
    typealias T = LogEntry
}


