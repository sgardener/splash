//
//  DiveNumberPicker.swift
//  Splash
//
//  Created by Simon Gardener on 14/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class DiveNumberPicker: UIPickerView , UIPickerViewDelegate, UIPickerViewDataSource {
    
    let thousandsPosition = 0
    let hundredsPosition = 1
    let tensPosition = 2
    let singlesPostion = 3
 
    let digits = ["0","1","2","3","4","5","6","7","8","9"]
    
    var logEntry: LogEntry!
    weak var cell : LogAttributeDataCell!
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 4
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 10
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return digits[row]
    }
    func setUpInitialValue(cell: LogAttributeDataCell, log: LogEntry) {
        self.cell = cell
        logEntry = log
        setLogNumberOnPicker()
    }

    fileprivate func rowValuesForLogEntryNumber()-> (thousands: Int, hundreds: Int, tens: Int, singles: Int) {
        var logNumber = Int(logEntry.diveNumber)
        let thousands = logNumber / 1000
        logNumber = logNumber - thousands * 1000
        let hundreds = logNumber / 100
        logNumber = logNumber - hundreds * 100
        let tens = logNumber / 10
        let singles = logNumber - tens * 10
        return (thousands, hundreds, tens, singles)
    }
    
    private func setLogNumberOnPicker() {
       let rowValues = rowValuesForLogEntryNumber()
        selectRow(rowValues.thousands, inComponent: thousandsPosition, animated: true)
        selectRow(rowValues.hundreds, inComponent: hundredsPosition, animated: true)
        selectRow(rowValues.tens, inComponent: tensPosition, animated: true)
        selectRow(rowValues.singles, inComponent: singlesPostion, animated: true)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        updateDiveNumber ()
    }
    
    private func updateDiveNumber(){
        let number = Int16(selectedRow(inComponent: thousandsPosition) * 1000 +
                    selectedRow(inComponent: hundredsPosition) * 100 +
                    selectedRow(inComponent: tensPosition) * 10 +
                    selectedRow(inComponent: singlesPostion))
        
        logEntry.diveNumber = number
        cell.dataLabel.text = String(number)
        
    }

}
