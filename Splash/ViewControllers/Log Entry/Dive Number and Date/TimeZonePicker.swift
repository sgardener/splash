//
//  TimeZonePicker.swift
//  Splash
//
//  Created by Simon Gardener on 14/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class TimeZonePicker: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return timeZones.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       return timeZones[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        logEntry.timeZone = timeZones[row]
        cell.dataLabel.text = timeZones[row]
    }
    
    func setUpInitialValues(cell: LogAttributeDataCell, log: LogEntry) {
        self.cell = cell
        logEntry = log
        setRowToTimeZone()
    }
    
    func setRowToTimeZone(){
        guard let timeZone = logEntry?.timeZone, let row = timeZones.firstIndex(of: timeZone) else { return }
        selectRow(row, inComponent: 0, animated: true)
    }
    var logEntry: LogEntry!
    let timeZones = NSTimeZone.knownTimeZoneNames
    weak var cell: LogAttributeDataCell!
    
}
