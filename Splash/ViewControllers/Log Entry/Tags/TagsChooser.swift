//
//  TagsChooser.swift
//  Splash
//
//  Created by Simon Gardener on 16/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class TagsChooser: UITableViewController ,NSFetchedResultsControllerDelegate{
    
    var dataModel : DataModel!
    var frc: NSFetchedResultsController<DiveTag>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        title = "Tags"
        frc = dataModel.fetchedResultsControllerForDiveTags()
        fetchTags()
    }
    
    private func fetchTags(){
        do{
            try frc.performFetch()
        }catch {
            print("Unable to Perform Fetch Request")
            print("\(error), \(error.localizedDescription)")
        }
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        guard let sections = frc.sections else {return 0}
        return sections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let theSection = frc.sections?[section] else { return 0 }
        return theSection.numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
        let tag =  frc.object(at: indexPath)
        cell.textLabel?.text = tag.name
        let userLogTags = dataModel.logEntry?.taggedWith
        if userLogTags?.contains(tag) == true {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tag =  frc.object(at: indexPath)
        if dataModel.logEntry?.taggedWith?.contains(tag) == true {
            dataModel.logEntry?.removeFromTaggedWith(tag)
        }else{
            dataModel.logEntry?.addToTaggedWith(tag)
        }
        if 700...702 ~=  Int(tag.id) {
            flipProTag(withID: tag.id)
        }
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return  frc.sections![section].name
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return frc.sectionIndexTitles
    }
    override func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return frc.section(forSectionIndexTitle: title, at: index)
    }
    
    //MARK: Handle Pro Tags
    fileprivate func flipProTag(withID tagID: Int32) {
        let tagIdNumber = Int(tagID)
        let proType = optionOrderFor(position: tagIdNumber)

        switch proType {
            
        case .Instructor:
            dataModel.logEntry?.isInstructing = !(dataModel.logEntry?.isInstructing)!
        case .Assistant:
            dataModel.logEntry?.isAssisting = !(dataModel.logEntry?.isAssisting)!
        case .Guide:
            dataModel.logEntry?.isGuiding = !(dataModel.logEntry?.isGuiding)!

        }
    }
}
extension TagsChooser : OptionOrder {
    enum OptionOrder: Int {
        case Instructor = 700, Assistant, Guide
    }
}

extension TagsChooser : Injectable {
    func inject(_ dm: DataModel) {
        dataModel = dm
    }
    
    func assertDependencies() {
        assert(dataModel != nil, "Expected a dataModel")
    }
    
    typealias T = DataModel
}
