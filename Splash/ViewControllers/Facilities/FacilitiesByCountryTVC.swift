//
//  FacilitiesByCountryTVC.swift
//  Splash
//
//  Created by Simon Gardener on 04/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class FacilitiesByCountryTVC: UITableViewController {
    
    var dataModel : DataModel!
    var countryName: String!
    
    let segueID = "showFacilityDetails"
    let firstLetterKeyPath = "firstLetterForIndex"
    
    lazy var frc : NSFetchedResultsController<Facility> = {
        let fetchRequest: NSFetchRequest<Facility> = Facility.fetchRequest()
        let predicate = NSPredicate(format:"country == %@", countryName)
        fetchRequest.predicate = predicate
        
        let nameSortDescriptor = NSSortDescriptor(key: #keyPath(Facility.name), ascending: true)
        fetchRequest.sortDescriptors = [nameSortDescriptor]
        let countryFirstKeyPath = \Facility.country?.firstCharacter
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: (dataModel?.container.viewContext)!,
                                                                  sectionNameKeyPath: firstLetterKeyPath,
                                                                  cacheName: nil)
        fetchedResultsController.delegate = self
        return fetchedResultsController
    }()
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return  frc.sections![section].name
    }
    
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return frc.sectionIndexTitles
    }
    override func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return frc.section(forSectionIndexTitle: title, at: index)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        fetchFacilities()
        tableView.tableFooterView = UIView()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchFacilities()
    }
    private func fetchFacilities(){
        do{
            try frc.performFetch()
        }catch {
            print("\(error), \(error.localizedDescription)")
        }
    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return frc.sections!.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let secInfo = frc.sections![section]
        return secInfo.numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
        let facility = frc.object(at: indexPath)
        
        cell.textLabel?.text = facility.name
        
        if facility.isDataPartner {
            cell.textLabel?.font = cell.textLabel?.font.bold
            
        }
        cell.detailTextLabel?.text = areaStringFor(facility)
        return cell
    }
    
    fileprivate func areaStringFor(_ facility: Facility)-> String {
        var facilityAreaString = ""
        
        if facility.islandAtoll != nil {
            facilityAreaString = facilityAreaString + facility.islandAtoll! + ", "
        }
        if facility.localAreaName != nil {
            facilityAreaString = facilityAreaString + facility.localAreaName! + ", "
        }
        
        
        if facilityAreaString != "" {
            facilityAreaString = facilityAreaString.trimmingCharacters(in: CharacterSet.init(charactersIn: ", "))
            facilityAreaString += "."
        }
        return facilityAreaString
    }
    //MARK:- TableViewDelegate
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let facilityToDelete = self.frc.object(at: indexPath)
        if facilityToDelete.addedByUser == true { return true } else {return false}
        
    }
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let facilityToDelete = self.frc.object(at: indexPath)
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete", handler: {action,indexPath in
            
            let alertController = UIAlertController(title: "Delete Facility?", message: "Are you sure you want to delete this facility?", preferredStyle: .actionSheet )
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {action in
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
            })
            let goAheadAction = UIAlertAction(title: "Yes", style: .destructive, handler: {(action) in
                self.dataModel.container.viewContext.delete(facilityToDelete)})
            alertController.addAction(goAheadAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        })
        return [deleteAction]
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.actualDestination() as? FacilityDetailsTVC else {fatalError("expecting a SiteDetails Scene")}
        let indexPath = tableView.indexPathForSelectedRow
        dataModel.facility = frc.object(at: indexPath!)
        vc.inject(dataModel)
        
    }
}
extension  FacilitiesByCountryTVC: NSFetchedResultsControllerDelegate{
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch  type {
            
        case .insert:
            let sectionIndexSet = NSIndexSet(index: sectionIndex)
            self.tableView.insertSections(sectionIndexSet as IndexSet, with: UITableView.RowAnimation.fade)
        case .delete:
            let sectionIndexSet = NSIndexSet(index: sectionIndex)
            self.tableView.deleteSections(sectionIndexSet as IndexSet, with: UITableView.RowAnimation.fade)
        default:
            break
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
            
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .automatic)
        case .move:
            tableView.reloadRows(at: [indexPath!], with: .automatic)
        case .update:
            tableView.deleteRows(at: [indexPath!], with: .automatic)
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
        @unknown default:
            print("Futureproof: NSFetchedResultscontroller didChange - and has added a new enum type")
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}
extension FacilitiesByCountryTVC: Injectable {
    func inject(_ injected: (dateModel: DataModel?, countryName: String)) {
        dataModel = injected.dateModel!
        countryName = injected.countryName
    }
    
    func assertDependencies() {
        assert(dataModel != nil, "didnt get a datamodel ")
        assert(countryName != nil, "didnt get a countryname")
    }
    
    
    typealias T = (dateModel: DataModel?,countryName: String)
    
    
}

