//
//  FacilitiesViewBy.swift
//  Splash
//
//  Created by Simon Gardener on 04/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class FacilitiesViewBy: UITableViewController, NeedsDataModel {
    
    let numberOfSections = 2
    let newIdentifier = "addNewFacility"
    var container: NSPersistentContainer!
    var dataModel: DataModel!
    let CountryCellID = "countryCell"
    
    let showByCountrySegue = "showSitesByCountry"
    let headerTitles = ["Nearby","Regular","By country"]
    let sectionDisplayNames = ["List of nearby dive facilities","List of your regular dive facilities"]
    lazy var frc : NSFetchedResultsController<Facility>  = dataModel.fetchedResultsControllerForFacilityWithSectionNameKeyPathCountry()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assert(dataModel != nil, "Datamodel not passed in")
        setUpNavBar()
    }
    func setUpNavBar(){
    navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchDiveSites()
        tableView.reloadData()
    }
    
    private func fetchDiveSites() {
        do{
            try frc.performFetch()
        }catch {
            print("\(error), \(error.localizedDescription)")
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int{
        return OptionOrder.countries.rawValue + 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionName = optionOrderFor(position: section)
        switch sectionName {
        case .nearby:
            return 1
        case .regular:
            return dataModel.numberOfFacilitiesMarkedAsRegular() > 0 ? 1:0
        case .countries:
            return (frc.sections?.count)!
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        let sectionName = optionOrderFor(position: indexPath.section)
        switch sectionName{
        case .nearby, .regular:
            cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
            cell.textLabel?.text = sectionDisplayNames[indexPath.section]
            
        case .countries:
            let countryCell = tableView.dequeueReusableCell(withIdentifier: CountryCellID, for: indexPath)
            let secInfo = frc.sections?[indexPath.row]
            countryCell.textLabel?.text = secInfo?.name
            countryCell.detailTextLabel?.text = String("\((secInfo?.numberOfObjects)!) dive facilities")
            cell = countryCell
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sectionName = optionOrderFor(position: section)
        switch sectionName{
        case .nearby, .countries:
            return headerTitles[section]
        case .regular:
            if dataModel.numberOfFacilitiesMarkedAsRegular() > 0 {
                return headerTitles[section]
            } else {return nil}
            
        }
    }
    
    //MARK: - TableView Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sectionName = optionOrderFor(position: indexPath.section)
        
        switch sectionName{
        case .countries: break
        case.regular:
            performSegue(withIdentifier: Identifier.regular.rawValue, sender: self)
        case .nearby:
            performSegue(withIdentifier: Identifier.nearby.rawValue, sender: self)
        }
    }
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let segueID = segueIdentifierFor(segue: segue)
        
        switch segueID{
        case .nearby:
            guard let vc = segue.actualDestination() as? FacilitiesByNearbyVC else { fatalError("didnt get a FacilitiesByNearby scene")}
            vc.inject(dataModel)
            
        case .regular:
            guard let vc = segue.actualDestination() as? FacilitiesByRegularTVC else { fatalError("Expected a FacilitieByRegular scene - didnt get one")}
            vc.inject(dataModel)
            
        case .countries:
            guard let vc = segue.actualDestination() as? FacilitiesByCountryTVC else { fatalError("didnt get a FacilitiesByCountry scene")}
            let indexPath = tableView.indexPathForSelectedRow!
            let secInfo = frc.sections?[indexPath.row]
            
            let countryName = secInfo?.name
            vc.countryName = countryName
            vc.title = countryName
            vc.dataModel = dataModel!
            
        case .addNewFacility:
            guard let vc = segue.actualDestination() as? FacilityDetailsEditor else { fatalError("expected a FacilityDetailsEditor - didnt get one ")}
            _ = dataModel.newFacility()
            vc.inject((dataModel, .add))
            
        }
    }
    /// unwind segue
    @IBAction func reloadAfterNewFacilityAdded(segue:UIStoryboardSegue){
        print("Unwound to facilityViewBy")
        //  tableView.reloadData()
    }
    
    @IBAction func addNewDiveFacility(_ sender: UIBarButtonItem) {
        
        let message  = "Did you do a manual search for the country you are in? \n\n if you havent checked, click check, otherwise go ahead and add a new facility"
        let alertController = UIAlertController(title: "Are you sure", message: message, preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Ok, I'll check first", style: .cancel, handler: nil)
        let addNewAction = UIAlertAction(title: "Add New Dive Facility", style: .default, handler: {(action) in
            self.performSegue(withIdentifier: self.newIdentifier , sender: self)
        })
        alertController.addAction(addNewAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
}

extension FacilitiesViewBy: SegueHandlerType, OptionOrder{
    
    
    enum Identifier: String {
        case nearby
        case regular
        case countries
        case addNewFacility
        
    }
    enum OptionOrder: Int {
        case nearby
        case regular
        case countries
        
    }
    
}
