//
//  FacilityDetailsTVC.swift
//  Splash
//
//  Created by Simon Gardener on 04/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class FacilityDetailsTVC: UITableViewController {
    var dataModel: DataModel!
    var facility : Facility!
    var viewModel : FacilityDetailsViewModel!

    private enum cellId: String {
        case note = "noteCell"
        case map = "mapCell"
        case attribution = "attributionCell"
        case switcher = "SwitcherCell"
        case basicWithHeader = "BasicHeaderCell"
    }
    
    let visitWebSegueId = "visitWeb"
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        assertDependencies()
        facility = dataModel.facility
        viewModel = FacilityDetailsViewModel(with: facility)
        addEditBarButton()
        title = facility.name
    }
    
    func addEditBarButton(){
        let editButton  = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editButtonTapped))
        navigationItem.rightBarButtonItem = editButton
    }
    @objc  fileprivate func editButtonTapped(){
        performSegue(withIdentifier: "editSite", sender: self)
    }
    
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return OptionOrder.note.rawValue + 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionPosition = optionOrderFor(position: section)
        switch sectionPosition{
        case .markAsRegular: return 1
        case .name:
            guard facility.name != nil else { return 0}
            return 1
        case .address:
            return 1
            
        case .map:
            guard facility.coordinate != nil else { return 0}
            return 1
        case .note :
            guard facility.note != nil , !facility.note!.isEmpty else {return 0}
            return 1
        case.email :
            guard facility.email != nil, !facility.email!.isEmpty else { return 0 }
            return 1
        case .website:
            guard facility.website != nil, !facility.website!.isEmpty else { return 0 }
            return 1
        case .phone:
            guard facility.phoneNumber != nil, !facility.phoneNumber!.isEmpty else { return 0 }
            return 1
        case .affiliation:
           return  viewModel.isAffiliated() ? 1 : 0
        case .type:
            return viewModel.hasType() ? 1:0
        case .offers:
            return viewModel.doesOfferService() ? 1:0
        case .trainingFrom:
            return viewModel.numberOfRowForTrainingFrom()
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let whichCell = optionOrderFor(position: indexPath.section)
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId.basicWithHeader.rawValue, for: indexPath) as! BasicWithHeaderTableViewCell
        cell.selectionStyle = .none
        cell.accessoryType = .none
        
        switch whichCell {
            
        case .name:
            if let name = facility.name {
                cell.configureWith(header: "facility name", info: name)
            }
            
        case .address:
            cell.configureWith(header: "address", info: viewModel.addressAsString())
    
            
        case .note:
            guard let noteCell = tableView.dequeueReusableCell(withIdentifier: cellId.note.rawValue, for: indexPath) as? NoteCell else {fatalError("Expecting a NoteCell- didnt get one")}
            noteCell.textView.isEditable = false
            noteCell.textView.text = "\(facility.note!)\n\n\(attributeString(for: facility))"
            noteCell.textView.sizeToFit()
            return noteCell
            
        case .map:
            guard  let mapCell = tableView.dequeueReusableCell(withIdentifier: cellId.map.rawValue, for: indexPath) as? MapViewCell else {fatalError("didnt get a mapcell - expecteed one")}
            mapCell.centerMapOn(facility.coordinate, annotateWithtitle: facility.name, andSubTitle: facility.localAreaName, withColor: .blue)
            return mapCell
        
        case .markAsRegular:
            guard let switchCell = tableView.dequeueReusableCell(withIdentifier: cellId.switcher.rawValue, for: indexPath) as? SwitcherTableViewCell else { fatalError("Didnt get a switcherCell")}
          
            switchCell.selectionStyle = .none
            switchCell.configureWith("regular dive ops", bool: facility.isRegularFacility)
            return switchCell
            
        case .email:
            cell.configureWith(header: "email", info: facility.email!)
        case .website:
            guard let webCell =
            tableView.dequeueReusableCell(withIdentifier: cellId.attribution.rawValue) as? AttributionCell else {fatalError("Expecting an Attribution cell - didnt get one")}
            webCell.attributedTo.text = facility.website
            return webCell
            
        case .phone:
            cell.configureWith(header: "phone", info: facility.phoneNumber!)
        case .affiliation:
            cell.configureWith(header: "resort or centre type", info: viewModel.affiliation())
        case .type:
            cell.configureWith(header: "facility type", info: viewModel.typeOfFacility())
        case .offers:
            cell.configureWith(header: "services offered...", info: viewModel.servicesOffered())
        case .trainingFrom:
            cell.configureWith(header: "offer training from...", info: viewModel.trainingBy())
        }
        return cell
    }
    
    private func attributeString(for facility: Facility)-> String {
        
        var attributeString: String = ""
        if let noteAssociatedName = facility.noteAssocName, !noteAssociatedName.isEmpty{
            attributeString = "Submitted by : \(noteAssociatedName)\n"
        }
        if let noteAssociatedWeb = facility.noteAssocWeb, !noteAssociatedWeb.isEmpty {
            if attributeString.isEmpty == true {
                attributeString = "Submitted by : \(noteAssociatedWeb)"
            }else {
                attributeString = attributeString + "\(noteAssociatedWeb)"
            }
        }
        return attributeString
    }
    
    @IBAction func switchChanged(_ sender: UISwitch) {
            facility.isRegularFacility = sender.isOn
    }
   
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editSite"{
            guard let vc = segue.actualDestination() as? FacilityDetailsEditor else {fatalError()}
            vc.inject((dataModel, .edit))
        } else {
            guard let vc = segue.actualDestination()  as? WebViewController else { fatalError("Expected a webViewController")}
            vc.inject(facility.website!)
        }
    }
    //unwind segue for facilitydetailseditor
    @IBAction func reloadFacilityDetailsAfterEditting(segue: UIStoryboardSegue){
        tableView.reloadData()
    }
}
extension FacilityDetailsTVC: OptionOrder {
    
    enum OptionOrder: Int {
        case name
        case affiliation
        case trainingFrom
        case type
        case offers
        case address
        case email
        case website
        case phone
        case markAsRegular
        case map
        case note
    }
}
    
extension FacilityDetailsTVC : Injectable {
    func inject(_ dm: DataModel) {
        dataModel  = dm
    }
    
    func assertDependencies() {
        assert(dataModel != nil, " expected a dataModel")
        assert(dataModel.facility != nil, "Expected datamodelfacility to have value")
    }
    
    typealias T = DataModel
    
    
}
