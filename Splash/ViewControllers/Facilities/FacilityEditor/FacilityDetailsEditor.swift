//
//  FacilityDetailsEditor.swift
//  Splash
//
//  Created by Simon Gardener on 15/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreLocation
import BLTNBoard

class FacilityDetailsEditor: UIViewController, LocationUser {
    
    var locationService: LocationService!
    var mode: Mode = .show
    var dataModel:DataModel!
    let saveEditSegueId = "reloadFacilityDetailsAfterEditting"
    
    let locationTableLocationSection =  IndexSet.init(integer: 0)
    let locationTableCoordinationSection = IndexSet.init(integer: 1)
    let countryIndexPath = IndexPath.init(row: 12, section: 0)
    
    
    fileprivate enum activeTable: Int {
        case location, type, offers, notes
    }
    
    @IBOutlet weak var tableSwitcher: UISegmentedControl!
    @IBOutlet weak var locationTable: FacilityLocationTable!
    @IBOutlet weak var offersTable: FacilityOffersTable!
    @IBOutlet weak var typeTable: FacilityTypeTable!
    @IBOutlet weak var noteView: UITextView!
    
    var isSearchingForGPS = false
    
    lazy var bulletinManager: BLTNItemManager = {
        var page = BulletinDataSource.locationWasDeniedPage()
        return BLTNItemManager(rootItem: page)
    }()
    
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        noteView.text = dataModel.facility?.note
        showLocation()
        setUpTitle()
        setupTables()
        if mode == .add || mode == .addedWhileLogging {
            locationTable.initiateGPSLookup()
        }
    }
    fileprivate func setUpTitle(){
        if mode == .edit {
            title = "Edit Facility"
        }else {
            title = " Add Facility"
        }
    }
    fileprivate func setupTables(){
        setupViewModelsForTables()
        setupDelegatesandDataSources()
        passOnSelf()
    }
    fileprivate func passOnSelf(){
        locationTable.parentView = self
        typeTable.parentView = self
        offersTable.parentView = self
    }
    fileprivate func setupViewModelsForTables(){
        locationTable.viewModel = FacilityLocationViewModel.init(with: dataModel)
        typeTable.viewModel = FacilityTypeViewModel(with: dataModel)
        offersTable.viewModel = FacilityOffersViewModel(with: dataModel)
    }
    fileprivate func setupDelegatesandDataSources(){
        locationTable.delegate = locationTable
        locationTable.dataSource = locationTable
        typeTable.delegate = typeTable
        typeTable.dataSource = typeTable
        offersTable.delegate = offersTable
        offersTable.dataSource = offersTable
        
    }
    
    
    //MARK:- Cancel Save
    
    @IBAction func cancel(_ sender: Any) {
        switch mode{
        case .add:
            dataModel.container.viewContext.delete(dataModel.facility!)
            presentingViewController?.dismiss(animated: true, completion: nil)
            
        case .edit:
            dataModel.facility?.cancelChanges()
            performSegue(withIdentifier: Identifier.reloadFacilityDetailsAfterEditting.rawValue , sender: self)
            
        case .addedWhileLogging:
            performSegue(withIdentifier: Identifier.cancelNewFacilityForLogEntry.rawValue, sender: self)
            
        case .show: break
        }
    }
    
    @IBAction func saveFacilityDetails(_ sender: UIBarButtonItem) {
        locationTable.endEditing(true)
        if canSave() {
            
            switch mode {
                
            case .add:
                
                dataModel.saveContext()
                performSegue(withIdentifier: Identifier.reloadAfterNew.rawValue, sender: self)
                
            case .edit:
                if dataModel.facility?.hasChanges == false {
                    navigationController?.popViewController(animated: true)
                } else {
                    ChangeChecker.noteChangesTo(dataModel.facility!)
                    dataModel.saveContext()
                    performSegue(withIdentifier: saveEditSegueId, sender: self)
                }
            case .addedWhileLogging:
                //add facility to logentry but dont save as that will happen in logEntry
                dataModel.logEntry?.facility = dataModel.facility
                //unwind to facility options
                performSegue(withIdentifier: Identifier.facilityChoosen.rawValue, sender: self)
                
            case .show: break
                
            }
        }else {
            showAlert(withTitle: "Can't Save", message: "A facility must have a name, a country and a captured location before it can be saved.") }
    }
    
    fileprivate func canSave()-> Bool{
        if (nameCheck() ==  true && countryCheck() == true && locationCheck() == true ){
            return true
        }
        else {return false}
    }
    fileprivate func nameCheck ()->Bool{
        guard let name = dataModel.facility?.name , name.isEmpty == false else { return false }
        return true
    }
    fileprivate func countryCheck()-> Bool{
        guard let country = dataModel.facility?.country, country.isEmpty == false else { return false }
        return true
    }
    fileprivate func locationCheck()-> Bool{
        return  dataModel.facility?.longitude != nil
    }
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let segueName = segueIdentifierFor(segue: segue)
        switch segueName{
        case .country:
            guard let vc = segue.actualDestination() as? CountryChooser else { fatalError("didn't get a countrychooser")}
            vc.handleSelection = { (country:String) in
                self.dataModel.facility?.country = country
                self.locationTable.reloadRows(at: [ self.countryIndexPath ], with: .none)
            }
            
        case .AgencyForCenterType:
            guard let vc = segue.actualDestination() as? AgencyForCenterType else { fatalError()}
            vc.inject(dataModel)
            
        case .AgencyForResortType:
            guard let vc = segue.actualDestination() as? AgencyForResortType else { fatalError() }
            vc.inject(dataModel)
        case .trainingBy:
            guard let vc = segue.actualDestination() as? OffersTrainingBy else {fatalError() }
            vc.inject(dataModel)
        case .facilityChoosen, .cancelNewFacilityForLogEntry, .reloadFacilityDetailsAfterEditting, .reloadAfterNew :
            break // all unwind segues
        }
    }
    //MARK:- Location Stuff
    
    func locationDidUpdate(location: CLLocation) {
        guard locationTable.locationUpdateCount == 0 else {
           // print ("discarding locationUpdate")
            return
        }
        locationTable.locationUpdateCount += 1
       // print("*******************locationDidUpdate got called\n")
        locationTable.capturedLocation = location
        locationTable.viewModel.save(location: location)
        locationTable.isSearchingForGPS = false
        
        reloadCoordinateSection()
        let handler = {(placemarks:[CLPlacemark]?,error: Error?) in
            guard error == nil else {return}
            let alreadyStoredKeys = GeocoderService.keysForStoredValues(in: self.locationTable.viewModel.facility!)
            if alreadyStoredKeys == nil {
                GeocoderService.insertAll(placemarks: placemarks, into: self.locationTable.viewModel.facility!)
            }else{
                let warning = UIAlertController(title: "Data Found", message: "Some location data has been found for your location.\n\n What action should Splash take ", preferredStyle: .actionSheet)
                let overwrite = UIAlertAction(title: "Overwrite all values", style: .destructive, handler: {action in
                    GeocoderService.insertAll(placemarks: placemarks, into: self.locationTable.viewModel.facility!)
                    self.reloadLocationSection()
                })
                let insert = UIAlertAction(title: "Insert new Values", style: .default, handler: {action in
                    GeocoderService.insertNew(placemarks: placemarks, into: self.locationTable.viewModel.facility!)
                    self.reloadLocationSection()
                })
                let cancel = UIAlertAction(title: "Cancel", style: .cancel , handler: nil)
                warning.addAction(insert)
                warning.addAction(overwrite)
                warning.addAction(cancel)
                self.present(warning, animated: true, completion: nil)
            }
            self.reloadLocationSection()
        }
        GeocoderService.lookUp(location: location, with: handler)
    }
    
    func partialFix(location: CLLocation) {
        locationTable.capturedLocation = location
        reloadCoordinateSection()
    }
    func locationWasDenied(){
        bulletinManager.showBulletin(above: self)
    }
    
    fileprivate func reloadLocationSection() {
        locationTable.reloadSections(locationTableLocationSection, with: .automatic)
    }
    func reloadCoordinateSection(){
        locationTable.reloadSections(locationTableCoordinationSection, with: .automatic)
    }
    // MARK:- Switcher Stuff
    fileprivate func showLocation(){
        locationTable.isHidden = false
        typeTable.isHidden = true
        offersTable.isHidden = true
        noteView.isHidden = true
    }
    
    fileprivate func showType(){
        locationTable.isHidden = true
        typeTable.isHidden = false
        offersTable.isHidden = true
        noteView.isHidden = true
    }
    fileprivate func showOffers(){
        locationTable.isHidden = true
        typeTable.isHidden = true
        offersTable.isHidden = false
        noteView.isHidden = true
    }
    fileprivate func showNote(){
        locationTable.isHidden = true
        typeTable.isHidden = true
        offersTable.isHidden = true
        noteView.isHidden = false
    }
    @IBAction func tableSwitcherChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case activeTable.location.rawValue: showLocation()
        case activeTable.type.rawValue: showType()
        case activeTable.offers.rawValue: showOffers()
        case activeTable.notes.rawValue: showNote()
        default: break
        }
    }
    
}
extension FacilityDetailsEditor : UITextViewDelegate{
    func textViewDidEndEditing(_ textView: UITextView) {
        dataModel.facility?.note = StringWranglers.cleanedUpText(from: textView)
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

extension  FacilityDetailsEditor: Injectable{
    
    func inject(_ injected: (dm: DataModel, mode:Mode)) {
        dataModel = injected.dm
        mode  = injected.mode
    }
    
    func assertDependencies() {
        assert(dataModel != nil)
        assert(dataModel.facility != nil)
    }
    
    typealias T = (dm:DataModel, mode: Mode)
    
}
extension FacilityDetailsEditor: SegueHandlerType {
    enum Identifier : String {
        case country, AgencyForCenterType, AgencyForResortType, trainingBy, facilityChoosen, cancelNewFacilityForLogEntry,reloadFacilityDetailsAfterEditting, reloadAfterNew
    }
}
