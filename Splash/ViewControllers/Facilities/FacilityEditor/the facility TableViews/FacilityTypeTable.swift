//
//  FacilityTypeTable.swift
//  Splash
//
//  Created by Simon Gardener on 15/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//


import UIKit

class FacilityTypeTable: UITableView , UITableViewDelegate, UITableViewDataSource {
    
    var viewModel: FacilityTypeViewModel!
    weak var parentView : FacilityDetailsEditor!
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return viewModel.numberOfRows()
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
        
        cell.textLabel?.text = viewModel.textString(at: indexPath.row)
        
        let rowName = optionOrderFor(position: indexPath.row)
        switch rowName {
        case  .diveResort, .diveCenter:
            cell.accessoryType = .disclosureIndicator
            
        default:
            cell.accessoryType = viewModel.isCheckmark(at: indexPath.row) ? .checkmark: .none
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowName = optionOrderFor(position: indexPath.row)
        switch rowName {
        case .diveCenter:
            parentView.performSegue(withIdentifier: "AgencyForCenterType", sender: self)
            
            case .diveResort:
                parentView.performSegue(withIdentifier: "AgencyForResortType", sender: self)
            break
            //TODO:- segue
        default:
            viewModel.flipValue(at: indexPath)
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
}
extension FacilityTypeTable : OptionOrder{
    enum OptionOrder: Int {
        case diveResort, diveCenter, retailshop ,liveaboardOperation ,
        gasFillingOperation , photoVideoOperation,  marina, jettyOrDock , recompressionChamber, commercialDiveOperation, diveTravelRetailer, dveClub, freediveOp
    }
}
