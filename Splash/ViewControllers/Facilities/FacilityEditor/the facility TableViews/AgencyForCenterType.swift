//
//  AgencyForCenterType.swift
//  Splash
//
//  Created by Simon Gardener on 15/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class AgencyForCenterType: UITableViewController {
    var dataModel: DataModel!
    var agencies = [String]()
    var agency = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        agencies = dataModel.allAgenciesForCenterType()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return agencies.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
        cell.textLabel?.text = agencies[indexPath.row]
        cell.accessoryType = .disclosureIndicator
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        agency = agencies[indexPath.row]
        performSegue(withIdentifier: "choose", sender: self)
    }
  
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.actualDestination() as? CenterTypeChooser else { fatalError()}
        vc.inject((dataModel,agency))
    }
}
extension AgencyForCenterType: Injectable{
    func inject(_ dm: DataModel) {
        dataModel = dm
    }
    func assertDependencies() {
        assert(dataModel != nil)
        assert(dataModel.facility != nil)
    }
    typealias T = DataModel
}
