//
//  FacilityLocationTable.swift
//  Splash
//
//  Created by Simon Gardener on 15/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreLocation

class FacilityLocationTable: UITableView, UITableViewDelegate, UITableViewDataSource {
    
//    let locationTableLocationSection =  IndexSet.init(integer: 0)
   let locationTableCoordinationSection = IndexSet.init(integer: 1)
//
    var viewModel: FacilityLocationViewModel!
    weak var parentView : FacilityDetailsEditor!

    let countryRow = FacilityLocationViewModel.OptionOrder.country.rawValue
    let tapToSelectRow = FacilityLocationViewModel.OptionOrder.tapToBringForward.rawValue
    
    var capturedLocation : CLLocation?
    var locationService: LocationService!
    var isSearchingForGPS = false
    var locationUpdateCount = 0

    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsIn(section)
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.headerStringFor(section)
    }
    fileprivate func setCoordinateCellValues(_ cell: CoordinateCell, _ location: CLLocation?) {
        cell.latitude.text = location?.coordinate.longitude.toFourDecimalPlaces()
        cell.latitude.text = location?.coordinate.longitude.toFourDecimalPlaces()
        cell.longitude.text = location?.coordinate.longitude.toFourDecimalPlaces()
        cell.latitude.text = location?.coordinate.latitude.toFourDecimalPlaces()
        cell.accuracy.text = "± " + (location?.horizontalAccuracy.toZeroDecimalPlaceString())! + "m"
        cell.recent.text = Date().timeIntervalSince((location?.timestamp)!).toZeroDecimalPlaceString() + "second(s)"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        switch indexPath.section{
        case 0:
            guard  let cell = tableView.dequeueReusableCell(withIdentifier: "nameLocation", for: indexPath) as? TextFieldTableViewCell else { fatalError("did not get a TextFieldTableViewCell")}
             cell.textField.placeholder = viewModel.placeholder(at: indexPath)
            cell.textField.text = viewModel.value(at: indexPath)
            cell.textField.tag = indexPath.row
            cell.textField.delegate = self
            cell.accessoryType = .none
            cell.textField.isEnabled = true
            cell.selectionStyle = .none
            if indexPath.row == countryRow {
                cell.accessoryType = .disclosureIndicator
                cell.textField.isEnabled = false
                cell.selectionStyle = .default
            }
            if indexPath.row == tapToSelectRow {
                cell.textField.isEnabled = false
                cell.selectionStyle = .default
            }
            cell.textField.keyboardType = viewModel.keyboardType(at: indexPath)
            cell.textField.autocapitalizationType = viewModel.capitalization(at: indexPath)
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "coordinates", for: indexPath ) as? CoordinateCell else {fatalError("did not get a CoordinateCell")}
            
            if isSearchingForGPS == true {
                cell.indicator.isHidden = false
                cell.indicator.startAnimating()
            } else {
                cell.indicator.isHidden = true
                cell.indicator.stopAnimating()
            }
            
            if let location = capturedLocation {
                cell.longitude.text = location.coordinate.longitude.toFourDecimalPlaces()
                cell.latitude.text = location.coordinate.latitude.toFourDecimalPlaces()
            }else {
                cell.longitude.text = viewModel.longitude()
                cell.latitude.text = viewModel.latitude()
            }
            return cell
            
        default: return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        deselectRow(at: indexPath, animated: true)
        if indexPath.section == 0 {
            switch indexPath.row{
            case countryRow : parentView.performSegue(withIdentifier: "country", sender: self)
                
            case tapToSelectRow: bringForward()
            default : break
                
            }
        }else {
            //tapped coordinate to refresh
            if isSearchingForGPS == false  {
                isSearchingForGPS = true
                locationUpdateCount = 0
                if locationService != nil {
                    locationService.requestLocation()
                }else {
                    initiateGPSLookup()
                }
            }
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    //MARK: - Location Stuff
    
    func initiateGPSLookup() {
        locationService = LocationService(owner: parentView, desiredAccuracy: 10, within: 30)
        locationService.requestLocation()
        isSearchingForGPS = true
        reloadCoordinateSection()
        
    }
    func reloadCoordinateSection(){
        reloadRows(at: [IndexPath.init(row: 0, section: 1)], with: .automatic)
    }
    
    //MARK:-
    
    fileprivate func bringForward(){
        //TODO: - write this bring forward code
        print("bring forward tapped")
    }
}
//extension FacilityLocationTable {
//    enum OptionOrder:Int {
//        case  facilityName,
//        webSiteUrl,
//        emailAddress,
//        phoneNumber,
//        faxNumber,
//        buildingN,
//        streetName,
//        localAreaName,
//        cityTown,
//        island,
//        county,
//        stateProvince,
//        country,
//        postCode,
//        tapToBringForward
//    }
//}
extension FacilityLocationTable: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        viewModel.process(textField)
    }
    
}

