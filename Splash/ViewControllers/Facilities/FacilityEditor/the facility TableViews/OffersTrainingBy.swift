//
//  OffersTrainingBy.swift
//  Splash
//
//  Created by Simon Gardener on 16/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class OffersTrainingBy: UITableViewController {
    
    var dataModel:DataModel!
    var frc: NSFetchedResultsController<TrainingAgency>!
//    var trainingAgencies : [TrainingAgency]!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
     //   trainingAgencies = dataModel.allTrainingAgencies()
        frc = dataModel.frcForTrainingAgencies()
        fetchTrainingAgencies()
    }

    fileprivate func fetchTrainingAgencies(){
        do{
            try frc.performFetch()
        }catch {
            print("\(error), \(error.localizedDescription)")
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        guard let sections = frc.sections else { return 0 }
        return sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = frc.sections?[section] else { return 0 }
        return section.numberOfObjects
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)

       let agency = frc.object(at: indexPath)
        cell.textLabel?.text = agency.name
        cell.detailTextLabel?.text = agency.longName
        if (dataModel.facility?.trainingAgency?.contains(agency))!{
            cell.accessoryType = .checkmark
            }else{
            cell.accessoryType = .none
        }

        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let agency = frc.object(at: indexPath)

        if (dataModel.facility?.trainingAgency?.contains(agency))! {
            dataModel.facility?.removeFromTrainingAgency(agency)
        }else {
            dataModel.facility?.addToTrainingAgency(agency)
        }
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return frc.sectionIndexTitles
    }
    override func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return frc.section(forSectionIndexTitle: title, at: index)
    }
}
extension OffersTrainingBy :Injectable{
    func inject(_ dm: DataModel) {
        dataModel = dm
    }
    
    func assertDependencies() {
       assert( dataModel != nil)
        assert(dataModel.facility != nil)
    }
    typealias T = DataModel
}
