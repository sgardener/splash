//
//  CenterTypeChooser.swift
//  Splash
//
//  Created by Simon Gardener on 15/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData
class CenterTypeChooser: UITableViewController {

    var dataModel:DataModel!
    var agency : String!
    var centerTypes = [CenterType]()
    var frc : NSFetchedResultsController<CenterType>!
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        frc = dataModel.frcForCenterTypes(for: agency)
       fetchCenters()
    }
    fileprivate func fetchCenters(){
        do{
            try frc.performFetch()
            
        }catch {
            print("\(error), \(error.localizedDescription)")
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (frc.fetchedObjects?.count)!
        
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
        let center = frc.object(at: indexPath)
        cell.textLabel?.text = center.type ?? "missing center name"
        if (dataModel.facility?.centerType?.contains(center))! {
            cell.accessoryType = .checkmark
        }else { cell.accessoryType = .none}
        return cell
    }
 

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let center = frc.object(at: indexPath)
        if (dataModel.facility?.centerType?.contains(center))! {
            dataModel.facility?.removeFromCenterType(center)
        }else {
            dataModel.facility?.addToCenterType(center)
        }
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }


}
extension CenterTypeChooser : Injectable{
    func inject(_ injected: (dm: DataModel, agency: String)) {
        dataModel = injected.dm
        agency = injected.agency
    }
    
    func assertDependencies() {
        assert(dataModel != nil)
        assert(agency != nil)
        assert(agency.isEmpty == false)
    }
    
    typealias T = (dm: DataModel,agency:String)
    
    
}
