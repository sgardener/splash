//
//  FacilityOffersTable.swift
//  Splash
//
//  Created by Simon Gardener on 15/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class FacilityOffersTable: UITableView, UITableViewDelegate,UITableViewDataSource {

    var viewModel: FacilityOffersViewModel!

    weak var parentView : FacilityDetailsEditor!

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }

 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID , for: indexPath)
        
        cell.textLabel?.text = viewModel.textString(at: indexPath.row)
        if indexPath.row == 0 {
            cell.accessoryType = .disclosureIndicator
        }else {
        cell.accessoryType = viewModel.isCheckmark(at: indexPath.row) ? .checkmark: .none
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
          parentView.performSegue(withIdentifier: "trainingBy", sender: self)
        }else {
            viewModel.flipValue(at: indexPath.row)
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        reloadRows(at: [indexPath], with: .automatic)
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.header()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
