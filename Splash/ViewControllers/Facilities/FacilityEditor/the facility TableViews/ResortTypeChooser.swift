//
//  ResortTypeChooser.swift
//  Splash
//
//  Created by Simon Gardener on 15/04/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class ResortTypeChooser: UITableViewController {
    
    var dataModel:DataModel!
    var agency : String!
    var resortTypes = [ResortType]()
    var frc : NSFetchedResultsController<ResortType>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        frc = dataModel.frcForResortTypes(for: agency)
        fetchResorts()
    }
    fileprivate func fetchResorts(){
        do{
            try frc.performFetch()
            
        }catch {
            print("\(error), \(error.localizedDescription)")
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (frc.fetchedObjects?.count)!
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: solitaryCellTypeID, for: indexPath)
        let resort = frc.object(at: indexPath)
        cell.textLabel?.text = resort.type ?? "missing resort name"
        if (dataModel.facility?.resortType?.contains(resort))! {
            cell.accessoryType = .checkmark
        }else { cell.accessoryType = .none}
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let resort = frc.object(at: indexPath)
        if (dataModel.facility?.resortType?.contains(resort))! {
            dataModel.facility?.removeFromResortType(resort)
        }else {
            dataModel.facility?.addToResortType(resort)
        }
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
}
extension ResortTypeChooser : Injectable{
    func inject(_ injected: (dm: DataModel, agency: String)) {
        dataModel = injected.dm
        agency = injected.agency
    }
    
    func assertDependencies() {
        assert(dataModel != nil)
        assert(agency != nil)
        assert(agency.isEmpty == false)
    }
    
    typealias T = (dm: DataModel,agency:String)
    
    
}
