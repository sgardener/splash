//
//  FacilitiesByRegularTVC.swift
//  Splash
//
//  Created by Simon Gardener on 04/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class FacilitiesByRegularTVC: UITableViewController {
    
    var dataModel : DataModel!
    var regularFacilities = [Facility]()
    let cellID = "cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Regular Facilities"
        assertDependencies()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        regularFacilities = dataModel.regularFacilites()
        tableView.reloadData()
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return dataModel.numberOfFacilitiesMarkedAsRegular()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        
        cell.textLabel?.text = regularFacilities[indexPath.row].name
        
        return cell
    }
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.actualDestination() as? FacilityDetailsTVC else { fatalError("Expecting a SiteDetails scene - didnt get one")}
        guard let indexPath = tableView.indexPathForSelectedRow else {fatalError("Didnt get a valid indexpath")}
        dataModel.facility = regularFacilities[indexPath.row]
        vc.inject(dataModel)
    }
}

extension FacilitiesByRegularTVC : Injectable {
    func inject(_ dm : DataModel) {
        dataModel = dm
    }
    func assertDependencies() {
        assert(dataModel != nil , "Expected datamodel got nil")
    }
    typealias T = DataModel
}

