//
//  FacilitiesByNearbyVC.swift
//  Splash
//
//  Created by Simon Gardener on 04/03/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import BLTNBoard

class FacilitiesByNearbyVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageLabel: UILabel!
    
    var dataModel: DataModel!
    var locationService: LocationService!
    var searchComplete = false
    var coordinateRange: CoordinateRange!
    var facilities = [Facility]()
    let headerTitles = ["known local dive ops"]
    let cellID = "cell"
    
    lazy var bulletinManager: BLTNItemManager = {
        var page = BulletinDataSource.locationWasDeniedPage()
        return BLTNItemManager(rootItem: page)
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assertDependencies()
        title = "Nearby Facilities"
        setUpView()
        locationService = LocationService(owner: self, desiredAccuracy: 50, within: 100)
        locationService.requestLocation()
    }
    
    func setUpView(){
        setUpMessageLabel()
        setUpTableView()
        updateView()
    }
    
    func setUpMessageLabel(){
        if searchComplete == false {
            messageLabel.text = "Waiting on GPS.\n\nAre you inside ?"
        }else {
            messageLabel.text = "Splash doesn't know any local dive operations.\n\n If you are at one, please add it so it will show up next time.\n\n Also show the app to the shop staff and get them to fill out a detailed entry."
        }
    }
    private func setUpTableView() {
        tableView.tableFooterView = UIView()

    }
    
    func updateView() {
        tableView.isHidden = !hasFacilities
        messageLabel.isHidden = hasFacilities
        setUpMessageLabel()
        tableView.reloadData()
    }
    
    private var hasFacilities: Bool {
        if facilities.count == 0 {
            return false
        }else {
            return true
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.actualDestination() as? FacilityDetailsTVC else { fatalError("Expecting a Facility Details scene - didnt get one")}
        guard let indexPath = tableView.indexPathForSelectedRow else {fatalError("Didnt get a valid indexpath")}
        dataModel.facility = facilities[indexPath.row]
        vc.inject(dataModel)
        
    }
    
    
}
//MARK: - TableView Delegate
extension FacilitiesByNearbyVC: UITableViewDelegate {
    
}
//MARK: - TableView DataSource
extension FacilitiesByNearbyVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return facilities.count

    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
                return headerTitles[section]
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        
        cell.textLabel?.text = facilities[indexPath.row].name
        var facilityAddressLines = [String?]()
        facilityAddressLines.append(facilities[indexPath.row].localAreaName)
        facilityAddressLines.append(facilities[indexPath.row].islandAtoll)
        facilityAddressLines.append(facilities[indexPath.row].cityTown)
        
        cell.detailTextLabel?.text = facilityAddressLines.compactMap{$0}.joined(separator: ",  ")
        
        return cell
    }
}
//MARK: - Needs
extension FacilitiesByNearbyVC: LocationUser {
    func partialFix(location: CLLocation) {
        
    }
    func locationWasDenied(){
        bulletinManager.showBulletin(above: self)
    }
    
    func locationDidUpdate(location: CLLocation) {
        
        coordinateRange = CoordinateRange.init(withCenter: location, offset: 0.5)
        let rangePredicate = coordinateRange.searchPredicates()
        if var facilitesInRange = dataModel.allFacilitiesIn(range: rangePredicate){
            Splash.sortFetched(nearbyFacilities: &facilitesInRange, byDistanceFrom: location)
            facilities = facilitesInRange
        }
        searchComplete = true
        updateView()
    }
 
}
//MARK: - Injectable
extension FacilitiesByNearbyVC: Injectable{
    func inject(_ dm: DataModel) {
        dataModel = dm
    }
    
    func assertDependencies() {
        assert(dataModel != nil, "expected a dataModel - didnt get one")
    }
    
    typealias T = DataModel
    
}
