//
//  DistancesViewController.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 21/01/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData
import BLTNBoard

enum WhichEntity {
    case diveSite
    case facility
}
enum ToOrFrom {
    case from
    case to
}

class DistancesViewController: UIViewController , LocationUser, NeedsDataModel {
    
    
    var locationService: LocationService!
    var dataModel: DataModel!
    
    //MARK: - Enums
    
    fileprivate enum From: Int{
        case userLocation
        case diveSite
        case diveFacility
    }
    fileprivate enum To: Int {
        case diveSite
        case diveFacility
    }
    
    
    let countryListSegue = "CountryListForDistance"
    //MARK: - Properties
    
    var container: NSPersistentContainer!
    
    var entityType : WhichEntity = .diveSite
    var toOrFrom : ToOrFrom = .to
    
    var fromLocation: CLLocation? {
        didSet{
            calculateAndDisplayDistanceBetween(startLocation: fromLocation, endLocation: toLocation)
        }
    }
    var toLocation: CLLocation? {
        didSet{
            calculateAndDisplayDistanceBetween(startLocation: fromLocation, endLocation: toLocation)
        }
    }
    
    @IBOutlet weak var fromLocationSegmentedControl: UISegmentedControl!
    @IBOutlet weak var toLocationSegmentedController: UISegmentedControl!
    
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    lazy var bulletinManager: BLTNItemManager = {
        var page = BulletinDataSource.locationWasDeniedPage()
        return BLTNItemManager(rootItem: page)
    }()
    
    //MARK: - code
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        locationService = LocationService(owner: self, desiredAccuracy: kCLLocationAccuracyHundredMeters , within: 100)
        assertDependencies()
        container = dataModel.container
        toLocationSegmentedController.isMomentary = true
        fromLocationSegmentedControl.isMomentary = true
        setUpNavBar()
    }
    fileprivate  func setUpNavBar(){
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    //    fileprivate func adjustMaxFontSize(){
    //        if let font = toLabel.font {
    //            let newFont = 
    //        }
    //    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func fromLocationChanged(_ sender: UISegmentedControl) {
        toOrFrom = .from
        switch sender.selectedSegmentIndex  {
        case From.userLocation.rawValue :
            print ("tapped user location")
            locationService.requestLocation()
            fromLabel.text = "Seeking GPS location"
            distanceLabel.text = "----"
            
        case From.diveSite.rawValue :
            entityType = .diveSite
            performSegue(withIdentifier: countryListSegue, sender: self)
            
        case From.diveFacility.rawValue :
            entityType = .facility
            performSegue(withIdentifier: countryListSegue, sender: self)
            
        default:
            fatalError("non valid From segment value")
        }
    }
    
    @IBAction func toLocationChanged(_ sender: UISegmentedControl) {
        toOrFrom = .to
        
        switch sender.selectedSegmentIndex {
        case To.diveSite.rawValue :
            entityType = .diveSite
            performSegue(withIdentifier: countryListSegue, sender: self)
            
        case To.diveFacility.rawValue :
            
            entityType = .facility
            performSegue(withIdentifier: countryListSegue, sender: self)
            
        default:
            fatalError("non valid To segment value")
        }
    }
    
    func locationDidUpdate(location: CLLocation) {
        fromLocation = location
        let locationString = String(format: "lat: %.4f, long: %.4f",location.coordinate.latitude,location.coordinate.longitude)
        fromLabel.text = locationString
        
    }
    func partialFix(location: CLLocation) {
        
    }
    func locationWasDenied(){
        bulletinManager.showBulletin(above: self)
    }
    
    
    func calculateAndDisplayDistanceBetween(startLocation: CLLocation?, endLocation: CLLocation?){
        guard startLocation != nil,  endLocation != nil  else { return}
        let  distance = startLocation!.distance(from: endLocation!) as Double
        distanceLabel.text = distance.formattedDistanceString()
    }
    
    // MARK: - Navigation
    @IBAction func backToDistance(segue: UIStoryboardSegue){
        print ("and I'm back")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let destination = segue.destination as? CountryListForDistanceTVC else {return}
        let context = (container.viewContext)        
        destination.inject((entityType: entityType, toOrFrom: toOrFrom, delegate: self, context: context ))
    }
}


//MARK: - ReceivesLocationChoices

extension DistancesViewController : ReceivesLocationChoice {
    func setFromLocationTo(diveSite: DiveSite) {
        fromLabel.text = diveSite.name
        fromLocation = CLLocation(latitude: diveSite.latitude as! CLLocationDegrees, longitude: diveSite.longitude as! CLLocationDegrees)
    }
    
    func setFromLocationTo(facility: Facility) {
        fromLabel.text = facility.name
        fromLocation = CLLocation(latitude: facility.latitude as! CLLocationDegrees, longitude: facility.longitude as! CLLocationDegrees)
    }
    
    func setToLocationTo(diveSite: DiveSite) {
        toLabel.text = diveSite.name
        toLocation = CLLocation(latitude: diveSite.latitude as! CLLocationDegrees, longitude: diveSite.longitude as! CLLocationDegrees)
    }
    
    func setToLocationTo(facility: Facility) {
        toLabel.text = facility.name
        toLocation = CLLocation(latitude: facility.latitude as! CLLocationDegrees, longitude: facility.longitude as! CLLocationDegrees)
    }
}

//MARK: - Injectable

extension DistancesViewController : Injectable {
    func inject(_ dm: DataModel) {
        dataModel = dm
    }
    
    func assertDependencies() {
        assert(dataModel != nil, "Bugger, its nil")
    }
    
}



