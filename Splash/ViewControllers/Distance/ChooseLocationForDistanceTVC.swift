//
//  ChooseLocationForDistanceTVC.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 21/01/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation


class ChooseLocationForDistanceTVC: UITableViewController, NSFetchedResultsControllerDelegate {
    let firstLetterKeyPath = "firstLetterForIndex"
    var entityType: WhichEntity!
    var toOrFrom: ToOrFrom!
    var context: NSManagedObjectContext!
    weak var delegate: ReceivesLocationChoice!
    var country: String!
    
    let unwindSegue = "unwindToBackToDistance"
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchLocation()
        
    }
    
    func fetchLocation(){
        if entityType == .diveSite {
            fetchSiteLocations()
        }else {
            fetchFacilityLocations()
        }
    }
    func fetchSiteLocations() {
        do {
            try frcSite.performFetch()
        } catch {
            print("\(error), \(error.localizedDescription)")
        }
        
    }
    func fetchFacilityLocations (){
        do {
            try frcFacility.performFetch()
        } catch {
            print("\(error), \(error.localizedDescription)")
        }
    }

    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        switch entityType {
        case .diveSite?:
            return frcSite.sections!.count
        default:
              return frcFacility.sections!.count
        }
      
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var secInfo : NSFetchedResultsSectionInfo?
        
        if entityType == .diveSite {
            secInfo = frcSite.sections?[section]
        } else {
            secInfo = frcFacility.sections?[section]
        }
        return (secInfo?.numberOfObjects)!
    }
    
 
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if entityType == .diveSite {
            let diveSite = frcSite.object(at: indexPath)
            cell.textLabel?.text = diveSite.name
            cell.detailTextLabel?.text = diveSite.localAreaName
        } else {
            let facility = frcFacility.object(at: indexPath)
            cell.textLabel?.text = facility.name
            cell.detailTextLabel?.text = facility.localAreaName
        }
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var secInfo : NSFetchedResultsSectionInfo?
        if entityType == .diveSite {
            secInfo = frcSite.sections?[indexPath.section]
            let diveSite = secInfo?.objects![indexPath.row] as! DiveSite
            if toOrFrom == .to {
                delegate.setToLocationTo(diveSite: diveSite)
            }else {
                delegate.setFromLocationTo(diveSite: diveSite)
            }
        } else {
            secInfo = frcFacility.sections?[indexPath.section]
            let facility = secInfo?.objects![indexPath.row] as! Facility
            if toOrFrom == .to {
                delegate.setToLocationTo(facility: facility)
            }else {
                delegate.setFromLocationTo(facility: facility)
            }
        }
        //unwind to segue
        performSegue(withIdentifier: unwindSegue, sender: self)
    }
    //MARK:- Section header stuff
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch entityType {
        case .diveSite? :
            return  frcSite.sections![section].name
        default:
            return  frcFacility.sections![section].name

        }
    }
    override func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        switch entityType {
        case .diveSite?:
            return frcSite.section(forSectionIndexTitle: title, at: index)

        default:
            return frcFacility.section(forSectionIndexTitle: title, at: index)

        }
    }

    //MARK:- Fetch stuff
    
    private lazy var frcSite: NSFetchedResultsController<DiveSite> = {
        let fetchRequest: NSFetchRequest<DiveSite> = DiveSite.fetchRequest()
        let predicate = NSPredicate(format: "country = %@ && latitude != nil && gpsFix != 0 && gpsFix != 8", country )
    
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(DiveSite.name), ascending: true)]
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: context,
                                                                  sectionNameKeyPath: firstLetterKeyPath,
                                                                  cacheName: nil)
        
        fetchedResultsController.delegate = self
        return fetchedResultsController
    }()
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        switch entityType {
        case .diveSite? :
            return frcSite.sectionIndexTitles
        default:
            return frcFacility.sectionIndexTitles
        }
    }

//    accurateLocationPredicate = [NSPredicate predicateWithFormat:@"country = %@ && latitude != nil && keepLocationSecret == 0  &&  gpsFix != %d && gpsFix != %d",self.countryName,GPSFixIsUnknown,GPSFixIsNowhereNear];

    private lazy var frcFacility: NSFetchedResultsController<Facility> = {
        
        let fetchRequest :NSFetchRequest<Facility> = Facility.fetchRequest()
        let predicate = NSPredicate(format: "country = %@ && latitude != nil", country)
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(Facility.name), ascending: true)]
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: context,
                                                                  sectionNameKeyPath: firstLetterKeyPath,
                                                                  cacheName: nil)
        
        fetchedResultsController.delegate = self
        return fetchedResultsController
    }()
    
  


}

extension ChooseLocationForDistanceTVC: Injectable {
    
    func inject(_ injected: (entityType: WhichEntity, toOrFrom: ToOrFrom, delegate: ReceivesLocationChoice, country: String, context: NSManagedObjectContext)) {
        entityType = injected.entityType
        context = injected.context
        toOrFrom = injected.toOrFrom
        delegate = injected.delegate
        country  = injected.country
    }
    
    func assertDependencies() {
        assert (entityType != nil, "ChooseLocationForDistanceTVC Not passed an entity type")
        assert (context != nil, "ChooseLocationForDistanceTVC not passed a context")
        assert (toOrFrom  != nil, "ChooseLocationForDistanceTVC not passed a toOrFrom")
        assert (delegate != nil, "ChooseLocationForDistanceTVC not passed a delegate")
        assert (country != nil,"ChooseLocationForDistanceTVC not passed a country")
    }
    
    typealias T = (entityType: WhichEntity, toOrFrom: ToOrFrom, delegate: ReceivesLocationChoice ,country: String, context: NSManagedObjectContext)
}

