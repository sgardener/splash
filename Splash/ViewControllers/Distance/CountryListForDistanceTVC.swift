//
//  CountryListForDistanceTVC.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 21/01/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class CountryListForDistanceTVC: UITableViewController, NSFetchedResultsControllerDelegate {
    
    weak var delegate: ReceivesLocationChoice?
    var entityType: WhichEntity!
    var toOrFrom : ToOrFrom!
    var country : String?
    var context: NSManagedObjectContext!
    let locationListSegue = "ChooseLocationForDistance"
    //            accurateLocationPredicate = NSPredicate(format: "latitude != %@", nil)
    
    //            accurateLocationPredicate = NSPredicate(format: "latitude != %@ && gpsFix != %d && gpsFix != %d", nil, GPSFix.isUnknown.rawValue, GPSFix.isNowhereNear.rawValue)
    
    //MARK: - Fetching Stuff
    
    private lazy var frcSite: NSFetchedResultsController<DiveSite> = {
        
        let fetchRequest: NSFetchRequest<DiveSite> = DiveSite.fetchRequest()
        let predicate = NSPredicate(format: "latitude != nil && gpsFix != 0 && gpsFix != 8") // 0 = unkown 8 = nowhere near
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(DiveSite.country), ascending: true)]
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: context,
                                                                  sectionNameKeyPath: #keyPath(DiveSite.country),
                                                                  cacheName: nil)
        
        fetchedResultsController.delegate = self
        
        return fetchedResultsController
    }()
    
    private lazy var frcFacility: NSFetchedResultsController<Facility> = {
        
        let fetchRequest :NSFetchRequest<Facility> = Facility.fetchRequest()
        let predicate = NSPredicate(format: "latitude != nil")
        fetchRequest.predicate = predicate
        
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(Facility.country), ascending: true)]
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: context,
                                                                  sectionNameKeyPath: #keyPath(Facility.country),
                                                                  cacheName: nil)
        
        fetchedResultsController.delegate = self
        
        return fetchedResultsController
        
    }()
    

    
    fileprivate func fetchCountryforFacilities() {
        do {
            try frcFacility.performFetch()
        } catch {
            print("\(error), \(error.localizedDescription)")
        }
    }
    fileprivate func fetchCountryForDiveSites() {
        do {
            try frcSite.performFetch()
        } catch {
            print("\(error), \(error.localizedDescription)")
        }
    }

    private func fetchCountries() {
       
        if entityType == .diveSite {
            fetchCountryForDiveSites()
        }else {
            fetchCountryforFacilities()
        }
    }
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearsSelectionOnViewWillAppear = false
        fetchCountries()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if entityType == .diveSite {
            return (frcSite.sections?.count)!
        } else {
            return (frcFacility.sections?.count)!
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        var sectionInfo : NSFetchedResultsSectionInfo?
        if entityType == .diveSite {
            sectionInfo = frcSite.sections?[indexPath.section]
        } else {
            sectionInfo = frcFacility.sections?[indexPath.section]
        }
        cell.textLabel?.text = sectionInfo?.name
        cell.accessoryType = .disclosureIndicator
        cell.detailTextLabel?.text = "\((sectionInfo?.numberOfObjects)! ) locations."
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if entityType == .diveSite {
            country = frcSite.sections?[indexPath.section].name
        } else {
            country = frcFacility.sections?[indexPath.section].name
        }
        performSegue(withIdentifier: locationListSegue, sender: self)
        
    }
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destination = segue.destination as? ChooseLocationForDistanceTVC else { fatalError("Unexpected VC TYPE")}
        destination.inject((entityType: entityType, toOrFrom: toOrFrom , delegate: delegate!, country: country!, context: context ))
    }
}

extension   CountryListForDistanceTVC  : Injectable {
    func inject(_ injected: (entityType: WhichEntity, toOrFrom: ToOrFrom, delegate: ReceivesLocationChoice,  context: NSManagedObjectContext)) {
        entityType = injected.entityType
        context = injected.context
        toOrFrom = injected.toOrFrom
        delegate = injected.delegate
    }
    
    func assertDependencies() {
        assert (entityType != nil, "CountryListForDistanceTVC Not passed an entity type")
        assert (context != nil, "CountryListForDistanceTVC not passed a context")
        assert (toOrFrom  != nil, "CountryListForDistanceTVC not passed a toOrFrom")
        assert (delegate != nil, "CountryListForDistanceTVC not passed a delegate")
        
    }
    typealias T = (entityType: WhichEntity, toOrFrom:ToOrFrom, delegate: ReceivesLocationChoice,  context: NSManagedObjectContext)
}
