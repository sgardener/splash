//
//  SplashTabBarController.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 16/11/2017.
//  Copyright © 2017 Simon Gardener. All rights reserved.
//

import UIKit
import CoreData

class SplashTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    var dataModel: DataModel!
    
    var container: NSPersistentContainer!
    
    fileprivate func passOnDataModel() {
        for controller in viewControllers! {
            if let navController = controller as? UINavigationController{
                if var needsDataModel = navController.topViewController  as? NeedsDataModel {
                    needsDataModel.dataModel = dataModel
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        delegate = self
        assertDependencies()
        revertToSavedOrder()
        removeHomeAndDiveFromCustomizableViewControllers()
        container = dataModel.container

        passOnDataModel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController){
    dataModel.saveContext()
    }
    
    //MARK:- UITab Bar Reordering
    func removeHomeAndDiveFromCustomizableViewControllers(){
        customizableViewControllers?.remove(at: 0)
        customizableViewControllers?.remove(at: 0)
    }
   
    func tabBarController(_ tabBarController: UITabBarController, didEndCustomizing viewControllers: [UIViewController], changed: Bool) {
        var titles = [String]()
        for vc in viewControllers{
            guard let title = vc.title else {fatalError("doesnt have a tittle and should")}
            titles.append(title)
        }
        UserDefaults.setTabBarOrderArray(titles)
    }
  
    func revertToSavedOrder(){
        guard let savedOrder = UserDefaults.tabBarOrderArray() else { return }
        var rearranged = [UIViewController]()
        for title in savedOrder{
            for vc in viewControllers! {
                if vc.title == title{
                    rearranged.append(vc)
                }
            }
        }
        viewControllers = rearranged
    }
    
 
}
//MARK:- Dependency checker

extension SplashTabBarController : Injectable{    typealias T = DataModel
    
    func inject(_ dm: DataModel) {
        dataModel = dm
    }
    
    func assertDependencies() {
        assert(dataModel != nil, "CoreDataManage is NIL")
    }
}
