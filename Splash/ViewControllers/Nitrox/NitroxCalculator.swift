//
//  NitroxCalculator.swift
//  Splash(Swift)
//
//  Created by Simon Gardener on 12/01/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit

class NitroxCalculator: UIViewController {
    
//    MARK: - Properties
    @IBOutlet weak var maxPP: UISegmentedControl!
    @IBOutlet weak var measurementUnits: UISegmentedControl!
    
    @IBOutlet weak var oxygenPercentLabel: UILabel!
    @IBOutlet weak var oxygenPercentSlider: UISlider!
    @IBOutlet weak var modAtPressureLabel: UILabel!
    @IBOutlet weak var modResultLabel: UILabel!
    
    @IBOutlet weak var depthLabel: UILabel!
    @IBOutlet weak var depthSlider: UISlider!
    @IBOutlet weak var bestMixResultLabel: UILabel!
    
    var isSalty = true
    var isMetric = UserDefaults.depthUnits() == .metric
    var maxPP02 = 1.4
    
    //MARK: - Code Starts Here
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavBar()
        updateDisplayForMOD()
        updateDisplayForBestMix()
    }
    fileprivate  func setUpNavBar(){
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    @IBAction func switchBetweenRecAndDecoMode(_ sender: UIBarButtonItem) {
        if sender.title == "use higher 0₂ %" {
            sender.title = "use lower 0₂ %";
            oxygenPercentSlider.maximumValue = 100.0
            oxygenPercentSlider.minimumValue = 40.0
            updateDisplayForMOD()
        } else {
            sender.title = "use higher 0₂ %"
            oxygenPercentSlider.minimumValue = 21.0
            oxygenPercentSlider.maximumValue = 40.0
            updateDisplayForMOD()
        }
    }
    
    @IBAction func unitsChanged(_ sender: UISegmentedControl) {
        isMetric =  sender.selectedSegmentIndex == 0 ? true :  false
        depthSlider.maximumValue = isMetric ? 66.0 : 216.0
        updateDisplayForMOD()
        updateDisplayForBestMix()
    }
    
    @IBAction func maxPP02Changed(_ sender: UISegmentedControl) {
        maxPP02 = sender.selectedSegmentIndex == 0 ? 1.4 : 1.6
        updateDisplayForMOD()
        updateDisplayForBestMix()
    }
    
    @IBAction func oxygenContentChanged(_ sender: UISlider) {
        updateDisplayForMOD()
    }
    
    @IBAction func depthDidChange(_ sender: UISlider) {
        updateDisplayForBestMix()
    }
    //MARK: - Display Update Methods
    func updateDisplayForMOD() {
        
        oxygenPercentLabel.text = "Oxygen content: \(Int(oxygenPercentSlider.value))%"
        let modResult = calculateMOD()
        let unitString = isMetric ? "Meters" : "Feet"
        modResultLabel.text = String(format:"%.2f %@:", modResult, unitString)
        modAtPressureLabel.text = "MOD at \(maxPP02) bar"
    }
    
    func updateDisplayForBestMix() {
        
        let unitString = isMetric ? "Meters" : "Feet"
        depthLabel.text = "Depth: \(Int(depthSlider.value)) \(unitString)"
        
        if depthSlider.value < 1 {
            bestMixResultLabel.text = "Air"
        } else {
            let bestMix = calculateBestMix()
            
            switch bestMix {
            case 100 :
                bestMixResultLabel.text = "Oxygen"
            case 22...99 :
                depthSlider.tintColor = view.tintColor
                bestMixResultLabel.text = "Nitrox \(bestMix)"
            case 21:
                bestMixResultLabel.text = "Air"
                depthSlider.tintColor = view.tintColor
            default :
                bestMixResultLabel.text = "Air (pp02 > 1.4)"
                depthSlider.tintColor = .red
            }
        }
    }
//    MARK:- calculations
    
    func calculateMOD()-> Double {
        
        let oxygenPercentage = Double(Int(oxygenPercentSlider.value))
        var mod = 0.0
        if isMetric {
            mod = 10 * (maxPP02 / (oxygenPercentage/100) - 1);
        }else{
            mod = 33*( maxPP02 / (oxygenPercentage/100) - 1);
        }
        return mod;
    }
    
    func calculateBestMix()-> Int {
        
        var depth = Double(depthSlider.value)
        depth = Double(Int(isMetric ? depth : depth/3.281))
        let bestMix = Int(maxPP02 / (depth/10 + 1) * 100)
        return bestMix < 100 ? bestMix : 100
    }
    
}
