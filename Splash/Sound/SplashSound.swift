//
//  SplashSound.swift
//  Splash
//
//  Created by Simon Gardener on 21/02/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation
import AudioToolbox

struct SplashSound {
    static let splashFileName = "splash"
    static let fileExtension = "mp3"
    
    static func play() {
    if let splashSoundUrl = Bundle.main.url(forResource: splashFileName , withExtension: fileExtension) {
        var customSoundId: SystemSoundID = 0
        AudioServicesCreateSystemSoundID(splashSoundUrl as CFURL, &customSoundId)
      
        
        AudioServicesAddSystemSoundCompletion(customSoundId, nil, nil, { (customSoundId, _) -> Void in AudioServicesDisposeSystemSoundID(customSoundId)}, nil)
        
          AudioServicesPlaySystemSound(customSoundId)
    }else {
        fatalError("didnt get sound file")
        }
    }
}
