//
//  Submitter.swift
//  Splash
//
//  Created by Simon Gardener on 11/05/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
import MessageUI

class Submitter {
    
    var dataModel: DataModel!
    
    var siteData : Data?
    var facilityData : Data?
    var confirmData : Data?
    
    let facilitiesFileName = "facilities.csv"
    let sitesFileName = "sites.csv"
    let confirmsFileName = "confirms.csv"
    let confirmsCSVFirstLine = "siteId,siteName,latitude,longitude,accessBy,gpsFix,whoSubbed\n"
    let facilityCSVFirstLine = "FacilityIDNumber,country,name,longitude,latitude,buildingNameNumber,streetName,localAreaName,cityTown,islandAtoll,county,stateProvince,postcode,website,email,phoneNumber,faxNumber,note,resortTypes,centerType,trainingAgencies,facilityType,facilityOffers,whoSubbed,noteAssocName,noteAssocWeb,dateLogged,dateUpdatedByUser,attributesChanged\n"
    let siteCSVFirstLine = "diveSiteID,name,alternativeName,country,longitude,latitude,localAreaName,islandAtoll,bodyOfWater,cityTown,county,stateProvince,note,GPSFixIs,accessBy,located,environment,features,bottomComp,suitableFor,salinity,averageDepthRangeDeeper,averageDepthRangeShallower,maximumDepth,minimumDepth,temperatureRangeHigh,temperatureRangeLow,highSeasonVisibilityRangeHigh,highSeasonVisibilityRangeLow,lowSeasonVisibilityRangeHigh,lowSeasonVisibilityRangeLow,currentFrequency,currentStrength,mantaPossibility,sharkPossibility,turtlePossibility,technicalDive,whoSubbed,noteAssocName,noteAssocWeb,dateLogged,dateUpdatedByUser,secretSite,attributesChanged\n"
    
    /// prepareCSVfile uwraps up unsubmitted site and facilities as Data
    ///
    /// - Returns: a MailComposerViewController configured with site and facility files
    func prepareCSVfiles()-> MFMailComposeViewController{
        
        let sites = dataModel.sitesThatNeedSubmitting()
        if let sitesToSubmit = wrap(sites){
            siteData = sitesToSubmit.data(using: String.Encoding.utf8)
        }
        let facilities = dataModel.facilitiesThatNeedSubmitting()
        if let facilitiesToSubmit = wrap(facilities) {
            facilityData = facilitiesToSubmit.data(using: String.Encoding.utf8)
        }
        let confirms = dataModel.fetchEntity(ConfirmValues.self , predicate: nil)
        if let confirmToSubmit = wrap(confirms) {
            confirmData = confirmToSubmit.data(using: String.Encoding.utf8)
        }
        return getMailComposerForCSVSubmission()
    }
    
    fileprivate func wrap(_ sites:[DiveSite])->String?{
        guard sites.count > 0 else { return nil}
        
        var allSites = siteCSVFirstLine
        for site in sites{
            allSites = allSites.appending(SiteWrapper.wrap(site))
        }
        return allSites
    }
    fileprivate func wrap(_ facilities:[Facility])-> String?{
        guard facilities.count > 0 else { return nil }
        var allFacilities = facilityCSVFirstLine
        for facility in facilities {
            allFacilities = allFacilities.appending(FacilityWrapper.wrap(facility))
        }
        return allFacilities
    }
    fileprivate func wrap(_ confirms: [ConfirmValues])->String?{
        guard confirms.count > 0 else {return nil}
        var allConfirms = confirmsCSVFirstLine
        for confirm in confirms {
            allConfirms = allConfirms.appending(ConfirmWrapper.wrap(confirm))
        }
        return allConfirms
    }
 
    //MARK:- Mailer Section
    
    fileprivate func getMailComposerForCSVSubmission()-> MFMailComposeViewController{
        let mailer = MFMailComposeViewController()
        mailer.setSubject("Splash Data")
        mailer.setToRecipients(["splashdivelog@icloud.com"])
        if let siteData  = siteData {
            mailer.addAttachmentData(siteData, mimeType: "text/csv", fileName: sitesFileName)
        }
        if let facilityData = facilityData {
            mailer.addAttachmentData(facilityData, mimeType: "text/csv", fileName: facilitiesFileName)
        }
        if let confirmData = confirmData {
            mailer.addAttachmentData(confirmData, mimeType: "text/csv", fileName: confirmsFileName)
        }
        mailer.setMessageBody("Data from Splash Dive Log", isHTML: false)
        return mailer
    }
    
   
}
