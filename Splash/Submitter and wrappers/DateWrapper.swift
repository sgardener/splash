//
//  DateWrapper.swift
//  Splash
//
//  Created by Simon Gardener on 10/05/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation

struct DateWrapper{
    
    /// method for getting a human readable data string for wrapping in a csv
    ///
    /// - Parameter date: optional -
    /// - Returns: empty string if date is nil; otherwise a date in format"YYYY-MMM-dd - HH:mm"
    static func dateStringForCSV(from date:Date?)-> String{
        guard  let date = date else {return ""}
        let df = DateFormatter()
        df.dateFormat = "YYYY-MMM-dd - HH:mm"
        return df.string(from: date)
    }
    static func dateFromCSVFormat(date:String)-> Date?{
        guard date.isEmpty == false else {return nil}
        let df = DateFormatter()
        df.dateFormat = "YYYY-MMM-dd - HH:mm"
        let recreatedDate = df.date(from: date)
        return recreatedDate
    }

}
