//
//  ConfirmWrapper.swift
//  Splash
//
//  Created by Simon Gardener on 19/12/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import UIKit
struct ConfirmWrapper {
    static func wrap(_ confirmValues: ConfirmValues )-> String{
        let sendCoordinate = confirmValues.latitude != 0.0 && confirmValues.longitude != 0.0
        var latitude = ""
        var longitude = ""
        var gpsFix = ""
        if sendCoordinate {
            latitude = "\(confirmValues.latitude)"
            longitude = "\(confirmValues.longitude)"
            gpsFix = "\(confirmValues.gpsFix)"
        }
        return "\(confirmValues.siteID!),\(confirmValues.siteName!),\(latitude),\(longitude),\(confirmValues.accessBy),\(gpsFix),\(UIDevice.current.identifierForVendor!.uuidString)\n  "
    }
}
