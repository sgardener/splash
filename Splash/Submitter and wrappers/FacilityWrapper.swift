//
//  FacilityWrapper.swift
//  Splash
//
//  Created by Simon Gardener on 07/05/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation

struct FacilityWrapper{
    /// Takes a Facility NSManagedObject and wraps its values up as a single line string.
    /// Used by caller to make a csv file of facilities
    static func wrap(_ facility: Facility) -> String {
    
        return "\(facility.id ?? "Error no facility id"),\(facility.country!),\(StringWranglers.cleanForCSV(for: facility.name)),\(facility.longitude!.stringValue),\(facility.latitude!.stringValue),\(StringWranglers.cleanForCSV(for: facility.buildingNameNumber)),\(StringWranglers.cleanForCSV(for: facility.streetName )),\(StringWranglers.cleanForCSV(for: facility.localAreaName )),\(StringWranglers.cleanForCSV(for: facility.cityTown)),\(StringWranglers.cleanForCSV(for: facility.islandAtoll)),\(StringWranglers.cleanForCSV(for: facility.county )),\(StringWranglers.cleanForCSV(for: facility.stateProvince)),\(StringWranglers.cleanForCSV(for: facility.postcode)),\(StringWranglers.cleanForCSV(for: facility.website)),\(StringWranglers.cleanForCSV(for: facility.email )),\(StringWranglers.cleanForCSV(for: facility.phoneNumber)),\(StringWranglers.cleanForCSV(for: facility.faxNumber )),\(StringWranglers.cleanForCSV(for: facility.note)),\(generateResortString(for: facility)),\(generateCenterType(for: facility)),\(generateTrainingAgency(for: facility)),\(generateTypeString(for: facility)),\(generateOffersString(for: facility)),\(StringWranglers.cleanForCSV(for: facility.whoSubbed )),\(noteAssociatedName()),\(noteAssociatedWeb()),\(DateWrapper.dateStringForCSV(from: facility.dateLogged)),\(DateWrapper.dateStringForCSV(from: facility.dateUpdatedByUser)),\( facility.changedAttributes ?? "")\n"

    }
    
    static func generateTrainingAgency(for facility: Facility)-> String {
        var agencyString = ""
        guard let agencies = facility.trainingAgency as? Set<TrainingAgency>, agencies.count > 0  else {return ""}
        for agency in agencies {
            agencyString = agencyString.appending("\(agency.name!)/")
        }
        agencyString.removeLast()
        return agencyString
    }
    
    static func generateCenterType(for facility:Facility)-> String{
        var centersString = ""
        
        guard let centers = facility.centerType as? Set<CenterType>, centers.count > 0 else { return "" }
        for center in centers {
            centersString = centersString.appending("\(center.type!)/")
        }
        centersString.removeLast()
        return centersString
    }
    static func generateResortString(for facility: Facility)-> String {
        var resortsString = ""
        guard let resorts = facility.resortType  as? Set<ResortType>, resorts.isEmpty == false else { return ""}
        for resort in resorts {
            resortsString = resortsString.appending("\(resort.type!)/")
        }
        resortsString.removeLast()
        return resortsString
    }
    static func generateTypeString(for facility:Facility)-> String{
        var type = ""
        let typeValue = Int(facility.type)
        if typeValue == FacilityTypeValues.unknown.rawValue {
            type = "unknown"
        }
        else {
            for i in 0...(FacilityTypeViewModel.facilityTypes.count-1) {
                let value = Int(pow(2,Double(i)))
                if typeValue & value == value {
                    type = type.appending("\(FacilityTypeViewModel.facilityTypes[i])/")
                }
            }
            type.removeLast()
        }
        return type
    }
    
    static func generateOffersString(for facility: Facility)-> String{
        var offers = ""
        let offerValue = Int64(facility.offers)
        if offerValue == FacilityOffersValues.offersUnknown.rawValue { offers = "unknown"
        } else {
            for i in 0...FacilityOffersViewModel.facilityOffers.count-1{
                let value = Int64(pow(2,Double(i)))
                if offerValue  & value == value {
                    offers = offers.appending("\(FacilityOffersViewModel.facilityOffers[i])/")
                }
            }
            offers.removeLast()
        }
        return offers
    }
    
    static func noteAssociatedName()-> String{
        guard let name = UserDefaults.associatedName() else { return ""}
        return StringWranglers.cleanForCSV(for: name)
  
    }
    
    static func noteAssociatedWeb()-> String {
        guard let web = UserDefaults.associateWeb() else { return ""}
       return StringWranglers.cleanForCSV(for: web)
    }
    
}
