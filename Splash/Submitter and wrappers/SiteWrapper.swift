//
//  SiteWrapper.swift
//  Splash
//
//  Created by Simon Gardener on 07/05/2018.
//  Copyright © 2018 Simon Gardener. All rights reserved.
//

import Foundation

struct SiteWrapper {
    
    static let accessByOptions = ["shore","RIB","outrigger","longtail","speedboat","day boat","liveaboard"]
    static let suitableOptions = ["Beginners","Recreational","Technical", "Night", "Snorkelling", "Freediving"]
    static let gpsIsOptions = ["AboveSite","ShoreEntryPoint","NearbyMooring","NowhereNear"]
    static let salinityOptions = ["Sea","Fresh","Brackish","MixedLayers","HighlySalted"]
    static let locationOptions = ["Inland", "Coastal", "Island", "Offshore"]
    static let environmentOption = ["Bay", "Channel", "Lagoon","Cenote", "Lake", "Quarry", "Reservoir", "River", "Estuary", "Mangrove", "Fjord", "Seamount","Offshore Reef", "Atoll","Fringing Reef", "Barrier Reef"]
    static let featureOptions = ["Altitude", "Boulders", "CoralGarden", "Cave", "Cavern", "Drift", "Dropoff", "Ice", "Kelp", "Muck","Pinnacle","Plateau" ,"Slope", "Swimthru", "Wall", "Wreck"]
    static let bottomOptions = ["Sand","Rock","Mud","Coral","SeaGrass","NoBottom"]
    
    static let currentStrength = ["N/A","Mild","Mild to Steady","Mild to Strong","Mild to Insane","Steady","Steady to Strong", "Steady to insane","Strong","Strong to insane","Hell's Teeth!"]
    static let currentFrequency = ["Dont know", "Never","Rarely","Sometimes","no value 4","50/50","novalue 6","Usually","no value 8","Almost Always","Always"]
    enum Attributes : String {
        case  accessBy, environment, features, suitableFor, gpsFix, salinity, whereSiteLocated, bottomComposition
    }
    
    static func wrap(_ site: DiveSite)-> String {
        
        let  siteString = "\(site.id ?? "Error no site id"),\(StringWranglers.cleanForCSV(for: site.name) ),\(StringWranglers.cleanForCSV(for: site.alternativeName)),\(site.country!),\(site.longitude?.stringValue ?? ""),\(site.latitude?.stringValue ?? ""),\(StringWranglers.cleanForCSV(for: site.localAreaName)),\(StringWranglers.cleanForCSV(for: site.islandAtoll)),\(StringWranglers.cleanForCSV(for: site.bodyOfWater)),\(StringWranglers.cleanForCSV(for: site.cityTown)),\(StringWranglers.cleanForCSV(for: site.county)),\(StringWranglers.cleanForCSV(for: site.stateProvince)),\(StringWranglers.cleanForCSV(for: site.note)),\(multiOption(for: site, withAttribute: Attributes.gpsFix.rawValue , andOptions: gpsIsOptions)),\(multiOption(for: site, withAttribute: Attributes.accessBy.rawValue, andOptions: accessByOptions)),\(singleOption(for: site, withAttribute: Attributes.whereSiteLocated.rawValue, andOptions: locationOptions)),\(singleOption(for: site, withAttribute: Attributes.environment.rawValue, andOptions: environmentOption)),\(multiOption(for: site, withAttribute: Attributes.features.rawValue, andOptions: featureOptions)),\(multiOption(for: site, withAttribute: Attributes.bottomComposition.rawValue , andOptions: bottomOptions)),\(multiOption(for: site, withAttribute: Attributes.suitableFor.rawValue, andOptions: suitableOptions)),\(singleOption(for: site, withAttribute: Attributes.salinity.rawValue, andOptions: salinityOptions)),\(adjustedDepth(site.avDepthRangeDeeper)),\(adjustedDepth(site.avDepthRangeShallow)),\(adjustedDepth(site.maximumDepth)),\(adjustedDepth(site.minimumDepth)),\(site.temperatureRangeHigh),\(site.temperatureRangeLow),\(site.highSeasonVisibilityRangeHigh),\(site.highSeasonVisibilityRangeHigh),\(site.lowSeasonVisibilityRangeHigh),\(site.lowSeasonVisibilityRangeLow),\(generateCurrentFrequency(for: site)),\(generateCurrentStrength(for: site)),\(site.mantaPossibility),\(site.sharkPossibility),\(site.turtlePossibility),\(site.technicalDive),\(site.whoSubbed ?? ""),\(noteAssociatedName()),\(noteAssociatedWeb()),\(DateWrapper.dateStringForCSV(from: site.dateLogged)),\(DateWrapper.dateStringForCSV(from: site.dateUpdatedByUser)),\(site.keepLocationSecret),\(site.changedAttributes ?? "")\n"
        return siteString
    }
    static func adjustedDepth(_ depth: Double)-> Double{
        switch depth {
        case -1.0 ..< 101.0 :return depth
        case 101.0 ..< 105.0 : return 104.0
        case 105.0 ..< 110.0: return 109.0
        case 110.0 ..< 115.0 : return 114.0
        case 115.0 ..< 121.0: return 120.0
        default : return 121.0
        }
    }
    static func multiOption(for site:DiveSite, withAttribute attribute: String, andOptions options:[String] )-> String{
        let attributeValue = site.value(forKey: attribute) as! Int
        if attributeValue == 0 {
            return "unknown"
        } else {
            var newOptions = ""
            for i in 0...options.count-1 {
                let value = Int(pow(2,Double(i)))
                if attributeValue & value == value {
                    newOptions = newOptions.appending("\(options[i])/")
                }
            }
            newOptions.removeLast()
            return newOptions
        }
    }
    static func singleOption(for site:DiveSite, withAttribute attribute: String, andOptions options:[String])-> String {
        let attributeValue = site.value(forKey: attribute) as! Int
        if attributeValue == 0 {
            return "unknown"
        }else {
            let index = Int(log2(Double(attributeValue)))
            return options[index]
        }
    }
    static func generateCurrentFrequency(for site:DiveSite)-> String{
        if site.currentFrequency >= 1.0 {
            return currentFrequency[10]
        } else {
            let index = Int(site.currentFrequency * 10.0)
            return currentFrequency[index]
        }
    }
    static func generateCurrentStrength(for site: DiveSite)-> String {
        if site.currentStrength == 1.0 {
            return currentStrength[10]
        }else {
            let index = Int(site.currentStrength * 10.0)
            return currentStrength[index]
        }
    }
    static func noteAssociatedName()-> String{
        guard let name = UserDefaults.associatedName() else { return ""}
        return StringWranglers.cleanForCSV(for: name)
        
    }
    
    static func noteAssociatedWeb()-> String {
        guard let web = UserDefaults.associateWeb() else { return ""}
        return  StringWranglers.cleanForCSV(for: web)
    }
    
}
